{
	"name" : "SoDA-node",
	"version" : 1,
	"creationdate" : 3691242956,
	"modificationdate" : 3699621981,
	"viewrect" : [ 641.0, 79.0, 375.0, 513.0 ],
	"autoorganize" : 1,
	"hideprojectwindow" : 0,
	"showdependencies" : 1,
	"autolocalize" : 0,
	"contents" : 	{
		"patchers" : 		{
			"SoDA-node.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1,
				"toplevel" : 1
			}
,
			"amex_TCPclient.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"unix-time.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"rms.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"amex_syntax.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"amex_shell.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"SoDA-syntax.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"amex_string.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"tcpClient.maxhelp" : 			{
				"kind" : "helpfile",
				"local" : 1
			}
,
			"Motion-model.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"sensing_direction_sandbox.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"Motion-model_BACKUP.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"hide-controls-example.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"Motion-model-MODIFIED.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"ml.classification.gmm-adapted.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}

		}
,
		"media" : 		{
			"SoDA - icon - transparent.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"drag-and-drop.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"numbers.mp3" : 			{
				"kind" : "audiofile",
				"local" : 1
			}
,
			"zsa.splash.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}

		}
,
		"code" : 		{
			"TimedSocket$SocketThread.class" : 			{
				"kind" : "java",
				"local" : 1
			}
,
			"TimedSocket.java" : 			{
				"kind" : "java",
				"local" : 1
			}
,
			"TimedSocket.class" : 			{
				"kind" : "java",
				"local" : 1
			}
,
			"tcpClient$1.class" : 			{
				"kind" : "java",
				"local" : 1
			}
,
			"tcpClient.java" : 			{
				"kind" : "java",
				"local" : 1
			}
,
			"tcpClient.class" : 			{
				"kind" : "java",
				"local" : 1
			}

		}
,
		"data" : 		{
			"settings.txt" : 			{
				"kind" : "textfile",
				"local" : 1
			}

		}
,
		"externals" : 		{
			"zsa.mfcc~.mxo" : 			{
				"kind" : "object",
				"local" : 1
			}
,
			"zsa.descriptors.mxo" : 			{
				"kind" : "object",
				"local" : 1
			}
,
			"zsa.descriptors.mxe" : 			{
				"kind" : "object",
				"local" : 1
			}
,
			"zsa.descriptors.mxe64" : 			{
				"kind" : "object",
				"local" : 1
			}

		}
,
		"other" : 		{
			"tcpClientCompile.command" : 			{
				"kind" : "file",
				"local" : 1
			}

		}

	}
,
	"layout" : 	{

	}
,
	"searchpath" : 	{

	}
,
	"detailsvisible" : 0,
	"amxdtype" : 0,
	"readonly" : 0,
	"devpathtype" : 0,
	"devpath" : ".",
	"sortmode" : 0,
	"viewmode" : 0
}
