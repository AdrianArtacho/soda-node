
# AMEX #

[AMEX](https://bitbucket.org/omedinad/amexserver/src/master/) is a message broker server developed by [Oscar Medina Duarte](https://www.neuesatelier.org/team)
in collaboration with [Adrián Artacho](https://www.neuesatelier.org/team) as part of the science communication project 
[Klänge der Materie](https://sounds-of-matter.univie.ac.at/) (Sounds of Matter) for the Faculty of Physics of the University of Vienna. 
It was used initially as part of the workshop [Embodied Simulations of Physical Phenomena](https://dl.acm.org/doi/10.1145/3212721.3212894), 
at the MOCO conference 2018 in Genoa, Italy. 
This highly efficient piece of software has been repurposed to manage the communication between the nodes within a [SoDA network](README.md) across the network.

## Download AMEX ##

In order to get the AMEX software, please send an email to [adrian@neuesatelier.org](adrian@neuesatelier.org) 
explaining roughly what do you intend to use the program for.

## Installation (linux server) ##
This are the instructions to install the AMEX message broker software on a linux server.

[→ use the wget command](https://docs.google.com/document/d/19zmfNIoj1_2xFzfdmbrf2WF07r1THcHrIfdPdGfLEz0/preview#h.w1o4q4i4lvpc) 
in the unix terminal to download the binary file.

Use this command to unpack it:

>  `tar xvjf Amex_X_********.tbz2`

Make the binary file executable:

>  `chmod +x Amex_X`

The newly installed AMEX binary might yield an ERROR while loading shared libraries:

>  libevent_pthreads-2.1.so.7: cannot open shared object file: No such file or directory

In order to solve this dependency, update and install the missing package:

>  `sudo apt-get update`
>  `sudo apt-get install libevent-pthreads-2.1-7`

## SSH login ##

Use your server instance user/password to log into a remote installation of AMEX. 
The example below shows the link to our dedicated *lightsail* instance bei Amazon Web Services (AWS).

>  [https://lightsail.aws.amazon.com/ls/webapp/home/instances](https://lightsail.aws.amazon.com/ls/webapp/home/instances)

Use this code to remotely login using your private key, instance name and IP address:

>  `ssh -i /path/to/privatekey.pem instance@**.***.+**.**`

[→ Login info](https://docs.google.com/document/d/19zmfNIoj1_2xFzfdmbrf2WF07r1THcHrIfdPdGfLEz0/preview#heading=h.ljsw07ku1u86)

[→ Static IP](https://docs.google.com/document/d/19zmfNIoj1_2xFzfdmbrf2WF07r1THcHrIfdPdGfLEz0/preview#heading=h.5xi8x7vpk3rw)

[→ Private key](https://docs.google.com/document/d/19zmfNIoj1_2xFzfdmbrf2WF07r1THcHrIfdPdGfLEz0/preview#h.iqkjtcgsiyyg)


## Running AMEX ##

Once you log into the instance of the server, 
go to the folder where the AMEX program is 
and execute the binary **Amex_X**:

>  `cd amex`
>  `./Amex_X`

The terminal will then display the program status and the 
[→ default TCP port](https://docs.google.com/document/d/19zmfNIoj1_2xFzfdmbrf2WF07r1THcHrIfdPdGfLEz0/preview#heading=h.vk40slukz6n3).

In order to terminate the program, use the *.end* command:

>  `end.`

Or use the terminal to terminate the process:

>  sudo kill -9 ``pidof Amex_X``

## Default Messages ##

The current version of AMEX uses a small set of default messages. 
Besides internal status massages, the default messages are the following:

![repo:SODA:amex-help](https://docs.google.com/drawings/d/e/2PACX-1vTk2E408RLkfeS4mywJj9-4XTnu6QIAsR_mjx-HDbfgUSzVZS_YkGCx8vHxfUzpVqkTBbnozUe86Ic3/pub?w=654&h=689)


## Assigned Messages ##

![repo:SODA:amex-messages](https://docs.google.com/drawings/d/e/2PACX-1vQyUxCewOGY7UBCjl_w1So1ceY1c1zL9D8dwf_6M1r3GhbrRzlnCBqfAibflIqz_pzkt9ZRrKLq8tEe/pub?w=744&h=833)


