# SoDA application kit #


This code repository also hosts the documentation and help files of the set of applications included in the **So**cial **D**istancing **A**pplications kit (SoDA),
developed by [Adrián Artacho](https://www.neuesatelier.org/artacho). 
The standalone programs has been written in MaxMSP, while the message broker ([AMEX](AMEX.md)) is C# programm 
written by [Oscar Medina Duarte](https://www.neuesatelier.org/team). 
The build standalone for the [SoDA node](#markdown-header-soda-node) can be 
downloaded [here](#markdown-header-download-soda-node).


## Required externals:
From the [CNMAT package:](https://cnmat.berkeley.edu/downloads)
- OSC-route.mxo

[Computer Vision for Jitter](https://jmpelletier.com/cvjit/) (Jean-Marc Pelletier)
- cv.jit.resize.mxo
- cv.jit.track.mxo

[Probabilistic Models](https://www.julesfrancoise.com/mubu-probabilistic-models) by Jules Francoise:
- mubu.mxo
- mubu.gmm.mxo
- mubu.hhmm.mxo

From the [Ejies](http://www.e--j.com/) library:
- zsa.descriptors.mxo



___

# SoDA ecosystem #

The SoDA ecosystem is composed by a network of [SoDA node](#markdown-header-soda-node) applications 
in addition to [standalone components](#markdown-header-standalone-applications) that add some features to the system.
The local standalone components communicate by means of [OSC messages](#markdown-header-osc-messages) 
with the [SoDA node](#markdown-header-soda-node), 
and the individual [nodes](#markdown-header-soda-node) exchange messages via TCP protocol 
with the [AMEX.md](AMEX.md) message broker application usually running in a remote server.

- [OSC messages](#markdown-header-osc-messages) (Description of the SoDA message ecosystem)
- [AMEX.md](AMEX.md) (Message broker program running in the remote server)

## Standalone applications ##

- [SoDA node](#markdown-header-soda-node) is the main application you will need to participate in a networked performance session.

___

- [SoDA pov](https://bitbucket.org/AdrianArtacho/soda-pov/) (This application monitors and corrects latency problems)
- [SoDATA](https://bitbucket.org/AdrianArtacho/sodata)




## Max for Live devices ##
  
- [SoDA for Live (SoD4L)](https://bitbucket.org/AdrianArtacho/sod4l) is a M4L object that acts as a bridge between Ableton Live and the SoDA-node standalone.



___

# SoDA node #

This are the instructions for the installation and use of the SoDA-node application. 
The installation process will be different between Mac and Windows operating systems.

## Download SoDA node ##

The toolkit heavily relies on the MuBu lib developed by the ISMM team at IRCAM. 
This a Max/MSP library that is free (only to register to the [IRCAM forum](http://forumnet.ircam.fr/product/mubu-en/): [http://forumnet.ircam.fr/product/mubu-en/](http://forumnet.ircam.fr/product/mubu-en/).)

### SoDA-node v.1.5 Standalones (2021.03.26) ###

#### For Mac OS: ####


:  [SoDA-node-standalone-v1.5_Max8-MacOS-10-13-6](https://drive.google.com/file/d/1H85R-hG2LOrEDiJkaradgi7abKkF2dlV/view?usp=sharing)


#### For Windows: ####


:  [SoDA-node-standalone-v1.5_Max8-Windows7](https://drive.google.com/file/d/1UTr4-nFOChlBTT3hbW8iQjOvGBc2FXus/view?usp=sharing)


**Note for Windows' users:**  you need to install 
[Visual Studio 2015 Redistributable Package](https://www.microsoft.com/en-US/download/details.aspx?id=48145) 
to have MuBu working!
Once downloaded, add the Mubu library into the ext-libs folder within the GST. 

Last step: in MaxMSP add all the toolkit to your file preferences: 

>  Options > File Preferences 






## Installation in Mac operation systems ##

1.  First you will need to download the appropriate version from the Downloads page to your Desktop.
2.  Uncompress the .zip file by double clicking on it.
3.  Move the uncompressed file (SoDA-node.app) to your Applications folder (optional)
4.  Lauch it as you would with any other application and enjoy!

→ (error) If your computer protests that the application is broken / corrupted / or from an unknown developer, 
you may solve with a little line in your computer Terminal:

1.  Open your Terminal application (⌘CMD + SPACEBAR to open Spotlight, then type terminal)
2.  Write the following: 

>  xattr -cr /Applications/SoDA-node.app


## Installation in Windows operation systems ##

1.  First you will need to [download](#markdown-header-download-soda-node) the appropriate version to your Desktop.
2.  Uncompress the .zip file by double clicking on it.
3.  Lauch it as you would with any other application and enjoy!

## Using the SoDA-node application (both operating systems) ##
Using the program is pretty straightforward. 
Once the remote server is on (which can also be done from the [SoDA pov](https://bitbucket.org/AdrianArtacho/soda-pov/) application),  
hit connect button:

![repo:SODA:node-neutral](https://docs.google.com/drawings/d/e/2PACX-1vSJ5FCASo6BAuiR4yPHKTzajPQCh-S2JHVWEcVHsxf4khU7L9LshdzC1ZMXapsRTGuvE7PMTRjZZqoI/pub?w=638&h=669)

Then, the application should show the status "connected" and the toggle "tcp on".

![repo:SODA:tcp-on](https://docs.google.com/drawings/d/e/2PACX-1vSNv5-zx37dQ8qUf2NSkdF2cyMIoGtCWb493S4fxR4JkOs8EJdfYbx_VN77i5OYFgxnQal8fStCHW6k/pub?w=531&h=311)

Now, select your participant ID:

![repo:SODA:select-pid](https://docs.google.com/drawings/d/e/2PACX-1vQE9MCIZAhoLwJSkh_sjDHcUWq1LTJSysE1b16AuHk2uJq9gwvyrvOJ119vH72SQ_Kcwbpe5bF7ev1d/pub?w=509&h=308)

If we are going to use audio for the session, make sure to drop the audiofile (.wav, .mp3 etc.) in the rectangle on the right.


![repo:SODA:drop-audiofile](https://docs.google.com/drawings/d/e/2PACX-1vQgatK7ZnKV8NtbOe58puM00bYi5WKAJct5Bb7cxixKxiPHOct_5a3LjpcyN6udOblpP6ts0wF6by79/pub?w=511&h=312)

You will know that a connection is established with the POV application when your internal SID number appears near your Participant ID. You will also see a red blink at the top corner right whenever 'pinged' by the POV Application.

![repo:SODA:internal-sid](https://docs.google.com/drawings/d/e/2PACX-1vQTiOiYJ8lF9giftlLdRXPQWzLNpAoumqc6Kwxz12s01p5Mw6-lpTqlMbZeuPdECu8jVgce7gdv1mxD/pub?w=512&h=319)

Set your volume level as you like with the horizontal slider, but make sure to leave the audio **ON** (the speaker symbol on the left) if we are going to be working with audio.


___


## OSC messages ##


![soda-message-ecosystem](https://docs.google.com/drawings/d/e/2PACX-1vT3lG0NQlwyfC2dZh_K48Rlgtn7vWiWrb9zKGk36ZUqA71JVWLsl_Xz-nYSz_OtgMAHK28DTNbkt04B/pub?w=698&h=543)


Quick message reference [(pdf)](https://docs.google.com/drawings/d/15il59ONSGrhhOAjKgR3WA8PrR-5bSlD2_EU7bhtatEs/export/pdf):

![quick-reference](https://docs.google.com/drawings/d/e/2PACX-1vSXmupSA06B9D59LgeXMyqLkgngU6-DMzDMudZ1mtWBSguL8Ok3P3GWzKbVjiv_7geNMIAIdG4RJLXT/pub?w=1124&h=1548)

___





