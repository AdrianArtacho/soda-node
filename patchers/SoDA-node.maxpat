{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 1,
			"revision" : 8,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ 80.0, 79.0, 705.0, 500.0 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"assistshowspatchername" : 0,
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-308",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 433.0, 2096.0, 70.0, 22.0 ],
					"text" : "loadmess 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-307",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 43.0, 1310.737704753875732, 90.0, 22.0 ],
					"text" : "set Performing!"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-304",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 29.0, 1256.0, 81.0, 22.0 ],
					"text" : "set Training..."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-300",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 3,
					"outlettype" : [ "bang", "bang", "" ],
					"patching_rect" : [ 144.0, 1180.0, 44.0, 22.0 ],
					"text" : "sel 0 1"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 30.0,
					"id" : "obj-244",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 181.0, 1231.0, 251.0, 40.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 354.0, 410.0, 335.0, 40.0 ],
					"text" : "Performing!",
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-197",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 144.0, 1144.0, 19.0, 22.0 ],
					"text" : "t l"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.4375, 1.0, 0.736328, 1.0 ],
					"id" : "obj-178",
					"knobcolor" : [ 0.4375, 1.0, 0.736328, 1.0 ],
					"knobshape" : 5,
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 181.0, 1222.5, 251.0, 57.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 353.999999999999943, 397.0, 335.000000000000057, 66.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-433",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 1,
							"revision" : 8,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 1769.0, 314.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-20",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 202.0, 127.0, 29.5, 22.0 ],
									"text" : "7"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-18",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 202.0, 386.0, 29.5, 22.0 ],
									"text" : "1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-16",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 206.0, 354.0, 55.0, 22.0 ],
									"text" : "del 2000"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-15",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 11.0, 297.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "bang", "", "int" ],
									"patching_rect" : [ 76.0, 227.0, 40.0, 22.0 ],
									"text" : "t b l 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-12",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 76.0, 158.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 76.0, 194.0, 52.0, 22.0 ],
									"text" : "gate 1 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-9",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 476.0, 93.0, 150.0, 20.0 ],
									"text" : "milliseconds"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 419.0, 91.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-3",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 91.0, 382.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-2",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 419.0, 45.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 109.0, 45.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 1 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"source" : [ "obj-10", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 0 ],
									"source" : [ "obj-13", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-13", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"source" : [ "obj-15", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-18", 0 ],
									"source" : [ "obj-16", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 0 ],
									"source" : [ "obj-18", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 1 ],
									"source" : [ "obj-20", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 1 ],
									"source" : [ "obj-7", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 941.333333333333144, 940.737704753875732, 131.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p timegap-milliseconds"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-432",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1359.0, 2114.0, 35.0, 22.0 ],
					"text" : "open"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-428",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1290.0, 2150.0, 51.0, 22.0 ],
					"text" : "pcontrol"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-425",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 1,
							"revision" : 8,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 59.0, 104.0, 263.0, 209.0 ],
						"bglocked" : 0,
						"openinpresentation" : 1,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 14.321678161617513,
									"id" : "obj-175",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 694.68854808807373, 437.0, 48.0, 24.0 ],
									"text" : "fbe $1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-197",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 707.68854808807373, 380.167892456054688, 20.0, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 225.0, 163.833333499999981, 20.0, 20.0 ]
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 13.128204981482719,
									"id" : "obj-308",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 577.68854808807373, 426.167892456054688, 58.0, 21.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 136.333333999999923, 64.166667500000017, 58.0, 21.0 ],
									"text" : "calibrate",
									"textcolor" : [ 0.93, 0.93, 0.97, 1.0 ],
									"varname" : "zsa_text[3]"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 14.321678161617513,
									"id" : "obj-316",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patcher" : 									{
										"fileversion" : 1,
										"appversion" : 										{
											"major" : 8,
											"minor" : 1,
											"revision" : 8,
											"architecture" : "x64",
											"modernui" : 1
										}
,
										"classnamespace" : "box",
										"rect" : [ 539.0, 507.0, 640.0, 480.0 ],
										"bglocked" : 0,
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 1,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 1,
										"objectsnaponopen" : 1,
										"statusbarvisible" : 2,
										"toolbarvisible" : 1,
										"lefttoolbarpinned" : 0,
										"toptoolbarpinned" : 0,
										"righttoolbarpinned" : 0,
										"bottomtoolbarpinned" : 0,
										"toolbars_unpinned_last_save" : 0,
										"tallnewobj" : 0,
										"boxanimatetime" : 200,
										"enablehscroll" : 1,
										"enablevscroll" : 1,
										"devicewidth" : 0.0,
										"description" : "",
										"digest" : "",
										"tags" : "",
										"style" : "",
										"subpatcher_template" : "",
										"assistshowspatchername" : 0,
										"boxes" : [ 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-8",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 50.0, 254.0, 33.0, 20.0 ],
													"text" : "* -1."
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-7",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "int" ],
													"patching_rect" : [ 50.0, 156.0, 30.0, 20.0 ],
													"text" : "abs"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-4",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "int" ],
													"patching_rect" : [ 191.0, 45.0, 24.0, 20.0 ],
													"text" : "t 1"
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-3",
													"index" : 2,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 191.0, 15.0, 25.0, 25.0 ]
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-2",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "int", "" ],
													"patching_rect" : [ 50.0, 361.0, 33.0, 20.0 ],
													"text" : "t 0 s"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-1",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 50.0, 93.0, 34.0, 20.0 ],
													"text" : "gate"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-17",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "", "" ],
													"patching_rect" : [ 50.0, 216.0, 62.0, 20.0 ],
													"text" : "zl median"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-16",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 50.0, 332.0, 116.0, 20.0 ],
													"text" : "prepend setminmax"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-15",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "float", "int" ],
													"patching_rect" : [ 145.0, 125.0, 76.0, 20.0 ],
													"text" : "maximum 0."
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-13",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 50.0, 283.0, 114.0, 20.0 ],
													"text" : "pack 0. 0."
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-11",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "", "" ],
													"patching_rect" : [ 50.0, 188.0, 70.0, 20.0 ],
													"text" : "zl group 50"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-6",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "float", "int" ],
													"patching_rect" : [ 50.0, 125.0, 73.0, 20.0 ],
													"text" : "minimum 0."
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-18",
													"index" : 1,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 65.0, 15.0, 25.0, 25.0 ]
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-22",
													"index" : 1,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 64.0, 401.0, 25.0, 25.0 ]
												}

											}
 ],
										"lines" : [ 											{
												"patchline" : 												{
													"destination" : [ "obj-15", 0 ],
													"midpoints" : [ 59.5, 116.5, 154.5, 116.5 ],
													"order" : 0,
													"source" : [ "obj-1", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-6", 0 ],
													"order" : 1,
													"source" : [ "obj-1", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-17", 0 ],
													"source" : [ "obj-11", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-16", 0 ],
													"source" : [ "obj-13", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-7", 0 ],
													"source" : [ "obj-15", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-2", 0 ],
													"source" : [ "obj-16", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-13", 1 ],
													"order" : 0,
													"source" : [ "obj-17", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-8", 0 ],
													"order" : 1,
													"source" : [ "obj-17", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-1", 1 ],
													"source" : [ "obj-18", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-1", 0 ],
													"midpoints" : [ 59.5, 390.0, 44.0, 390.0, 44.0, 83.0, 59.5, 83.0 ],
													"source" : [ "obj-2", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-22", 0 ],
													"source" : [ "obj-2", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-4", 0 ],
													"source" : [ "obj-3", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-1", 0 ],
													"source" : [ "obj-4", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-7", 0 ],
													"source" : [ "obj-6", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-11", 0 ],
													"source" : [ "obj-7", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-13", 0 ],
													"source" : [ "obj-8", 0 ]
												}

											}
 ]
									}
,
									"patching_rect" : [ 435.68854808807373, 459.167892456054688, 139.0, 24.0 ],
									"saved_object_attributes" : 									{
										"description" : "",
										"digest" : "",
										"globalpatchername" : "",
										"tags" : ""
									}
,
									"text" : "p \"Calibrate Display\""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 13.128204981482719,
									"id" : "obj-320",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 570.68854808807373, 380.167892456054688, 148.0, 21.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 64.333333999999923, 161.333333499999981, 148.0, 21.0 ],
									"text" : "Number of Mel Band (s)",
									"textcolor" : [ 0.93, 0.93, 0.97, 1.0 ],
									"varname" : "zsa_text[1]"
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.184314, 0.254902, 0.34902, 1.0 ],
									"id" : "obj-330",
									"maxclass" : "multislider",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 378.68854808807373, 491.167892456054688, 184.0, 75.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 11.333333999999923, 86.833333499999981, 184.0, 71.0 ],
									"setminmax" : [ -7.5, 7.5 ],
									"size" : 24,
									"slidercolor" : [ 0.85098, 0.533333, 0.254902, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 14.321678161617513,
									"id" : "obj-331",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 379.68854808807373, 426.167892456054688, 159.0, 24.0 ],
									"text" : "zsa.easy_mfcc~ 1024 4"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 14.321678161617513,
									"id" : "obj-332",
									"maxclass" : "number",
									"maximum" : 100,
									"minimum" : 4,
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 518.68854808807373, 380.167892456054688, 50.0, 24.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 11.333333999999923, 161.333333499999981, 50.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-335",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 554.68854808807373, 425.167892456054688, 20.0, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 113.333333999999923, 63.166667500000017, 20.0, 20.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-313",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 51.0, 468.0, 34.0, 22.0 ],
									"text" : "sel 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-312",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 215.68854808807373, 236.167892456054688, 77.0, 22.0 ],
									"text" : "loadmess 70"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-310",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "int", "int" ],
									"patching_rect" : [ 51.0, 432.5, 48.0, 22.0 ],
									"text" : "change"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-309",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 51.0, 391.167892456054688, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-307",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 51.0, 353.0, 39.0, 22.0 ],
									"text" : ">= 70"
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-304",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 51.0, 316.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-302",
									"knobcolor" : [ 0.545098039215686, 0.686274509803922, 0.584313725490196, 1.0 ],
									"maxclass" : "slider",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 74.0, 155.0, 20.0, 140.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 229.0, 10.833333499999981, 16.0, 151.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-300",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 51.0, 553.0, 133.0, 22.0 ],
									"text" : "s voice-command-bang"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-243",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 51.0, 100.0, 107.0, 22.0 ],
									"text" : "scale -94. 0. 0 127"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-244",
									"maxclass" : "slider",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 50.0, 155.0, 20.0, 140.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 210.333333999999923, 10.833333499999981, 20.0, 151.0 ]
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.6, 0.6, 0.6, 0.0 ],
									"blinkcolor" : [ 0.925490196078431, 0.364705882352941, 0.341176470588235, 1.0 ],
									"id" : "obj-178",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 50.0, 510.0, 20.0, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 174.666666999999961, 9.333333499999981, 33.666666999999961, 33.666666999999933 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-422",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.999949088073663, 40.00004545605475, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-423",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 379.68854808807373, 40.00004545605475, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-424",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 379.68854808807373, 634.999923456054603, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-331", 1 ],
									"midpoints" : [ 704.18854808807373, 423.167892456054688, 529.18854808807373, 423.167892456054688 ],
									"source" : [ "obj-175", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-300", 0 ],
									"source" : [ "obj-178", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-175", 0 ],
									"source" : [ "obj-197", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-244", 0 ],
									"source" : [ "obj-243", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-304", 0 ],
									"source" : [ "obj-244", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-307", 1 ],
									"source" : [ "obj-302", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-307", 0 ],
									"source" : [ "obj-304", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-309", 0 ],
									"source" : [ "obj-307", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-310", 0 ],
									"source" : [ "obj-309", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-313", 0 ],
									"source" : [ "obj-310", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-302", 0 ],
									"source" : [ "obj-312", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-178", 0 ],
									"source" : [ "obj-313", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-330", 0 ],
									"source" : [ "obj-316", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-316", 0 ],
									"order" : 0,
									"source" : [ "obj-331", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-330", 0 ],
									"order" : 2,
									"source" : [ "obj-331", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-424", 0 ],
									"order" : 1,
									"source" : [ "obj-331", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-331", 1 ],
									"source" : [ "obj-332", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-316", 1 ],
									"source" : [ "obj-335", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-243", 0 ],
									"source" : [ "obj-422", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-331", 0 ],
									"source" : [ "obj-423", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 1293.311451999999917, 2255.832108000000062, 110.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p audio-processing"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-421",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1084.0, 2378.195606708526611, 67.0, 33.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 524.75, 366.5, 103.5, 20.0 ],
					"text" : "Timegap (OUT)",
					"textjustification" : 2
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-408",
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1053.33333333333303, 881.736503824050942, 179.0, 10.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 353.999999999999943, 357.5, 272.500000000000057, 10.0 ],
					"size" : 60000.0
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-417",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1053.33333333333303, 902.166691526229897, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 630.5, 345.5, 59.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-418",
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 721.31145191192627, 2180.0, 179.0, 10.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 353.999999999999943, 385.0, 272.500000000000057, 10.0 ],
					"size" : 60000.0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-402",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 1,
							"revision" : 8,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 1769.0, 314.0, 1079.0, 599.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-41",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 277.0, 485.0, 29.5, 22.0 ],
									"text" : "t l l"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-40",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 285.0, 542.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-39",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 837.0, 135.0, 95.0, 22.0 ],
									"text" : "prepend bgcolor"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-38",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 810.0, 66.0, 108.0, 22.0 ],
									"text" : "prepend knobcolor"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-37",
									"linecount" : 4,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 896.0, 275.0, 50.0, 62.0 ],
									"text" : "0.4375 1. 0.736328 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-36",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 439.0, 378.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-34",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 837.0, 106.0, 125.0, 22.0 ],
									"text" : "1. 0.490234 0.4375 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-33",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 810.0, 37.0, 99.0, 22.0 ],
									"text" : "1. 0.0625 0.0625"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-32",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "bang", "bang", "bang" ],
									"patching_rect" : [ 376.0, 189.0, 42.0, 22.0 ],
									"text" : "t b b b"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-31",
									"linecount" : 4,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 822.0, 438.0, 50.0, 62.0 ],
									"text" : "0.4375 1. 0.736328 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-29",
									"maxclass" : "swatch",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "", "float" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 647.0, 406.0, 128.0, 32.0 ],
									"saturation" : 1.0
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-28",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 686.0, 492.0, 98.0, 22.0 ],
									"text" : "bgcolor $1 $2 $3"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-25",
									"linecount" : 6,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 757.0, 240.0, 50.0, 89.0 ],
									"text" : "knobcolor 0.125 1. 0.179688"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-23",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 607.0, 284.0, 111.0, 22.0 ],
									"text" : "knobcolor $1 $2 $3"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-21",
									"maxclass" : "swatch",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "", "float" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 607.0, 194.0, 128.0, 32.0 ],
									"saturation" : 1.0
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-11",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 376.0, 232.0, 51.0, 22.0 ],
									"text" : "int 3000"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-8",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 376.0, 280.0, 59.0, 22.0 ],
									"text" : "0, 127 $1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"patching_rect" : [ 376.0, 315.0, 40.0, 22.0 ],
									"text" : "line"
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.4375, 1.0, 0.736328, 1.0 ],
									"id" : "obj-4",
									"knobcolor" : [ 0.4375, 1.0, 0.736328, 1.0 ],
									"knobshape" : 5,
									"maxclass" : "slider",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 407.0, 480.0, 251.0, 57.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 317.0, 416.0, 251.0, 57.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-20",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 202.0, 127.0, 29.5, 22.0 ],
									"text" : "7"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-18",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 202.0, 386.0, 29.5, 22.0 ],
									"text" : "1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-16",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 202.0, 330.0, 55.0, 22.0 ],
									"text" : "del 2000"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-15",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 11.0, 297.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "bang", "bang", "", "int" ],
									"patching_rect" : [ 76.0, 227.0, 50.5, 22.0 ],
									"text" : "t b b l 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-12",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 76.0, 158.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 76.0, 194.0, 52.0, 22.0 ],
									"text" : "gate 1 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-9",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 476.0, 93.0, 150.0, 20.0 ],
									"text" : "milliseconds"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 419.0, 91.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-3",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 97.0, 386.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-2",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 419.0, 45.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 109.0, 45.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 1 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"source" : [ "obj-10", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"source" : [ "obj-11", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 0 ],
									"source" : [ "obj-13", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"source" : [ "obj-13", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-13", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-32", 0 ],
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"source" : [ "obj-15", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-18", 0 ],
									"source" : [ "obj-16", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 0 ],
									"source" : [ "obj-18", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 1 ],
									"source" : [ "obj-20", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-23", 0 ],
									"source" : [ "obj-21", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-25", 1 ],
									"order" : 0,
									"source" : [ "obj-23", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-41", 0 ],
									"order" : 1,
									"source" : [ "obj-23", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"source" : [ "obj-28", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-28", 0 ],
									"order" : 1,
									"source" : [ "obj-29", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-31", 1 ],
									"order" : 0,
									"source" : [ "obj-29", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"source" : [ "obj-32", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-33", 0 ],
									"source" : [ "obj-32", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-34", 0 ],
									"source" : [ "obj-32", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-38", 0 ],
									"source" : [ "obj-33", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-39", 0 ],
									"source" : [ "obj-34", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 0 ],
									"source" : [ "obj-36", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-38", 0 ],
									"order" : 1,
									"source" : [ "obj-37", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-39", 0 ],
									"order" : 0,
									"source" : [ "obj-37", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-41", 0 ],
									"source" : [ "obj-38", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-41", 0 ],
									"source" : [ "obj-39", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"source" : [ "obj-41", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-40", 0 ],
									"source" : [ "obj-41", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-36", 0 ],
									"source" : [ "obj-5", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-41", 0 ],
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 1 ],
									"order" : 0,
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 1 ],
									"order" : 1,
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-8", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 347.0, 2393.0, 131.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p timegap-milliseconds"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-400",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1092.0, 996.0, 87.0, 22.0 ],
					"text" : "speedlim 5000"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-386",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 402.0, 2344.0, 50.0, 22.0 ],
					"text" : "2000"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-384",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 1,
							"revision" : 8,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 1576.0, 197.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 2,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 512.0, 246.0, 52.0, 22.0 ],
									"text" : "gate 1 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-12",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 507.0, 175.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 507.0, 101.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-8",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 507.0, 136.0, 102.0, 22.0 ],
									"text" : "expr ($f1 * -1) + 1"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-7",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 507.0, 50.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 20.0,
									"id" : "obj-233",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "", "", "", "" ],
									"patching_rect" : [ 184.0, 264.0, 267.0, 31.0 ],
									"saved_object_attributes" : 									{
										"embed" : 0,
										"precision" : 6
									}
,
									"text" : "coll PID-SID-correspondence"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "int", "" ],
									"patching_rect" : [ 184.0, 168.0, 63.0, 22.0 ],
									"text" : "unpack i s"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"linecount" : 2,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 362.0, 73.0, 50.0, 49.0 ],
									"text" : "4 Hanne"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-212",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 77.0, 446.0, 169.0, 22.0 ],
									"text" : "sprintf %d/audio/number_10/0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 330.0, 492.0, 165.0, 22.0 ],
									"text" : "Poke sent to Adrian"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-436",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 288.0, 430.0, 131.0, 22.0 ],
									"text" : "sprintf Poke sent to %s"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-2",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 288.0, 489.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-409",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 243.5, 101.0, 50.0, 22.0 ],
									"text" : "6"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-433",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 184.0, 40.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-434",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 166.0, 489.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"source" : [ "obj-10", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-434", 0 ],
									"source" : [ "obj-212", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-212", 0 ],
									"source" : [ "obj-233", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 1 ],
									"order" : 0,
									"source" : [ "obj-433", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 1 ],
									"order" : 1,
									"source" : [ "obj-433", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"order" : 1,
									"source" : [ "obj-436", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 1 ],
									"order" : 0,
									"source" : [ "obj-436", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-233", 0 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-436", 0 ],
									"source" : [ "obj-6", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"source" : [ "obj-8", 0 ]
								}

							}
 ],
						"styles" : [ 							{
								"name" : "AudioStatus_Menu",
								"default" : 								{
									"bgfillcolor" : 									{
										"type" : "color",
										"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
										"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
										"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}

								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "maxscore.default.buttons",
								"default" : 								{
									"accentcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
 ]
					}
,
					"patching_rect" : [ 203.913042545318604, 2599.0, 52.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p POKE"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-382",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 3.0, 1400.0, 58.0, 22.0 ],
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-369",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -7.0, 1743.0, 19.0, 22.0 ],
					"text" : "t l"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-359",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 273.0, 1752.0, 19.0, 22.0 ],
					"text" : "t l"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-413",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 144.913042545318604, 2383.695606708526611, 116.0, 22.0 ],
					"text" : "prepend INTERACT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-412",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "", "", "", "", "" ],
					"patching_rect" : [ 144.913042545318604, 2329.832107543945312, 134.0, 22.0 ],
					"text" : "regexp / @substitute \" \""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-405",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 212.347825527191162, 2199.549504280090332, 133.695650577545166, 35.0 ],
					"text" : "INTERACT 5 audio number_1 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-396",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 46.739129543304443, 2216.304305553436279, 19.0, 22.0 ],
					"text" : "t l"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-373",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 555.0, 2034.0, 50.0, 35.0 ],
					"text" : "performance 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-357",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 523.31145191192627, 1981.0, 141.0, 22.0 ],
					"text" : "loadmess performance 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-415",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ -48.0, 2842.0, 37.0, 22.0 ],
					"text" : "zl.rev"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-414",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -48.0, 2806.0, 105.0, 22.0 ],
					"text" : "pack s i"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-409",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 185.0, 2905.0, 144.0, 22.0 ],
					"text" : "4/audio/number_1/0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-406",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 501.0, 2625.0, 37.0, 22.0 ],
					"text" : "close"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-397",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 501.0, 2590.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-395",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 504.31145191192627, 2361.0, 34.0, 22.0 ],
					"text" : "sel 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-390",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 451.0, 2162.0, 50.0, 22.0 ],
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-370",
					"linecount" : 3,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 404.0, 794.0, 53.0, 49.0 ],
					"text" : "EMPTY ml-mode~0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-365",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 614.0, 1435.0, 59.0, 35.0 ],
					"text" : "ml-mode 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-362",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 248.0, 1653.0, 58.0, 22.0 ],
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-346",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 248.0, 1697.0, 82.0, 22.0 ],
					"text" : "cols 3, rows 3"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-329",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 181.0, 1483.0, 135.0, 22.0 ],
					"text" : "r chosen_aprticipant_ID"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-358",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ -48.0, 1743.0, 29.5, 22.0 ],
					"text" : "t l l"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-352",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 3.0, 1436.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-345",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 193.0, 1697.0, 35.0, 22.0 ],
					"text" : "done"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-324",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "dump", "clear" ],
					"patching_rect" : [ 3.0, 1481.0, 75.0, 22.0 ],
					"text" : "t dump clear"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-375",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 193.0, 1665.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-371",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 3.0, 1697.0, 37.0, 22.0 ],
					"text" : "zl.rev"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-367",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3.0, 1665.0, 82.0, 22.0 ],
					"text" : "pack s i"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-347",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3.0, 1520.0, 39.0, 22.0 ],
					"text" : "dump"
				}

			}
, 			{
				"box" : 				{
					"args" : [ 330.0, 195.300000000000011 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-327",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "SoDA_gallery.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ -48.0, 1800.332107543945312, 340.0, 251.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 353.999999999999943, 85.065216064453125, 341.0, 198.434783935546875 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-321",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 118.5, 971.0, 29.5, 22.0 ],
					"text" : "t l l"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-196",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2.625, 1105.0, 134.0, 22.0 ],
					"text" : "print NODE(send)8889:"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-368",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 529.0, 1323.0, 43.0, 22.0 ],
					"text" : "record"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-366",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 583.0, 1593.0, 144.0, 35.0 ],
					"text" : "CON_CON_ENT_ENT_TUT_TUT_CAS_CAS"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-364",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 628.0, 1374.0, 160.0, 22.0 ],
					"text" : "interactions~tztz_err_ttt_uuu"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-355",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 447.0, 1545.0, 101.0, 22.0 ],
					"text" : "route interactions"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-354",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 144.913042545318604, 2292.832107543945312, 140.0, 22.0 ],
					"text" : "6/audio/number_1/0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-351",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "", "", "", "", "" ],
					"patching_rect" : [ -48.0, 2706.0, 129.0, 22.0 ],
					"text" : "regexp / @substitute ~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-349",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -71.260870456695557, 2978.956523895263672, 216.0, 35.0 ],
					"text" : "0/EMPTY/analysis_ORIG_1_DEST_4~audio~number_1~0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-344",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -48.0, 2755.0, 57.0, 22.0 ],
					"text" : "tosymbol"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-343",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -48.0, 2927.0, 266.0, 22.0 ],
					"text" : "sprintf 0/EMPTY/analysis_ORIG_%d_DEST_%s"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-342",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "" ],
					"patching_rect" : [ 95.0, 2790.91304349899292, 81.25, 22.0 ],
					"text" : "t l l l"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-333",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 447.0, 1748.0, 83.0, 22.0 ],
					"text" : "route analysis"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-326",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 447.0, 1709.0, 19.0, 22.0 ],
					"text" : "t l"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-325",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "", "", "", "", "" ],
					"patching_rect" : [ 447.0, 1665.0, 137.0, 22.0 ],
					"text" : "regexp _ @substitute \" \""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-319",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 449.0, 1509.0, 19.0, 22.0 ],
					"text" : "t l"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-301",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 573.0, 1395.0, 19.0, 22.0 ],
					"text" : "t l"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-212",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "", "", "", "", "" ],
					"patching_rect" : [ 449.0, 1463.0, 138.0, 22.0 ],
					"text" : "regexp ~ @substitute \" \""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-200",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 461.0, 1387.0, 89.0, 22.0 ],
					"text" : "performance_1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-450",
					"linecount" : 2,
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1039.0, 2625.0, 127.0, 35.0 ],
					"text" : "ml.classification.gmm-adapted"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-449",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 980.644785245259641, 2344.0, 52.0, 22.0 ],
					"text" : "gate 4 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-448",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 1,
							"revision" : 8,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 476.0, 61.0, 839.0, 821.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 2,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-24",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 354.333333333333314, 449.0, 52.0, 22.0 ],
									"text" : "gate 1 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-16",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 354.333333333333314, 503.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "int", "", "int" ],
									"patching_rect" : [ 386.5, 352.0, 40.0, 22.0 ],
									"text" : "t 1 l 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-22",
									"linecount" : 4,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 672.0, 547.0, 50.0, 62.0 ],
									"text" : "Cue 4 sent to CASCADE..."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-20",
									"linecount" : 3,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 375.0, 699.0, 50.0, 22.0 ],
									"text" : "4/audio/number_4/0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-18",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 272.0, 559.0, 190.0, 22.0 ],
									"text" : "pack i i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-15",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 426.0, 295.0, 29.5, 22.0 ],
									"text" : "- 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-14",
									"maxclass" : "newobj",
									"numinlets" : 5,
									"numoutlets" : 4,
									"outlettype" : [ "int", "", "", "int" ],
									"patching_rect" : [ 310.0, 405.0, 135.0, 22.0 ],
									"text" : "counter"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 293.0, 231.0, 38.0, 22.0 ],
									"text" : "zl.reg"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-12",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 272.0, 324.0, 29.5, 22.0 ],
									"text" : "+ 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 180.0, 352.0, 50.0, 22.0 ],
									"text" : "4"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-8",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 129.0, 179.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 129.0, 217.0, 63.0, 22.0 ],
									"text" : "metro 300"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 199.0, 54.0, 70.0, 22.0 ],
									"text" : "loadmess 8"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"maxclass" : "newobj",
									"numinlets" : 5,
									"numoutlets" : 4,
									"outlettype" : [ "int", "", "", "int" ],
									"patching_rect" : [ 272.0, 286.0, 61.0, 22.0 ],
									"text" : "counter"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-3",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 180.0, 101.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-212",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 272.0, 610.0, 173.0, 22.0 ],
									"text" : "sprintf %d/audio/number_%d/0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-445",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 180.0, 16.0, 156.0, 22.0 ],
									"text" : "r Total_participants_number"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-430",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ -3.5, 54.0, 135.0, 22.0 ],
									"text" : "r chosen_aprticipant_ID"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-403",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 6.5, 101.0, 150.0, 20.0 ],
									"text" : "chosen PID"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-401",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 92.5, 101.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-436",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 499.0, 427.0, 198.0, 22.0 ],
									"text" : "sprintf Cue %d sent to CASCADE..."
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-2",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 499.0, 486.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-416",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "bang", "int", "int", "bang" ],
									"patching_rect" : [ 289.0, 101.0, 250.0, 22.0 ],
									"text" : "t b 1 i b"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-409",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 421.5, 54.0, 90.0, 22.0 ],
									"text" : "4"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-433",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 289.0, 40.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-434",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 271.5, 669.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 1 ],
									"order" : 2,
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"order" : 0,
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-18", 0 ],
									"order" : 1,
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 2 ],
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-24", 1 ],
									"source" : [ "obj-14", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 4 ],
									"source" : [ "obj-15", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-212", 0 ],
									"source" : [ "obj-18", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 1 ],
									"order" : 0,
									"source" : [ "obj-212", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-434", 0 ],
									"order" : 1,
									"source" : [ "obj-212", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"order" : 0,
									"source" : [ "obj-24", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"order" : 1,
									"source" : [ "obj-24", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 0 ],
									"order" : 0,
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 4 ],
									"order" : 2,
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"order" : 1,
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 1 ],
									"source" : [ "obj-401", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-416", 0 ],
									"source" : [ "obj-409", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"source" : [ "obj-416", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-18", 1 ],
									"order" : 2,
									"source" : [ "obj-416", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-409", 1 ],
									"order" : 1,
									"source" : [ "obj-416", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-436", 0 ],
									"order" : 0,
									"source" : [ "obj-416", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"source" : [ "obj-416", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-401", 0 ],
									"source" : [ "obj-430", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-416", 0 ],
									"source" : [ "obj-433", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"order" : 1,
									"source" : [ "obj-436", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-22", 1 ],
									"order" : 0,
									"source" : [ "obj-436", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-445", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 3 ],
									"source" : [ "obj-7", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-24", 0 ],
									"source" : [ "obj-7", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-24", 0 ],
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-8", 0 ]
								}

							}
 ],
						"styles" : [ 							{
								"name" : "AudioStatus_Menu",
								"default" : 								{
									"bgfillcolor" : 									{
										"type" : "color",
										"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
										"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
										"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}

								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "maxscore.default.buttons",
								"default" : 								{
									"accentcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
 ]
					}
,
					"patching_rect" : [ 443.0, 2780.0, 43.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p CAS"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-447",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 1,
							"revision" : 8,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 34.0, 45.0, 839.0, 821.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 2,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-212",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 215.5, 241.0, 162.0, 22.0 ],
									"text" : "sprintf 0/audio/number_%d/0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-445",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 126.0, 13.0, 156.0, 22.0 ],
									"text" : "r Total_participants_number"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-430",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ -3.5, 54.0, 135.0, 22.0 ],
									"text" : "r chosen_aprticipant_ID"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-403",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 6.5, 101.0, 150.0, 20.0 ],
									"text" : "chosen PID"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-401",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 92.5, 101.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-436",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 499.0, 247.0, 204.0, 22.0 ],
									"text" : "sprintf Cue %d sent to EVERYBODY"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-2",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 499.0, 306.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-416",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "bang", "bang", "int", "int" ],
									"patching_rect" : [ 184.0, 101.0, 50.5, 22.0 ],
									"text" : "t b b i i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-409",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 291.5, 101.0, 90.0, 22.0 ],
									"text" : "3"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-433",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 184.0, 40.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-434",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 215.5, 289.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-434", 0 ],
									"source" : [ "obj-212", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-416", 0 ],
									"source" : [ "obj-409", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-212", 0 ],
									"source" : [ "obj-416", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-409", 1 ],
									"order" : 1,
									"source" : [ "obj-416", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-436", 0 ],
									"order" : 0,
									"source" : [ "obj-416", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-401", 0 ],
									"source" : [ "obj-430", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-416", 0 ],
									"source" : [ "obj-433", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-436", 0 ]
								}

							}
 ],
						"styles" : [ 							{
								"name" : "AudioStatus_Menu",
								"default" : 								{
									"bgfillcolor" : 									{
										"type" : "color",
										"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
										"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
										"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}

								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "maxscore.default.buttons",
								"default" : 								{
									"accentcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
 ]
					}
,
					"patching_rect" : [ 377.0, 2780.0, 42.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p TUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-446",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 1,
							"revision" : 8,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 34.0, 45.0, 839.0, 821.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 2,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-42",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 469.0, 561.0, 46.0, 22.0 ],
									"text" : "pack i i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-37",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 597.0, 561.0, 46.0, 22.0 ],
									"text" : "pack i i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-35",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 309.0, 715.0, 50.0, 22.0 ],
									"text" : "4 2 2"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-212",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 469.0, 720.0, 173.0, 22.0 ],
									"text" : "sprintf %d/audio/number_%d/0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-33",
									"linecount" : 2,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 834.0, 495.0, 50.0, 35.0 ],
									"text" : "Hanne Ben 2"
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 24.0,
									"id" : "obj-31",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "", "", "", "" ],
									"patching_rect" : [ 699.0, 372.0, 209.0, 35.0 ],
									"saved_object_attributes" : 									{
										"embed" : 0,
										"precision" : 6
									}
,
									"text" : "coll nodeID-names"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-30",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "int", "int", "int" ],
									"patching_rect" : [ 699.0, 329.0, 65.0, 22.0 ],
									"text" : "unpack i i i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-27",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 146.0, 720.0, 34.0, 22.0 ],
									"text" : "sel 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-26",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 194.5, 758.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-24",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 194.5, 727.0, 52.0, 22.0 ],
									"text" : "gate 1 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-23",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 194.5, 692.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-21",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 194.5, 660.0, 87.5, 22.0 ],
									"text" : "!="
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-20",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 356.5, 458.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-18",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 194.5, 625.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "bang", "int", "int" ],
									"patching_rect" : [ 162.0, 579.0, 84.5, 22.0 ],
									"text" : "t b i i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 57.25, 700.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-8",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 162.0, 543.0, 48.0, 22.0 ],
									"text" : "zl.reg 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-9",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 3,
									"outlettype" : [ "bang", "bang", "" ],
									"patching_rect" : [ 57.25, 660.0, 44.0, 22.0 ],
									"text" : "sel 1 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 57.5, 625.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-12",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 57.5, 589.0, 50.5, 22.0 ],
									"text" : "=="
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 57.5, 521.0, 29.5, 22.0 ],
									"text" : "+ 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-14",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 57.5, 458.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-15",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 57.5, 550.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-16",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 57.5, 492.0, 85.0, 22.0 ],
									"text" : "random 8"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "int" ],
									"patching_rect" : [ 180.0, 277.0, 29.5, 22.0 ],
									"text" : "t b i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-3",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 356.5, 424.0, 38.0, 22.0 ],
									"text" : "zl.reg"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-445",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 126.0, 13.0, 156.0, 22.0 ],
									"text" : "r Total_participants_number"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-430",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ -3.5, 54.0, 135.0, 22.0 ],
									"text" : "r chosen_aprticipant_ID"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-403",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 6.5, 101.0, 150.0, 20.0 ],
									"text" : "chosen PID"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-401",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 92.5, 101.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-11",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 699.0, 502.0, 65.0, 22.0 ],
									"text" : "pack s s i"
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 24.0,
									"id" : "obj-399",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "", "", "", "" ],
									"patching_rect" : [ 722.0, 440.0, 209.0, 35.0 ],
									"saved_object_attributes" : 									{
										"embed" : 0,
										"precision" : 6
									}
,
									"text" : "coll nodeID-names"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"linecount" : 2,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 764.0, 665.0, 165.0, 22.0 ],
									"text" : "Cue 2 sent to Ben and Hanne"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-436",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 722.0, 603.0, 190.0, 22.0 ],
									"text" : "sprintf Cue %d sent to %s and %s"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-2",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 722.0, 662.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-1",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 722.0, 567.0, 37.0, 22.0 ],
									"text" : "zl.rev"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-428",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 59.5, 404.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-424",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 340.0, 675.0, 52.0, 22.0 ],
									"text" : "pack i i i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-421",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 180.0, 239.0, 48.0, 22.0 ],
									"text" : "zl.reg 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-420",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 3,
									"outlettype" : [ "bang", "bang", "" ],
									"patching_rect" : [ 59.5, 364.0, 44.0, 22.0 ],
									"text" : "sel 1 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-419",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 59.75, 329.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-417",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 59.75, 293.0, 50.5, 22.0 ],
									"text" : "=="
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-416",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "bang", "bang", "int", "int" ],
									"patching_rect" : [ 184.0, 101.0, 50.5, 22.0 ],
									"text" : "t b b i i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-412",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 59.75, 225.0, 29.5, 22.0 ],
									"text" : "+ 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-413",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 59.75, 162.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-414",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 59.75, 254.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-415",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 59.75, 196.0, 85.0, 22.0 ],
									"text" : "random 8"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-409",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 291.5, 101.0, 90.0, 22.0 ],
									"text" : "2"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-433",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 184.0, 40.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-434",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 469.0, 768.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-436", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"source" : [ "obj-10", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 0 ],
									"order" : 1,
									"source" : [ "obj-11", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-33", 1 ],
									"order" : 0,
									"source" : [ "obj-11", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 0 ],
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"order" : 1,
									"source" : [ "obj-15", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 1 ],
									"order" : 0,
									"source" : [ "obj-15", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"source" : [ "obj-16", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-21", 0 ],
									"source" : [ "obj-18", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-21", 1 ],
									"order" : 2,
									"source" : [ "obj-20", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 0 ],
									"order" : 0,
									"source" : [ "obj-20", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-424", 1 ],
									"order" : 1,
									"source" : [ "obj-20", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-23", 0 ],
									"source" : [ "obj-21", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-434", 0 ],
									"source" : [ "obj-212", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-24", 0 ],
									"order" : 0,
									"source" : [ "obj-23", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-27", 0 ],
									"order" : 1,
									"source" : [ "obj-23", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-26", 0 ],
									"source" : [ "obj-24", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-42", 0 ],
									"order" : 0,
									"source" : [ "obj-26", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-424", 0 ],
									"order" : 1,
									"source" : [ "obj-26", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"source" : [ "obj-27", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 0 ],
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-31", 0 ],
									"source" : [ "obj-30", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-399", 0 ],
									"source" : [ "obj-30", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"source" : [ "obj-31", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-212", 0 ],
									"source" : [ "obj-37", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 1 ],
									"source" : [ "obj-399", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"midpoints" : [ 189.5, 444.0, 67.0, 444.0 ],
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 1 ],
									"source" : [ "obj-4", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 1 ],
									"order" : 1,
									"source" : [ "obj-401", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-417", 1 ],
									"order" : 0,
									"source" : [ "obj-401", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-416", 0 ],
									"source" : [ "obj-409", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-414", 0 ],
									"source" : [ "obj-412", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-415", 0 ],
									"source" : [ "obj-413", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-417", 0 ],
									"order" : 1,
									"source" : [ "obj-414", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-421", 1 ],
									"order" : 0,
									"source" : [ "obj-414", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-412", 0 ],
									"source" : [ "obj-415", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 2 ],
									"source" : [ "obj-416", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 1 ],
									"order" : 0,
									"source" : [ "obj-416", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-409", 1 ],
									"order" : 3,
									"source" : [ "obj-416", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-413", 0 ],
									"source" : [ "obj-416", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-42", 1 ],
									"order" : 1,
									"source" : [ "obj-416", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-424", 2 ],
									"order" : 2,
									"source" : [ "obj-416", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-419", 0 ],
									"source" : [ "obj-417", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-420", 0 ],
									"source" : [ "obj-419", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-212", 0 ],
									"source" : [ "obj-42", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-413", 0 ],
									"midpoints" : [ 69.0, 388.0, 46.5, 388.0, 46.5, 157.0, 69.25, 157.0 ],
									"order" : 1,
									"source" : [ "obj-420", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-421", 0 ],
									"source" : [ "obj-420", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-428", 0 ],
									"order" : 0,
									"source" : [ "obj-420", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"source" : [ "obj-421", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-30", 0 ],
									"order" : 0,
									"source" : [ "obj-424", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-35", 1 ],
									"order" : 1,
									"source" : [ "obj-424", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-401", 0 ],
									"source" : [ "obj-430", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-416", 0 ],
									"source" : [ "obj-433", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"order" : 1,
									"source" : [ "obj-436", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 1 ],
									"order" : 0,
									"source" : [ "obj-436", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 1 ],
									"order" : 1,
									"source" : [ "obj-445", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-415", 1 ],
									"order" : 0,
									"source" : [ "obj-445", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-18", 0 ],
									"source" : [ "obj-6", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-24", 1 ],
									"source" : [ "obj-6", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-8", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"midpoints" : [ 66.75, 684.0, 44.25, 684.0, 44.25, 453.0, 67.0, 453.0 ],
									"order" : 1,
									"source" : [ "obj-9", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"order" : 0,
									"source" : [ "obj-9", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"source" : [ "obj-9", 1 ]
								}

							}
 ],
						"styles" : [ 							{
								"name" : "AudioStatus_Menu",
								"default" : 								{
									"bgfillcolor" : 									{
										"type" : "color",
										"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
										"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
										"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}

								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "maxscore.default.buttons",
								"default" : 								{
									"accentcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
 ]
					}
,
					"patching_rect" : [ 319.0, 2780.0, 43.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p ENT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-445",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 183.0, 2128.0, 158.0, 22.0 ],
					"text" : "s Total_participants_number"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-442",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 139.0, 3043.0, 93.0, 22.0 ],
					"text" : "loadmess set ..."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-441",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 287.0, 2982.0, 72.0, 22.0 ],
					"text" : "prepend set"
				}

			}
, 			{
				"box" : 				{
					"bordercolor" : [ 0.349019607843137, 0.349019607843137, 0.349019607843137, 0.11 ],
					"fontsize" : 14.0,
					"id" : "obj-440",
					"ignoreclick" : 1,
					"linecount" : 2,
					"maxclass" : "textedit",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 287.0, 3020.832107543945312, 115.0, 50.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 353.999999999999943, 285.5, 335.333333000000096, 50.0 ],
					"readonly" : 1,
					"text" : "Cue 1 sent to Hanne",
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-435",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 1,
							"revision" : 8,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 59.0, 104.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 2,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-212",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 77.0, 446.0, 173.0, 22.0 ],
									"text" : "sprintf %d/audio/number_%d/0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-445",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 126.0, 13.0, 156.0, 22.0 ],
									"text" : "r Total_participants_number"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-430",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 49.0, -19.0, 135.0, 22.0 ],
									"text" : "r chosen_aprticipant_ID"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-403",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ -37.0, 13.0, 150.0, 20.0 ],
									"text" : "chosen PID"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-401",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 49.0, 13.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-11",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 288.0, 330.0, 49.0, 22.0 ],
									"text" : "pack s i"
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 24.0,
									"id" : "obj-399",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "", "", "", "" ],
									"patching_rect" : [ 288.0, 267.0, 209.0, 35.0 ],
									"saved_object_attributes" : 									{
										"embed" : 0,
										"precision" : 6
									}
,
									"text" : "coll nodeID-names"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 330.0, 492.0, 165.0, 22.0 ],
									"text" : "Cue 1 sent to Hanne"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-436",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 288.0, 430.0, 147.0, 22.0 ],
									"text" : "sprintf Cue %d sent to %s"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-2",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 288.0, 489.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-1",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 288.0, 394.0, 37.0, 22.0 ],
									"text" : "zl.rev"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-428",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 64.5, 405.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-424",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 166.0, 354.0, 46.0, 22.0 ],
									"text" : "pack i i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-423",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 166.0, 280.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-421",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 166.0, 238.0, 48.0, 22.0 ],
									"text" : "zl.reg 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-420",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 3,
									"outlettype" : [ "bang", "bang", "" ],
									"patching_rect" : [ 64.5, 365.0, 44.0, 22.0 ],
									"text" : "sel 1 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-419",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 64.75, 330.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-417",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 64.75, 294.0, 50.5, 22.0 ],
									"text" : "=="
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-416",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "bang", "int", "int" ],
									"patching_rect" : [ 184.0, 101.0, 40.0, 22.0 ],
									"text" : "t b i i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-412",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 64.75, 226.0, 29.5, 22.0 ],
									"text" : "+ 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-413",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 64.75, 163.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-414",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 64.75, 255.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-415",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 64.75, 197.0, 85.0, 22.0 ],
									"text" : "random 8"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-409",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 243.5, 101.0, 50.0, 22.0 ],
									"text" : "1"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-433",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 184.0, 40.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-434",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 166.0, 489.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-436", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 0 ],
									"source" : [ "obj-11", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-434", 0 ],
									"source" : [ "obj-212", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"source" : [ "obj-399", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-417", 1 ],
									"source" : [ "obj-401", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-416", 0 ],
									"source" : [ "obj-409", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-414", 0 ],
									"source" : [ "obj-412", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-415", 0 ],
									"source" : [ "obj-413", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-417", 0 ],
									"order" : 1,
									"source" : [ "obj-414", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-421", 1 ],
									"order" : 0,
									"source" : [ "obj-414", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-412", 0 ],
									"source" : [ "obj-415", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 1 ],
									"source" : [ "obj-416", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-409", 1 ],
									"order" : 0,
									"source" : [ "obj-416", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-413", 0 ],
									"source" : [ "obj-416", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-424", 1 ],
									"order" : 1,
									"source" : [ "obj-416", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-419", 0 ],
									"source" : [ "obj-417", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-420", 0 ],
									"source" : [ "obj-419", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-413", 0 ],
									"midpoints" : [ 74.0, 389.0, 51.5, 389.0, 51.5, 158.0, 74.25, 158.0 ],
									"order" : 1,
									"source" : [ "obj-420", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-421", 0 ],
									"source" : [ "obj-420", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-428", 0 ],
									"order" : 0,
									"source" : [ "obj-420", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-423", 0 ],
									"source" : [ "obj-421", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-399", 0 ],
									"order" : 0,
									"source" : [ "obj-423", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-424", 0 ],
									"order" : 1,
									"source" : [ "obj-423", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-212", 0 ],
									"source" : [ "obj-424", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-401", 0 ],
									"source" : [ "obj-430", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-416", 0 ],
									"source" : [ "obj-433", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"order" : 1,
									"source" : [ "obj-436", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 1 ],
									"order" : 0,
									"source" : [ "obj-436", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-415", 1 ],
									"source" : [ "obj-445", 0 ]
								}

							}
 ],
						"styles" : [ 							{
								"name" : "AudioStatus_Menu",
								"default" : 								{
									"bgfillcolor" : 									{
										"type" : "color",
										"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
										"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
										"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}

								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "maxscore.default.buttons",
								"default" : 								{
									"accentcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
 ]
					}
,
					"patching_rect" : [ 243.0, 2780.0, 63.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p CON"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-430",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 38.0, 2593.0, 135.0, 22.0 ],
					"text" : "r chosen_aprticipant_ID"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-429",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 77.0, 838.885246276855469, 137.0, 22.0 ],
					"text" : "s chosen_aprticipant_ID"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-411",
					"maxclass" : "newobj",
					"numinlets" : 5,
					"numoutlets" : 5,
					"outlettype" : [ "", "", "", "", "" ],
					"patching_rect" : [ 347.0, 2698.0, 147.0, 22.0 ],
					"text" : "route CON ENT TUT CAS"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-410",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 347.0, 2649.0, 37.0, 22.0 ],
					"text" : "zl.rev"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-407",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 347.0, 2612.0, 82.0, 22.0 ],
					"text" : "pack i s"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-404",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "int", "int" ],
					"patching_rect" : [ 347.0, 2472.5, 41.5, 22.0 ],
					"text" : "t i i"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-403",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ -48.0, 2625.0, 150.0, 20.0 ],
					"text" : "chosen PID"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-401",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 38.0, 2625.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 24.0,
					"id" : "obj-399",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 3.0, 1616.0, 209.0, 35.0 ],
					"saved_object_attributes" : 					{
						"embed" : 0,
						"precision" : 6
					}
,
					"text" : "coll nodeID-names"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-398",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 347.0, 2434.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"coll_data" : 					{
						"count" : 4,
						"data" : [ 							{
								"key" : 1,
								"value" : [ "CON" ]
							}
, 							{
								"key" : 2,
								"value" : [ "ENT" ]
							}
, 							{
								"key" : 3,
								"value" : [ "TUT" ]
							}
, 							{
								"key" : 4,
								"value" : [ "CAS" ]
							}
 ]
					}
,
					"color" : [ 0.435294117647059, 0.247058823529412, 0.796078431372549, 1.0 ],
					"fontsize" : 20.0,
					"id" : "obj-394",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 1852.0, 849.333333000000039, 149.0, 31.0 ],
					"saved_object_attributes" : 					{
						"embed" : 1,
						"precision" : 6
					}
,
					"text" : "coll interActions"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-393",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "clear" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 1,
							"revision" : 8,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 59.0, 104.0, 640.0, 566.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-26",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 114.0, 520.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-22",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 114.0, 452.0, 37.0, 22.0 ],
									"text" : "zl.rev"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-21",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 114.0, 415.0, 65.0, 22.0 ],
									"text" : "pack s i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-20",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"patching_rect" : [ 114.0, 330.0, 64.5, 22.0 ],
									"text" : "t l b"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-15",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 298.0, 445.0, 50.0, 22.0 ],
									"text" : "4 CAS"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "newobj",
									"numinlets" : 5,
									"numoutlets" : 4,
									"outlettype" : [ "int", "", "", "int" ],
									"patching_rect" : [ 159.5, 366.0, 61.0, 22.0 ],
									"text" : "counter"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-12",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "int", "clear" ],
									"patching_rect" : [ 114.0, 143.0, 152.0, 22.0 ],
									"text" : "t l 1 clear"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-8",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 114.0, 298.0, 61.0, 22.0 ],
									"text" : "zl.group 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 179.0, 243.0, 129.0, 22.0 ],
									"text" : "CON ENT TUT CAS"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 114.0, 243.0, 19.0, 22.0 ],
									"text" : "t l"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "", "", "", "", "" ],
									"patching_rect" : [ 114.0, 189.0, 137.0, 22.0 ],
									"text" : "regexp _ @substitute \" \""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-3",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 303.0, 172.0, 151.0, 22.0 ],
									"text" : "CON_ENT_TUT_CAS"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 114.0, 49.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"order" : 1,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 1 ],
									"order" : 0,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 2 ],
									"source" : [ "obj-12", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-26", 0 ],
									"source" : [ "obj-12", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-21", 1 ],
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"source" : [ "obj-20", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-21", 0 ],
									"source" : [ "obj-20", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-22", 0 ],
									"source" : [ "obj-21", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 1 ],
									"order" : 0,
									"source" : [ "obj-22", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-26", 0 ],
									"order" : 1,
									"source" : [ "obj-22", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-4", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 1 ],
									"order" : 0,
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"order" : 1,
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 0 ],
									"source" : [ "obj-8", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 1852.0, 820.333333000000039, 80.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p interactions"
				}

			}
, 			{
				"box" : 				{
					"coll_data" : 					{
						"count" : 4,
						"data" : [ 							{
								"key" : 1,
								"value" : [ "CON" ]
							}
, 							{
								"key" : 2,
								"value" : [ "ENT" ]
							}
, 							{
								"key" : 3,
								"value" : [ "TUT" ]
							}
, 							{
								"key" : 4,
								"value" : [ "CAS" ]
							}
 ]
					}
,
					"color" : [ 0.435294117647059, 0.247058823529412, 0.796078431372549, 1.0 ],
					"fontsize" : 20.0,
					"id" : "obj-392",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 370.0, 2516.0, 149.0, 31.0 ],
					"saved_object_attributes" : 					{
						"embed" : 1,
						"precision" : 6
					}
,
					"text" : "coll interActions"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-391",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 637.978118578593012, 2934.0, 25.99609375, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-389",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 637.978118578593012, 2904.0, 29.5, 22.0 ],
					"text" : "+ 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-388",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 637.978118578593012, 2962.0, 52.0, 22.0 ],
					"text" : "gate 2 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-387",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 670.978118578593012, 2934.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"checkedcolor" : [ 0.945098039215686, 0.0, 0.0, 1.0 ],
					"id" : "obj-385",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 589.31145191192627, 2162.0, 46.0, 46.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 558.166666000000077, 35.0, 31.0, 31.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-383",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 614.644785245259754, 1823.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-381",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 614.644785245259754, 1862.0, 93.0, 22.0 ],
					"text" : "performance $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-380",
					"maxclass" : "newobj",
					"numinlets" : 5,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 670.978118578593012, 2829.0, 76.333333333333258, 22.0 ],
					"text" : "switch 4 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-379",
					"maxclass" : "newobj",
					"numinlets" : 5,
					"numoutlets" : 5,
					"outlettype" : [ "", "", "", "", "" ],
					"patching_rect" : [ 523.31145191192627, 2099.0, 283.0, 22.0 ],
					"text" : "route ml-mode performance timegap-in timegap-out"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-378",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 555.0, 1823.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-376",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 555.0, 1862.0, 55.0, 22.0 ],
					"text" : "mode $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-374",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 518.31145191192627, 2897.0, 66.0, 22.0 ],
					"text" : "number $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-372",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 518.31145191192627, 2934.0, 19.0, 22.0 ],
					"text" : "t l"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.905882352941176, 0.850980392156863, 0.850980392156863, 1.0 ],
					"bgcolor2" : [ 0.2, 0.2, 0.2, 1.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.2, 0.2, 0.2, 1.0 ],
					"bgfillcolor_color1" : [ 0.905882352941176, 0.850980392156863, 0.850980392156863, 1.0 ],
					"bgfillcolor_color2" : [ 0.2, 0.2, 0.2, 1.0 ],
					"bgfillcolor_proportion" : 0.5,
					"bgfillcolor_type" : "gradient",
					"fontsize" : 8.0,
					"gradient" : 1,
					"id" : "obj-363",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1064.31145191192627, 2693.832107543945312, 207.0, 26.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 353.999999999999943, 467.0, 335.500000000000057, 17.0 ],
					"text" : "139 131 78 178 131 179 106 156 109 119 58 117 51 20 15 58",
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.270588235294118, 0.72156862745098, 0.098039215686275, 1.0 ],
					"id" : "obj-356",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1636.285714285714221, 973.833333000000039, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-350",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 29.0, 2097.0, 150.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 482.0, 9.0, 73.0, 20.0 ],
					"text" : "Participants",
					"textjustification" : 2
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-341",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 184.0, 2096.0, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 555.0, 9.0, 35.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-318",
					"maxclass" : "newobj",
					"numinlets" : 5,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 685.644785245259641, 2624.832107543945312, 104.333333333333258, 22.0 ],
					"text" : "switch 4 1"
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-193",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "ml.classification.gmm-adapted.maxpat",
					"numinlets" : 2,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 728.31145191192627, 2698.0, 301.0, 74.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 8.333334000000001, 416.0, 333.0, 74.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-353",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 628.478118578593012, 2717.5, 39.0, 35.0 ],
					"text" : "qmetro 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-348",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "open", "bang", "open" ],
					"patching_rect" : [ 557.06145191192627, 2671.832107543945312, 82.0, 22.0 ],
					"text" : "t open b open"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-339",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 557.06145191192627, 2724.0, 51.0, 22.0 ],
					"text" : "pcontrol"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-116",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 1,
							"revision" : 8,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 703.0, 118.0, 315.0, 268.0 ],
						"bglocked" : 0,
						"openinpresentation" : 1,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "jit_matrix", "" ],
									"patching_rect" : [ 58.0, 224.5, 128.0, 22.0 ],
									"text" : "jit.dimmap @invert 1 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 844.0, 625.0, 150.0, 20.0 ],
									"text" : "list length"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-2",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 810.0, 615.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 677.0, 615.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-114",
									"maxclass" : "newobj",
									"numinlets" : 4,
									"numoutlets" : 4,
									"outlettype" : [ "", "", "", "" ],
									"patching_rect" : [ 57.333333333333343, 57.0, 138.0, 22.0 ],
									"text" : "route qmetro open close"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-101",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "open" ],
									"patching_rect" : [ 57.333333333333343, 15.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-100",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 796.0, 345.0, 110.0, 22.0 ],
									"text" : "listlength 16"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-98",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 677.0, 345.0, 103.0, 22.0 ],
									"text" : "prepend listlength"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-94",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 90.0, 642.0, 32.0, 22.0 ],
									"text" : "16 9"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-95",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 90.0, 604.0, 32.0, 22.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 70.0, 233.0, 63.0, 22.0 ],
									"text" : "16:9",
									"textjustification" : 1
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-93",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 474.5, 729.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-89",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 474.5, 695.0, 29.5, 22.0 ],
									"text" : "*"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-88",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 323.0, 662.0, 170.5, 22.0 ],
									"text" : "t l l"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-87",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 323.0, 708.0, 77.0, 22.0 ],
									"text" : "prepend size"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-86",
									"linecount" : 2,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 709.0, 477.0, 275.0, 35.0 ],
									"text" : "139 131 78 178 131 179 106 156 109 119 58 117 51 20 15 58"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 13.0,
									"id" : "obj-5",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 677.0, 397.0, 190.0, 23.0 ],
									"text" : "jit.spill @plane 2 @listlength 16"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-82",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "jit_matrix", "" ],
									"patching_rect" : [ 432.0, 265.0, 95.0, 19.0 ],
									"text" : "cv.jit.resize @size 4 4"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-80",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "jit_matrix", "" ],
									"patching_rect" : [ 432.0, 226.0, 64.0, 19.0 ],
									"text" : "jit.rgb2luma"
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-79",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 606.0, 322.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-77",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 606.0, 182.0, 41.0, 22.0 ],
									"text" : "jit.spill"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-76",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 17,
									"outlettype" : [ "jit_matrix", "jit_matrix", "jit_matrix", "jit_matrix", "jit_matrix", "jit_matrix", "jit_matrix", "jit_matrix", "jit_matrix", "jit_matrix", "jit_matrix", "jit_matrix", "jit_matrix", "jit_matrix", "jit_matrix", "jit_matrix", "" ],
									"patching_rect" : [ 606.0, 140.0, 399.0, 22.0 ],
									"text" : "jit.unpack 16"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-74",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "int", "bang" ],
									"patching_rect" : [ 316.5, 146.0, 32.0, 22.0 ],
									"text" : "t 0 b"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-75",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 316.5, 96.0, 63.0, 22.0 ],
									"text" : "closebang"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-73",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 366.0, 595.0, 34.0, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 230.0, 233.0, 22.0, 20.0 ],
									"text" : "Y:",
									"textjustification" : 2
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-72",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 271.5, 595.0, 34.0, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 149.0, 233.0, 22.0, 20.0 ],
									"text" : "X:",
									"textjustification" : 2
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-70",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 128.0, 642.0, 29.5, 22.0 ],
									"text" : "4 4"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-68",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "int", "int" ],
									"patching_rect" : [ 128.0, 678.0, 59.0, 22.0 ],
									"text" : "unpack i i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-67",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 128.0, 604.0, 37.0, 22.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 5.0, 233.0, 63.0, 22.0 ],
									"text" : "small",
									"textjustification" : 1
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-65",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 405.0, 548.0, 89.0, 22.0 ],
									"text" : "loadmess set 9"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-64",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 389.0, 520.0, 96.0, 22.0 ],
									"text" : "loadmess set 16"
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.866666666666667, 0.866666666666667, 0.866666666666667, 1.0 ],
									"id" : "obj-63",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 402.0, 595.0, 50.0, 22.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 258.0, 233.0, 50.0, 22.0 ],
									"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.866666666666667, 0.866666666666667, 0.866666666666667, 1.0 ],
									"id" : "obj-61",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 307.5, 595.0, 50.0, 22.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 173.0, 233.0, 50.0, 22.0 ],
									"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-25",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 323.0, 631.0, 77.0, 22.0 ],
									"text" : "pak i i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-9",
									"maxclass" : "jit.pwindow",
									"name" : "u410004767",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "jit_matrix", "" ],
									"patching_rect" : [ 57.0, 328.0, 160.0, 120.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 5.0, 5.0, 303.0, 224.0 ]
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-11",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "jit_matrix", "" ],
									"patching_rect" : [ 58.0, 270.0, 95.0, 19.0 ],
									"text" : "cv.jit.resize @size 4 4"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-24",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 78.0, 103.0, 135.0, 17.0 ],
									"text" : "Use live camera input",
									"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-33",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 136.666666666666686, 154.0, 33.0, 19.0 ],
									"text" : "close"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-34",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 97.0, 154.0, 30.0, 19.0 ],
									"text" : "open"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-35",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 58.0, 103.0, 15.0, 15.0 ]
								}

							}
, 							{
								"box" : 								{
									"fontface" : 0,
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-36",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 58.0, 127.0, 46.0, 19.0 ],
									"text" : "qmetro 2"
								}

							}
, 							{
								"box" : 								{
									"fontface" : 0,
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-37",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "jit_matrix", "" ],
									"patching_rect" : [ 58.0, 182.0, 84.0, 19.0 ],
									"text" : "jit.grab @unique 1"
								}

							}
, 							{
								"box" : 								{
									"angle" : 270.0,
									"bgcolor" : [ 0.435294, 0.498039, 0.494118, 1.0 ],
									"bordercolor" : [ 0.0, 0.0, 0.0, 1.0 ],
									"id" : "obj-39",
									"maxclass" : "panel",
									"mode" : 0,
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 50.0, 96.0, 214.0, 118.0 ],
									"proportion" : 0.39,
									"rounded" : 0
								}

							}
, 							{
								"box" : 								{
									"angle" : 270.0,
									"bgcolor" : [ 0.858824, 0.878431, 0.862745, 1.0 ],
									"border" : 1,
									"bordercolor" : [ 1.0, 1.0, 1.0, 1.0 ],
									"id" : "obj-42",
									"maxclass" : "panel",
									"mode" : 0,
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 53.0, 265.0, 153.0, 30.0 ],
									"proportion" : 0.39,
									"rounded" : 0
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-114", 0 ],
									"source" : [ "obj-101", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"source" : [ "obj-11", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-33", 0 ],
									"source" : [ "obj-114", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-34", 0 ],
									"source" : [ "obj-114", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-35", 0 ],
									"source" : [ "obj-114", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-88", 0 ],
									"source" : [ "obj-25", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 0 ],
									"midpoints" : [ 146.166666666666686, 175.0, 67.5, 175.0 ],
									"source" : [ "obj-33", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 0 ],
									"midpoints" : [ 106.5, 175.0, 67.5, 175.0 ],
									"source" : [ "obj-34", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-36", 0 ],
									"source" : [ "obj-35", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 0 ],
									"source" : [ "obj-36", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-37", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 0 ],
									"order" : 1,
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-86", 1 ],
									"order" : 0,
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"order" : 1,
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-80", 0 ],
									"order" : 0,
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-25", 0 ],
									"source" : [ "obj-61", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-25", 1 ],
									"source" : [ "obj-63", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-61", 0 ],
									"source" : [ "obj-64", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-63", 0 ],
									"source" : [ "obj-65", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-70", 0 ],
									"source" : [ "obj-67", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-61", 0 ],
									"source" : [ "obj-68", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-63", 0 ],
									"source" : [ "obj-68", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-68", 0 ],
									"source" : [ "obj-70", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-33", 0 ],
									"source" : [ "obj-74", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-35", 0 ],
									"source" : [ "obj-74", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-74", 0 ],
									"source" : [ "obj-75", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-77", 0 ],
									"source" : [ "obj-76", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-79", 0 ],
									"source" : [ "obj-77", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-76", 0 ],
									"order" : 0,
									"source" : [ "obj-80", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-82", 0 ],
									"order" : 1,
									"source" : [ "obj-80", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-82", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"order" : 1,
									"source" : [ "obj-87", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-82", 0 ],
									"order" : 0,
									"source" : [ "obj-87", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-87", 0 ],
									"source" : [ "obj-88", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-89", 0 ],
									"source" : [ "obj-88", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-93", 0 ],
									"source" : [ "obj-89", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"order" : 0,
									"source" : [ "obj-93", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-98", 0 ],
									"order" : 1,
									"source" : [ "obj-93", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-68", 0 ],
									"source" : [ "obj-94", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-94", 0 ],
									"source" : [ "obj-95", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-100", 1 ],
									"order" : 0,
									"source" : [ "obj-98", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"order" : 1,
									"source" : [ "obj-98", 0 ]
								}

							}
 ],
						"styles" : [ 							{
								"name" : "AudioStatus_Menu",
								"default" : 								{
									"bgfillcolor" : 									{
										"type" : "color",
										"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
										"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
										"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}

								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "maxscore.default.buttons",
								"default" : 								{
									"accentcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
 ]
					}
,
					"patching_rect" : [ 557.06145191192627, 2769.0, 83.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p PixelStream"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-328",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 872.0, 2052.0, 50.0, 22.0 ],
					"text" : "0. 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-322",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1076.0, 2024.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-242",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1107.31145191192627, 2071.832107543945312, 59.0, 22.0 ],
					"text" : "gate~ 1 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-361",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 867.0, 1536.332107543945312, 87.0, 22.0 ],
					"text" : "speedlim 2000"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-360",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 729.0, 1564.0, 87.0, 22.0 ],
					"text" : "speedlim 1000"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-340",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1188.0, 2389.832107543945312, 67.0, 33.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 524.75, 339.0, 103.5, 20.0 ],
					"text" : "Timegap (IN)",
					"textjustification" : 2
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-338",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 480.0, 2221.0, 90.0, 22.0 ],
					"text" : "loadmess 3000"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-337",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 480.0, 2329.832107543945312, 29.5, 22.0 ],
					"text" : "* 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-336",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 480.0, 2267.832107543945312, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 630.5, 373.0, 59.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-323",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 416.0, 1840.0, 35.0, 22.0 ],
					"text" : "clear"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-317",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 368.0, 1860.0, 31.0, 22.0 ],
					"text" : "play"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-315",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 388.0, 1927.0, 43.0, 22.0 ],
					"text" : "record"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-237",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 480.0, 1981.0, 19.0, 22.0 ],
					"text" : "t l"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-215",
					"items" : [ "CON", ",", "ENT", ",", "TUT", ",", "CAS" ],
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 370.0, 2561.832107543945312, 100.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 353.999999999999943, 333.5, 120.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.5, 0.0, 0.5, 1.0 ],
					"id" : "obj-203",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 157.25, 2873.0, 119.0, 22.0 ],
					"text" : "s amexserver_speak"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 20.0,
					"id" : "obj-305",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 1700.0, 1919.0, 267.0, 31.0 ],
					"saved_object_attributes" : 					{
						"embed" : 0,
						"precision" : 6
					}
,
					"text" : "coll PID-SID-correspondence"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-303",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1427.0, 1752.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-299",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1427.0, 1689.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 20.0,
					"id" : "obj-233",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 1427.0, 1714.5, 267.0, 31.0 ],
					"saved_object_attributes" : 					{
						"embed" : 0,
						"precision" : 6
					}
,
					"text" : "coll PID-SID-correspondence"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-214",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1396.0, 1593.0, 50.0, 22.0 ],
					"text" : "4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-211",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1566.0, 1419.0, 150.0, 20.0 ],
					"text" : "Up_Right_Down_Left"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-201",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1386.0, 1208.0, 50.0, 35.0 ],
					"text" : "4_8_0_0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-199",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"patching_rect" : [ 842.31145191192627, 2096.0, 135.0, 22.0 ],
					"text" : "webcamTracking"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-314",
					"maxclass" : "ezadc~",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 1147.31145191192627, 1998.0, 45.0, 45.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 628.0, 9.0, 61.333333000000039, 61.333333000000039 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-311",
					"linecount" : 4,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 904.0, 2509.0, 52.0, 62.0 ],
					"text" : "-2.828771 -220.565686"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-306",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1854.238931715488434, 298.856008291244507, 90.0, 22.0 ],
					"text" : "Webcam Areas"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-222",
					"maxclass" : "newobj",
					"numinlets" : 5,
					"numoutlets" : 5,
					"outlettype" : [ "bang", "bang", "bang", "bang", "" ],
					"patching_rect" : [ 523.31145191192627, 2516.0, 292.0, 22.0 ],
					"text" : "sel 1 2 3 4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-229",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 1076.0, 1990.832107543945312, 33.0, 22.0 ],
					"text" : "== 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-238",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 523.31145191192627, 2444.832107543945312, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"align" : 1,
					"fontsize" : 20.0,
					"id" : "obj-239",
					"items" : [ "(training", "data)", ",", "audio", "stream", ",", "webcam", "tracking", ",", "RMS", "stream", ",", "PixelStream" ],
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 523.31145191192627, 2400.832107543945312, 146.333333333333371, 31.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 353.999999999999943, 35.0, 198.0, 31.0 ],
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-240",
					"maxclass" : "newobj",
					"numinlets" : 5,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 699.644785245259641, 2292.832107543945312, 164.0, 22.0 ],
					"text" : "switch 4 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-251",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1057.31145191192627, 2191.832107543945312, 69.0, 22.0 ],
					"text" : "-inf"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-254",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1107.31145191192627, 2116.832107543945312, 28.0, 22.0 ],
					"text" : "rms"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-255",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 637.978118578593012, 3077.832107543945312, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-256",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 637.978118578593012, 3041.0, 87.0, 22.0 ],
					"text" : "speedlim 1000"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-258",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 670.978118578593012, 2999.5, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-261",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "int", "int" ],
					"patching_rect" : [ 670.978118578593012, 2899.0, 48.0, 22.0 ],
					"text" : "change"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-263",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 670.978118578593012, 2867.832107543945312, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-264",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "ml.temporal.classification.hhmm-adapted.maxpat",
					"numinlets" : 2,
					"numoutlets" : 4,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 699.644785245259641, 2389.832107543945312, 300.0, 72.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 8.333334000000001, 339.0, 333.0, 75.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-267",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 932.31145191192627, 2239.832107543945312, 31.0, 22.0 ],
					"text" : "play"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-270",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 894.31145191192627, 2276.832107543945312, 43.0, 22.0 ],
					"text" : "record"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-272",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 978.31145191192627, 2477.832107543945312, 50.0, 35.0 ],
					"text" : "0.222251"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-274",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 798.31145191192627, 2477.832107543945312, 50.0, 22.0 ],
					"text" : "1. 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-278",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 654.644785245259641, 1939.832107543945312, 37.0, 22.0 ],
					"text" : "close"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-280",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 765.644785245259641, 1990.832107543945312, 93.0, 22.0 ],
					"text" : "prepend qmetro"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-285",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 744.144785245259641, 1902.832107543945312, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-293",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "bang", "int", "open" ],
					"patching_rect" : [ 744.144785245259641, 1935.832107543945312, 62.0, 22.0 ],
					"text" : "t b 1 open"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-295",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 699.644785245259641, 1939.832107543945312, 35.0, 22.0 ],
					"text" : "open"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-297",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 728.31145191192627, 2055.832107543945312, 51.0, 22.0 ],
					"text" : "pcontrol"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-236",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 1352.0, 1448.0, 33.0, 22.0 ],
					"text" : "== 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-234",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1352.0, 1477.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-232",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1352.0, 1516.0, 52.0, 22.0 ],
					"text" : "gate 1 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-231",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1852.0, 1092.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-228",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 1852.0, 991.0, 33.0, 22.0 ],
					"text" : "== 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-227",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1852.0, 1136.0, 52.0, 22.0 ],
					"text" : "gate 1 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-226",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1777.0, 1011.0, 54.0, 22.0 ],
					"text" : "deferlow"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-225",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "bang" ],
					"patching_rect" : [ 1467.0, 811.833333000000039, 32.0, 22.0 ],
					"text" : "t b b"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-298",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 867.0, 1629.0, 29.5, 22.0 ],
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-296",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1975.0, 1877.0, 29.5, 22.0 ],
					"text" : "1 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-294",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 867.0, 1593.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-292",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 1,
							"revision" : 8,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 59.0, 104.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-8",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 164.0, 308.0, 141.0, 22.0 ],
									"text" : "route entangled adjacent"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-7",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 295.0, 385.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 164.0, 238.0, 19.0, 22.0 ],
									"text" : "t l"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "", "", "", "", "" ],
									"patching_rect" : [ 164.0, 182.0, 138.0, 22.0 ],
									"text" : "regexp ~ @substitute \" \""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"linecount" : 2,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 358.0, 214.0, 50.0, 35.0 ],
									"text" : "entangled 1"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-2",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 164.0, 385.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 164.0, 97.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-5", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 1 ],
									"order" : 0,
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"order" : 1,
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-8", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"source" : [ "obj-8", 1 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 867.0, 1503.0, 113.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p bubble-messages"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.5, 0.0, 0.5, 1.0 ],
					"id" : "obj-291",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1525.0, 1936.0, 119.0, 22.0 ],
					"text" : "s amexserver_speak"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-290",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1543.0, 1773.0, 97.0, 22.0 ],
					"text" : "pack i 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-289",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1525.0, 1826.0, 184.0, 22.0 ],
					"text" : "sprintf %d/bubble/adjacent~%d/0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-288",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1840.0, 1976.0, 118.0, 22.0 ],
					"text" : "pack i 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-287",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1840.0, 2018.0, 192.0, 22.0 ],
					"text" : "sprintf %d/bubble/entangled~%d/0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-286",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1150.0, 1928.0, 29.5, 22.0 ],
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-284",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1140.090164661407471, 1899.0, 55.0, 22.0 ],
					"text" : "del 2000"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-283",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "bang", "bang", "bang" ],
					"patching_rect" : [ 1140.090164661407471, 1845.0, 60.0, 22.0 ],
					"text" : "t b b b"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-282",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1022.090164661407471, 1752.0, 83.0, 22.0 ],
					"text" : "append wait..."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-281",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1218.0, 1522.0, 58.0, 22.0 ],
					"text" : "toggle $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-279",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1218.0, 1483.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-277",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "bang", "bang", "bang" ],
					"patching_rect" : [ 1262.090164661407471, 1899.0, 42.0, 22.0 ],
					"text" : "t b b b"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-276",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 3,
					"outlettype" : [ "bang", "bang", "" ],
					"patching_rect" : [ 1262.090164661407471, 1819.0, 44.0, 22.0 ],
					"text" : "sel 1 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-275",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1022.090164661407471, 1478.0, 87.0, 22.0 ],
					"text" : "append React!"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-273",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1128.0, 1509.0, 35.0, 22.0 ],
					"text" : "clear"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-271",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1262.090164661407471, 1780.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-269",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1262.090164661407471, 1748.0, 72.0, 22.0 ],
					"text" : "route toggle"
				}

			}
, 			{
				"box" : 				{
					"align" : 1,
					"color" : [ 0.925490196078431, 0.364705882352941, 0.341176470588235, 1.0 ],
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 48.0,
					"id" : "obj-268",
					"ignoreclick" : 1,
					"items" : "<empty>",
					"maxclass" : "umenu",
					"menumode" : 3,
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1022.090164661407471, 1593.0, 259.0, 62.0 ],
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-266",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1726.0, 1635.0, 19.0, 22.0 ],
					"text" : "t l"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-265",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1726.0, 1443.0, 19.0, 22.0 ],
					"text" : "t l"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-262",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1753.0, 1527.0, 50.0, 35.0 ],
					"text" : "1_3_7_9"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-260",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1726.0, 1390.0, 77.0, 22.0 ],
					"text" : "route symbol"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-259",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1972.0, 1304.0, 29.5, 22.0 ],
					"text" : "5"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-257",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1954.0, 1256.0, 29.5, 22.0 ],
					"text" : "9"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-253",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 1852.0, 1190.0, 29.5, 22.0 ],
					"text" : "i 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-252",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1525.0, 1241.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-249",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1885.0, 1049.0, 131.0, 22.0 ],
					"text" : "r voice-command-bang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-250",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1885.0, 1078.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-248",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 1726.0, 1348.0, 70.0, 22.0 ],
					"saved_object_attributes" : 					{
						"embed" : 0,
						"precision" : 6
					}
,
					"text" : "coll ENTGL"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-247",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1726.0, 1250.0, 37.0, 22.0 ],
					"text" : "join 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-246",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1600.0, 1221.0, 19.0, 22.0 ],
					"text" : "t l"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-245",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1600.0, 1181.0, 77.0, 22.0 ],
					"text" : "route symbol"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-235",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1299.5, 1105.0, 107.5, 22.0 ],
					"text" : "symbol 6_0_0_8"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-213",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1753.0, 1860.0, 47.0, 22.0 ],
					"text" : "zl iter 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-216",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1840.0, 1872.0, 19.0, 22.0 ],
					"text" : "t l"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-217",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1753.0, 1815.0, 77.0, 22.0 ],
					"text" : "route symbol"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-218",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1944.0, 1706.0, 39.0, 35.0 ],
					"text" : "1 3 7 9"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-219",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1753.0, 1780.0, 19.0, 22.0 ],
					"text" : "t l"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-220",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "", "", "", "", "" ],
					"patching_rect" : [ 1753.0, 1736.0, 135.0, 22.0 ],
					"text" : "regexp - @substitute \" \""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-221",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1726.0, 1700.0, 46.0, 22.0 ],
					"text" : "route 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-223",
					"linecount" : 2,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1840.0, 2054.0, 118.0, 35.0 ],
					"text" : "print SEND-ENTANGLED"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-224",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "", "", "", "", "" ],
					"patching_rect" : [ 1726.0, 1582.0, 137.0, 22.0 ],
					"text" : "regexp _ @substitute \" \""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-230",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1726.0, 1291.0, 37.0, 22.0 ],
					"text" : "zl rev"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-205",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1717.0, 289.856008291244507, 35.0, 22.0 ],
					"text" : "open"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-208",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1719.0, 332.45538204908371, 51.0, 22.0 ],
					"text" : "pcontrol",
					"varname" : "pcontrol-for-motion"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-110",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1852.0, 911.166691526229897, 76.0, 22.0 ],
					"text" : "loadmess 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-210",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1360.761068284511566, 516.0, 133.0, 22.0 ],
					"text" : "s voice-command-bang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-209",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1521.5, 532.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-207",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1339.761068284511566, 109.044257044792175, 59.0, 22.0 ],
					"text" : "gate~ 1 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-206",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1521.5, 478.192297458648682, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-204",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1668.0, 236.0, 41.0, 22.0 ],
					"text" : "set $1"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-202",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1668.0, 198.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 10.0,
					"format" : 6,
					"id" : "obj-198",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1527.761068284511566, 64.044257044792175, 46.0, 20.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-195",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1464.761068284511566, 218.0, 96.0, 22.0 ],
					"text" : "loadmess set 20"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-173",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1271.0, 398.546273350715637, 29.5, 22.0 ],
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-106",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1271.0, 369.342731177806854, 55.0, 22.0 ],
					"text" : "del 1000"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-170",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1360.761068284511566, 478.192297458648682, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-168",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "bang", "bang", "int" ],
					"patching_rect" : [ 1346.761068284511566, 436.599373757839203, 47.0, 22.0 ],
					"text" : "t b b 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-107",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1346.761068284511566, 403.856008291244507, 52.0, 22.0 ],
					"text" : "gate 1 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-108",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1527.761068284511566, 23.369253218173981, 80.0, 22.0 ],
					"text" : "loadmess 0.5"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-143",
					"maxclass" : "number",
					"maximum" : 5000,
					"minimum" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1464.761068284511566, 250.752216875553131, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-152",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 1455.261068284511566, 317.0, 42.0, 22.0 ],
					"text" : "active"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-155",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1422.761068284511566, 250.752216875553131, 32.5, 22.0 ],
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-158",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1422.761068284511566, 281.442481935024261, 61.0, 22.0 ],
					"text" : "thresh 20"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-172",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1379.761068284511566, 250.752216875553131, 20.0, 20.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-176",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1422.761068284511566, 373.25, 20.0, 20.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-180",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1422.761068284511566, 319.0, 20.0, 20.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-181",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1379.761068284511566, 319.0, 20.0, 20.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-185",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "bang" ],
					"patching_rect" : [ 1379.761068284511566, 346.0, 62.0, 23.0 ],
					"text" : "onebang"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-190",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1379.761068284511566, 173.044257044792175, 167.0, 22.0 ],
					"text" : "thresh~ 0. 1."
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-191",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "bang" ],
					"patching_rect" : [ 1379.761068284511566, 203.929212868213654, 44.0, 22.0 ],
					"text" : "edge~"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 1.0, 1.0, 1.0, 0.0 ],
					"floatoutput" : 1,
					"id" : "obj-192",
					"knobcolor" : [ 0.0, 1.0, 1.0, 1.0 ],
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"orientation" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1527.761068284511566, 109.044257044792175, 100.0, 50.0 ],
					"size" : 1.0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-194",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 1640.761068284511566, 109.044257044792175, 100.0, 50.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-164",
					"linecount" : 3,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1279.0, 902.166691526229897, 63.0, 49.0 ],
					"text" : "s populate-PID-menu"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-161",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "bang", "bang", "bang" ],
					"patching_rect" : [ 1279.0, 824.833333000000039, 42.0, 22.0 ],
					"text" : "t b b b"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-154",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 351.0, 555.0, 19.0, 22.0 ],
					"text" : "t l"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-189",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1485.0, 1620.0, 47.0, 22.0 ],
					"text" : "zl iter 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-188",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1543.0, 1620.0, 19.0, 22.0 ],
					"text" : "t l"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-187",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1485.0, 1575.0, 77.0, 22.0 ],
					"text" : "route symbol"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-186",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1591.5, 1370.0, 36.0, 22.0 ],
					"text" : "5 9-8"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-184",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1485.0, 1540.0, 19.0, 22.0 ],
					"text" : "t l"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-183",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "", "", "", "", "" ],
					"patching_rect" : [ 1485.0, 1496.0, 135.0, 22.0 ],
					"text" : "regexp - @substitute \" \""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-182",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1458.0, 1460.0, 46.0, 22.0 ],
					"text" : "route 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-179",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1329.0, 1418.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-177",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1458.0, 1370.0, 122.0, 22.0 ],
					"text" : "1 $1, 2 $2, 3 $3, 4 $4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-174",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "", "", "", "", "" ],
					"patching_rect" : [ 1458.0, 1328.0, 137.0, 22.0 ],
					"text" : "regexp _ @substitute \" \""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-171",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1365.0, 1253.0, 82.0, 20.0 ],
					"text" : "T - R - B - L"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-169",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1365.0, 1282.737704753875732, 77.0, 22.0 ],
					"text" : "4_8_0_0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-167",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 1458.0, 1418.0, 106.0, 22.0 ],
					"saved_object_attributes" : 					{
						"embed" : 0,
						"precision" : 6
					}
,
					"text" : "coll CONNECTED"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-166",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1458.0, 1282.737704753875732, 86.0, 22.0 ],
					"text" : "route 0"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.72156862745098, 0.545098039215686, 0.098039215686275, 1.0 ],
					"id" : "obj-165",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1399.0, 1036.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-163",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1458.0, 1231.0, 37.0, 22.0 ],
					"text" : "zl rev"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-162",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1458.0, 1197.0, 49.0, 22.0 ],
					"text" : "pack s i"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-160",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1653.0, 1092.0, 39.0, 22.0 ],
					"text" : "dump"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-159",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1550.0, 1086.5, 39.0, 22.0 ],
					"text" : "dump"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-157",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "bang", "" ],
					"patching_rect" : [ 1550.0, 1049.0, 328.000000000000227, 22.0 ],
					"text" : "sel 1 2 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-153",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1329.0, 1384.0, 70.0, 22.0 ],
					"text" : "r poke-area"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-151",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 1600.0, 1136.0, 102.0, 22.0 ],
					"saved_object_attributes" : 					{
						"embed" : 0,
						"precision" : 6
					}
,
					"text" : "coll ENTANGLED"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-156",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 1458.0, 1136.0, 109.000000000000227, 22.0 ],
					"saved_object_attributes" : 					{
						"embed" : 0,
						"precision" : 6
					}
,
					"text" : "coll ADJACENT"
				}

			}
, 			{
				"box" : 				{
					"align" : 1,
					"fontsize" : 16.0,
					"id" : "obj-149",
					"items" : [ "sequence", ",", "adjacent", ",", "entangled" ],
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1853.0, 943.833333000000039, 105.0, 26.0 ],
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-114",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1595.5, 797.833333000000039, 39.0, 22.0 ],
					"text" : "dump"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-142",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "clear" ],
					"patching_rect" : [ 1387.0, 739.833333000000039, 173.0, 22.0 ],
					"text" : "t b clear"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-120",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1412.0, 960.833333000000039, 50.0, 22.0 ],
					"text" : "12345"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-122",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1356.0, 960.833333000000039, 50.0, 35.0 ],
					"text" : "12.8.5.6"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-125",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1387.0, 889.833333000000039, 55.0, 22.0 ],
					"text" : "route set"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-128",
					"maxclass" : "newobj",
					"numinlets" : 8,
					"numoutlets" : 8,
					"outlettype" : [ "", "", "", "", "", "", "", "" ],
					"patching_rect" : [ 1387.0, 925.166691526229897, 368.0, 22.0 ],
					"text" : "route serverip serverport mode adjacent entangled total interactions"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 24.0,
					"id" : "obj-135",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 1583.0, 847.333333000000039, 209.0, 35.0 ],
					"saved_object_attributes" : 					{
						"embed" : 0,
						"precision" : 6
					}
,
					"text" : "coll nodeID-names"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-131",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1412.0, 844.166691526229897, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-132",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1307.0, 865.833333000000039, 39.0, 22.0 ],
					"text" : "dump"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-133",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1307.0, 775.833333000000039, 35.0, 22.0 ],
					"text" : "open"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.866666666666667, 0.866666666666667, 0.866666666666667, 1.0 ],
					"bgcolor2" : [ 0.733333333333333, 0.733333333333333, 0.733333333333333, 1.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.2, 0.2, 0.2, 1.0 ],
					"bgfillcolor_color1" : [ 0.866666666666667, 0.866666666666667, 0.866666666666667, 1.0 ],
					"bgfillcolor_color2" : [ 0.733333333333333, 0.733333333333333, 0.733333333333333, 1.0 ],
					"bgfillcolor_proportion" : 0.5,
					"bgfillcolor_type" : "gradient",
					"gradient" : 1,
					"id" : "obj-136",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1307.0, 702.833333000000039, 73.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 427.0, 9.0, 38.999999999999972, 22.0 ],
					"text" : "edit",
					"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-140",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1467.0, 775.833333000000039, 58.0, 22.0 ],
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-144",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1480.0, 844.166691526229897, 93.0, 22.0 ],
					"text" : "read settings.txt"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.866666666666667, 0.866666666666667, 0.866666666666667, 1.0 ],
					"bgcolor2" : [ 0.733333333333333, 0.733333333333333, 0.733333333333333, 1.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.2, 0.2, 0.2, 1.0 ],
					"bgfillcolor_color1" : [ 0.866666666666667, 0.866666666666667, 0.866666666666667, 1.0 ],
					"bgfillcolor_color2" : [ 0.733333333333333, 0.733333333333333, 0.733333333333333, 1.0 ],
					"bgfillcolor_proportion" : 0.5,
					"bgfillcolor_type" : "gradient",
					"gradient" : 1,
					"id" : "obj-145",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1387.0, 702.833333000000039, 173.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 353.999999999999943, 9.0, 71.0, 22.0 ],
					"text" : "load project",
					"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-146",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1387.0, 775.833333000000039, 33.0, 22.0 ],
					"text" : "read"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-148",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "bang", "int" ],
					"patching_rect" : [ 1387.0, 811.833333000000039, 69.0, 22.0 ],
					"text" : "text"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-104",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 673.0, 1059.0, 61.5, 22.0 ],
					"text" : "t l l"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-100",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 1,
							"revision" : 8,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 34.0, 89.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-24",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 314.0, 337.0433349609375, 57.0, 22.0 ],
									"text" : "tosymbol"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-136",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 314.0, 367.0, 180.0, 36.0 ],
									"text" : "/ITL/scene/TextObject set txt $1;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-21",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 106.0, 374.0, 72.0, 22.0 ],
									"text" : "prepend set"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-20",
									"linecount" : 3,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 436.0, 251.0, 50.0, 49.0 ],
									"text" : "\"hola amigos\""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-18",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 106.0, 324.0, 19.0, 22.0 ],
									"text" : "t l"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-17",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "", "", "", "", "" ],
									"patching_rect" : [ 106.0, 278.0, 137.0, 22.0 ],
									"text" : "regexp _ @substitute \" \""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-14",
									"maxclass" : "newobj",
									"numinlets" : 5,
									"numoutlets" : 5,
									"outlettype" : [ "", "", "", "", "" ],
									"patching_rect" : [ 106.0, 244.0, 181.0, 22.0 ],
									"text" : "route set size xposition yposition"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 273.0, 218.0, 155.0, 22.0 ],
									"text" : "set hola_amigos"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-11",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 106.0, 97.0, 19.0, 22.0 ],
									"text" : "t l"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 373.0, 164.0, 100.0, 22.0 ],
									"text" : "set~hola_amigos"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-8",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 377.0, 112.0, 38.0, 22.0 ],
									"text" : "text1/"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 106.0, 194.0, 19.0, 22.0 ],
									"text" : "t l"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "", "", "", "", "" ],
									"patching_rect" : [ 106.0, 147.0, 138.0, 22.0 ],
									"text" : "regexp ~ @substitute \" \""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-3",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 314.0, 419.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-2",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 106.0, 424.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 106.0, 41.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"source" : [ "obj-10", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"source" : [ "obj-11", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-136", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-18", 0 ],
									"source" : [ "obj-17", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-18", 0 ],
									"source" : [ "obj-17", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-21", 0 ],
									"order" : 1,
									"source" : [ "obj-18", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-24", 0 ],
									"order" : 0,
									"source" : [ "obj-18", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-21", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-136", 0 ],
									"order" : 1,
									"source" : [ "obj-24", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 1 ],
									"order" : 0,
									"source" : [ "obj-24", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-4", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 1 ],
									"order" : 0,
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"order" : 1,
									"source" : [ "obj-5", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 409.0, 1124.0, 45.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p text1"
				}

			}
, 			{
				"box" : 				{
					"bordercolor" : [ 0.349019607843137, 0.349019607843137, 0.349019607843137, 0.11 ],
					"id" : "obj-88",
					"ignoreclick" : 1,
					"linecount" : 2,
					"maxclass" : "textedit",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 715.5, 1282.737704753875732, 100.0, 50.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 8.333334000000001, 285.5, 336.666666000000021, 50.0 ],
					"readonly" : 1,
					"text" : "(no audiofile loaded)",
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-73",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1074.0, 1135.0, 19.0, 22.0 ],
					"text" : "t l"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-97",
					"linecount" : 5,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 489.0, 1213.0, 187.0, 76.0 ],
					"text" : "\"Macintosh HD:/Users/artacho/Documents/Max 7/Projects/SoDA-node/media/numbers.mp3\""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-83",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"patching_rect" : [ 673.0, 1167.5, 67.0, 22.0 ],
					"text" : "opendialog"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-80",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 673.0, 1135.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.5, 0.0, 0.5, 1.0 ],
					"id" : "obj-61",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1154.0, 617.0, 119.0, 22.0 ],
					"text" : "s amexserver_speak"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-150",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 1,
							"revision" : 8,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 59.0, 104.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-2",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 238.0, 199.0, 139.0, 22.0 ],
									"text" : "midievent 52 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-143",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 129.278716953765866, 41.0, 22.0 ],
									"text" : "set $1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-140",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 105.5, 129.278716953765866, 19.0, 22.0 ],
									"text" : "t l"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-136",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 105.5, 169.278716953765866, 63.0, 22.0 ],
									"text" : "prepend 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-128",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "", "", "", "", "" ],
									"patching_rect" : [ 105.5, 100.0, 137.0, 22.0 ],
									"text" : "regexp _ @substitute \" \""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-146",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 191.0, 40.000029953765875, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-148",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 105.5, 40.000029953765875, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-149",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 105.5, 251.278716953765866, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-140", 0 ],
									"source" : [ "obj-128", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-140", 0 ],
									"source" : [ "obj-128", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-149", 0 ],
									"source" : [ "obj-136", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-136", 0 ],
									"order" : 1,
									"source" : [ "obj-140", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 1 ],
									"order" : 0,
									"source" : [ "obj-140", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-136", 0 ],
									"source" : [ "obj-143", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-143", 0 ],
									"source" : [ "obj-146", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-128", 0 ],
									"source" : [ "obj-148", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 585.0, 935.721282999999971, 80.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p colour1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-126",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 941.0, 844.0, 114.0, 22.0 ],
					"text" : "1 midievent 52 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-123",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 718.0, 731.114753723144531, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-121",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 975.25, 737.114753723144531, 129.5, 22.0 ],
					"text" : "1/audio/pause/0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-111",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 635.5, 656.0, 117.0, 35.0 ],
					"text" : "amex-message 1/audio/pause/0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-109",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 817.0, 696.114753723144531, 68.0, 22.0 ],
					"text" : "route sod4l"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.5, 0.0, 0.5, 1.0 ],
					"id" : "obj-96",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 936.0, 781.0, 119.0, 22.0 ],
					"text" : "s amexserver_speak"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-95",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 962.0, 702.833333000000039, 87.0, 20.0 ],
					"text" : "local shortcut"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-86",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 936.0, 696.114753723144531, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-81",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 1,
							"revision" : 8,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 70.0, 172.0, 640.0, 653.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-12",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 345.5, 425.0, 19.0, 22.0 ],
									"text" : "t l"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-11",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 163.0, 34.0, 116.0, 22.0 ],
									"text" : "1/audio/pause/0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 488.0, 411.0, 100.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-2",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 50.0, 78.0, 121.0, 22.0 ],
									"text" : "route amex-message"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-24",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 150.0, 470.0, 170.0, 22.0 ],
									"text" : "1/colour1/midievent_52_0/0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-22",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 399.0, 135.0, 22.0 ],
									"text" : "sprintf %d/colour1/%s/0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-21",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 142.0, 366.0, 131.0, 22.0 ],
									"text" : "1 midievent_52_0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-19",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 319.0, 132.0, 22.0 ],
									"text" : "pack i s"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-18",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 163.0, 285.0, 19.0, 22.0 ],
									"text" : "t l"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-17",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "", "", "", "", "" ],
									"patching_rect" : [ 163.0, 245.0, 137.0, 22.0 ],
									"text" : "regexp \" \" @substitute _"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-16",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 163.0, 202.0, 57.0, 22.0 ],
									"text" : "tosymbol"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-15",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 50.0, 245.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 302.0, 239.0, 106.0, 22.0 ],
									"text" : "midievent_52_0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-9",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 50.0, 174.0, 55.0, 22.0 ],
									"text" : "zl slice 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-8",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 50.0, 134.0, 29.5, 22.0 ],
									"text" : "t l l"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 415.0, 196.0, 52.0, 22.0 ],
									"text" : "gate 1 0"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-6",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 415.0, 582.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 415.0, 100.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-3",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 476.0, 45.0, 150.0, 20.0 ],
									"text" : "local??"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 415.0, 40.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-109",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 201.5, 108.0, 141.0, 22.0 ],
									"text" : "1 midievent 52 0"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-61",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 40.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-73",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 50.0, 582.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"source" : [ "obj-109", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"color" : [ 0.501960813999176, 0.0, 0.501960813999176, 1.0 ],
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-19", 0 ],
									"source" : [ "obj-15", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"source" : [ "obj-16", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-18", 0 ],
									"source" : [ "obj-17", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-18", 0 ],
									"source" : [ "obj-17", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 1 ],
									"order" : 0,
									"source" : [ "obj-18", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-19", 1 ],
									"order" : 1,
									"source" : [ "obj-18", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-21", 1 ],
									"order" : 0,
									"source" : [ "obj-19", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-22", 0 ],
									"order" : 1,
									"source" : [ "obj-19", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 1 ],
									"order" : 1,
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"order" : 0,
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"source" : [ "obj-2", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"order" : 0,
									"source" : [ "obj-22", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-24", 1 ],
									"order" : 1,
									"source" : [ "obj-22", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-61", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-73", 0 ],
									"midpoints" : [ 424.5, 557.0, 59.5, 557.0 ],
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-109", 1 ],
									"order" : 1,
									"source" : [ "obj-8", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 1 ],
									"order" : 0,
									"source" : [ "obj-8", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"source" : [ "obj-8", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 0 ],
									"source" : [ "obj-9", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"source" : [ "obj-9", 1 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 817.0, 737.114753723144531, 138.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p soda4l broadcast/local"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-103",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 668.5, 731.114753723144531, 34.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-98",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 619.0, 731.114753723144531, 34.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-82",
					"maxclass" : "newobj",
					"numinlets" : 5,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 1,
							"revision" : 8,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 411.0, 163.0, 837.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-25",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 717.666666666666629, 271.17476499080658, 57.0, 22.0 ],
									"text" : "tosymbol"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-22",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 598.0, 92.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-19",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 664.0, -72.0, 29.5, 22.0 ],
									"text" : "t l l"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-15",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 554.0, 26.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 482.0, -72.0, 68.0, 20.0 ],
									"text" : "local SID"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-68",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 776.0, 126.0, 50.0, 22.0 ],
									"text" : "w it"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-61",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 552.0, -72.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-53",
									"index" : 4,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 552.0, -115.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-52",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 664.0, -15.0, 46.0, 22.0 ],
									"text" : "route 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-51",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 517.0, 416.0, 50.0, 76.0 ],
									"text" : "1 /ITL/scene/TextObject y -0.8"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-47",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 30.0, 451.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-44",
									"linecount" : 2,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 705.0, 17.0, 50.0, 35.0 ],
									"text" : "midievent 52 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-39",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patcher" : 									{
										"fileversion" : 1,
										"appversion" : 										{
											"major" : 8,
											"minor" : 1,
											"revision" : 8,
											"architecture" : "x64",
											"modernui" : 1
										}
,
										"classnamespace" : "box",
										"rect" : [ 59.0, 104.0, 640.0, 480.0 ],
										"bglocked" : 0,
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 1,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 1,
										"objectsnaponopen" : 1,
										"statusbarvisible" : 2,
										"toolbarvisible" : 1,
										"lefttoolbarpinned" : 0,
										"toptoolbarpinned" : 0,
										"righttoolbarpinned" : 0,
										"bottomtoolbarpinned" : 0,
										"toolbars_unpinned_last_save" : 0,
										"tallnewobj" : 0,
										"boxanimatetime" : 200,
										"enablehscroll" : 1,
										"enablevscroll" : 1,
										"devicewidth" : 0.0,
										"description" : "",
										"digest" : "",
										"tags" : "",
										"style" : "",
										"subpatcher_template" : "",
										"assistshowspatchername" : 0,
										"boxes" : [ 											{
												"box" : 												{
													"id" : "obj-10",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 4,
													"outlettype" : [ "bang", "bang", "bang", "" ],
													"patching_rect" : [ 171.0, 101.0, 50.5, 22.0 ],
													"text" : "t b b b l"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-9",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 350.0, 303.0433349609375, 161.0, 36.0 ],
													"text" : "/ITL/scene/TextObject y -0.8;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-8",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 343.0, 251.0433349609375, 157.0, 36.0 ],
													"text" : "/ITL/scene/TextObject x 0.2;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-7",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 324.0, 176.0433349609375, 191.0, 36.0 ],
													"text" : "/ITL/scene/TextObject fontSize 50;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-139",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 288.0, 33.0433349609375, 174.0, 22.0 ],
													"text" : "\"vamos a probar con espacios\""
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-136",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 50.0, 167.0433349609375, 180.0, 36.0 ],
													"text" : "/ITL/scene/TextObject set txt $1;\r"
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-140",
													"index" : 1,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 50.0, 39.999999960937501, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-141",
													"index" : 1,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 50.0, 335.806396960937491, 30.0, 30.0 ]
												}

											}
 ],
										"lines" : [ 											{
												"patchline" : 												{
													"destination" : [ "obj-136", 0 ],
													"source" : [ "obj-10", 3 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-7", 0 ],
													"source" : [ "obj-10", 2 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-8", 0 ],
													"source" : [ "obj-10", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-9", 0 ],
													"source" : [ "obj-10", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-141", 0 ],
													"source" : [ "obj-136", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-10", 0 ],
													"source" : [ "obj-139", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-10", 0 ],
													"source" : [ "obj-140", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-141", 0 ],
													"source" : [ "obj-7", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-141", 0 ],
													"source" : [ "obj-8", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-141", 0 ],
													"source" : [ "obj-9", 0 ]
												}

											}
 ]
									}
,
									"patching_rect" : [ 717.666666666666629, 310.0, 73.0, 22.0 ],
									"saved_object_attributes" : 									{
										"description" : "",
										"digest" : "",
										"globalpatchername" : "",
										"tags" : ""
									}
,
									"text" : "p text object"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-38",
									"linecount" : 4,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 663.0, 398.0, 50.0, 62.0 ],
									"text" : "/ITL/scene/Text1 set txt wait"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-36",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 656.0, 358.0, 149.0, 22.0 ],
									"text" : "/ITL/scene/Text1 set txt $1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-34",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 792.0, 38.0, 50.0, 22.0 ],
									"text" : "w it"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-30",
									"index" : 3,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 424.333336353302002, -115.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-31",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 424.333336353302002, -77.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-29",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 355.333336353302002, -40.0, 41.0, 22.0 ],
									"text" : "i 2000"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-28",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 199.818769127130508, -35.0, 41.0, 22.0 ],
									"text" : "i 2000"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-27",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 292.0, -115.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-26",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 292.0, -77.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-24",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "float", "float" ],
									"patching_rect" : [ 22.658578306436539, 75.0, 29.5, 22.0 ],
									"text" : "t f f"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-23",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 125.925567120313644, 40.776698470115662, 29.5, 22.0 ],
									"text" : "f 6."
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-20",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 33.158578306436539, 40.776698470115662, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-18",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 33.158578306436539, -102.398065745830536, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-17",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "bang", "bang", "bang" ],
									"patching_rect" : [ 199.711973875761032, -72.0, 42.0, 22.0 ],
									"text" : "t b b b"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-16",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 724.0, 218.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-14",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 664.0, 218.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 664.0, 182.0, 79.0, 22.0 ],
									"text" : "sel 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-9",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 664.0, 143.0, 37.0, 22.0 ],
									"text" : "zl rev"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 880.0, 109.674764215946198, 50.0, 22.0 ],
									"text" : "0 52"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"maxclass" : "newobj",
									"numinlets" : 4,
									"numoutlets" : 4,
									"outlettype" : [ "", "", "", "" ],
									"patching_rect" : [ 664.0, 68.674764215946198, 180.0, 22.0 ],
									"text" : "route midievent parsedtext reset"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-3",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 435.833336353302002, 467.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-2",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 83.333336353302002, 393.0, 19.0, 22.0 ],
									"text" : "t l"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 5,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 664.0, -115.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-243",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 280.731390446424484, 0.0, 79.0, 22.0 ],
									"text" : "750, 1000 $1"
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-242",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 262.818769127130508, 40.776698470115662, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-240",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 199.711973875761032, 46.174764215946198, 45.0, 22.0 ],
									"text" : "/ 1000."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-239",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 199.711973875761032, 0.0, 59.0, 22.0 ],
									"text" : "0, 250 $1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-237",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"patching_rect" : [ 199.711973875761032, 25.174765229225159, 40.0, 22.0 ],
									"text" : "line"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-236",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 153.110032767057419, 122.330095410346985, 29.5, 22.0 ],
									"text" : "1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-234",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 125.925567120313644, 90.291260898113251, 29.5, 22.0 ],
									"text" : "6."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-230",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 33.158578306436539, 119.825249493122101, 104.0, 22.0 ],
									"text" : "s ---RectWidth_xy"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-159",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patcher" : 									{
										"fileversion" : 1,
										"appversion" : 										{
											"major" : 8,
											"minor" : 1,
											"revision" : 8,
											"architecture" : "x64",
											"modernui" : 1
										}
,
										"classnamespace" : "box",
										"rect" : [ 84.0, 106.0, 1256.0, 527.0 ],
										"bglocked" : 0,
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 1,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 1,
										"objectsnaponopen" : 1,
										"statusbarvisible" : 2,
										"toolbarvisible" : 1,
										"lefttoolbarpinned" : 0,
										"toptoolbarpinned" : 0,
										"righttoolbarpinned" : 0,
										"bottomtoolbarpinned" : 0,
										"toolbars_unpinned_last_save" : 0,
										"tallnewobj" : 0,
										"boxanimatetime" : 200,
										"enablehscroll" : 1,
										"enablevscroll" : 1,
										"devicewidth" : 0.0,
										"description" : "",
										"digest" : "",
										"tags" : "",
										"style" : "",
										"subpatcher_template" : "",
										"assistshowspatchername" : 0,
										"boxes" : [ 											{
												"box" : 												{
													"format" : 6,
													"id" : "obj-80",
													"maxclass" : "flonum",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 899.0, 451.0, 50.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-78",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 93.0, 381.0, 150.0, 20.0 ],
													"text" : "proportion"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-76",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 608.5, 304.0, 36.0, 22.0 ],
													"text" : "f -0.5"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-75",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "float", "bang" ],
													"patching_rect" : [ 581.0, 267.0, 46.5, 22.0 ],
													"text" : "t f b"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-74",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 1178.0, 165.0, 29.5, 22.0 ],
													"text" : "-0.5"
												}

											}
, 											{
												"box" : 												{
													"format" : 6,
													"id" : "obj-72",
													"maxclass" : "flonum",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 608.5, 366.0, 50.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-70",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 608.5, 333.0, 73.0, 22.0 ],
													"text" : "expr 1. - $f1"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-67",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 581.0, 414.0, 79.0, 22.0 ],
													"text" : "expr $f2 / $f1"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-66",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 633.0, 227.0, 95.0, 20.0 ],
													"text" : "proportion"
												}

											}
, 											{
												"box" : 												{
													"format" : 6,
													"id" : "obj-64",
													"maxclass" : "flonum",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 581.0, 226.0, 50.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-62",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 581.0, 196.0, 46.5, 22.0 ],
													"text" : "/ 1."
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-61",
													"index" : 1,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 581.0, 490.0, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-60",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 636.0, 451.0, 150.0, 20.0 ],
													"text" : "RectWidth_|xy|"
												}

											}
, 											{
												"box" : 												{
													"format" : 6,
													"id" : "obj-57",
													"maxclass" : "flonum",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 581.0, 450.0, 50.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-55",
													"maxclass" : "newobj",
													"numinlets" : 3,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 899.0, 406.0, 153.0, 22.0 ],
													"text" : "expr (1. + $f3) * $f2 / $f1"
												}

											}
, 											{
												"box" : 												{
													"format" : 6,
													"id" : "obj-54",
													"maxclass" : "flonum",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 1033.0, 359.0, 50.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-52",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 1033.0, 328.0, 78.0, 22.0 ],
													"text" : "f -0.5"
												}

											}
, 											{
												"box" : 												{
													"format" : 6,
													"id" : "obj-23",
													"maxclass" : "flonum",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 1092.0, 146.0, 50.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-12",
													"maxclass" : "newobj",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 1092.0, 116.0, 89.0, 22.0 ],
													"text" : "r ---Nowline_xy"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-49",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 1021.0, 278.0, 150.0, 20.0 ],
													"text" : "width_abl"
												}

											}
, 											{
												"box" : 												{
													"format" : 6,
													"id" : "obj-47",
													"maxclass" : "flonum",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 966.0, 277.0, 50.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-45",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 966.0, 170.0, 29.5, 22.0 ],
													"text" : "$3"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-43",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 954.0, 228.0, 150.0, 20.0 ],
													"text" : "start_abl"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-41",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 41.0, 248.0, 29.5, 22.0 ],
													"text" : "$5"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-39",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 709.0, 56.0, 159.0, 22.0 ],
													"text" : "52 0.5 0.25 127 0"
												}

											}
, 											{
												"box" : 												{
													"format" : 6,
													"id" : "obj-37",
													"maxclass" : "flonum",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 899.0, 227.0, 50.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-35",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 899.0, 170.0, 29.5, 22.0 ],
													"text" : "$2"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-32",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 3,
													"outlettype" : [ "", "", "bang" ],
													"patching_rect" : [ 899.0, 116.0, 153.0, 22.0 ],
													"text" : "t l l b"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-31",
													"linecount" : 5,
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 954.0, 12.0, 150.0, 74.0 ],
													"text" : "(i) Midinote\n(f) Start_abl\n(f) Width_abl (length)\n(i) Velocity\n(i) ???",
													"textcolor" : [ 0.866666666666667, 0.866666666666667, 0.866666666666667, 1.0 ]
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-30",
													"index" : 2,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 899.0, 12.0, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"format" : 6,
													"id" : "obj-29",
													"maxclass" : "flonum",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 41.0, 379.0, 50.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-27",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 41.0, 321.0, 102.0, 22.0 ],
													"text" : "expr $f2 / $f1"
												}

											}
, 											{
												"box" : 												{
													"format" : 6,
													"id" : "obj-26",
													"maxclass" : "flonum",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 41.0, 287.0, 50.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-22",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 198.0, 56.0, 332.0, 22.0 ],
													"text" : "0. -1. 1.5 -1.5 1. -0.5"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-20",
													"maxclass" : "newobj",
													"numinlets" : 6,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 124.0, 248.0, 135.0, 22.0 ],
													"text" : "expr $f3 - $f6 - ($f5 / 2.)"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-19",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 176.0, 288.0, 150.0, 20.0 ],
													"text" : "Start_|xy|"
												}

											}
, 											{
												"box" : 												{
													"format" : 6,
													"id" : "obj-17",
													"maxclass" : "flonum",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 124.0, 287.0, 50.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-15",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patcher" : 													{
														"fileversion" : 1,
														"appversion" : 														{
															"major" : 8,
															"minor" : 1,
															"revision" : 8,
															"architecture" : "x64",
															"modernui" : 1
														}
,
														"classnamespace" : "box",
														"rect" : [ 84.0, 129.0, 640.0, 480.0 ],
														"bglocked" : 0,
														"openinpresentation" : 0,
														"default_fontsize" : 12.0,
														"default_fontface" : 0,
														"default_fontname" : "Arial",
														"gridonopen" : 1,
														"gridsize" : [ 15.0, 15.0 ],
														"gridsnaponopen" : 1,
														"objectsnaponopen" : 1,
														"statusbarvisible" : 2,
														"toolbarvisible" : 1,
														"lefttoolbarpinned" : 0,
														"toptoolbarpinned" : 0,
														"righttoolbarpinned" : 0,
														"bottomtoolbarpinned" : 0,
														"toolbars_unpinned_last_save" : 0,
														"tallnewobj" : 0,
														"boxanimatetime" : 200,
														"enablehscroll" : 1,
														"enablevscroll" : 1,
														"devicewidth" : 0.0,
														"description" : "",
														"digest" : "",
														"tags" : "",
														"style" : "",
														"subpatcher_template" : "",
														"assistshowspatchername" : 0,
														"boxes" : [ 															{
																"box" : 																{
																	"format" : 6,
																	"id" : "obj-12",
																	"maxclass" : "flonum",
																	"numinlets" : 1,
																	"numoutlets" : 2,
																	"outlettype" : [ "", "bang" ],
																	"parameter_enable" : 0,
																	"patching_rect" : [ 211.666666666666657, 142.0, 50.0, 22.0 ]
																}

															}
, 															{
																"box" : 																{
																	"format" : 6,
																	"id" : "obj-10",
																	"maxclass" : "flonum",
																	"numinlets" : 1,
																	"numoutlets" : 2,
																	"outlettype" : [ "", "bang" ],
																	"parameter_enable" : 0,
																	"patching_rect" : [ 86.666666666666657, 142.0, 50.0, 22.0 ]
																}

															}
, 															{
																"box" : 																{
																	"id" : "obj-4",
																	"maxclass" : "newobj",
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"outlettype" : [ "" ],
																	"patching_rect" : [ 86.666666666666657, 176.0, 144.0, 22.0 ],
																	"text" : "expr abs ($f1) + abs ($f2)"
																}

															}
, 															{
																"box" : 																{
																	"id" : "obj-3",
																	"maxclass" : "newobj",
																	"numinlets" : 1,
																	"numoutlets" : 4,
																	"outlettype" : [ "float", "float", "float", "float" ],
																	"patching_rect" : [ 50.0, 100.0, 74.0, 22.0 ],
																	"text" : "unpack f f f f"
																}

															}
, 															{
																"box" : 																{
																	"comment" : "",
																	"id" : "obj-13",
																	"index" : 1,
																	"maxclass" : "inlet",
																	"numinlets" : 0,
																	"numoutlets" : 1,
																	"outlettype" : [ "" ],
																	"patching_rect" : [ 50.0, 40.0, 30.0, 30.0 ]
																}

															}
, 															{
																"box" : 																{
																	"comment" : "",
																	"id" : "obj-14",
																	"index" : 1,
																	"maxclass" : "outlet",
																	"numinlets" : 1,
																	"numoutlets" : 0,
																	"patching_rect" : [ 86.666663999999997, 258.0, 30.0, 30.0 ]
																}

															}
 ],
														"lines" : [ 															{
																"patchline" : 																{
																	"destination" : [ "obj-4", 0 ],
																	"source" : [ "obj-10", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-4", 1 ],
																	"source" : [ "obj-12", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-3", 0 ],
																	"source" : [ "obj-13", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-10", 0 ],
																	"source" : [ "obj-3", 2 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-12", 0 ],
																	"midpoints" : [ 114.5, 130.0, 221.166666666666657, 130.0 ],
																	"source" : [ "obj-3", 3 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-14", 0 ],
																	"source" : [ "obj-4", 0 ]
																}

															}
 ]
													}
,
													"patching_rect" : [ 150.666666666666657, 170.0, 69.0, 22.0 ],
													"saved_object_attributes" : 													{
														"description" : "",
														"digest" : "",
														"globalpatchername" : "",
														"tags" : ""
													}
,
													"text" : "p Total_|xy|"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-8",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 202.666666666666657, 203.0, 150.0, 20.0 ],
													"text" : "Total_|xy|"
												}

											}
, 											{
												"box" : 												{
													"format" : 6,
													"id" : "obj-6",
													"maxclass" : "flonum",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 150.666666666666657, 202.0, 50.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-58",
													"linecount" : 6,
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 79.0, 12.0, 150.0, 87.0 ],
													"text" : "RecordStart_xy\nRecordEnd_xy\nInitial_xy\nInitial_xy\nRectWidth_xy\nNowLine_xy",
													"textcolor" : [ 0.866666666666667, 0.866666666666667, 0.866666666666667, 1.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-2",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 3,
													"outlettype" : [ "", "", "" ],
													"patching_rect" : [ 41.0, 116.0, 40.0, 22.0 ],
													"text" : "t l l l"
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-1",
													"index" : 1,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 41.0, 12.0, 30.0, 30.0 ]
												}

											}
 ],
										"lines" : [ 											{
												"patchline" : 												{
													"destination" : [ "obj-2", 0 ],
													"source" : [ "obj-1", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-23", 0 ],
													"source" : [ "obj-12", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-6", 0 ],
													"source" : [ "obj-15", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-27", 1 ],
													"source" : [ "obj-17", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-15", 0 ],
													"order" : 1,
													"source" : [ "obj-2", 2 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-20", 0 ],
													"source" : [ "obj-2", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-22", 1 ],
													"order" : 0,
													"source" : [ "obj-2", 2 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-41", 0 ],
													"source" : [ "obj-2", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-17", 0 ],
													"source" : [ "obj-20", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-2", 0 ],
													"source" : [ "obj-22", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-52", 1 ],
													"order" : 0,
													"source" : [ "obj-23", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-76", 1 ],
													"order" : 1,
													"source" : [ "obj-23", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-27", 0 ],
													"source" : [ "obj-26", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-29", 0 ],
													"source" : [ "obj-27", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-32", 0 ],
													"source" : [ "obj-30", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-35", 0 ],
													"order" : 0,
													"source" : [ "obj-32", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-39", 1 ],
													"order" : 1,
													"source" : [ "obj-32", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-45", 0 ],
													"source" : [ "obj-32", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-52", 0 ],
													"source" : [ "obj-32", 2 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-37", 0 ],
													"source" : [ "obj-35", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-55", 0 ],
													"order" : 0,
													"source" : [ "obj-37", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-62", 0 ],
													"order" : 1,
													"source" : [ "obj-37", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-32", 0 ],
													"source" : [ "obj-39", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-26", 0 ],
													"source" : [ "obj-41", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-47", 0 ],
													"source" : [ "obj-45", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-55", 1 ],
													"order" : 0,
													"source" : [ "obj-47", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-62", 1 ],
													"order" : 1,
													"source" : [ "obj-47", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-54", 0 ],
													"source" : [ "obj-52", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-55", 2 ],
													"source" : [ "obj-54", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-80", 0 ],
													"source" : [ "obj-55", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-61", 0 ],
													"source" : [ "obj-57", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-64", 0 ],
													"source" : [ "obj-62", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-75", 0 ],
													"source" : [ "obj-64", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-57", 0 ],
													"source" : [ "obj-67", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-72", 0 ],
													"source" : [ "obj-70", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-67", 1 ],
													"source" : [ "obj-72", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-23", 0 ],
													"source" : [ "obj-74", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-67", 0 ],
													"source" : [ "obj-75", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-76", 0 ],
													"source" : [ "obj-75", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-70", 0 ],
													"source" : [ "obj-76", 0 ]
												}

											}
 ],
										"boxgroups" : [ 											{
												"boxes" : [ "obj-8", "obj-6" ]
											}
, 											{
												"boxes" : [ "obj-17", "obj-19" ]
											}
, 											{
												"boxes" : [ "obj-43", "obj-37" ]
											}
, 											{
												"boxes" : [ "obj-49", "obj-47" ]
											}
, 											{
												"boxes" : [ "obj-57", "obj-60" ]
											}
, 											{
												"boxes" : [ "obj-78", "obj-29" ]
											}
 ]
									}
,
									"patching_rect" : [ 330.333336353302002, 370.174764215946198, 53.0, 22.0 ],
									"saved_object_attributes" : 									{
										"description" : "",
										"digest" : "",
										"globalpatchername" : "",
										"tags" : ""
									}
,
									"text" : "p abl_xy"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-66",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 146.333336353302002, 370.174764215946198, 107.0, 22.0 ],
									"text" : "scale 1.5 -1.5 0. 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-77",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "float", "float", "float", "float" ],
									"patching_rect" : [ 146.333336353302002, 334.841434478759766, 74.0, 22.0 ],
									"text" : "unpack f f f f"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-58",
									"linecount" : 2,
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "float", "float", "float", "float" ],
									"patching_rect" : [ 100.333336353302002, 223.174764513969421, 52.0, 35.0 ],
									"text" : "unpack f f f f"
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-191",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 232.333336353302002, 233.174764513969421, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-183",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 198.333336353302002, 418.841434478759766, 63.0, 20.0 ],
									"text" : "start_0-1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-182",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 150.333336353302002, 273.174764215946198, 63.0, 20.0 ],
									"text" : "start_xy"
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-141",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 146.333336353302002, 417.841434478759766, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-185",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 232.333336353302002, 187.174764215946198, 29.5, 22.0 ],
									"text" : "f 0."
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-178",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 100.333336353302002, 271.17476499080658, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-190",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 330.333336353302002, 399.174764215946198, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-96",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "bang", "bang", "float" ],
									"patching_rect" : [ 232.333336353302002, 140.174764215946198, 45.0, 22.0 ],
									"text" : "t b b 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-65",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 371.333336353302002, 168.174764215946198, 67.0, 20.0 ],
									"text" : "width_xy"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-56",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 319.333336353302002, 194.174764215946198, 104.0, 22.0 ],
									"text" : "s ---RectWidth_xy"
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-57",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 319.333336353302002, 168.174764215946198, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"floatoutput" : 1,
									"id" : "obj-13",
									"knobshape" : 5,
									"maxclass" : "slider",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 232.333336353302002, 261.572822421789169, 242.0, 22.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 87.5, 26.0, 239.999999999999943, 17.0 ],
									"size" : 1.0
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-147",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 20.333336353302002, 255.174764215946198, 49.0, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 103.060606060606062, 46.0, 46.0, 20.0 ],
									"text" : "Active",
									"textcolor" : [ 0.866666666666667, 0.866666666666667, 0.866666666666667, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.0, 0.0, 0.996078431372549, 1.0 ],
									"fontsize" : 14.0,
									"id" : "obj-101",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 3,
									"outlettype" : [ "int", "", "" ],
									"patcher" : 									{
										"fileversion" : 1,
										"appversion" : 										{
											"major" : 8,
											"minor" : 1,
											"revision" : 8,
											"architecture" : "x64",
											"modernui" : 1
										}
,
										"classnamespace" : "box",
										"rect" : [ 34.0, 79.0, 1049.0, 787.0 ],
										"bglocked" : 0,
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 1,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 1,
										"objectsnaponopen" : 1,
										"statusbarvisible" : 2,
										"toolbarvisible" : 1,
										"lefttoolbarpinned" : 0,
										"toptoolbarpinned" : 0,
										"righttoolbarpinned" : 0,
										"bottomtoolbarpinned" : 0,
										"toolbars_unpinned_last_save" : 0,
										"tallnewobj" : 0,
										"boxanimatetime" : 200,
										"enablehscroll" : 1,
										"enablevscroll" : 1,
										"devicewidth" : 0.0,
										"description" : "",
										"digest" : "",
										"tags" : "",
										"style" : "",
										"subpatcher_template" : "",
										"assistshowspatchername" : 0,
										"boxes" : [ 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-13",
													"index" : 2,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 325.0, 1303.0, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-58",
													"linecount" : 6,
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 748.0, 1089.0, 150.0, 87.0 ],
													"text" : "RecordStart_xy\nRecordEnd_xy\nInitial_xy\nInitial_xy\nRectWidth_xy\nNowLine_xy",
													"textcolor" : [ 0.866666666666667, 0.866666666666667, 0.866666666666667, 1.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-19",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 713.0, 889.0, 19.0, 22.0 ],
													"text" : "t l"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-18",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "" ],
													"patching_rect" : [ 413.0, 675.399998307228088, 29.5, 22.0 ],
													"text" : "t l l"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-17",
													"maxclass" : "newobj",
													"numinlets" : 6,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 713.0, 841.0, 168.375, 22.0 ],
													"text" : "pack f f f f f f"
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-9",
													"index" : 3,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 713.0, 1104.0, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-105",
													"index" : 1,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 194.50000274181366, 1303.200019419193268, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-104",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "int", "int" ],
													"patching_rect" : [ 53.60000079870224, 1016.800015151500702, 159.90000194311142, 22.0 ],
													"text" : "t i i"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-103",
													"maxclass" : "toggle",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "int" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 53.60000079870224, 979.200014591217041, 24.0, 24.0 ]
												}

											}
, 											{
												"box" : 												{
													"format" : 6,
													"id" : "obj-101",
													"maxclass" : "flonum",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 53.60000079870224, 946.799999952316284, 50.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 13.0,
													"id" : "obj-99",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 229.799999237060547, 878.399998724460602, 155.0, 23.0 ],
													"text" : "if $f1 <= $f2 then 0 else 1"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-98",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 451.200006723403931, 579.200008630752563, 29.5, 22.0 ],
													"text" : "-0.5"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-96",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 369.0, 1035.200015425682068, 19.0, 22.0 ],
													"text" : "t f"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-94",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "float", "float" ],
													"patching_rect" : [ 17.0, 756.800011277198792, 55.799999237060547, 22.0 ],
													"text" : "t f f"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-93",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 488.000007271766663, 579.200008630752563, 29.5, 22.0 ],
													"text" : "0.5"
												}

											}
, 											{
												"box" : 												{
													"format" : 6,
													"id" : "obj-89",
													"maxclass" : "flonum",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 65.0, 551.200007855892181, 50.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-87",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 3,
													"outlettype" : [ "float", "float", "float" ],
													"patching_rect" : [ 811.5, 605.600009024143219, 40.0, 22.0 ],
													"text" : "t f f f"
												}

											}
, 											{
												"box" : 												{
													"format" : 6,
													"id" : "obj-86",
													"maxclass" : "flonum",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 822.0, 675.399998307228088, 45.999999940395355, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"format" : 6,
													"id" : "obj-84",
													"maxclass" : "flonum",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 552.0, 761.60000866651535, 50.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"format" : 6,
													"id" : "obj-82",
													"maxclass" : "flonum",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 413.0, 761.60000866651535, 50.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"format" : 6,
													"id" : "obj-79",
													"maxclass" : "flonum",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 229.799999237060547, 846.40000993013382, 50.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"floatoutput" : 1,
													"id" : "obj-69",
													"maxclass" : "slider",
													"numinlets" : 1,
													"numoutlets" : 1,
													"orientation" : 1,
													"outlettype" : [ "" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 65.0, 527.400000929832458, 143.200001835823059, 15.199998140335083 ],
													"size" : 1.0
												}

											}
, 											{
												"box" : 												{
													"format" : 6,
													"id" : "obj-64",
													"maxclass" : "flonum",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 17.0, 655.800001621246338, 50.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-61",
													"maxclass" : "newobj",
													"numinlets" : 6,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 17.0, 621.800009369850159, 107.0, 22.0 ],
													"text" : "scale 0. 1. 1.5 -1.5"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 13.0,
													"id" : "obj-52",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "", "" ],
													"patching_rect" : [ 53.799999237060547, 817.799998044967651, 195.0, 23.0 ],
													"text" : "if $f1 >= $f2 then 0 else out2 $f1"
												}

											}
, 											{
												"box" : 												{
													"format" : 6,
													"id" : "obj-34",
													"maxclass" : "flonum",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 965.5, 677.200010418891907, 50.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-24",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 17.0, 252.000003755092621, 19.0, 22.0 ],
													"text" : "t f"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-21",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 326.0, 273.0, 39.0, 22.0 ],
													"text" : "f 0.15"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-20",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 163.0, 437.599999189376831, 19.0, 22.0 ],
													"text" : "t l"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-80",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 553.5, 576.0, 256.0, 20.0 ],
													"text" : "dsitance from RecordRect center to its edge",
													"textcolor" : [ 0.866666666666667, 0.866666666666667, 0.866666666666667, 1.0 ],
													"textjustification" : 2
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-78",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 3,
													"outlettype" : [ "float", "float", "float" ],
													"patching_rect" : [ 811.5, 399.0, 40.0, 22.0 ],
													"text" : "t f f f"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-77",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 912.0, 508.0, 19.0, 22.0 ],
													"text" : "t l"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-76",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 912.0, 390.5, 246.0, 36.0 ],
													"text" : "/ITL/scene/RecordRect set rect $1 0.579116;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-75",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 326.0, 356.0, 88.0, 20.0 ],
													"text" : "Nowline_xy",
													"textcolor" : [ 0.866666666666667, 0.866666666666667, 0.866666666666667, 1.0 ],
													"textjustification" : 2
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-72",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 912.0, 289.0, 29.5, 22.0 ],
													"text" : "f 1."
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-68",
													"maxclass" : "newobj",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 811.5, 138.0, 102.0, 22.0 ],
													"text" : "r ---RectWidth_xy"
												}

											}
, 											{
												"box" : 												{
													"format" : 6,
													"id" : "obj-67",
													"maxclass" : "flonum",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 811.5, 172.0, 50.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-56",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 320.0, 500.0, 19.0, 22.0 ],
													"text" : "t l"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-50",
													"linecount" : 2,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 490.0, 390.5, 241.0, 49.0 ],
													"text" : "/ITL/scene/Nownow set rect 0.032656 2., /ITL/scene/Nownow x $1;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-47",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 490.0, 289.0, 36.0, 22.0 ],
													"text" : "f -0.5"
												}

											}
, 											{
												"box" : 												{
													"format" : 6,
													"id" : "obj-23",
													"maxclass" : "flonum",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 413.0, 172.0, 50.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-12",
													"maxclass" : "newobj",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 413.0, 142.0, 89.0, 22.0 ],
													"text" : "r ---Nowline_xy"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-49",
													"maxclass" : "button",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 1170.5, 58.0, 24.0, 24.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-46",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 1198.5, 62.0, 75.0, 20.0 ],
													"text" : "RESET"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-40",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 65.0, 59.0, 51.0, 20.0 ],
													"text" : "0. <> 1."
												}

											}
, 											{
												"box" : 												{
													"format" : 6,
													"id" : "obj-37",
													"maxclass" : "flonum",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 17.0, 59.0, 44.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-33",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 154.099998831748962, 58.0, 52.0, 20.0 ],
													"text" : "on/off"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-27",
													"maxclass" : "toggle",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "int" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 125.599998831748962, 58.0, 24.0, 24.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-70",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patcher" : 													{
														"fileversion" : 1,
														"appversion" : 														{
															"major" : 8,
															"minor" : 1,
															"revision" : 8,
															"architecture" : "x64",
															"modernui" : 1
														}
,
														"classnamespace" : "box",
														"rect" : [ 59.0, 104.0, 818.0, 762.0 ],
														"bglocked" : 0,
														"openinpresentation" : 0,
														"default_fontsize" : 12.0,
														"default_fontface" : 0,
														"default_fontname" : "Arial",
														"gridonopen" : 1,
														"gridsize" : [ 15.0, 15.0 ],
														"gridsnaponopen" : 1,
														"objectsnaponopen" : 1,
														"statusbarvisible" : 2,
														"toolbarvisible" : 1,
														"lefttoolbarpinned" : 0,
														"toptoolbarpinned" : 0,
														"righttoolbarpinned" : 0,
														"bottomtoolbarpinned" : 0,
														"toolbars_unpinned_last_save" : 0,
														"tallnewobj" : 0,
														"boxanimatetime" : 200,
														"enablehscroll" : 1,
														"enablevscroll" : 1,
														"devicewidth" : 0.0,
														"description" : "",
														"digest" : "",
														"tags" : "",
														"style" : "",
														"subpatcher_template" : "",
														"assistshowspatchername" : 0,
														"boxes" : [ 															{
																"box" : 																{
																	"id" : "obj-10",
																	"maxclass" : "message",
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"outlettype" : [ "" ],
																	"patching_rect" : [ 70.0, 501.0, 29.5, 22.0 ],
																	"text" : "0."
																}

															}
, 															{
																"box" : 																{
																	"id" : "obj-6",
																	"maxclass" : "newobj",
																	"numinlets" : 2,
																	"numoutlets" : 2,
																	"outlettype" : [ "bang", "" ],
																	"patching_rect" : [ 70.0, 92.0, 37.0, 22.0 ],
																	"text" : "sel 0."
																}

															}
, 															{
																"box" : 																{
																	"id" : "obj-54",
																	"maxclass" : "number",
																	"numinlets" : 1,
																	"numoutlets" : 2,
																	"outlettype" : [ "", "bang" ],
																	"parameter_enable" : 0,
																	"patching_rect" : [ 170.0, 545.0, 50.0, 22.0 ]
																}

															}
, 															{
																"box" : 																{
																	"comment" : "",
																	"id" : "obj-50",
																	"index" : 1,
																	"maxclass" : "outlet",
																	"numinlets" : 1,
																	"numoutlets" : 0,
																	"patching_rect" : [ 170.0, 644.0, 30.0, 30.0 ]
																}

															}
, 															{
																"box" : 																{
																	"id" : "obj-49",
																	"maxclass" : "message",
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"outlettype" : [ "" ],
																	"patching_rect" : [ 170.0, 590.0, 181.0, 22.0 ],
																	"text" : "/ITL/scene/RecordRect alpha $1"
																}

															}
, 															{
																"box" : 																{
																	"id" : "obj-46",
																	"maxclass" : "newobj",
																	"numinlets" : 6,
																	"numoutlets" : 1,
																	"outlettype" : [ "" ],
																	"patching_rect" : [ 170.0, 501.0, 110.0, 22.0 ],
																	"text" : "scale 0. 0.1 0. 255."
																}

															}
, 															{
																"box" : 																{
																	"format" : 6,
																	"id" : "obj-44",
																	"maxclass" : "flonum",
																	"numinlets" : 1,
																	"numoutlets" : 2,
																	"outlettype" : [ "", "bang" ],
																	"parameter_enable" : 0,
																	"patching_rect" : [ 170.0, 463.0, 50.0, 22.0 ]
																}

															}
, 															{
																"box" : 																{
																	"format" : 6,
																	"id" : "obj-42",
																	"maxclass" : "flonum",
																	"numinlets" : 1,
																	"numoutlets" : 2,
																	"outlettype" : [ "", "bang" ],
																	"parameter_enable" : 0,
																	"patching_rect" : [ 170.0, 422.0, 50.0, 22.0 ]
																}

															}
, 															{
																"box" : 																{
																	"id" : "obj-40",
																	"maxclass" : "newobj",
																	"numinlets" : 6,
																	"numoutlets" : 1,
																	"outlettype" : [ "" ],
																	"patching_rect" : [ 170.0, 386.0, 100.0, 22.0 ],
																	"text" : "scale 0.9 1 0.1 0."
																}

															}
, 															{
																"box" : 																{
																	"format" : 6,
																	"id" : "obj-39",
																	"maxclass" : "flonum",
																	"numinlets" : 1,
																	"numoutlets" : 2,
																	"outlettype" : [ "", "bang" ],
																	"parameter_enable" : 0,
																	"patching_rect" : [ 600.0, 340.0, 50.0, 22.0 ]
																}

															}
, 															{
																"box" : 																{
																	"id" : "obj-37",
																	"maxclass" : "newobj",
																	"numinlets" : 1,
																	"numoutlets" : 1,
																	"outlettype" : [ "" ],
																	"patching_rect" : [ 600.0, 308.0, 69.0, 22.0 ],
																	"text" : "expr 1 - $f1"
																}

															}
, 															{
																"box" : 																{
																	"format" : 6,
																	"id" : "obj-36",
																	"maxclass" : "flonum",
																	"numinlets" : 1,
																	"numoutlets" : 2,
																	"outlettype" : [ "", "bang" ],
																	"parameter_enable" : 0,
																	"patching_rect" : [ 170.0, 339.0, 50.0, 22.0 ]
																}

															}
, 															{
																"box" : 																{
																	"id" : "obj-34",
																	"maxclass" : "newobj",
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"outlettype" : [ "" ],
																	"patching_rect" : [ 170.0, 303.0, 134.0, 22.0 ],
																	"text" : "gate 1 0"
																}

															}
, 															{
																"box" : 																{
																	"id" : "obj-32",
																	"maxclass" : "toggle",
																	"numinlets" : 1,
																	"numoutlets" : 1,
																	"outlettype" : [ "int" ],
																	"parameter_enable" : 0,
																	"patching_rect" : [ 170.0, 222.0, 24.0, 24.0 ]
																}

															}
, 															{
																"box" : 																{
																	"id" : "obj-33",
																	"maxclass" : "newobj",
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"outlettype" : [ "int" ],
																	"patching_rect" : [ 170.0, 188.0, 43.0, 22.0 ],
																	"text" : ">= 0.9"
																}

															}
, 															{
																"box" : 																{
																	"id" : "obj-31",
																	"maxclass" : "newobj",
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"outlettype" : [ "" ],
																	"patching_rect" : [ 250.0, 262.0, 99.0, 22.0 ],
																	"text" : "gate 1 0"
																}

															}
, 															{
																"box" : 																{
																	"format" : 6,
																	"id" : "obj-30",
																	"maxclass" : "flonum",
																	"numinlets" : 1,
																	"numoutlets" : 2,
																	"outlettype" : [ "", "bang" ],
																	"parameter_enable" : 0,
																	"patching_rect" : [ 250.0, 422.0, 50.0, 22.0 ]
																}

															}
, 															{
																"box" : 																{
																	"id" : "obj-9",
																	"maxclass" : "message",
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"outlettype" : [ "" ],
																	"patching_rect" : [ 680.0, 65.0, 29.5, 22.0 ],
																	"text" : "0.1"
																}

															}
, 															{
																"box" : 																{
																	"format" : 6,
																	"id" : "obj-7",
																	"maxclass" : "flonum",
																	"numinlets" : 1,
																	"numoutlets" : 2,
																	"outlettype" : [ "", "bang" ],
																	"parameter_enable" : 0,
																	"patching_rect" : [ 600.0, 101.0, 50.0, 22.0 ]
																}

															}
, 															{
																"box" : 																{
																	"id" : "obj-5",
																	"maxclass" : "newobj",
																	"numinlets" : 1,
																	"numoutlets" : 3,
																	"outlettype" : [ "float", "float", "float" ],
																	"patching_rect" : [ 170.0, 135.0, 179.0, 22.0 ],
																	"text" : "t f f f"
																}

															}
, 															{
																"box" : 																{
																	"bgcolor" : [ 0.996078431372549, 0.874509803921569, 0.874509803921569, 0.0 ],
																	"floatoutput" : 1,
																	"id" : "obj-15",
																	"knobcolor" : [ 0.862745098039216, 0.905882352941176, 0.803921568627451, 1.0 ],
																	"maxclass" : "slider",
																	"numinlets" : 1,
																	"numoutlets" : 1,
																	"outlettype" : [ "" ],
																	"parameter_enable" : 0,
																	"patching_rect" : [ 283.0, 59.0, 240.0, 22.0 ],
																	"size" : 1.0
																}

															}
, 															{
																"box" : 																{
																	"format" : 6,
																	"id" : "obj-3",
																	"maxclass" : "flonum",
																	"numdecimalplaces" : 2,
																	"numinlets" : 1,
																	"numoutlets" : 2,
																	"outlettype" : [ "", "bang" ],
																	"parameter_enable" : 0,
																	"patching_rect" : [ 283.0, 92.0, 50.0, 22.0 ]
																}

															}
, 															{
																"box" : 																{
																	"id" : "obj-67",
																	"maxclass" : "toggle",
																	"numinlets" : 1,
																	"numoutlets" : 1,
																	"outlettype" : [ "int" ],
																	"parameter_enable" : 0,
																	"patching_rect" : [ 250.0, 222.0, 24.0, 24.0 ]
																}

															}
, 															{
																"box" : 																{
																	"id" : "obj-65",
																	"maxclass" : "newobj",
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"outlettype" : [ "int" ],
																	"patching_rect" : [ 250.0, 188.0, 43.0, 22.0 ],
																	"text" : "<= 0.1"
																}

															}
, 															{
																"box" : 																{
																	"comment" : "",
																	"id" : "obj-68",
																	"index" : 1,
																	"maxclass" : "inlet",
																	"numinlets" : 0,
																	"numoutlets" : 1,
																	"outlettype" : [ "float" ],
																	"patching_rect" : [ 70.0, 23.0, 30.0, 30.0 ]
																}

															}
, 															{
																"box" : 																{
																	"comment" : "",
																	"id" : "obj-69",
																	"index" : 2,
																	"maxclass" : "inlet",
																	"numinlets" : 0,
																	"numoutlets" : 1,
																	"outlettype" : [ "float" ],
																	"patching_rect" : [ 600.0, 40.0, 30.0, 30.0 ]
																}

															}
 ],
														"lines" : [ 															{
																"patchline" : 																{
																	"destination" : [ "obj-54", 0 ],
																	"source" : [ "obj-10", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-3", 0 ],
																	"source" : [ "obj-15", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-5", 0 ],
																	"source" : [ "obj-3", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-44", 0 ],
																	"source" : [ "obj-30", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-30", 0 ],
																	"source" : [ "obj-31", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-34", 0 ],
																	"source" : [ "obj-32", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-32", 0 ],
																	"source" : [ "obj-33", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-36", 0 ],
																	"source" : [ "obj-34", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-40", 0 ],
																	"source" : [ "obj-36", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-39", 0 ],
																	"source" : [ "obj-37", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-33", 1 ],
																	"order" : 0,
																	"source" : [ "obj-39", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-40", 1 ],
																	"order" : 1,
																	"source" : [ "obj-39", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-42", 0 ],
																	"source" : [ "obj-40", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-44", 0 ],
																	"source" : [ "obj-42", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-46", 0 ],
																	"source" : [ "obj-44", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-54", 0 ],
																	"source" : [ "obj-46", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-50", 0 ],
																	"source" : [ "obj-49", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-31", 1 ],
																	"order" : 0,
																	"source" : [ "obj-5", 2 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-33", 0 ],
																	"source" : [ "obj-5", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-34", 1 ],
																	"order" : 1,
																	"source" : [ "obj-5", 2 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-65", 0 ],
																	"source" : [ "obj-5", 1 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-49", 0 ],
																	"source" : [ "obj-54", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-10", 0 ],
																	"source" : [ "obj-6", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-5", 0 ],
																	"source" : [ "obj-6", 1 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-67", 0 ],
																	"source" : [ "obj-65", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-31", 0 ],
																	"source" : [ "obj-67", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-6", 0 ],
																	"source" : [ "obj-68", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-7", 0 ],
																	"source" : [ "obj-69", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-37", 0 ],
																	"order" : 0,
																	"source" : [ "obj-7", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-40", 3 ],
																	"order" : 2,
																	"source" : [ "obj-7", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-46", 2 ],
																	"order" : 3,
																	"source" : [ "obj-7", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-65", 1 ],
																	"order" : 1,
																	"source" : [ "obj-7", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-7", 0 ],
																	"source" : [ "obj-9", 0 ]
																}

															}
 ]
													}
,
													"patching_rect" : [ 163.0, 324.39999920129776, 97.0, 22.0 ],
													"saved_object_attributes" : 													{
														"description" : "",
														"digest" : "",
														"globalpatchername" : "",
														"tags" : ""
													}
,
													"text" : "p ProgressAlpha"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-63",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patcher" : 													{
														"fileversion" : 1,
														"appversion" : 														{
															"major" : 8,
															"minor" : 1,
															"revision" : 8,
															"architecture" : "x64",
															"modernui" : 1
														}
,
														"classnamespace" : "box",
														"rect" : [ 111.0, 208.0, 640.0, 480.0 ],
														"bglocked" : 0,
														"openinpresentation" : 0,
														"default_fontsize" : 12.0,
														"default_fontface" : 0,
														"default_fontname" : "Arial",
														"gridonopen" : 1,
														"gridsize" : [ 15.0, 15.0 ],
														"gridsnaponopen" : 1,
														"objectsnaponopen" : 1,
														"statusbarvisible" : 2,
														"toolbarvisible" : 1,
														"lefttoolbarpinned" : 0,
														"toptoolbarpinned" : 0,
														"righttoolbarpinned" : 0,
														"bottomtoolbarpinned" : 0,
														"toolbars_unpinned_last_save" : 0,
														"tallnewobj" : 0,
														"boxanimatetime" : 200,
														"enablehscroll" : 1,
														"enablevscroll" : 1,
														"devicewidth" : 0.0,
														"description" : "",
														"digest" : "",
														"tags" : "",
														"style" : "",
														"subpatcher_template" : "",
														"assistshowspatchername" : 0,
														"boxes" : [ 															{
																"box" : 																{
																	"id" : "obj-5",
																	"maxclass" : "newobj",
																	"numinlets" : 1,
																	"numoutlets" : 1,
																	"outlettype" : [ "" ],
																	"patching_rect" : [ 113.0, 40.0, 89.0, 22.0 ],
																	"text" : "loadmess set 1"
																}

															}
, 															{
																"box" : 																{
																	"id" : "obj-11",
																	"maxclass" : "button",
																	"numinlets" : 1,
																	"numoutlets" : 1,
																	"outlettype" : [ "bang" ],
																	"parameter_enable" : 0,
																	"patching_rect" : [ 148.0, 216.5, 24.0, 24.0 ]
																}

															}
, 															{
																"box" : 																{
																	"id" : "obj-7",
																	"maxclass" : "button",
																	"numinlets" : 1,
																	"numoutlets" : 1,
																	"outlettype" : [ "bang" ],
																	"parameter_enable" : 0,
																	"patching_rect" : [ 50.0, 216.5, 24.0, 24.0 ]
																}

															}
, 															{
																"box" : 																{
																	"id" : "obj-16",
																	"linecount" : 2,
																	"maxclass" : "message",
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"outlettype" : [ "" ],
																	"patching_rect" : [ 415.0, 211.0, 50.0, 35.0 ],
																	"text" : "21 0 255"
																}

															}
, 															{
																"box" : 																{
																	"id" : "obj-14",
																	"maxclass" : "message",
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"outlettype" : [ "" ],
																	"patching_rect" : [ 385.0, 137.0, 75.0, 22.0 ],
																	"text" : "set $1 $2 $3"
																}

															}
, 															{
																"box" : 																{
																	"id" : "obj-10",
																	"maxclass" : "message",
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"outlettype" : [ "" ],
																	"patching_rect" : [ 256.0, 129.0, 75.0, 22.0 ],
																	"text" : "set $1 $2 $3"
																}

															}
, 															{
																"box" : 																{
																	"id" : "obj-8",
																	"maxclass" : "message",
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"outlettype" : [ "" ],
																	"patching_rect" : [ 531.0, 79.0, 88.0, 22.0 ],
																	"text" : "21 0 255"
																}

															}
, 															{
																"box" : 																{
																	"id" : "obj-6",
																	"linecount" : 2,
																	"maxclass" : "message",
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"outlettype" : [ "" ],
																	"patching_rect" : [ 171.0, 79.0, 50.0, 35.0 ],
																	"text" : "255 0 21"
																}

															}
, 															{
																"box" : 																{
																	"id" : "obj-4",
																	"maxclass" : "newobj",
																	"numinlets" : 0,
																	"numoutlets" : 1,
																	"outlettype" : [ "" ],
																	"patching_rect" : [ 385.0, 85.0, 101.0, 22.0 ],
																	"text" : "r ---color_inactive"
																}

															}
, 															{
																"box" : 																{
																	"id" : "obj-3",
																	"maxclass" : "newobj",
																	"numinlets" : 0,
																	"numoutlets" : 1,
																	"outlettype" : [ "" ],
																	"patching_rect" : [ 256.0, 85.0, 92.0, 22.0 ],
																	"text" : "r ---color_active"
																}

															}
, 															{
																"box" : 																{
																	"id" : "obj-2",
																	"maxclass" : "message",
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"outlettype" : [ "" ],
																	"patching_rect" : [ 369.0, 363.0, 257.0, 22.0 ],
																	"text" : "/ITL/scene/RecordRect dcolor $1 $2 $3 $4"
																}

															}
, 															{
																"box" : 																{
																	"id" : "obj-1",
																	"maxclass" : "newobj",
																	"numinlets" : 1,
																	"numoutlets" : 3,
																	"outlettype" : [ "", "int", "int" ],
																	"patching_rect" : [ 50.0, 137.0, 48.0, 22.0 ],
																	"text" : "change"
																}

															}
, 															{
																"box" : 																{
																	"id" : "obj-46",
																	"maxclass" : "message",
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"outlettype" : [ "" ],
																	"patching_rect" : [ 50.0, 289.0, 69.0, 22.0 ],
																	"text" : "21 0 255"
																}

															}
, 															{
																"box" : 																{
																	"id" : "obj-42",
																	"maxclass" : "message",
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"outlettype" : [ "" ],
																	"patching_rect" : [ 148.0, 289.0, 73.0, 22.0 ],
																	"text" : "255 0 21"
																}

															}
, 															{
																"box" : 																{
																	"id" : "obj-37",
																	"maxclass" : "toggle",
																	"numinlets" : 1,
																	"numoutlets" : 1,
																	"outlettype" : [ "int" ],
																	"parameter_enable" : 0,
																	"patching_rect" : [ 50.0, 100.0, 24.0, 24.0 ]
																}

															}
, 															{
																"box" : 																{
																	"id" : "obj-33",
																	"maxclass" : "newobj",
																	"numinlets" : 3,
																	"numoutlets" : 3,
																	"outlettype" : [ "bang", "bang", "" ],
																	"patching_rect" : [ 50.0, 182.0, 215.0, 22.0 ],
																	"text" : "sel 0 1"
																}

															}
, 															{
																"box" : 																{
																	"id" : "obj-31",
																	"maxclass" : "message",
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"outlettype" : [ "" ],
																	"patching_rect" : [ 50.0, 356.0, 234.0, 22.0 ],
																	"text" : "/ITL/scene/RecordRect color $1 $2 $3 255"
																}

															}
, 															{
																"box" : 																{
																	"comment" : "",
																	"id" : "obj-56",
																	"index" : 1,
																	"maxclass" : "inlet",
																	"numinlets" : 0,
																	"numoutlets" : 1,
																	"outlettype" : [ "int" ],
																	"patching_rect" : [ 50.0, 40.0, 30.0, 30.0 ]
																}

															}
, 															{
																"box" : 																{
																	"comment" : "",
																	"id" : "obj-61",
																	"index" : 1,
																	"maxclass" : "outlet",
																	"numinlets" : 1,
																	"numoutlets" : 0,
																	"patching_rect" : [ 50.0, 409.0, 30.0, 30.0 ]
																}

															}
 ],
														"lines" : [ 															{
																"patchline" : 																{
																	"destination" : [ "obj-33", 0 ],
																	"source" : [ "obj-1", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-42", 0 ],
																	"source" : [ "obj-10", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-42", 0 ],
																	"source" : [ "obj-11", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-16", 0 ],
																	"order" : 0,
																	"source" : [ "obj-14", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-46", 0 ],
																	"order" : 1,
																	"source" : [ "obj-14", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-10", 0 ],
																	"order" : 0,
																	"source" : [ "obj-3", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-6", 1 ],
																	"order" : 1,
																	"source" : [ "obj-3", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-61", 0 ],
																	"source" : [ "obj-31", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-11", 0 ],
																	"source" : [ "obj-33", 1 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-7", 0 ],
																	"source" : [ "obj-33", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-1", 0 ],
																	"source" : [ "obj-37", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-14", 0 ],
																	"order" : 1,
																	"source" : [ "obj-4", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-8", 1 ],
																	"order" : 0,
																	"source" : [ "obj-4", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-31", 0 ],
																	"source" : [ "obj-42", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-31", 0 ],
																	"source" : [ "obj-46", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-1", 0 ],
																	"source" : [ "obj-5", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-37", 0 ],
																	"source" : [ "obj-56", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-10", 0 ],
																	"source" : [ "obj-6", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-46", 0 ],
																	"source" : [ "obj-7", 0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"destination" : [ "obj-14", 0 ],
																	"source" : [ "obj-8", 0 ]
																}

															}
 ]
													}
,
													"patching_rect" : [ 53.60000079870224, 1056.400014340877533, 118.0, 22.0 ],
													"saved_object_attributes" : 													{
														"description" : "",
														"digest" : "",
														"globalpatchername" : "",
														"tags" : ""
													}
,
													"text" : "p Colorize_when_on"
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-4",
													"index" : 2,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 125.599998831748962, 18.0, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"format" : 6,
													"id" : "obj-6",
													"maxclass" : "flonum",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 241.0, 172.0, 50.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-2",
													"maxclass" : "newobj",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 241.0, 142.0, 107.0, 22.0 ],
													"text" : "r ---ProgressAlpha"
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-1",
													"index" : 3,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 1170.5, 18.0, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-7",
													"index" : 1,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 17.0, 18.0, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-60",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 5,
													"outlettype" : [ "float", "bang", "bang", "bang", "bang" ],
													"patching_rect" : [ 1170.5, 138.0, 65.0, 22.0 ],
													"text" : "t 0. b b b b"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-59",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 1170.5, 94.0, 58.0, 22.0 ],
													"text" : "loadbang"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-53",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 921.5, 950.0, 63.0, 22.0 ],
													"text" : "-1.5"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-54",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 1019.5, 678.200010418891907, 59.0, 20.0 ],
													"text" : "Final_xy",
													"textcolor" : [ 0.866666666666667, 0.866666666666667, 0.866666666666667, 1.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-55",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 965.5, 651.399998307228088, 104.0, 22.0 ],
													"text" : "expr -1. - $f1 - $f2"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-51",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 552.0, 731.599997341632843, 79.0, 22.0 ],
													"text" : "expr $f1 - $f2"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-44",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 508.0, 950.0, 63.0, 22.0 ],
													"text" : "-1."
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-45",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 605.0, 762.60000866651535, 126.0, 20.0 ],
													"text" : "RecordEnd position",
													"textcolor" : [ 0.866666666666667, 0.866666666666667, 0.866666666666667, 1.0 ]
												}

											}
, 											{
												"box" : 												{
													"format" : 6,
													"id" : "obj-43",
													"maxclass" : "flonum",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 17.0, 1186.200000107288361, 50.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-41",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 782.0, 950.0, 59.0, 22.0 ],
													"text" : "1.5"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-39",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 369.0, 950.0, 63.0, 22.0 ],
													"text" : "0."
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-36",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 873.265306111501104, 675.399998307228088, 55.0, 20.0 ],
													"text" : "Initial_xy",
													"textcolor" : [ 0.866666666666667, 0.866666666666667, 0.866666666666667, 1.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-35",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 822.0, 651.399998307228088, 106.0, 22.0 ],
													"text" : "expr 1. + $f1 + $f2"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-32",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 293.199998676776886, 761.60000866651535, 126.0, 20.0 ],
													"text" : "RecordStart position",
													"textcolor" : [ 0.866666666666667, 0.866666666666667, 0.866666666666667, 1.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-30",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 413.0, 731.599997341632843, 29.5, 22.0 ],
													"text" : "+ 0."
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-29",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 413.0, 651.399998307228088, 41.0, 22.0 ],
													"text" : "pak f f"
												}

											}
, 											{
												"box" : 												{
													"format" : 6,
													"id" : "obj-28",
													"maxclass" : "flonum",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 811.5, 575.0, 50.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-26",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "float" ],
													"patching_rect" : [ 811.5, 547.0, 29.5, 22.0 ],
													"text" : "/ 2."
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-25",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 684.5, 357.0, 125.0, 20.0 ],
													"text" : "RectWidth_xy",
													"textcolor" : [ 0.866666666666667, 0.866666666666667, 0.866666666666667, 1.0 ],
													"textjustification" : 2
												}

											}
, 											{
												"box" : 												{
													"format" : 6,
													"id" : "obj-22",
													"maxclass" : "flonum",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 811.5, 356.0, 50.0, 22.0 ],
													"textcolor" : [ 0.866666666666667, 0.866666666666667, 0.866666666666667, 1.0 ]
												}

											}
, 											{
												"box" : 												{
													"format" : 6,
													"id" : "obj-16",
													"maxclass" : "flonum",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 413.0, 356.0, 50.0, 22.0 ],
													"textcolor" : [ 0.866666666666667, 0.866666666666667, 0.866666666666667, 1.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-14",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 1216.5, 967.0, 120.0, 22.0 ],
													"text" : "/ITL/scene reset"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-11",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 17.0, 1221.400000214576721, 157.0, 22.0 ],
													"text" : "/ITL/scene/RecordRect x $1"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-10",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 843.0, 449.0, 246.0, 36.0 ],
													"text" : "/ITL/scene/RecordRect set rect $1 0.579116;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-331",
													"maxclass" : "newobj",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 777.0, 1225.0, 105.0, 22.0 ],
													"text" : "r copiedfrompatch"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-8",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 320.0, 404.0, 143.0, 36.0 ],
													"text" : "/ITL/scene/Nownow x $1;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-5",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 625.0, 1266.0, 257.0, 22.0 ],
													"text" : "/ITL/scene reset"
												}

											}
 ],
										"lines" : [ 											{
												"patchline" : 												{
													"destination" : [ "obj-49", 0 ],
													"source" : [ "obj-1", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-77", 0 ],
													"source" : [ "obj-10", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-103", 0 ],
													"source" : [ "obj-101", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-104", 0 ],
													"source" : [ "obj-103", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-105", 0 ],
													"source" : [ "obj-104", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-63", 0 ],
													"source" : [ "obj-104", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-13", 0 ],
													"source" : [ "obj-11", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-23", 0 ],
													"source" : [ "obj-12", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-13", 0 ],
													"midpoints" : [ 1226.0, 1200.200001180171967, 334.5, 1200.200001180171967 ],
													"source" : [ "obj-14", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-17", 5 ],
													"order" : 0,
													"source" : [ "obj-16", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-29", 0 ],
													"order" : 1,
													"source" : [ "obj-16", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-8", 0 ],
													"midpoints" : [ 422.5, 389.0, 329.5, 389.0 ],
													"order" : 2,
													"source" : [ "obj-16", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-19", 0 ],
													"source" : [ "obj-17", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-30", 0 ],
													"source" : [ "obj-18", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-51", 0 ],
													"source" : [ "obj-18", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-9", 0 ],
													"source" : [ "obj-19", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-6", 0 ],
													"source" : [ "obj-2", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-56", 0 ],
													"midpoints" : [ 172.5, 486.0, 329.5, 486.0 ],
													"source" : [ "obj-20", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-70", 1 ],
													"midpoints" : [ 335.5, 307.0, 250.5, 307.0 ],
													"source" : [ "obj-21", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-78", 0 ],
													"source" : [ "obj-22", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-16", 0 ],
													"order" : 1,
													"source" : [ "obj-23", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-47", 1 ],
													"order" : 0,
													"source" : [ "obj-23", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-61", 0 ],
													"order" : 1,
													"source" : [ "obj-24", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-70", 0 ],
													"midpoints" : [ 26.5, 306.0, 172.5, 306.0 ],
													"order" : 0,
													"source" : [ "obj-24", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-28", 0 ],
													"source" : [ "obj-26", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-87", 0 ],
													"source" : [ "obj-28", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-18", 0 ],
													"source" : [ "obj-29", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-82", 0 ],
													"source" : [ "obj-30", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-5", 1 ],
													"source" : [ "obj-331", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-17", 3 ],
													"order" : 1,
													"source" : [ "obj-34", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-53", 1 ],
													"order" : 0,
													"source" : [ "obj-34", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-61", 4 ],
													"midpoints" : [ 975.0, 711.0, 135.0, 711.0, 135.0, 615.0, 96.900000000000006, 615.0 ],
													"order" : 2,
													"source" : [ "obj-34", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-86", 0 ],
													"source" : [ "obj-35", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-24", 0 ],
													"source" : [ "obj-37", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-96", 0 ],
													"source" : [ "obj-39", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-27", 0 ],
													"source" : [ "obj-4", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-96", 0 ],
													"source" : [ "obj-41", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-11", 0 ],
													"source" : [ "obj-43", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-96", 0 ],
													"source" : [ "obj-44", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-16", 0 ],
													"order" : 1,
													"source" : [ "obj-47", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-50", 0 ],
													"order" : 0,
													"source" : [ "obj-47", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-59", 0 ],
													"source" : [ "obj-49", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-13", 0 ],
													"source" : [ "obj-5", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-56", 0 ],
													"midpoints" : [ 499.5, 456.0, 329.5, 456.0 ],
													"source" : [ "obj-50", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-84", 0 ],
													"source" : [ "obj-51", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-101", 0 ],
													"source" : [ "obj-52", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-79", 0 ],
													"source" : [ "obj-52", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-96", 0 ],
													"source" : [ "obj-53", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-34", 0 ],
													"source" : [ "obj-55", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-13", 0 ],
													"source" : [ "obj-56", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-60", 0 ],
													"source" : [ "obj-59", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-21", 1 ],
													"order" : 0,
													"source" : [ "obj-6", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-70", 1 ],
													"order" : 1,
													"source" : [ "obj-6", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-14", 0 ],
													"source" : [ "obj-60", 4 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-21", 0 ],
													"midpoints" : [ 1214.5, 256.0, 335.5, 256.0 ],
													"source" : [ "obj-60", 3 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-47", 0 ],
													"midpoints" : [ 1191.5, 246.0, 499.5, 246.0 ],
													"source" : [ "obj-60", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-70", 0 ],
													"midpoints" : [ 1180.0, 238.0, 172.5, 238.0 ],
													"source" : [ "obj-60", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-72", 0 ],
													"midpoints" : [ 1203.0, 270.0, 921.5, 270.0 ],
													"source" : [ "obj-60", 2 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-64", 0 ],
													"source" : [ "obj-61", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-13", 0 ],
													"source" : [ "obj-63", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-94", 0 ],
													"source" : [ "obj-64", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-22", 0 ],
													"order" : 1,
													"source" : [ "obj-67", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-72", 1 ],
													"order" : 0,
													"source" : [ "obj-67", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-67", 0 ],
													"source" : [ "obj-68", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-89", 0 ],
													"source" : [ "obj-69", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-37", 0 ],
													"source" : [ "obj-7", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-20", 0 ],
													"source" : [ "obj-70", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-22", 0 ],
													"order" : 1,
													"source" : [ "obj-72", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-76", 0 ],
													"order" : 0,
													"source" : [ "obj-72", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-77", 0 ],
													"source" : [ "obj-76", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-56", 0 ],
													"midpoints" : [ 921.5, 531.0, 361.0, 531.0, 361.0, 489.0, 329.5, 489.0 ],
													"source" : [ "obj-77", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-10", 0 ],
													"source" : [ "obj-78", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-17", 4 ],
													"source" : [ "obj-78", 2 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-26", 0 ],
													"source" : [ "obj-78", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-99", 0 ],
													"source" : [ "obj-79", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-56", 0 ],
													"source" : [ "obj-8", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-17", 0 ],
													"order" : 0,
													"source" : [ "obj-82", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-39", 1 ],
													"order" : 1,
													"source" : [ "obj-82", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-52", 1 ],
													"order" : 2,
													"source" : [ "obj-82", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-17", 1 ],
													"order" : 0,
													"source" : [ "obj-84", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-44", 1 ],
													"order" : 1,
													"source" : [ "obj-84", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-99", 1 ],
													"order" : 2,
													"source" : [ "obj-84", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-17", 2 ],
													"order" : 1,
													"source" : [ "obj-86", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-41", 1 ],
													"order" : 0,
													"source" : [ "obj-86", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-61", 3 ],
													"midpoints" : [ 831.5, 704.600000083446503, 156.600000321865082, 704.600000083446503, 156.600000321865082, 602.199999809265137, 79.299999999999997, 602.199999809265137 ],
													"order" : 2,
													"source" : [ "obj-86", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-29", 1 ],
													"midpoints" : [ 821.0, 643.599999666213989, 444.5, 643.599999666213989 ],
													"source" : [ "obj-87", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-35", 0 ],
													"source" : [ "obj-87", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-55", 0 ],
													"midpoints" : [ 842.0, 642.800000190734863, 975.0, 642.800000190734863 ],
													"source" : [ "obj-87", 2 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-61", 0 ],
													"source" : [ "obj-89", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-28", 0 ],
													"source" : [ "obj-93", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-43", 0 ],
													"source" : [ "obj-94", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-52", 0 ],
													"source" : [ "obj-94", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-43", 0 ],
													"midpoints" : [ 378.5, 1158.400001347064972, 26.5, 1158.400001347064972 ],
													"source" : [ "obj-96", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-16", 0 ],
													"source" : [ "obj-98", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-101", 0 ],
													"source" : [ "obj-99", 0 ]
												}

											}
 ],
										"boxgroups" : [ 											{
												"boxes" : [ "obj-80", "obj-28" ]
											}
, 											{
												"boxes" : [ "obj-75", "obj-16" ]
											}
, 											{
												"boxes" : [ "obj-25", "obj-22" ]
											}
, 											{
												"boxes" : [ "obj-32", "obj-82" ]
											}
, 											{
												"boxes" : [ "obj-45", "obj-84" ]
											}
, 											{
												"boxes" : [ "obj-36", "obj-86" ]
											}
, 											{
												"boxes" : [ "obj-54", "obj-34" ]
											}
 ]
									}
,
									"patching_rect" : [ 20.333336353302002, 187.174764215946198, 145.0, 24.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 14.5, 189.0, 145.0, 24.0 ],
									"saved_object_attributes" : 									{
										"description" : "",
										"digest" : "",
										"globalpatchername" : "",
										"tags" : ""
									}
,
									"text" : "p SlidingRectange"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-62",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 20.333336353302002, 223.174764215946198, 24.0, 24.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 87.0, 46.0, 18.0, 18.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-59",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 232.333336353302002, 83.174764215946198, 58.0, 22.0 ],
									"text" : "loadbang"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-8",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 258.333336353302002, 111.174764215946198, 95.0, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 89.5, 145.0, 50.0, 20.0 ],
									"text" : "RESET",
									"textcolor" : [ 0.950752079486847, 0.950723588466644, 0.950739741325378, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.379298150539398, 0.379286825656891, 0.379293262958527, 1.0 ],
									"id" : "obj-12",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"outlinecolor" : [ 0.96055269241333, 0.960523903369904, 0.960540175437927, 1.0 ],
									"parameter_enable" : 0,
									"patching_rect" : [ 232.333336353302002, 108.174764215946198, 24.0, 24.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 67.5, 144.0, 20.0, 20.0 ]
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-72",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 232.333336353302002, 290.174764215946198, 50.0, 22.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-19", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"source" : [ "obj-10", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"source" : [ "obj-10", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-159", 0 ],
									"order" : 0,
									"source" : [ "obj-101", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-101", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-58", 0 ],
									"order" : 2,
									"source" : [ "obj-101", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-62", 0 ],
									"source" : [ "obj-101", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-77", 0 ],
									"order" : 1,
									"source" : [ "obj-101", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-96", 0 ],
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-72", 0 ],
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-29", 0 ],
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-190", 0 ],
									"source" : [ "obj-159", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"source" : [ "obj-16", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-23", 0 ],
									"source" : [ "obj-17", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-28", 0 ],
									"source" : [ "obj-17", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 0 ],
									"source" : [ "obj-18", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-191", 0 ],
									"source" : [ "obj-185", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-52", 0 ],
									"source" : [ "obj-19", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-57", 0 ],
									"midpoints" : [ 339.833336353302002, 427.174764215946198, 304.333336353302002, 427.174764215946198, 304.333336353302002, 322.174764215946198, 304.333336353302002, 322.174764215946198, 304.333336353302002, 172.174764215946198, 304.333336353302002, 172.174764215946198, 304.333336353302002, 156.174764215946198, 328.833336353302002, 156.174764215946198 ],
									"source" : [ "obj-190", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"source" : [ "obj-191", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-24", 0 ],
									"source" : [ "obj-20", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"source" : [ "obj-22", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 0 ],
									"source" : [ "obj-23", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 0 ],
									"source" : [ "obj-234", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 0 ],
									"source" : [ "obj-236", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-240", 0 ],
									"source" : [ "obj-237", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-237", 0 ],
									"source" : [ "obj-239", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-23", 1 ],
									"source" : [ "obj-24", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-230", 0 ],
									"source" : [ "obj-24", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-191", 0 ],
									"order" : 1,
									"source" : [ "obj-240", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-242", 0 ],
									"order" : 0,
									"source" : [ "obj-240", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-237", 0 ],
									"source" : [ "obj-243", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-39", 0 ],
									"source" : [ "obj-25", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-28", 1 ],
									"source" : [ "obj-26", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-26", 0 ],
									"source" : [ "obj-27", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-239", 0 ],
									"source" : [ "obj-28", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-243", 0 ],
									"source" : [ "obj-29", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-31", 0 ],
									"source" : [ "obj-30", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-29", 1 ],
									"source" : [ "obj-31", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"order" : 1,
									"source" : [ "obj-36", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-38", 1 ],
									"order" : 0,
									"source" : [ "obj-36", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-39", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-22", 0 ],
									"source" : [ "obj-4", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-25", 0 ],
									"order" : 2,
									"source" : [ "obj-4", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-34", 1 ],
									"order" : 0,
									"source" : [ "obj-4", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-68", 1 ],
									"order" : 1,
									"source" : [ "obj-4", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"source" : [ "obj-52", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-61", 0 ],
									"source" : [ "obj-53", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-56", 0 ],
									"source" : [ "obj-57", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-178", 0 ],
									"source" : [ "obj-58", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"source" : [ "obj-59", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-52", 1 ],
									"source" : [ "obj-61", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-141", 0 ],
									"source" : [ "obj-66", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-101", 0 ],
									"midpoints" : [ 241.833336353302002, 319.174764215946198, 9.833336353302002, 319.174764215946198, 9.833336353302002, 173.174764215946198, 29.833336353302002, 173.174764215946198 ],
									"source" : [ "obj-72", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-66", 2 ],
									"source" : [ "obj-77", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-66", 1 ],
									"source" : [ "obj-77", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-66", 0 ],
									"source" : [ "obj-77", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"order" : 1,
									"source" : [ "obj-9", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 1 ],
									"order" : 0,
									"source" : [ "obj-9", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-101", 2 ],
									"source" : [ "obj-96", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-185", 0 ],
									"midpoints" : [ 241.833336353302002, 175.174764215946198, 241.833336353302002, 175.174764215946198 ],
									"source" : [ "obj-96", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-57", 0 ],
									"source" : [ "obj-96", 2 ]
								}

							}
 ],
						"boxgroups" : [ 							{
								"boxes" : [ "obj-183", "obj-141" ]
							}
, 							{
								"boxes" : [ "obj-182", "obj-178" ]
							}
, 							{
								"boxes" : [ "obj-147", "obj-62" ]
							}
, 							{
								"boxes" : [ "obj-65", "obj-57" ]
							}
, 							{
								"boxes" : [ "obj-8", "obj-12" ]
							}
 ]
					}
,
					"patching_rect" : [ 619.0, 781.0, 217.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p SoD4L messages"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-71",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 920.590143203735352, 633.114753723144531, 150.0, 20.0 ],
					"text" : "<< to SoD4L (ableton)"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.324192345142365, 0.598094642162323, 0.459026545286179, 1.0 ],
					"id" : "obj-65",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 772.090143203735352, 633.114753723144531, 138.0, 22.0 ],
					"text" : "udpsend 127.0.0.1 9999"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-62",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 921.0, 665.114753723144531, 150.0, 20.0 ],
					"text" : "<< from SoD4L (ableton)"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.290196078431373, 0.666666666666667, 0.525490196078431, 1.0 ],
					"id" : "obj-59",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 817.0, 665.114753723144531, 97.0, 22.0 ],
					"text" : "udpreceive 9998"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-47",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 507.761638048772056, 37.0, 100.0, 22.0 ],
					"text" : "readany"
				}

			}
, 			{
				"box" : 				{
					"autofit" : 1,
					"data" : [ 42861, "png", "IBkSG0fBZn....PCIgDQRA..EX...XPJHX.....AwF7z....DLmPIQEBHf.B7g.YHB..f.PRDEDU3wI6c2G6tWeeee+Ylh1ZDxMsUHZWEh0hPasCYs0hLyEb0ZODz7NXqjdvVIcmghYH24SrmaB02jNBwKT6EScFIAAwV8HFQhcZSEAm3gglI7glpfoYfXSani1MBc1D5HsMz4+P+9Wz9iKNk6N276tqqOe+985wCoKwIVwlWhC+9cz40486OuK......157iL5.......LQcqU+3U+IdW+0+LU+Iq9wd6++4G6s+O+c+4Gq5ei20O9e820O9p1bw+h5aU8Uq5GcvAA.....3vzEJy6Oaevh79SW8m5s+O+B+0e729+7QWX2FmhAA....fonasUk68iW8muUk68muUE58mt5OWuSgeack5cXPwf.....vlvQZUod+Yq927s+7m5s+qWnjuqdXoa6welK7CTLH.....recjVUr2e129yElnu+bU+akh9lzTLH.....79cg038uPuSge+4p9K91+ea0cW.TLH.....aWd2k98WnUk8YB+1BoXP.....VVNZqJ46uP002pU885pt1AlIlN9SbgefhAA....Xd4BE+8WtUk98WpUk.Z8dY2Pwf.....LQcjVU72ew20m+syZ9xgLECB....vl2Q6clzu+hsZ5+t9QFH19nXP.....VOT9GSQVkX.....NDbjp+cZUoeJ+i4.ECB....vdvQq9qzpo+6ub0+d4XevLmhAA....3cb7dmI.7uT0MN13.qOJFD....Xazc2ph+92sUS+20N13.adJFD....XI6cOAf+Ux6+G7m5B+.ECB....rDbg2.v+RU+6mU.FtTb7Q.....lkt4pap5uZqJ.7uTNBHv9hhAA....lpNRqJ+6uRlBP3PmhAA....lBNVuSIfNFHvFfhAA....1zt6VU.3e0VUBnUAF1b9wtvOPwf.....qSGu5+fTBHL4nXP....fCKGqUk.dqoDPXxSwf.....6GW3MA7uVqJA7pGab.1k9SdgefhAA....tRNZqdS.+n4vf.ycdiAA....tjNQqdS.+nU23fyBvZhhAA...fsaWXZ.+ajUBF1pnXP....X6xcWcKYZ.gsd+HiN......qMlFPfKl+0JSLH....rjbrVUB3eyLMf.WAJFD....lurVv.6aJFD....lONQqJB7+vpqcvYAXlSwf....vzzMW8wp9q0ph.89.BbnRwf....vzvs1p2Gv+FsZ0fupQFFfkOECB....iwMWc6oHPfAQwf....vly81pUC9imh.AFLECB....qO2c0QyaDHvDjhAA...fCOGuUqF7eypqerQAfKOECB....6eGq2oHvabrQAf8FECB....6dW3fg7SUcSCNK.bfnXP....3xy6DHvhjhAA...f2qiT8eTqJCz5ACrDcyUujhAA...f5da06D3eipqZrQAfMCECB...v1nKbzPNZtdv.aoTLH....aKtup+iqtkQGD.lBTLH....KUGs5i2pKH70N3r.vTxUWJFD...fkkKbAg+34sBDfKKECB...vb280poB7lFcP.XNQwf....L2XEgA3PfhAA...XNvJBCvgm+DkhAA...X5xJBCvZjhAA...Xp3HspHPqHL.a.JFD...fQ5nU+sp9Iqt5AmE.1pnXP....1zNVqJC7mJuWf.LB+3khAA...Xy33spHPGOD.FOGeD....Vqt6p+SptsQGD.3CRwf....bX5DsZUgUFH.SbJFD...fCp6sUkAdKiNH.vtmhAA...X+3dq9aWcSiNH.vd1elRwf....r6chVcDQLYf.r.nXP....tbb.Q.XgRwf....79crVUH3cL5f..qOJFD...fpNZqdy.+optpAmE.3v04q9+q5+6p+eq9WV0Ox.CD....i0QZUYf+jUW8fyB.r+ctpWu5+q20m+Oq9gWt+KoXP...fsK2Z0Oc0OSJCDf4lyT8+d0+aU+eTcpCx+ioXP...fsCOXqlNvqczAA.tr1oUk+cgO+OW8rqi+FoXP...fkqST8eV0MM5f..WTmu5+kp+mp9esC3D.tWoXP...fkki0pIC7NGcP.f2iy1pU.9Ot03T.tWnXP...f4uat5+z7tABvTw4q9erUk.9ur54Gabt3TLH...v708Uc2UW+nCB.a4NSqJA7OrM75.ePnXP...f4kiW8YptsAmC.1VMKlFvcCECB...L8cjVMYf+TUW0fyB.aaNS0+7VUF3rYZ.2MTLH...vz0CzpoC7ZGbN.XaxqzpIA7OnIvABYcRwf....SKmnUqK7sL5f.vVhWoUqC7+hV3EA99oXP...fw6lq9rs5pBaUgAX85rsp.v+vpu6XixXoXP...fw4DU+bU23nCB.KXmqUSC3ePKr2HvCJECB...rY4Ph.v52oq9mU8PiNHSYJFD...Xy3daUgflNP.N7cg0CdwevPNLoXP...f0mKLcfGezAAfEnmoUk.9sGcPlqTLH...vgu6qUEBd8iNH.rfb9puWqVQ3s5iFxgEECB...b33nspLv6bzAAfEjyT86U88qdoAmkEGECB...bvbeU+mWcsiNH.rPb5VMQfVQ30LECB...r2cjpegp6XzAAfEfcp9CaUYfO9Xix1EECB...r6chp+d4sCDfCpcp98a0ZB68BbPTLH...vU1C2p2OvqZzAAfYrKb7P98Z00DlASwf....WbGqUSH3sM5f.vL1ElLv+IoLvIGECB...7dcuU+bYcgAX+RYfyDJFD...XkSV8yj0EFf8icp9CZUYfdy.mITLH...rM63sZcgukQGD.lodlpempSM5fvdmhAA..fsQOP0mo5ZGbN.XN5UZ0jA9PiNHbvnXP...XawQZ0zAdmiNH.LCc1p+wU+xiNHb3Qwf...vR2c2pCJxMN5f.vLy4q9dU+1U+vwFEVGTLH...rTcuU+hUW8nCB.yLd2.2RnXP...Xo4ga0TB55BCvt2Yqd7puwnCBaNJFD...VBt4pubd+.AXuXmpe+puSVU3sRJFD...lyNVqVY3aYzAAlXdwp+ep93Y5Y4C5EqdrVMgfrESwf...vbzIp96Uc8iNHvDwKV87U+gu8e8Aa0TzBWv4a06F3WbzAgoiezQG....f8fGn5ykCJB79KB7Bt6pWu5ZGPlXZ5Eq914PhvEgIFD...lCNY0OSVIR1dcoJB7Bt4pGs5l1fYhoqcZ0zA9YGcPXZSwf...vT0Qp9EptiQGDX.tPQf+wUO6U3+eOY08r1SDyAuR0+ssZBAgqHECB...SMGu5Wr5FGcPfMnWrUWE1+EckKB7Bt2pekLIsa6tvzAdxpWZvYgYFECB...SEmn5uedazX6vYq9mW8Oq56tG+u6Qp9MS44a6Na0uU0CM5fv7khAA..fQ69p96lCJBKamuUSC3y2AaMOexp67PIQLWc5V8uCsWKTF9.TLH...LJOX0OWVCRVtdwVMQfOWG7U77Ap9kNvIh4JGSDVKTLH...ro4BCyR0Ya06C3eP692IvqjiW8qlUrea04p9Mx5ByZhhAA..fMEq.IKMWX8fe1pG+P9+su4pGs5lNj+eWlGdwVUFn0El0JECB...qS2b08WcGiNHvgjSW8GV8MVi+83IZ0jBx1mSUcWiNDr8Pwf...v5vQZ0EF91FcPfCnyW88Z+c8f2qtupewrl8aa1oUWW3u5nCBaeTLH...bXRgfrD7JU+9sdmJv2Muifam79AxvoXP...3vvQq9ur5VFcPf8gc5cNXHG1uUfWIuP95lsMmoUkAto+20fO.ECB...GDGs5WIGHAleNSqJB7+gpme.+8+jU2y.96Kiyoa0jgNh+8M3hRwf...v9gBAYN5zs5cB7aOvLbus5qc7NBt8vAEgIKECB...6EJDj4jM4gC4J4nUOb00O3bvlyiU8YGcHfKGECB...6FJDj4hyT86zl6vgra7b4f7rsXmV81A9EGcPfcCECB...WNGo5AxwQfosWrUqq4HWQ3KlGn5WZzgfMhcp9sp9piNHvdghAA..fKFEBxT1Huhv6FGs56TcsiNHr1c9peylVSnJrqoXP...3c6Vq9uJq8HSOmuUWP3euF+6E3kySWcGiNDr1c9puY0CM5f.GDJFD...t.uCZL0btpe+29yyO3rbkbeU+CFcHXs6bU+pM8VacXeQwf...vSVcmiNDva6BkA9Os5GN3rrabqsZsguwQGDVqNe0WKEBxBihAA..X60IqtmQGBn2oLv+6pdoAmk8hGt5KL5PvZkUFlEMECB..v1GkYvTvYqdp29ybpLvxwEYafiJBaETLH...aOdfpegpqZzAgsVWnLvu5nCxAfUueYamVMgfJDjsBJFD..fku6q5ua0UO5fvVoyU8Oo5+9lGuYfWJGuUSInh0Wl1o5Wu5WdzAA1jTLH...KWmn5WIEBxl24q9cZ9b.QtRLkfKW6T8a07dJVg8MECB..vxyQa06H30O5fvVkyW88Z0zA97CNKGVNVqlRPkquL8HUewQGBXjTLH...KG2Z0CVcKiNHr0XmVcMg+mT8rCNKG1dhVs9vr77TUe5QGBXJPwf...rLXUGYS5zU+iqd7QGj0.Wb3kqSWc6iNDvThhAA..Xd6gq9BiNDrU3UZ0jA9PiNHqQ95okoWoUugfKkUbGNznXP...lmdfpegbgTY85rsZp.+FiNHa.ub0MM5PvgpyV80pN0nCBLUoXP...lWt6VcogslirtbgiHxIqdoAmkMgi2pUGVI6KGmu5a1xd5VgCEJFD..f4giT8aVciiNHrX8LsZ5.+tiNHaPmr5dFcH3PyNU+5U+xiNHvbghAA..X56oqtiQGBVjNSqJCbabxpd0Tz9RxiU8YGcHf4FECB..vzkCg.qCmuUkA9UGcPFj6tUeskUGdYvkFFN.9WazA....9.t2p2LkBxgqmp5STcMs8VJ3Ia0jkoTv4uyV8oRofvAhIFD..foiiU8Or55GcPXw3rU+Vsctpvuet5vKC6zpCKx1vkxFV6TLH...SCOW0sM5PvhvNU+NU+1U+vwFkIgizp+YgK487m2QP3PlhAA..Xr7NBxgkWrUEm73iNHSHmn5QGcH3.6Eq9XiNDvRj2XP...FiST8FoTPNXNe02pU+d69XoTv2sSlRAm6tv6HnRAg0DSLH...aVGo52r5FGcPXV6zsZ8XO0fywTk2Sv4scp9Z4swDV6TLH...aNOY0cN5Pvr04a0DAtsdQg2MNRqdeEu5QGD12dppO8nCArsvpDC..v52CT8VoTP1edwp6p5ZRofWN2a0OHkBNWclpehTJHrQYhAA..X84XUemTTA6cmuUS91WbzAYl3IpN9nCA6K6T8Mq9FiNHv1HECB..v5wyUcaiNDL67hUe671AtW38Db9xZCCClUIF..fCWOXqVaXkBxt0NUOVuykEVof6NGoUW1akBN+b1r1vvjfhAA..3vwwqd8pu7nCByFms5KU8gp9rCNKyMdOAmm1o59qtgpmevYAHqRL...GFr1vrW7LsZcge1QGjYpSVcOiNDrmYsggIHECB..v92ClIDjcmcpd7bLQNndgpaYzgf8jyU84RQ3vjjhAA..Xu6XU+FUW6nCBSdmo52pUSHHGLuV00O5Pvdx2p5qN5P.booXP...1ad5p6XzgfIumo5WOuiZGFNR02q5pFcPXW6LsZcueoQGDfKOGeD...1ct2p2LkBxk1NUORq98Y8ISofGFNQqNxHJEbd3BGWjObJEDlELwf...b4cyUOV0MN5fvj0Ya05B+PiNHKLOb0WXzgfcsSWc6iND.6MJFD..fKMESvkyK1px.+tiNHKPVY+4iy2p2QvGezAAXuSwf...7Ac7pe0bbQ3h6TU20nCwB1qlIzctvWK.ybdiAA..3854pdhTJHuWmu5q2peOTJBY83lqd8TJ3bv4p9D4qEfYOECB..vJ2WqNtH21nCBSJmq5KUcMU+xCNKKYGuUuQcJje56QpttpmczAA3fypDC..fUWjOnWo52nUqJIqW2W0+fQGBthNa0Oa0ObzAA3viIFD..Xa1CV8VoTPdGmt5mn5ijRA2DdxTJ3bviTcCoTPXwwDCB..rM5HU+ipt9QGDlLbDE17dgpaYzgfKqyV82o54GcP.VOTLH..v1lSVcOiNDLIrS0iW8EGcP1BY88m9drpO6nCAv5khAA..1VbrV8lw43Fv4q9N4XhLJud95voryU8YxTBBaETLH..v1fmt5NFcHX3NWqJG9gFcP1RcjpuW0UM5fvkjoDD1x33i...rjc2UuYJEba24p97UWWJEbTNV0OHkBNUctVczcTJHrkwDCB..rT8bU21nCAC0YZUQfO9nCxVtST8niNDbI4v6.awLwf...KMWXJAUJ31qyzphN9voTvQ6ASofSUmq5SjRAgsZlXP..fkDSI31sWr5+5pmczAgp5IqtyQGBtndlpO4nCAv3YhAA..VBNQlRvsYmtUuOZerTJ3TwykRAmh1oUu2lJEDnxDCB..L+YJA2d8LUeype3nCBuGub0MM5PvGvqT8QFcH.lVLwf...yUlRvsWOUq98x7ISofSMuVJEbJ5akRAAtHLwf...yQlRvsSOV0mczgfKpat56Wc0iNH7dbtpOS0yO3b.LQYhAA..lSNd0ajRA21bpV86cQofSSGsUuyiJEbZ4opttTJHvkgIFD..XtvENc6yoptqQGBtrNd0SL5Pv6wNUewpGezAAX5Swf...ScGs56TcsiNHrwnPv4gST8niND7d3.i.rmXUhA..lxNYqd2xTJ31gKrxvJEb56dSofSMe8TJHvdjIFD..XJ5HU+ipt9QGD1HdppO8nCA6ZOP0uznCA+qbtp+14BcCrOnXP..folGr5KO5PvFwyT8IGcHXO4gq9BiND7uhuFB3.wpDC..Lk7poTvsAmt5ilBMlaNYJEbJ49yWCAb.oXP..fof6s5sptwQGDVqdwpehpau5kFbVXu4IptmQGBpVs5v+0q9FiNH.yeVkX..fQ64ptsQGBVqdkpuV0yN5fv9xSWcGiNDTsZZau8QGBfkCSLH..vnbzp2HkBtjc1pOUqtTpJEbd54RofSE2eJED3PlIFD..XDb.CV1NWqlPvGezAgCjWn5VFcHnyW82JWcXf0.ECB..ro8ZUW+nCAqEmu5aV8PiNHbf8p4M+bJvpCCrVYUhA..1TtvAFQofKO6T80qtlTJ3RfRAmFr5v.qclXP..fMAGXjkqGq5yN5PvgFSz63Y0gA1XTLH..v5zQq9sqt5AmCN78TUe5QGBNT85UW6nCwVNqNLvFkUIF..Xc4gq99oTvklSW8QSofKMuQJEbz9VoTPfMLSLH..v5f2nrkmyV8Eqd1QGDNz8Fo.+QZmpe1pu6nCBv1GSLH..vgoiW8loTvkjyW84qtgTJ3RjRAGqyV8gRof.ChhAA..Nrbxpmn5pFcP3Pw69RC+sGbVX8Pofi0oaUg6.LLVkX..fCCtjoKKmp5tFcHXsRofi0Wu5Wdzg..SLH..vAwIZ0pCqTvkgS2peOBJEbYSofiyNUepTJHvDgIFD..X+5IqtyQGBNT3vhrc3lykBejNaVcXfIFECB..reX0gWFNe0WKugfaKdy7FfNJOS0mbzg.f2OqRL..vdwIpdqTJ3RvijCKx1j2HkBNJ2eJEDXhxDCB..ra8DUGezgfCrSWc6iNDrQ4MEbL1o5mNqnOvDlhAA..1Mr5vyedGA2NoTvwv6IHvrfUIF..3x434pCO2sS0WpUkTnTvsKudJEbDNcJEDXlPwf...WJmrUqOr2kr4qGq5CU8PiNHrw85UW6nCwVnGKqpOvLhUIF..3h4kqtoQGB12dkpOe0KM5fvPnTvw39q9FiND.rWXhAA..d2NVqVcXkBNOc9p6o5ijRA2VoTvMucp9ToTPfYnezQG...fIiGn5WZzgf8sGoUGWD1d8ZoTvMsyUcciND.reoXP..fpdtpaazgf8kSm2zLpWMGInMsWoUSmK.yVVkX..fWOkBNGc9p6JkBxp2DzabzgXKySkRAAV.TLH..r85ta06InUOb94aUcMUmZzAgg6ExaB5l1Wu5SO5P.vgAqRL..rc5js5HUv7xKV8wFcHXx34ptkQGhsL2UJjGXAQwf..v1mWHkIL2b9pegTHAuimLOA.aRmu5Sjq8MvBihAA.fsG2Z0uW0UO5fvdhqMLueOQ0cN5PrE4rU2vnCA.qCdiAA.fsC2a0eTJEbN4Up9noTPdud3piO5PrE4URof.KXJFD..V9dhpesQGB101o5K0pKdp0Vj2sGn5KL5PrE4YxkGFXg6GYzA...Xs5kyEKcN4ox0NkKtST8niNDaQrB+.aELwf..vxzsV85oTv4hy1pCafRA4h43oTvMoudJED...fYp6t5MqdKelEed3K9OMBU0Qa7+6naSeNwt6mV.XYvpDC..KKOX0WdzgfckWIueYb4cqU+AUW0nCxVfcp9Yq9tiNH.rIoXP..X434ptsQGBth1o5qU8PiNHL48F4RhuIb9VsJ+N1O.acTLH..rL7ZUW+nCAWQmt51GcHXVPofaFms5FFcH.XTb7Q..f4si1p2SPkBNsc9p6JkBxtyqkRA2DdkTJHvVNECB..yW2W02Ou+XScmp5Zd6+Jbk7xon+MgSm23S.5GczA...Xe4jU2ynCAWVmq5yU8riNHLa7zU2znCwVfmp5SO5P.vTfhAA.f4mWn5VFcH3x5wp9riNDLqbxp6XzgXKfu1Df2EECB..yKNxHSams5uS0yO5fvrxCjI.dS3aU8UGcH.XJwaLH..LObjbjQl5djVcHCTJH6Emn5WZzgXKv8mRAA...XF5DUukOS1OuZ0sdI+YO3R6XM9+82sgOmX29SH....vTxC23+MU6yk9yCdo+oN3x5Va0T.O5+c3k9m6d29SH.rM5GYzA...3R5oywHXp5Ls5Mg6kFcPX15Mpt5QGhEtOU02czg.foLGeD..XZ5UqtwQGBtndjpu3nCAyZuVJEbcZmpex7deBvUjhAA.fomWu5ZGcH3C3r4hCyA2KjiHz5zNUe7pe3nCB.yAtJw..vzwwZ0aNlRAmdbwg4vvSVcKiNDKXmu51Rof.rqYhAA.fogST8niND7AbtpOSJDjCtGr5NGcHVvNW00M5P.vbiIFD..FuGLkBNE8XspnAkBxA0Ip9xiNDKXmMkBBv9hqRL..LVOYlhnolyU84pd1QGDVDNZ02ezgXA6rsZM+Af8AECB..iyKj2arolmp5SO5PvhxaVcUiNDKTJEDfCHqRL..LFuVJEbJYmp6IkBxgq2HkBttblTJH.GXN9H..vl0M2p0J7pGcP3ekWr5iM5PvhyqluNec4LUe3QGB.VBLwf..vlywpNcJKXJ49Sofb36IqtwQGhEJkBBvgHSLH..rYb2s5J2xzvYZ0pC+RiNHr37f4fBstnTP.NjYhAA.f0u6KkBNk7HspbAkBxgsiW8kGcHVnTJH.qAtJw..v50CW8EFcHnpNe0mo5YGbNXY5Vq9iFcHVnTJH.qIJFD..VedxrRgSEOS0mbzgfEs2HuenqCJEDf0HqRL..rd7BoTvohuTJEj0KWf30CkBBvZliOB..b36UyEIcJPoBrI7z4q2WG70u.rAXhAA.fCWuVJIXJ3wRoBr98fU2wnCwBzYyW+BvFghAA.fCG2bqdiwt9QGjsb6TcWUe1QGDV7bAhWONa0ML5P.v1BGeD..3f6HUeupqZzAYK2qT8QFcHXqwalul+vlRAAXCyDCB..GLGq5GjBBFsuUJEjMmWOeM+gsykRAAXiywGA..1+t6V8V1w3b9pelpmezAgsFuP00N5Prvb9pqazg.fsQJFD..1et2pesQGhsbmt51GcHXqxIqtkQGhElcptlQGB.1VYUhA.f8t6KkBNZe8TJHaVmn5dFcHVX1o5iO5P.v1LGeD..Xu4gq9BiNDawr5vLBGoUuknb35ST8riND.rMSwf..vt2STc7QGhsXVcXFk2n5pGcHVX9TUe2QGB.11YUhA.fcmmNkBNRVcXFkWNkBdX6tRof.LI33i...WYOW0sM5Prk57UelrtgLFmr5lFcHVX97UmZzg..VQwf..vk2KmhAFkWr5iM5PvVKGajCe2e02dzg..dGVkX..3R6USofix2JkBx3bqUO5nCwBy2p5aL5P..uWN9H..vE2qVciiNDag1o5ykUMjwxwF4v0iU8YGcH.fOHqRL..7A8ZUW+nCwVnyVcCiNDr06ERofGlNUJEDfIKqRL..7doTvw3oRofLdOb0sL5Prf7Ls5BDC.STVkX..3c75UW6nCwVnuT0CM5PvVuiW8DiNDKHNdP.LCnXP..XEkBt4c9p+VU+vQGDn5MqtpQGhEBOK..LS3MFD..bnAFASSDSIuZJE7vx4Sof.La3MFD.fscJEby6QRofLc7j4BjeXYmpqYzg..18TLH..aqt4Vs5fJEby5dp9hiNDva6dqtyQGhEjexQG..XuwaLH..ait4pSmUGbS5bUeppWZzAAda2b0e7nCwBxcUcpQGB.XuwaLH..aaTJ3lm2SPlh9cGc.VPt+TJH.yRVkX..1lnTvMuGKkBxzySmqP9gkGo5aL5P.....WNW3ME7s7Yi84t2U+LCrYceM9u1Xo74o2i+yd.XhwaLH..aCLofaVmq5uc0ObzAAdeNR0OXzgXgvSD..K.JFD.fkNkBtY8JUejQGB3R3MxkH+vvYqtgQGB.3fyaLH..KcJEby4oRofLc8boTvCCmOkBBvhghAA.XI6MRofaJ2e0mdzg.tDtupaazgXAXmpqYzg..N7XUhA.XoxJCtYrS0Oa02czAAtDt0p+nQGhEhOQ0yN5P..GdTLH..KQJEby3bUW2nCAbE75UW6nCwBv8T83iND.vgKqRL..KMJEby3URofL88joTvCCeqTJH.KRJFD.fkjWOkBtI3Hivbv8VcmiNDK.OS0Wczg..VOrJwv910VE...H.jDQAQE.rTXcA2Lt+puwnCAbEbyU+wiNDK.mo5CO5P..qOJFD.fkfWq55GcHV3bjQXNw2S3fyaHJ.aA9QGc...fCnWME.rtc9VcMReoQGDXW3Ix2S3fZmTJH.aETLH..yYuZ0MN5PrvYUBYN43u8GNX9oGc..fMCqRL..yUub0MM5Prv8hUerQGBXO3MqtpQGhYtuT0CM5P..aFtJw..LG8BoTv0sSkRAYd4ERofGTmJkBBvVEECB.vbyKTcKiNDKb2e0cM5P.6AOP99BGTuR95d.15XUhA.XN4oqtiQGhEt6pUSMDLWbjpevnCwLmKPL.aob7Q..Xt3ISofqSmu5mo54GcPf8ne6QGfYtcp9TiND.vXnXP..lCd3p6bzgXA6rU2vnCArO7jUW6nCwL2mq5kFcH.fwvaLH..ScOP0WXzgXA6URofLOc24OvfCpudd5..Xql2XP..lxt2pesQGhErmo5SN5P.6SuYtBwGD95e.vDCB.vj0cmRAWmNUJEf4qWHkBdPbl70+.PJFD.fooiU8XiNDKXeqVc8gg4n6q5VFcHlw1o5CO5P..SCVkX..lZNR0OXzgXA6yW8sGcHfCf2ZzAXl6tx6JH.71bUhA.XJ4lq9diNDKT6T8yV8cGcPfCfWczAXl6QRof.v6hIFD.foj2n5pGcHVf1o5mr54GcPfCfGr5KO5PLic5paezg..lVTLH..SEud00N5Pr.ctpqazg.Nf7DCbv36C..WTN9H..LE7ZoTv0gyjx.XY32dzAXl6SM5...SSJFD.fQ6Ept9QGhEnWIWdTVFdh7GbvAwmu5kFcH.foIECB.vH8zU2xnCwBzKV8QFcHfCAGq53iNDyXOVtB4.vkg2XP..FkSVcOiNDKPOU0mdzg.Nj3fDs+clL0v.vUfIFD.fQ39RofqCmJkBxxwSlRA2uNeJED.1ETLH..aZ2c0+fQGhEnGo5tFcHfCIGq5NGcHlw9LiN.....v62QqdKeNz+be6keR.lAdiF+WWMW+36G..6Z+niN...r03Vq9mN5Pr.84ywEfkkmNqP790yT8MFcH.f4CGeD..1TbDAN7cWs5cEDVJNd0SL5PLSc1paXzg..lWTLH..aBuV00O5Prv7Ipd1QGB3Pl+.D1e1o5CM5P..yON9H..rt8xoTvCS6jRAYYxJDu+8EGc..f4Iuwf..rN8zU2znCwBxNUe7pe3nCBbH6tqtiQGhYpSU83iND.v7jUIF.f0kGr5KO5PrfrS0sU8RiNHvZvaVcUiNDyPmo5CO5P..yWlXP..VGNQJE7vz4qtlQGBXM4ISof6G6jRAAfCHuwf..bX6nUO5nCwBx4Rofrbcrp6bzgXlx6JH.bfYUhA.3vlUB7vy4pttQGBXMxUHd+4TU20nCA.L+YhAA.3vzajRAOrb1TJHKaOQJEb+3LoTP.3PhIFD.fCKuZ0MN5PrPb1paXzg.ViNZ02ezgXFZmpOznCA.rbXhAA.3vvykRAOrblTJHKeemQGfYpuxnC..rrnXP..NnNY0sM5PrPblbkQY46jUW6nCwLzyT8sGcH.fkEqRL..GD2a0u1nCwBgRAYavsV8GM5PLC4PDA.qEJFD.f8KuQXGdTJHaKdspqezgXF5mn54GcH.fkGqRL..6W+SGc.VHTJHaKdfTJ39wWOkBB.qIlXP..1OL0OGNTJHaSdqQGfYnWr5iM5P..KWlXP..1qdgTJ3gAkBx1jWXzAXFZmTJH.rloXP..1KNY0sL5Pr.nTP1lbh78M1O9YGc..fkOqRL..6Vmn5QGcHV.TJHaadypqZzgXl4TU20nCA.r7oXP..1MNR0OXzgXA3rU2vnCArA8jU24nCwLiuOA.rwnXP..1MLwOGbmq55FcHfMniV88GcHlg9IxUHF.1P7FCB.vUxqkRAOnNeJEjsOemQGfYnudJED.1fTLH..WNOWt.wGTmu5ZFcHfMrGr5ZGcHlYdkpe4QGB.X6hUIF.fKkGt5KL5PLyoTP1V8ViN.yL6T8gFcH.fsOlXP..tXt6TJ3A0NoTP1N8xiN.yPekQG..X6jhAA.386lqdrQGhYtcp93iNDv.bhpaZzgXl4zUe6QGB.X6jUIF.f2uWOuMXGD6T8SlCH.amdipqdzgXFwyM..LTlXP..d2dgTJ3A0OcJEjsSmLkBtW84Fc.....fZ0wF4s74.84X64+oNrLbqM9u9at84I2W+SZ.fCQVkX..pUGaDuqfGL2S0iO5P.CxqVciiNDyHmq55FcH..rJw..3Xibv8kRofr85dSof6UVgX.XRvDCB.fiMxAyWu5Wdzg.Fn2r5pFcHlQdrpO6nCA.PYhAA.114Xibv7XoTP1t8DoTv8hykRAAfIDECB.r85gqtkQGhYrSkeC9rc6HUGezgXl4yL5......GuweQNmyed589+HGVbd0F+WKNm97v6u+wL.v5i2XP.fsSdSv1+dwpO1nCALX2a0u1nCwLhqPL.LIoXP.fsOuZtfn6Wmo5CO5P.S.9CWXu4mn54GcH..d+7FCB.rc4ISof6WmMkBBUcxTJ3dwikRAAfIJSLH.v1Cq92924qtlQGBXh3sFc.lQrBw.vjlIFD.X6vQRof6W6jRAgK3kGc.lY9biN...WNJFD.X6vuynCvL1O4nC.LQb2U2znCwLxopd1QGB.fKGqRL.vx2KTcKiNDyT2S0iO5P.SDud00N5PLS34G..lELwf..KaOPJEb+59SofvE7foTv8BqPL.LKXhAA.VtNZ02ezgXl5Qp9hiNDvDhCNxt2SU8oGcH..1MTLH.vx0aVcUiNDyP9M0CuWOW0sM5PLSrS0GZzg..X2xpDC.rL8xoTv8iWLkBBuaGMkBtWXRiAfYESLH.vxyCW8EFcHlgNS0Gdzg.lXdspqezgXl3zU29nCA.vdghAA.VVNV0u6nCwLjKHJ7AchpGczgXlvJDC.yRJFD.XY4Mpt5QGhYF+F5gKNe+jcu6u5aL5P..rW4MFD.X43kyuI98ie5QG.XB5Ay2OY25LoTP.XlRwf..KCOX0MM5PLC8kpd1QGBXB5KO5.Li74Fc...1uTLH.v72QxuI98iGo5gFcHfInmdzAXF4Qp9giND..6WdiAA.l+7Nfs28LUexQGBXB5HU+fQGhYhyUcciND..GDlXP.f4smKkBtWclTJHbo7aN5.Li7yO5...bPoXP.f4q6q51FcHlYNW0Gdzg.lnt6pabzgXl3zUe2QGB.fCJqRL.v7j08auampOznCALg85UW6nCwLfuWB.rXXhAA.lm9cFc.lg9oGc.fIr6KkBta8MGc...NrXhAA.ledtrBw6Ue9pu8nCALg8lUW0nCwLvYqtgQGB.fCKlXP.f4kSjRA2qdjTJHb4bxTJ3t0emQG..fCSlXP.f4ES0ydyoqt8QGBXh6sFc.lINU0cM5P..bXxDCB.Le7poTv8hylRAgqjmbzAXlXmTJH.r.oXP.f4gGt5FGcHlQ1IuCXvUxsVcmiNDyDekQG..f0AqRL.vz2Qq99iNDyLehpmczg.l3dgpaYzgXF3Up9HiND..qClXP.foue6QGfYl6OkBBWIGKkBta8yO5...rtnXP.fosmt5pGcHlQNU02Xzg.lA9GN5.LS7XU+vQGB.f0EqRL.vz0IpdzQGhYDq6Gr636sr6rS0GZzg..XcRwf..SWuYtBw6Vmq55FcHfYhWu5ZGcHlA9RUOznCA.v5jUIF.XZ5kSof6V6T8oFcHfYh6KkBtablTJH.rEPwf..SOOX0MM5PLi74pdoQGBXl3WbzAXl3u6nC..vlfhAA.lVNR0WdzgXF4QZ0AGA3J6AyjHuabppmezg..XSvaLH.vzxqUc8iNDyDuX0Gazg.lQ7tkdk4fi..aULwf..SGObJEb257oTPXu3gSof6FeyQG..fMISLH.vzvQq99iNDyHez7tBB6Eu0nCvLvYqtgQGB.fMISLH.vzv2YzAXF4ymRAg8hmXzAXl3qL5...roYhAA.Fumn53iNDyDOV0mczg.lYLsfWYdyRAfsRJFD.XrNV0u6nCwLwqT8QFcHfYlmr5NGcHlArIU.vVIECB.LVuQ0UO5PLCb9pqYzg.lYt4p+3QGhY.ShL.r0xexX..iySmRA2s9YFc.fYnuwnCvLvNoTP.XKlhAA.Fi6t5NFcHlI9RUO+nCALybqU21nCwLvWazA..XjrJw..iwaVcUiNDy.OU0mdzg.lgdtTL3UxYqtgQGB.fQxDCB.r48boTvciylRAg8iijRA2M9JiN...ilIFD.Xy5DUO5nCwLg+.Lg8mWn5VFcHl3dwpO1nCA.vnoXP.fMKqP7tymu5aO5P.yPGo5GL5PLC3O3A.f7KHB.rI8zoTvcimJkBB6WOvnCvLvSM5...LUXhAA.1Lt6pGazgXF3LUe3QGBXlxzBdksS0GZzg..XpvDCB.rY7fiN.y.6jRAgCBeelqreqQG..foDECB.r98zUW8nCwLvmazA.lwNZ0MM5PLwc9pu5nCA.vThhAA.VuNV0cL5PLC7XUmZzg.lw9UFc.lA9ZiN...SMdiAA.VudiLsfWIdWAgCliV88GcHl3Na0ML5P..L0XhAA.VedxTJ3Uh2UP3fyaK3U1WYzA..XJRwf..qGGs5NGcHlA7tBBGLGs5FGcHl3dwpu6nCA.vTjUIF.X8vJDekcpp6Zzg.l4d4bzQtR9qW8CGcH..lhLwf..G9NYJE7J4roTP3fxkH9J6zoTP.fKISLH.vgqiT8CFcHlA9nUuznCALy8BU2xnCwDmAg..3xvuPI.vgq+QiN.y.eoTJHbPcqoTvqjSM5...L0YhAA.N77.U+RiNDSbmt51GcHfEfmq51FcHl3LDD..WAJFD.3vyaM5.Lwctpqazg.V.t4p+3QGhItGo5KN5P..L04OEM.fCGuvnCvLvmYzA.VHt+QGfItcRof..6JJFD.3f6D4s95J4aU87iNDvBwcL5.Lw8qO5...LWXUhA.N3dypqZzgXB6Up9HiNDvBwSmhAubNe00L5P..LWXhAA.NXdxTJ3kyNoTP3vjRAu79liN...yIJFD.X+6nU24nCwDm24K3vySN5.LwctpGZzg..XNwpDC.r+85UW6nCwD1yT8IGcHfEDW97KuOe02dzg..XNwDCB.r+7foTvKmymRAgCSO7nCvD24Rof..6YlXP.f8GStyk2mn5YGcHfEDG4nKu6o5wGcH..laLwf..6cu7nCvD2ikRAgCSOPJE7x4roTP.f8ESLH.vdyIpdzQGhIryVcCiNDvBioE7x6tpN0nCA.vbjhAA.1adipqdzgXB6iV8RiNDvBx8V8qM5PLgclpO7nCA.vbkUIF.X26joTvKmudJEDNr8KN5.Lw80Fc...lyLwf..6N2Z0eznCwD1qT8QFcHfEFOcAWd99N..GPJFD.X24kqtoQGhIpcp9PiNDvBzqUc8iNDSXt94..GPVkX.fqrSjRAub9JiN.vBzwRofWNuRJED.3.yDCB.bk4hfdoc5paezg.VfLkxWdlVP.fCAlXP.fKumHkBdorSJEDVGNRJE7xwzBB.bHQwf..WZ2Z0wGcHlvrBwv5we+QGfINWhX.fCIVkX.fKsWs5FGcHlnrBwv5yaM5.Lg4RDC.bHxDCB.bwcuoTvKEqPLr9bxQGfIte0QG..fkDSLH.vEmCNxk18T83iNDvBkoE7R6LUe3QGB.fkDSLH.vGjCNxk1oSofv5xCL5.Lw4sED.3PlIFD.385Vq9iFcHln1o5CM5P.KXuQ0UO5PLQYZAA.VCLwf..uW+FiN.SXewQG.XA6DoTvKmu4nC..vRjIFD.3cb7VsFw7A8LUexQGBXA60pt9QGhIpyVcCiND..KQJFD.3c75UW6nCwDjUHFVuNZ02ezgXByAOB.XMwpDC.rxCjRAuT9JiN.vB2uvnCvD14Rof..qMlXP.fUdybIhuXNc0sO5P.KX2b0e7nCwD1mu5aO5P..rTYhAA.pmLkBdwrSJEDV29uXzAXB67oTP.f0JECB.a6NR0cN5PLQ4JfBqeGezAXBy2CB.XMypDC.a6d0pabzgXB5LUe3QGBXg6Ap9kFcHlnNe00L5P..rzYhAAfsY2cJE7R4dFc.fs.etQGfIruynC..v1.SLH.rM6Mpt5QGhInGo5KN5P.Kb2c0iM5PLQsS0GZzg..XafIFD.1V8foTvKlylRAgMg6czAXB62ZzA..XagIFD.1V8ViN.STehpmczg.V3NR0OXzgXByvK..rg3WzE.1F8ziN.STmJkBBaBmXzAXBy5UC.rAYhAAfsM2Z0eznCwDjK.Jr4XhkuzL3B..aP9EdAfsM+FiN.ST+BiN.vVhGbzAXB6YFc...11XhAAfsIGq52czgXB5zU29nCArkv0P+R6ud0Obzg..XahhAAfsIuV00O5PLAYCBfMiST8niNDSTuR0GYzg..Xaiei..v1hSjRAuX95iN.vVjetQGfIre0QG..fsQlXP.Xag026C5rU2vnCArkvgO5Ry2KB.XPLwf.v1fGLkBdw7EGc.fsH+7iN.SX+2L5...rsxDCB.aCdypqZzgXh4op9ziNDvVj2ZzAXh57UWynCA.v1JSLH.rz8DoTv2ucRofvlz8M5.Lg8aN5...rMyDCB.KclRmOnuT0CM5P.aQd8pqczgXBZmpOznCA.v1LSLH.rj8biN.SPmIkBBaRGKkBdo76L5...rsyDCB.KUGs56O5PLA8ST87iNDvVjmq51FcHlnLjB..ClewX.Xo5AGc.lfdrTJHrooTvKtmYzA...ECB.KSGu5FGcHlXNe0mczg.1x3OfhKsu8nC...VkX.XY50pt9QGhIlOe9MhCaZuQ0UO5PLAclpO7nCA..lXP.X44DoTv2uyjRAgMs6NkBdo3.HA.LQXhAAfkFSnyGjCNBr48xU2znCwDz4qtlQGB..VwDCB.KI2aJE786oRofvHnTvKtuynC...uCSLH.rjXZAeu1o5CM5P.agNY08L5PLQYvD..lP7KLC.KEOPJE786aN5..ao9YFc.lndrQG...duLwf.vRwaVcUiNDSHmq55FcHfsP2cJ.6RwPI..Lw3WbF.VBd3TJ362O+nC.rk5mazAXh5EGc...3CxDCB.KAu0nCvDyoqt8QGBXKkuezE2cUcpQGB..duLwf.vb2IGc.lfTJHLFO7nCvD04Sof..SRJFD.l6b4OeudjQG.XK1cO5.LQ8cFc...3hypDC.yYO4++r28aL24c8cd9uQytcnxa3exBnHqTSDSAlnr7OQ1PRQPcZTgPYGvAjfwBAYPnoQfh1IB1YWkBiD+QaQKS5tH1lYDBEFwjElIMtfXvzJlXnZqCPF4BLQtPqPFSjUAptU4e6sfYePz9fy35X6aaeeNmqqymeWW+d85In9Dm2OoW2mymy00uqppaMcDMjsppdZoi.5Tdoibw4lQ..nQ4ORC.SYFE7b8uHc.PGycu7N6ARG...WbtiAAfoJ2sfmqSVU8rSGAzw7RGYm8xppNV5H..Xm4NFD.lpLJ345+4zA.cLuzQ1YGuLJH.PSyvf.vTz8mNfFyCUUc3zQ.cr2X5.ZT+AoC..fKMOJw.vTjGYuykenOHmCVU8GlNhFjWFR..S.9hD.vTi6Vvy08kN.nycaoCnQcuoC..fKO2wf.vTi6VvyZ6ppqLcDPmy0j1YtAD..l.7GrAfoD2sfmqe+zA.ct6Jc.MpGLc...r63NFD.lRbm4bVmtp5pRGAz49NUUWc5HZPu9xKDI.fIA2wf.vTg6Vvy0+aoC.5bGnLJ3N4zkQAA.lLLLH.LUbqoCngbxpp6IcDPmyKcjc1+5zA..vtmGkX.XJ3yUU8pSGQC42tp5Hoi.5b+rpp8jNhFja7..fID+ga.XJvnfm0CVFEDR61JiBtS97oC..fkigAAfV2mKc.MFuETg7daoCnQ4HN..XhwiRL.z57lH9rdfpp2P5H.bcocvIqpd1oi..fki6XP.nk8ISGPiwnfPduuzAzn9CRG...KO2wf.PKyckyY8wqpd6oi.ndzpp8kNhFja3..fIH+Ab.nU8QRGPCY6xnfPK3.kQA2IOP5...X0XXP.nU8NSGPCwinGzFtszAznt2zA..vpwiRL.zh9HkgAOisqptxzQ.TUU0OqpZOoinwb5ppqJcD..rZbGCB.sH2UNm0ue5..ppp5PkQA2I+qSG...qN2wf.Pq48UU86lNhFwVUUOszQ.TUU0mqp5UmNhFjaz..fIL+gb.n076jNfFxuW5..96XTvKzmOc...rdLLH.zRtypp8lNhFwoqpt6zQ.TUs3ZSbg7RGA.XhyiRL.zRdzpp8kNhFwatp59RGAPUUUORU00jNhFii5..fY.2wf.Pq31KiBdFmrLJHzRLJ3ExcKH.vLf6XP.nU3tE7rd8UUGNcD.UUU8gppd2oinA4FL..XFvePG.ZAGpLJ3YbhxnfPK4MkNfFzCkN...FFFFD.ZA+ujNfFx+hzA.724.kezhcxGOc...LLLLH.j1sTN+tNC2sfPawcK3EZ6x4KH.vrggAAfz9mkNfFh6VPns7FSGPC5yjN...FNFFD.R6lRGPi33k6VPnkbvpp8jNhFj6VP.fYDCCB.I8ISGPCwcKHzV7XDegNcU0QSGA..CmqHc..PW6wRGPi33UUujzQ.bN9Yk6Xvy2Gnp58lNB..FNtiAAfT9HoCng3tEDZKGpLJ3Nwnf..yLFFD.R41RGPi33UUGIcD.mCuzQtPOT5...X3YXP.Hg6rb23bFtaAg1yqNc.MnOd5...X34LFD.R3Qqp1W5HZ.NaAg1ysUFA67scU0UlNB..FdtiAAfMsCUFE7Lb2BBsm+woCnA8GmN...FGtiAAfMsGop5ZRGQC3DUUWa5H.t.daoeg9sKmEp..yRtiAAfMoaoLJ3Y3tEDZO2Y5.ZPaUFED.X1xvf.vlz+rzAzHNYU0gSGAvE3foCnAcuoC..fwiGkX.XSxin2Bu4pp6KcD.W.Wi5B4FI..XFyenG.1T9XoCnQbxxnfPK5tRGPC5joC..fwkgAAfMk2X5.ZD+KSG.vN50lNfFjGiX.fYNOJw.vlvcUU89SGQCXqppmV5H.1QdLhuPtIB..l47G6AfMg+ooCnQ76kN.fcj2FwWnGJc...L9LLH.L1NXU09RGQCXqpp6NcD.6n2T5.ZPNKTA.5.dThAfw1eVU0MjNhFv6op5ClNBfcjGi3Kjaf..fNf+fO.L1LJXUaWFEDZUdLhuPe9zA..vlggAAfwzGKc.Mhe+zA.bQcvzAzf9zoC..fMCOJw.vXxim2h6VvqLcD.WTtN04x0r..5HtiAAfwxckNfFw8lN.fKJOFwWnOS5...XywcLH.LV9app1a5HZ.9Q3f1kWNRWne6ppijNB..1L7kU.fwvgJiBVkCven0YTvy0VkQAA.5JFFD.FC+OkNfFwuW5..tn7XDeg9roC..fMKCCB.Csqqp5EmNhFvCUUcrzQ.bQ4sQ7E5SkN...1rLLH.Lzd6oCnQb2oC.3RxiQ74ZqppilNB..1rLLH.LzdioCnAbxppCmNBfKpaKc.MHOFw..cHCCB.CoaupZOoinA7uLc..WRdLhuPehzA..vl2UjN..XV46TUc0oiHrsppdZoi.3R5wRGPi4zUUWU5H..XyycLH.LTNPYTvpp5+yzA.bI4tE7B8YRG...YXXP.Xnb6oCnArcU0GLcD.WRuozAzf92lN...xviRL.LT7n4U0Gsp5NRGAvkzOqbVn934wHF.ni4NFD.FB2U5.ZDFEDZaGnLJ346SkN...xwvf.vP3eZ5.Z.OP5..tr7XDegbsK.fNlgAAf00sTUsuzQz.tmzA.bY8OJc.MlSWU8voi..fbLLH.rt7RGopSTUczzQ.bIccUU6McDMFuMhA.5bFFD.VWu5zAz.t6zA.bYcqoCnA8uKc...jk2Jw.v53CUU8tSGQXaUU8zRGAvk02op5pSGQCw0t..vcLH.rVtszAz.t2zA.rqXTvy0mMc...jmgAAfU0gJmWWUU0+7zA.bYcWoCnA8oRG...4YXP.XU8VSGPC3ARG.vtxqJc.MlsJuvj..nLLH.r5tozAz.9+Hc..6J2P5.ZLdLhA.npxvf.vp48kNfFvwqpNV5H.trNT5.ZP+QoC..f1fgAAfUwaMc.Mf+fzA.rq7ZSGPiY6ppijNB..ZCFFD.VV2RU09RGQXaUdaDCSEuxzAzX9LoC..f1ggAAfk0skNfFv+pzA.rqbfpp8jNhFiGiX..96bEoC..lbdrzAz.7CqASCenpp2c5HZLt9E..+c7AC.fkwclNfFvCjN.fcsaMc.MlOe5...nsXXP.XY3wHtp6Ic..6ZWc5.ZLdoi..v4viRL.raccUUe0zQD1Iqpd1oi.XW4Nqp9voinw3lB..fygOb..ra81SGPC3OHc..6ZupzAzXdnzA..P6wcLH.ra8yp99s641UUWY5H.107hR5b8dpp9foi..f1h6XP.X231p9dTvpp5SmN.fcsCkNfFjQAA.3BXXP.X23skNfFvmHc..6Zu1zAzXNQ5...nMYXP.X23FRGPXmnp5Xoi.XW6kmNfFi2Fw..riLLH.b479RGPCvKcDX535pp1a5HZLOP5...nMYXP.3x4slNfv1tp5dRGAvt1MmNfFyoqpd3zQ..Paxvf.vkxApp1W5HByKcDXZw4K345OIc...ztLLH.bo7lRGPC3smN.fkxKNc.MluP5...ncYXP.3R4MlNfvdnzA.rTNT5.ZPGNc...ztLLH.bwbnpp8jNhvb1BBSKuhzAzXdvzA..Payvf.vESue2BtUU08kNBfkxuU5.ZLtaAA.3R5JRG..zrdrzAD1Gsp5NRGAvRo2ut04yMA...WR9vB.vN4NSGPCvnfvzxckNfFyIRG...sOCCB.6jaKc.g4KTCSOupzAzXNR5bfYn8C...B.IQTPTA..n8YXP.XmbMoCHr6Mc..KsaHc.Ml+jzA..P6yvf.v46CkNfFvcmN.fkxASGPiYqppilNB..ZeFFD.NeuozADl2DwvziGi3y0eZ5...XZvvf.vi2sTUsuzQD1mJc..KseizAzX9LoC..fogqHc..PS4SVUcnzQDzoqptpzQ.rzdrzAzX7i+C.vthOz..738ZSGPXehzA.rzt8zAzXNd5...X5vvf.vYbnpp8jNhvduoC.XocfzAzX7XDC.vtlgAAfyn2uaAevzA.rRd4oCnw7ESG...SGFFD.NiWY5.B6SjN.fk10UUs2zQzP1pp5gSGA..SGFFD.pppaq56Gi3spptuzQ.rzt4zAzX9SRG...SKFFD.ppp+woCHrOc5..VIupzAzXNZ5...XZ4JRG..zDdrzAD1Kqp5Xoi.Xo06W6574G8G.fkhO7..baoCHrSTFEDlh71H9bchzA..vzigAA.OFw.SQ+VoCnwbjzA..vziGkX.n2eT77ijASSORU00jNhFxuY4LFD.fkjuLD.8s6Lc.g8PoC.XkYTvyZ6xnf..rBLLH.8sClNfvtuzA.rRNT5.ZLe4zA..vzjgAAnucCoCHr6Ic..qjWQ5.ZLNeAA.XkXXP.5W89iQ7mOc..qreizAzX7ib..vJwvf.zudSoCHr6Mc..qrqNc.MjSlN...ltLLH.8qWb5.BZqppCmNBfUxskNfFyWJc...LcYXP.5S2U5.B6ylN.fU1ARGPi4KjN...ltthzA..Q7HUUWS5HB52rp5noi.Xk7nUU6KcDMD+P+..rxLLH.8oGKc.Acxppmc5H.VY870uNemnp5ZSGA..SW9EFAn+z6uMh++Nc..qLmufmqijN...l1LLH.8mClNfvduoC.Xk47E7b8mlN...l17nDCP+omeL77X2ASaNeAOW9Q9A.Xs3CS.Pe41SGPXe5zA.rVLJ3Y8PoC..foOCCBPeo2eLh+foC.Xk47E7b8ERG...SeFFDf9xMkNffNd5..VKNeAOWe4zA..vzmgAAnez62sMepzA.rV9sRGPCY6ppikNB..l9LLH.8id+wH9tSG.vZYuoCng7kSG...yCFFDf9wqNc.A4P5Gl158634y2WJc...LOXXP.5CGJc.gceoC.Xs37E7b4NfF.fAggAAnO7ZSGPX2S5..VKuzzAzP1Jc...LeXXP.5CuxzADjGiXX56pSGPC4OMc...LeXXP.l+NXU0dRGQPdLhgosd+nP378URG...yGFFDf4uWW5.ByiQLLs8JRGPiw4KH..CFCCBv72uU5.B5ASG.vZ65SGPCw4KH..CJCCBv71MVUs2zQDzgSG.vZ6ZRGPCw4KH..CJCCBv71+ioCHLOFwvz1sjNfFyQSG...yKFFDf4saMc.A4wHFl9d4oCnw3G6..fAkgAAXd6pSGPPdLhgouaLc.MDmuf..L3LLH.yW2Y5.BycVCL8cCoCng37ED.fAmgAAX95foCHnimN.f01ARGPiw4KH..CNCCBv7UOem17YRG.vZ6UjNfFi6BZ..FbFFDf4oCkNfv9foC.Xs8RSGPCw4KH..iBCCBv7zqMc.AchzA.LHt9zAzP9ZoC..f4ICCBv7zqLc.A8GkN.fAwdRGPC4KkN...lmLLH.yOGn56uP86Mc..qsaOc.MF2wf..LJLLH.yO87iQ7oSG.vfnme4Ic91tp5Xoi..f4ICCBv7ysjNff71HFlG7hG4r9loC..f4KCCBv7yUmNff92kN.fAQOecry2QSG...yWFFDf4k6Jc.AsU4wsClC54654cxWNc...LeYXP.lWdUoCHnOa5..FDWe5.ZLtiAA.XzXXP.lW54Cr++nzA.LHNP5.ZHmHc...LuYXP.lONX5.BZ6ppijNBfAwyOc.MjuT5...Xdyvf.LezyOFw+woC.XvrmzAzP9ZoC..f4MCCBv7wuU5.BxcKHLOb6oCnwbeoC..f4MCCBv7w9RGPP2a5..FD874j546joC..f4OCCBv7vclNffNd5..FLuzzAzP9JoC..f4OCCBv7POe9B9ERG.vf4pSGPC4gRG...yeWQ5..fAwikNfft9ppGNcD.qsCVU8GlNhFhe.e..Fc9.G.L8cKoCHnsJiBByEufzAzP1Nc...zGLLH.SeuxzADzmMc..CFmufm0WMc...zGLLH.Se87cLnyWPX935SGPCwKdD..1HbFCBvzWOe9B5G3BlO54qkc99sqpNR5H..X9yWnBfos6Lc.A8foC.XvbnzAzXLJH..aDFFDfoseizADjGiXX93+gzAzPNY5...neXXP.l1dEoCHn6Nc..CFu3QNKmuf..rwXXP.ltNPU0dRGQHmHc..Cpma5.ZHOT5...neXXP.ltdEoCHHm+Vv7Ru9ibrStmzA..P+vvf.LccfzADzeR5..FL2d5.ZHamN...5KFFDfoqaHc.grcU0QSGAvf4EkNfFxWMc...zWLLH.SS2R5.B5KmN.fA00mNfFxed5...nuXXP.lldkoCHHmufv7x0jNfFxWKc...zWthzA..qjGo52uLseTKX93.UU+GSGQCw02..XixG9.foodcTvSmN.fA0KHc.MjsRG...8GCCBvzygRGPPdaDCyKuzzAzP7XDC.vFmgAAX54UjNff9BoC.XP8BSGPC4qmN...5ONiAAX54Qqp1W5HBwOnELu7XoCng7aVUczzQ..PewWvBfomdcTvimN.fA0ARGPiwnf..rwYXP.lVt8zADjGiXXdwKdjy5joC..f9jgAAXZomuCa7hGAlWb9BdVekzA..Pexvf.Ls7xSGPHaWUcrzQ.LnbGCdVdwi..PDFFDfok8lNfP9iSG.vf6ZRGPC4ajN...5SFFDfoi6Lc.A4P4GlWttzAzXbMN..hvvf.Lc7pRGPP2S5..FTu3zAzPNQ5...neYXP.lNt9zADxoSG.vf6EkNfFxWMc...zuLLH.SG6Ic.g3sQLL+zq+PG6j+7zA..P+xvf.LMb6oCHnGJc..CNu3QNq+yoC..f90UjN..XW49qpt0zQDheDKX94wRGPCw03..HFePD.lFd4oCHjSlN.fA2skNfFhW7H..DkgAAXZXuoCHjuT5..Fb2P5.ZHeizA..Peyvf.z954yWvub5..FbOuzAzP95oC..f9lgAAn8cfzADz8kN.fA2yOc.MD2wf..DkW9H.z9dzpp8kNh.NQU00lNBfAmW7Hmkejd..hxGFAf1WONJXUNeAg4nClNfFhWtR..DmgAAns0yu8N+xoC.Xv47E7rb9BB.PbFFDf1VOe9Bd3zA.L3dAoCng37ED.f3LLH.ssWZ5.B4DoC.XT7BSGPCwcLH..wYXP.ZaWc5.Bw4KHLO0qWSambjzA...FFDf10gRGPP+woC.XvccoCngb5zA...UYXP.ZYuhzADj6jFX94EmNfFx2Lc...PUFFDfV10mNfPNd5..FEunzAzPb9BB.PSvvf.zttlzADxWIc..ihmW5.ZHdiDC.PSvvf.zltkzADzWNc..ihme5.ZHGNc...PUFFDfV0KLc.A4KLCyS6Ic.MhsSG...bFFFDf1zqHc.gbxzA.LJNX5.ZHe6zA...mggAAnM0qu3Qb9BBySNeAOKmuf..zLLLH.sod8Qt6noC.XT7BRGPC4akN...3LLLH.smaKc.AcuoC.XTzymapmO2wf..zLLLH.smaHc.grU5..FMWc5.ZHtynA.nYXXP.ZO+FoCHjuV5..Xjc5zA...OdFFDf1Sudm07kRG.vnvaj3y5alN...3wyvf.zVtkzADj6XPXdxKdjy5qmN...3wyvf.zVt9zADx1UUGKcD.ihmW5.ZH+koC...d7LLH.skWZ5.BwiWGLe4MR7YceoC...d7LLH.skd8NFzaoSX9pWO2TA.flmgAAnsrmzADxWMc..LxNQ5...fymgAAncb6oCHnijN.fQwgRGPC4amN...37YXP.ZG2P5.BwcQCLe8bRGPC4ajN...37YXP.ZGufzADhGiXX9xaj3y5akN...37YXP.ZGWS5.B4OOc..ilma5.ZHGNc...v46JRG..TUU0App9OlNhP7iTAyWOV5.ZDaWUckoi...Ne9xX.zF50Gi3sSG..a.mJc...vNwvf.zFdgoCHjuY5..FMdiDeVdwi..PSxvf.zFdooCHjikN.fQi2Hwm02Nc...vNwvf.zFt5zADxWKc..iFuQhOqud5...fchW9H.j2sTU8eHcDg3GnBludjpeeaqe9bsN..ZR9PJ.jWud9Bd5zA.Lp1e5.ZDdIKA.Pyxvf.j20mNfP7XDCya6Ic.MBmuf..zrLLH.487SGPHekzA.LZNP5.ZHdiDC.Pyxvf.j29RGPHtiAg4qWP5.ZHmJc...vEigAAHqaIc.AcrzA.LZ7FI9r9VoC...tXLLH.Y0qmufmHc..ip8mNfFxgSG...bwXXP.x5klNfP9poC.XT8bRG...vkmgAAHqd8EOxCkN.fQUud1od9b2QC.PSyvf.j0dSGPH2a5..FMWW5.ZHmJc...vkhgAAHmClN..FA+CRGPC4uHc...vkhgAAHmWP5.B43oC.XT47E7r91oC...tTLLH.47BSGPHeizA.LpddoCng3LFD.fllgAAHmd8EOxed5..FUO2zAzPd3zA...WJFFDfb502ZmdThg4s8mNfFwVoC...tbLLH.YbfzADj6fFXdaOoCnQ7WkN...3xwvf.jQu9hGw4sELu0y+nGmuuU5...fKGCCBPF85gyuW7Hv71uZ5.ZHFFD.flmgAAHid8NF7qmN.fQkW7Hm02Nc...vkigAAHiWb5.B4qkN.fQ0UmNfFxQRG...b4bEoC.fN0ikNfP7CRAyaORU00jNhFgq2A.PyyGXAfMuaKc.gbxzA.L51e5.ZDtdG..SBFFDfMud8L35qjN.fQ2dRGPivKdD..lDLLH.adunzADhW7Hv71MlNfFx2Mc...vtggAAXy64mNfP9FoC.XT8qkNfFh6XP..lDLLH.ad6Mc.gbzzA.LpdVoCng78RG...raXXP.1rtkzADxoSG.vn64kNfFxQRG...raXXP.1rdgoCHjuY5..Fc6Oc....KGCCBvl0KHc.g7WjN.fQWu9FW+7cxzA...6VFFDfMqd8NF7+b5..Fc6Ic.MBu3Q..Xxvvf.rYc0oCHj6Kc..ipaLc.Mjua5...fcKCCB..rt1e5.ZHtiAA.Xxvvf.r4bnzADxwSG.vna+oCng78RG...raYXP.1bdNoCHjuc5..Fc6Oc.MjijN...X2xvf.r477RGPHe8zA.L550quA..SZFFDfMmd8MRr6XPX96YjNfFwISG...rLthzA.PG4wRGPH9Qnf4ud85amuGrp5lSGA..ra4KqA.iosRG..aPmJc...vxvvf.rYzquQh+loC.XzcfzAzPNU5...fkggAAXy3+9zADx2Jc..ite0zAzPbMO..lTLLH.aF+CSGPH9Rxv72yJc.MjSmN...XYXXP.1LddoCHjimN.fQmgAOqGNc...vxvvf.rYb0oCHDeIYX9yvfKrc5...fkkgAAX7ccoCHjSlN.fMhqJc.MhSkN...XYYXP.FeWS5.Bw4KHzG1W5.ZDmJc...vxxvf.L9dtoCHj+hzA.vFjq4A.vjigAAX70quQh+1oC.XzcfzAzP9goC...VVFFDfwWu9FI9uJc..itmS5.ZH9wP..Xxwvf.L9502HwGKc..it8mNfFxQRG...rrLLH.LF1Jc..aDOyzA...r5LLH.iqClNfP7XDC8gmU5.ZD9wP..XRxvf.Lt50yWvuU5..1HtpzAzH9doC...VEFFDfw09SGPHFFD5C6Kc.MhSkN...XUXXP.FW85cLn2Nm.8jue5...fUggAAXb8qkNfP714Dl+twzAzPNU5...fUggAAXbs2zADv1oC.XiX+oCngbpzA...qBCCBv345RGPHdLhg9vSOc.MjCmN...XUXXP.FO+CRGPHFFD5C6Oc....qGCCBv344jNfPLLHzGdVoCnQ33S..fIKCCBv3oWeiD+sRG.vFwyHc.MhSkN...XUYXP.FOO2zADhyZKnO7qlNfFwOHc...vpxvf.Ld1e5..XD0iu002ImJc...vpxvf.Ld1S5.B3zoC.fMrSkN...XUYXP.FGGHc.g7WlN.fMhd8Zb6jSkN...XUYXP.FG85ajXu3Qf9fyWvy5ToC...VUFFDfww9SGPHmJc..aDOyzAzPNV5...fUkgAAXb7OLc.gbpzA.rQr+zA...r9LLH.ii8mNfPNb5..1HdFoCnQrU5...f0ggAAXbr+zA.vHZ+oCnQ78RG...rNLLH.ii8jNf.Nc5..1Xd5oCnQ7CRG...rNLLH.CuaLc.g7WlN.fMl8lNfFw2Mc...v5vvf.L71e5.B4ToC.fMreX5...f0ggAAX3s+zADxoRG.vFw0kNfFxec5...f0ggAAX38bSGPHeqzA.rQruzAzP7xGA.fIMCCBvv6YkNfPNb5..1H7hG4rNZ5...f0ggAAX3cUoC.fQzuR5....FFFFDfgWO9X1sU5..1XLL3BamN...XcYXP.XH7WkN.fMlmQ5.ZDdiDC.vjmgAAXXcKoCHDu3Qf9w9SGPi36lN...XcYXP.FV+poCHjSkN.fMFu7QV3GjN...XcYXP.FV6Oc.gbpzA.rwr2zAzHbGCB.vjmgAAXXc0oCHjue5..XCy08..Xxyvf.LrdtoCHjilN.fMhaLc.MDu7Q..Xxyvf.Lr1e5..XD47E7rLLH..SdFFDfg0dRGP.mLc..aL6Oc.MjikN...XcYXP.Xc4MyIzOdxoC...X3XXP.FNGLc.g3MyIzO9URGPiXqzA...CACCBvvoWO6sLLHzOdFoCnQ7SSG...LDLLH.CmmW5.BwvfP+vvfK35d..LKXXP.FN85iX22Kc..aLOkzAzH9woC...FBFFDfgSuNL3QSG.vFSudjIb999oC...FBFFDfgyuV5..XjsmzAzHNU5...fgfgAAX3r2zADvoSG..A7iRG...LDLLH.rNdzzA.rwbioCng7CSG...LDLLH.Cid8KL6MyIzOb9BdVGIc...vPvvf.LL1e5.BwvfP+vvf..vLigAAXXr+zADhgAg9wSMc....CKCCBvvX+oCHDmyVP+vcL3BakN...XnXXP.FF6Oc.g3b1B5G+JoCnQ78RG...LTLLH.CimQ5..XjYXvE9woC...FJFFDfgQO9H1sc5..1n7CfrvOHc...vPwvf.LL1a5.Bv4KHzW5we.jcxOJc...vPwvf.vpxcMCzW1S5.ZDmJc...vPwvf.r9tkzADx2Mc..Df6XP..lMLLH.qud8wq6uNc..DfiQA..lMLLH.qumY5.BwWNF5GGHc.MjsRG...LTLLH.qud8NF7ToC.Xi4ImNfFxCmN...XnXXP.VeOqzADh6XPnezq+.H..vrlgAAX88TRGPHGKc..aLO0zAzH1Nc...vPxvf.r9tpzA.vHycL3BdiDC.vrhgAAX8suzADfCeenuzq2YzmOGgB..LqXXP.XU78RG.vFkW9HK7iSG...LjLLH.rJNU5..1ndFoCnQ7CRG...LjLLH.qmaIc.g78SG.vFkGk3EbFCB.vrhgAAX8zqGH+Nmsf9Rudstymq8A.vrhgAAX87LSGPH+0oC.XiZOoCnQ3Ze..LqXXP.VO85cQiW9H.8neR5...fgjgAAX87qjNfPNZ5..H.OJw..LqXXP.VO85vf.8iCjNfFxwRG...LjLLH.qmmQ5..Xj8jSG...v3vvf.rd5wyXvsRG.vFUOdcN..nKXXP.VO83apSmwVPe4olNfFw1oC...FZFFD.VV+fzA.rQ8DSGPi3GkN...XnYXP.Vc85AxugAg9h6XvE9IoC...FZFFDfUWu9FI1cMCzW7xGYACCB.vrigAAX00qOdcNiAg9hgAW36mN...XnYXP.Vc85iW2ec5..1nLL3B+3zA...CMCCBvp6omNfPbGCB8kmR5.ZD+soC...FZFFDfUWudFCtU5..1n50iMgymeTD..lcLLH.qtd8wq6gSG.vF0dSGPivKdI..lcLLH.qtmQ5..fMFuUhA.X1wvf.r5dRoCHfsSG..g3kOB..yNWQ5..XB6wRGP.mnp5ZSGAvFUOdstchePc..lc7Ab.fkgGkNnubioC...X7XXP.XY78SG.vFUOdjI...cCCCBvpoWuKZbFaA8kmd5....FOFFDfUSu9kkcGCB8kmP5.ZDakN...XLXXP.VM85iWm6XPnu7TSGPi3mlN...XLXXP.VMOyzADxOJc..aT85cG846mmN...XLXXP.VM85WV9GlN.fMpmR5.ZDdirC.vrjgAAX0zqeYYe4Xnu3LFbA2sz..LKYXP.VMOizADxwRG.vF0SNc.MBmup..LKYXP.VM9xx.8.WqaA2wf..LKYXP.VM85YLHPewiR7BtiAA.XVxvf.rZ5wyXvsRG.vF2SJc.MBCCB.vrjgAAX0rmzADvOMc..ab83OBxNwiRL..yRFFD.1s7Eig9SO9ifrS7FYG.fYICCB.6VdT5.5U+hzA...iACCBvx65RGPHFFDnW4LVE.fYICCBvxauoCHjue5..HjGNc...vXvvf.r7dBoCHD2wfPeoWu6nA.ftggAAX48zSGPHFFD5K+2kN...fwkgAAX48TSGPH+vzA.rQ8jSG...v3xvf.r7dhoCHjeT5..1n9kSGPiX6zA...iECCBvxqWuiA+IoC.XipW+QPNe+7zA...iECCBvxqWe75NV5..1n50eDjy2OMc...vXwvf.r750gAA5KtiAWvcLH..yVFFDfkmgAA5ANiAW3GjN...XrXXP.VdOkzA.vFfgAA.fYNCCBvxqGe751Jc..abt6nW3GmN...XrXXP.Vd6Mc.A3v2G5OFFbACCB.vrkgAAfcCG99.8p+1zA...iECCB.6F+jzA.rw4NFbg+KoC...FKFFD.1M9QoC.Xi6IjNfFggAA.X1xvf.rbtwzADxuHc..abdqDufGkX..lsLLH.KmmT5.BwguOze9kRGPivOLB..yVFFDfkSu9n04NlA5OOkzAzHbFqB.vrkgAAX4zq2wf+zzA.rwsmzAzHbGCB.vrkgAAX4zq2wf+7zA.PHakN...XrXXP.VNO0zADh2Jw.8pGNc...vXwvf.rbdhoCHDmwV....yLFFDfkyub5.BwYrE...vLigAAX4zqCC5L1B...Xlwvf.rbdxoCHDmwVPe4FSG...v3yvf.rb50gAA5KOozA...L9LLH.Kmd8QIFnu7DRGPiX6zA...iICCBvx4ue5.....fgfgAAX47TRG..a.O8zAzH9QoC...FSFFDfkySLc.A3QoCnW8+W5...fwjgAAX4zimwf+7zA.rw0iWqam35e..LqYXP.VN6Ic.A7SSG.vFmgAW3WjN...XLYXP.3xwcLCze7hVZgeb5...fwjgAAfKG2wLP+wvfK35e..LqYXP.3xwcLCPuxvf..LqYXP.18ttzADhuXLzedpoCnQ35e..LqYXP.189uMc.g3KFCzqb8O..l0LLH.6dOozADhGkXn+7jSGPi3uMc...vXxvf......cHCCBvt2SHc.g3NlA5O8506Ne+WRG...LlLLH.6d+xoCHDewXn+XXvEb8O..l0LLH.6dOwzADhuXLze50eHD..nqXXP.1850unrGkXn+zqurkNet9G..yZFFDfcudcXP....lgLLH.6d+8SGPH+vzA.rw4GBYgeQ5...fwjgAAX2qWGFDfd0OIc...vXxvf.vki6XFn+zqurk..fthgAAX26olN..1P7nDufeXD..l0LLH.6dOgzADxVoC....fgmgAAX2qWGFDn+rmzAzHbGCB.vrlgAA..Xm8+a5...fwjgAAX26ImNfPd3zA....vvyvf.r64v3Gf9heXD..l0LLH.6dOozA.....CECCB.....zgLLH.6dtiAA...X13+lppuXU0MkND...ZLOV5..nwrUU0SKcD.Cmq3+5+6eSU0dSFB.zrb2kC8GChA.6jWVU0wRGAvv4LeYuWWzJ.....ZYumxnfvryYFF7XUUuqjg.....zjdfppOX5H.FdWw48+88WUcqIBA.ZRaWUckoi.XiyiRL.bFmtp5pRGAv337O2ndCUUmLQH...MgqKc..PSwnfvL1Ncfx+r23U..sped5....h4cjN.fw0E6MMo+e9Afpp5mlN...fHtupp6IcD.iqK1vf2Ss3h......8kSTU8lSGAv36hMLXUKtHvI1Tg..MIOJwP+YuoC..hZ6ppqMcD.aFWpgAqZwEC1dSDB....PbukzA.r4b4FFrJWT.....5AezppCmNBfMmcyvfGtVbwA....f4oiWUcGoi.XyZ2LLXUKt3vwGyP.....hXqppWR5H.171sCCV0hKRr0XEB....PDu0zA.jwxLLXUtXA.8leP5..13dBoC..1n9.UUGIcD.YrrCCdjZwEM.....l1dvpp2a5H.xYYGFrpEWz3gF5P.....1X1pp5lSGAPVqxvfUU0ud47FD....lpdcoC.HuUcXvpbQD....XJ58TUcrzQ.j25LL3wppdWCUH.....itGnp5ClNBf1v5LLXUUc20hKp.....z1NcU0aHcD.si0cXvpVbQkSN.+6.....LdtpzA.zVFhgAqppm8.8uC.zV9EoC...XP7NRG.P6YnFFrJWjAf4HCCB..Se2WU08jNBf1yPNL38TKtXC....Pa3DUUu4zQ.zlFxgAqZwEaNw.+uI..vlySOc..vfY6ppqMcD.sqgdXvpVbQmsGg+cA....18dKoC.nsMFCCVkK9.....I8QqpNb5H.Zai0vfGtVbQH....fMqdnOsUA..f.PRDEDUiWUcGoi.n8MVCCV0hKBc7Q7ee....fy0VUUujzQ.LMLlCCV0hKFs0H+eC....fEdqoC.X5XrGFrJWTB....1D9.UUGIcD.SGahgAORs3hS.....iiGrp58lNBfokMwvfUs3hSOzF5+V....POYqppaNcD.SOapgAqppe8x4MH....LzdcoC.XZZSNLXUtXE....LjdWUUGKcD.SSa5gAOVs3hV.....qmGnp5tSGAvz0ldXvpVbQqGHv+cA...f4hSWU8FRGAvzVhgAqZwEuNYn+aC....ScWU5..l9RMLXUU8rC9ea....Xp5cjN.f4gjCCVkKlA....Ki6qp5dRGAv7P5gAumZwE0.....tzNQU0aNcD.yGoGFrpEWT6Doi.....ZXaWUcsoi.XdoEFFrpEWba6zQ.....Mp2R5..leZkgAqxE4....fcxGsp5voi.X9okFF7v0hK1A....rvwqptizQ.LO0RCCV0hK1c7zQ.....MfsppdIoi.X9p0FFrpEWzaqzQ.....g8VSG.v7VKNLXUt3G....8sOPU0QRGAv7VqNL3QpEWDD....5MOXU06McD.yes5vfUs3hfOX5H....fMnsppt4zQ.zGZ4gAqZwECcdCB...Pu3MlN.f9QqOLXUtnH....8g2SU0QSGAP+XJLL3QqEWbD....lqdvppOX5H.5KSggAqZwEGcdCB...vbzoKmqf.ALUFFrJm2f....LO8lRG.PeZJMLXUU85RG.....LfdWUUGKcD.8oo1vfGqbdCB...v7vCTUc2oi.neM0FFrpEm2fe9zQ.....qgSWU8FRGAPeaJNLXUU8ZpEWDE....lhd8oC.fo5vfU4vYE...foo2UU0CmNB.lxCCdrZwESA...foBmqf.MiqHc.Cf6up5VSGwD16pp5mmNBfl0eYU0QSGAvF2smN.fl1+qUU6KcDSTmrp5YmNB.Ni4vvfUU02op5pSGwD0Gup5smNB...XR31pEeGBVMS4mZOfYn4xvfUU0ikNfIr2bU08kNB...n48ypp1S5HlndGUU2S5H.3waN8qU7NRGvD1+pzA....MuGoLJ3p5AJiBBzflSCCdO0hK1xxaO0h+HO...vN49qptlzQLQchpp2P5H.XmLmdThOiGo7GrVUOP4OXA...mqaup5+qzQLQscU0UlNB.tXliCCVky8h0gy8B...3wy449p6sUUcuoi.fKl4ziR7i2cjNfIL+Rf...vY7cRGvD18UFEDnwMWGF7dKukcWG9i+...v8WUc0oiXh5DUUu4zQ.vkybcXvpVbQ3SjNhIpqtV7g....f9zcVUcqoiXhZ6ppqMcD.raLmGFrpEWLd6zQLQcq0hOL....zWttppOb5Hlv9cRG..6Vy8gAqxEkWGe3ZwGJ....5G+goCXB6iWNVq.lPlquUhOeerZwaCJVdmtp5pRGA...rQb+kGg3U0IJOBw.SL8xvfUU0iTUcMoiXh5AppdCoi....FU2Y4QHdUscU0UlNB.VV8viR7Y37Fb047FD..f4sarLJ35vQXEvjTOcGCVUUGrbdYrNdYUUGKcD...vf6uopZuoiXh5iVUcGoi.fUQuMLXUU8QppdmoiXhx4MH...yOewppaJcDSTGup5kjNB.VU8ziR7YbG0hKdyxae0hOz....LObWkQAWUaUFEDXhqGGFrpEW714M3p4lpEe3A...XZ6.UUu+zQLg8VSG..qqdcXvpp5sjNfIr2es3CQ....SWe5zALg8+dU0QRGA.qqd7LF7w6CUU8tSGwD0VUUOszQ....qDmqfqtGpp5WOcD.LD546Xvpp5eds3h5r71a47FD..fon2WYTvU0VkQAAlQ58gAqZwE02JcDST2Ts3CU....SCGnp52McDSXuwzA.vPxvfK3h6qte2x4MH...SENWAWcefppilNB.FR89YL3i26q7KmspbdCB..P66Oqp5FRGwD0CVUcyoi.fglgAOWN.dWcN.dA..nc4Eu3pyMBAvrkGk3y0MWNuAWU2Ps3Ca....skaoLJ35vQOEvrkgAuPtn+p6cWK9PG...P63SjNfIr2S4bEDXFyiR7N6tppd+oiXhxsYO...si+SUUu3zQLQ4bEDX1yvfWbNuAWcGup5kjNB...5bejpp2Y5HlnbCO.zE7nDew47Fb08hqEeHD...HiCVFEbc75RG..aBFF7RyeLX08NqpNT5H...nS8uIc.SXumppikNB.1D7nDe447Fb0scU0UlNB...5LORU00jNhIpOeU0qIcD.roXXvcmOWU0qNcDSTmnp5ZSGA...chOVU0aKcDSTmtp5pRGA.aRdTh2cdM0h+HAKuqoV7gS...fw0gJiBtNdSoC.fMs+doCXB4gqp9mjNhIpWTU02oV7HM....iiGpp5WJcDSTuqpp+8oi.fMMOJwKm6rp5CmNhIJm2f...LdbtBt5dfpp2P5H.HAOJwKm6tV7GMX4smxcLH...igOYYTvU0IKiBBzwbGCtZdzpp8kNhIp6qp5MmNB...lItsppOd5Hlvbyx.z0bQvUyqOc.SXGpV7gW...f02GIc.SXuizA.PZtiAWcNuAWcNuAA..X84bEb04bEDfxcL35v4M3py4MH...qm6uLJ3p5DkQAAnpxcL3P36TUc0oiXhxuRG...KOO8RqNO8R.733NFb88rqE+wEVd2ZU0smNB...lPttxnfqi6Hc..zRbGCNL7l.a8XfZ...1cdzpp8kNhIp6qp5MmNB.ZIFjYXbu0h+HCqlGMc....LAb+kQAWUmnLJH.W.CCNbdy0h+XCKu8UK9PN...vN6NqEGEOr71tp5ZSGA.sHCCNrt1x4M3p5VqEeXG...3bcikyUv0wuS5..nUYXvgm+nyp6CWK9PO...vY8oRGvD1GubrOAvEkW9HiiOVU0aKcDSTmtp5pRGA...MhOWU0qNcDSTmn7HDCvkj6XvwwaubdCtp1Ws3C+...Pu6tJiBtpbtBBvtfgAGONuAWcu5ZwGBB..fd0MVU89SGwD1aIc..LE3QIdbcvpp+vzQLg8xppNV5H...Hf+lpp8lNhIpOZU0cjNB.lBLL336iTU8NSGwD0VUUOszQ...vF1Wrp5lRGwD0wqpdIoi.foBOJwiu6nV7GmX4s2ZwGJB..fdw6qLJ3pZ6xnf.rTLL3lwKobdCtptox4MH..Pe3.UU+toiXBy4JH.KIOJwaNNuAWO+lUUGMcD...LhbtBt5btBBvJvvfaVNuAWcNuAA..ly9ypptgzQLQ4bEDfUjGk3MKm2fqNm2f...yUenxnfqpsJiBBvJyvfadujZwe7hk2MUK9PS...LWbKUUu6zQLg8FSG..SYFFLi2Z5.lvd20hO7D...yAehzALg8AJmC4.rVbFClyGp7KCtpbdCB..LG7epp5EmNhIpGrp5lSGA.ScFFLKGvvqNGvv...SYdwDt5biB.v.wiRbV+5kyavU0KtV7go...Xp4fkQAWGNWAAXfXXv77G0VcuyZwGpB..foj+MoCXB68TNWAAXv3QItMbWUUu+zQLQscU0UlNB...1kbtBt5btBBv.yvfsiuXU0MkNhIJm2f...SANWAWcNWAAXD3QItcbykyavUkyaP..fVmyUv0yqKc..LGYXv1h+X2py4MH..PKy4J3p68TUcrzQ.vbjGk31iyavUmyaP..fVziTUcMoiXh5yWU8ZRGA.yUFFrM84ppd0oiXh5DUUWa5H...3+pOYU0gRGwD0oqptpzQ.vblGk31zqoV7GAY4cMUUerzQ...PU0sUFEbc7lRG..yc+8RG.WTObU0+jzQLQ8hppdzppuQ5P...5Z++TU8KkNhIp2UU0+9zQ.vbmGk311cVU8gSGwDkyaP..fjbtBt5dfpp2P5H.nGXXv128WUcqoiXhx4MH..PB9L7qNmqf.rA4LFr88FJm2fqpqoV7gx...XS41KiBtNd8oC.fdhyXvogiU0++r2cPH2c8cee9OvM3BE6lFbUFWDjtvqItngDdHw5hq7XfwLEwnQrUZ0p2EpniKBZGXRsKr1ERevEh2TXr1oSCNTLMyrvoOcgZ6BMQLoYPHDDwfARhPQbmjct3Yw+mNUsw304245b9d98++qWquuquoTSN++dc8+yI+6UGQm5lSxGmj+V0g...LIbxpCni8DI4OVcD.Lk3UIteXuAmO9siE..XQ6bIYaUGQmxtBBPAbrj9wykg+xRZy4pN...fQsiFGErUeXbTP.JgeiA6O9oP1N+THA..VD718Le7KrB.Ewe.b+4lpNfN1cmgOzF...aV1UbTv4wCWc..Lk4vf8oGs5.5X+WxvGdC..fMC9xxncubR9+n5H.XJyqRb+5nY32.NlcWJI2X0Q...z8d0jr+pinSc1jr8pi.foN+FC1uNXF9KSY1s0LbXU...Z0giiB1pKGGEDfUBNLXea6Y3uTkYm8FD..nU6II+hpini8SpN..Xv+V0Avb6umj6r5H5T6KIudRtX0g...zUNcRt1pinS8RI4WVcD.v.aL33vKFead0J6MH..vr30RxdqNhNkcEDfULdUhGG9wwdC1pslgObG...ecNbbTvVYWAAXEjCCNdXuAa2dyvGxC..fuJqG6J373ApN..3ekWk3wkCjj+X0Qzw9Omj+R0Q...rR5iSxVpNhN0KjjGu5H.f+UNL33yymjGq5H5TeRRtgpi...XkicErcmNI6r5H.fqLuJwiOOdF9KeY1skXuAA..9hd53nfs5xwQAAXklCCNNsyXuAa0dyvG9C..f0SxOq5H5X1UP.Vw4UId7xdCNer2f...1Uv1YWAAnC3vfia1av1YuAA.fos2JI6t5H5T1UP.5DdUhG2r2fsaKY3CCB..L87rwQAa0mDGEDftgCCN9syL7WNyra2Y3CEB..Lcbfj7jUGQG6AqN..XiyqR7zvcjj+eqNhN1+yI4+Z0Q...rT7oI45pNhN0uJI+uVcD.vFmCCNc7rwO4yVYuAA.fogSkjcTcDcpSjjas5H.fYiCCNsX.kamATF..F27E2W67CRGfNkMFbZ4Vi8FrU6HCeXQ..fwmCDGEbdbeUG..zFGFb5weoc6drL7gFA..FW9+r5.5XOSR9KUGA.zFuJwSSOcR9YUGQm5xI45qNB..fMMmIIqUcDcp2HI2d0Q..sygAmtdsjr2pinSc1jr8pi...Xt8hI4gqNhNkcEDfQ.uJwSW2dr2fsZsL7gHA..5W2ebTv4wcUc..v76eq5.nTmNI+vpinS8sSx4xvqdB..P+4DI4ZpNhN0SkjWt5H.f4mWkXNbR9EUGQmxdCB..8I6JX69SI46VcD.vlCGFjD6M37vdCB..8kijgWiXlcWJI2X0Q..adrwfjLr2fWp5H5TqkgObI..vpueTbTv4w2q5..fMW1XP9GNYRdnpinScKI4BI4cqND..fqp2L1UvV8DI4UpNB.XykWkX97NTR9uTcDcJ6MH..rZ6bIYaUGQm5XI4fUGA.r4ygA4K6nI4tqNhNk8FD..VM4y31N6JH.iX1XP9xNXr2fsZsL7gNA..Vcbn3nfyi6o5..fEGGFjqD+k+s6tSxiTcD...IIYWwT4LOdhLrE4.vHku7Q3J4iRxmlj8UcHcp8mj+bF9uGA..pyamjuQ0QzoNVF9ssD.Fwrwfb0XKVZmsXA..pkOKa69vjbSUGA.r34UIlqlClgOT.ytsF6MH..TE6J37wQAAXhvgA4qiOTP6t630u...V11SrqfyiGs5..fkGaLHaDebF1MOlc6KIudRtX0g...SDGO1UvVcrj7SqNB.X4wFCxFkMZoc1aP..X43Uief1s5rIY6UGA.rb4UIlMpClgOr.ytslgOjJ..vhygiiB1pKGGEDfIIGFjYw1yvGZfY29yvGVE..Xy2dRxun5H5XOd0A..0vqRLypeTRdopini8cxvt2...r44iSxVpNhN0KmjeP0Q..0vgAoEGII2e0Qzo9jjbCUGA..Lh7VIY2UGQmxtBBvDmWkXZwOH1avVskj7ZUGA..LR7zwQAakcED.bXPZl8Frc6M1aP..XdsdR9YUGQG6ApN..nd+aUG.csKjjCTcDcp0SxaljyWcH..Pm5cRx0VcDcpWHIOW0Q..0yFCx75ESxCWcDcJ6MH..zF6JX6NcR1Y0Q..qF7pDy75GG6MXq1RF9Ps...abOabTvVc43nf.vmiCCxlA6MX61cF9vs...e8tij7jUGQGytBB.eA1XP1r79I4dqNhN0dRxoRxGTcH..vJtiG6JXqrqf.v+BaLHald9j7XUGQmxdCB..WcmJI6n5H5T1UP.3JxgAYylOvV67A1..fqL+.name.z.vWIaLHa11YF9vGL61QF9Pu...+SGHNJ373AqN..X0kMFjEgyljue0Qzo1URNSRdupCA..VQbxjbMUGQm5Wkjec0Q..qt7pDyhxyFeiw0pKmjqu5H..fU.lol1chjbqUGA.rZygAYQ5sRxtqNhNk8FD.fotWLIOb0Qzorqf.vFhMFjEoaM1avVYuAA.XJ69iiBNOtupC..5C1XPVzNcR9gUGQmxdCB.vT0IhcErUOSR9sUGA.zG7pDyxvSmjeV0Qzor2f..L0bljrV0Qzodijb6UGA.zObXPVVdsjr2pinSc1jr8pi...VBNRFdMhY1YWAAfYlMFjkkaO1avVsVFFea..XL6GEGEbdXWAAfYlMFjkI6MX691I4BI4cqND..XA4MicErUOUR98UGA.ze7pDyx1gSxun5H5T1aP..FqrqfsytBB.MygAoB1av1YuAA.Xr4nI4tqNhNkcED.lK1XPpf8FrcqkgO7L..LF7HwQAmG1UP.XtXiAoJmHIOT0Qzot4j7wI4uUcH..vb5jUGPG6IRx+WUGA.z27pDSkr2fyG+F+B.PO6bIYaUGQm5XI4fUGA.z+bXPp1qlj8WcDcpOLI2T0Q...MvtB1tKkjar5H.fwA+FGQ09tY3C2vraawdCB.P+4PwQAmG2S0A..iG1XPVEbxXuAa0MmjOMIuc0g...a.6II+gpini8DI4OVcD.v3gCCxpfKlgiasupCoSsuj7mSxGUcH..vWiimjuQ0QzoNVF9ssD.XSiMFjUI1Zl1YqY..XUmsktc1VZ.XgvFCxpjClgOzCytsF6MH..qtNbbTv4giBB.KDNLHqZ7gdZ2cGudI..r5YOI4WTcDcrGs5..fwKaLHqh933mnbq1WRd8Lrai..vpfSmjqs5H5TGKI+zpi..FurwfrpxdC1tOII2P0Q...I40RxdqNhN0YSx1qNB.XbyqRLqpNXF9vPL61RF9P3..PkNbbTvVc43nf.vRfCCxprsmgOTDyt8lgOLN..Tg0icEbd73UG..LMXiAYU2eOI2Y0QzoVOIuYRNe0g..vjy6D6JXqd4j7yqNB.XZvFCRO3HI49qNhNk8FD.fkM6JX6rqf.vRkWkX5A+fXuAak8FD.fkomNNJXqrqf.vRmCCRuvdC1t8lgOjN..rHsdR9YUGQG6mTc..vziMFjdxERxApNhN0sE6MH..KV1Uv18RI4WVcD.vziMFjdyKljGt5H5T1aP..VTdqjr6pinSYWAAfx3vfziNSRVq5H5TmHI2Z0Q..vnxymjGq5H5TWNIWe0Q..SW1XP5Q1av1s6j7rUGA..iFGHNJ373ApN..XZyFCRu58Sx8VcDcp8jjSkjOn5P..n6cxjbMUGQm5ERxyUcD.vzlWkX5YdsUZm8FD.f40oRxNpNhN0oSxNqNB..GFjdmOPZ67ARA.nU9Az1N+.ZAfUF1XP5c6L1avVsiL7g5A.fYgcEb97fUG..v+fMFjw.6MX61UF9Vd98pND..5F1Uv18qRxut5H..9G7pDyXgWmk1c4jb8UGA..cgyjj0pNhN0IRxsVcD..edNLHiIuUR1c0QzoNaR1d0Q..vJsWLIOb0Qzorqf.vJIaLHiI2ZF9PWL6VKCeXe..3J49iiBNOtupC..3JwFCxXyoSxOr5H5Te6jbtL7JBA..edmH1UvV8LI42VcD..WIdUhYL5oSxOq5H5T1aP..9xrqfs6MRxsWcD..eUbXPFqdsjr2pinSYuAA.3e3HY30HlYmcED.V4YiAYr51i8FrUqkgGB..fosGINJ37vtBB.q7rwfLlYuAa2sjjKjj2s5P..nLmr5.5XOUR98UGA.vWGuJwL1c3j7KpNhNk8FD.X55bIYaUGQmxtBB.cCGFjo.6MX6r2f..SOGMI2c0Qzorqf.PWwFCxTf8FrcqkgGN..fogCEGEbdbWUG..vrvFCxTwIRxCUcDcpaNIeZRd6pCA.fEpckjWo5H5XOURd4pi..XV3vfLUbwj7YIY8pCoSsuj7mSxGUcH..rv71I4aTcDcp+TF9VbF.nqXiAYp4USx9qNhN0kRxMVcD..rPXWAamOiD.zsrwfL07cyvGdiY2Vi8FD.XLxtBNe9dUG..Pq7pDyTzIi8FrU1aP.fwk8jj+P0QzwdhXWFAfNlCCxTzEyvws1W0gzo1WRd8L7eOB.Pe63wtB1pikgeaKA.5V1XPlxrkNsyV5..z+r8xs6CSxMUcD..yKaLHSYGL1avVs0L7vD..zmNbbTv4giBB.iBNLHSc2S0Azw1eFdnB..5KqmjeQ0QzwdzpC..XyhMFjotOJ1av4w5wdCB.zadmjbsUGQm5XI4mVcD..aVrwfv.6MX69jjbCUGA..aHuVR1a0QzoNaR1d0Q..rYxqRLL3fYXDoY1skL7PF..rZ6oiiB1pKGGED.FgbXP3e5lxvG5iY2di8FD.XU15I4mUcDcrGu5...VDrwfvWzeOI2Y0QzoVOImJIeP0g..v+B6JX6d4j7yqNB.fEAaLH7u5HI49qNhNk8FD.X0yakjcWcDcJ6JH.Lp4UIF9W8CxvGBjY2VxvCe..vpgmMNJXqrqf.vnmCCBWYaO1avVs6L7PH..Tq6HIOY0Qzw9IUG..vhlMFD9pcgjbfpinSsmXuAA.p1wicErUuTR9kUGA.vhlMFDt5dwj7vUGQmxdCB.TmSkjcTcDcJ6JH.LY3vfvWuyjj0pNhN0oSxNqNB.fIlmOIOV0QzotbRt9pi..XYwFCBe8r2fsaGY3gS..X43.wQAmGOP0A..rLYiAgMl2OI2a0Qzo1UF9st78pND.fIfSljqo5H5TuPRdtpi..XYxqRLrw40xocdsb..V7rqfsy7m..SRNLHLa7AtamOvM.vhiuvzZme.l.vjkMFDlM6L1avVYuAA.VLt+3nfyC6JH.LYYiAgYm8Frc1aP.fMemH1UvVYWAAfIMuJwPar2fsyqqC.vlmyjj0pNhNkYNA.l7bXPnc1av1c1jr8pi..nycjL7ZDyr6SRxMTcD..UyFCBsamY3CUxrasL7vL..zleTbTv4w8Uc...qBrwfv74rI46WcDcpaIIWHIua0g..zgdyXWAa0yjjea0Q..rJvqRLL+d1j7jUGQmxdCB.L6rqfs6MRxsWcD..qJbXPXywakjcWcDcJ6MH.vF2QSxcWcDcJ6JH.vWhMFD1bbqwdC1p0xvC4..vU2iDGEbdXWAA.9RrwfvlmSmjeX0Qzot4j7wI4uUcH..qvNY0Azwdpj76qNB.fUMdUhgMWGNI+hpini42hY.fqrykjsUcDcJ6JH.vWAGFD178ZIYuUGQm5CSxMUcD..qXrqfsytBB.bU32NGXy2sG6MXq1Vr2f..edGJNJ373tpN..fUY1XPXw3DI4gpNhN0MmjOMIuc0g..Tr8jj+P0Qzwdpj7xUGA.vpLGFDVLtXR9rjrd0gzo1WR9yI4ipND.fBc7j7MpNhN0eJCeKNC.vUgMFDVrd0jr+pinScojbiUGA.PQ7YHZmOCA.vFjMFDVr9tY3CmxraqwdCB.SSGNNJ3736Uc...8BuJwvh2Ii8FrU2bFdkreypCA.XIYOI4HUGQG6IRxqTcD..8BGFDV7tXF9xzXeUGRmZ8j75Y3+dD.Xr6zI4ZqNhN0wxv2hy..rAYiAgkmilj6t5H5TeRRtgpi..XA60RxdqNhNkcED.nA1XPX44fwdC1psjgGVB.Xr5vwQAmG2S0A..zi7pDCKWGOI+6UGQmZawdCB.iSqmjeW0Qzwdhj7GqNB.fdjCCBKWeTr2fyi0yvgAOe0g..rI5chcErU1UP.f4fMFDpg8Frc1aP.XLwtB1tOLI2T0Q..zyrwfPMNXF9vrL6r2f.vXwSGGEbd3nf..yIGFDpiOLa61aFdXJ.fd0cjjeV0Qzwd3pC..XLvFCB05iSx9qNhN0sE6MH.zuNdrqfs5kSxOu5H..FCrwfP8r2fsydCB.8nSkjcTcDcpyljsWcD..iEdUhg5cvL7gbY1skj7VUGA.vL34iiB1pKGGED.XSkCCBqF1dF9vtL61cRd1pi..XC3.I4wpNhN1Oo5...Farwfvpi+dRtypinSsmL7ZY8AUGB.vUwISx0TcDcpWJI+xpi..XrwFCBqVdw3aYuVc4jb8UGA.vWA6JX6rqf..KHdUhgUK+3XuAa00kgG5B.XUicErc1UP.fEHGFDV8XuAa2NxvCeA.rpvtBNedfpC..XLyFCBqld+jbuUGQmZWI4LI48pND.fXWAmGuPRdtpi..XLyFCBqtd932vfVYuAAfUAmIIqUcDcpSmjcVcD..icNLHrZyPk2NCUN.TIegh0N+.9..VRrwfvpscF6MXqVKCOTF.vx18GGEbdXWAA.VRrwfvpO6MX691I4bY3U4B.XY4DwtB1J6JH.vRjWkXnOXuAamWGI.XYxtB1N6JH.vRlCCB8C6MX6r2f.vxvQSxcWcDcpOII2P0Q..L0XiAg9wNyvGZlY2ZI4HUGA.Lp8HwQAmGOX0A..LEYiAg9xYSx2u5H5T2RRtPRd2pCA.FkNY0Azw9UI4WWcD..SQdUhg9yyljmr5H5T1KNnFZK...H.jDQAQUP.XQ3bIYaUGQm5DI4VqNB.foJGFD5SuUR1c0Qzor2f.vlI6JX6rqf..EyFCB8oaM1avVsVFdHN.f40ghiBNOtupC..XpyFCB8qSmjeX0Qzot4j7oI4sqND.nasqj7JUGQG6YRxus5H..l5bXPnec9L7uCeaUGRmZeI4OmjOp5P.ftzamjuQ0Qzodij7PUGA..1XPXL30RxdqNhN0kRxMVcD.P2wtB1N6JH.vJDaLHz+t8XuAa0Vi8FD.lM1Uv4ycUc...7O4UIFFGr2fsydCB.aT6II+gpini8TI4kqNB..9mbXPXb37I4yRx5UGRmZeI40SxEqND.Xk1wicErU+oj7HUGA..eQ1XPXbwdC1N6MH.b07pIY+UGQmxeGK.vJJaLHLtb6Y3CeyraqY3g9..9xNbbTv4w2q5...3JyqRLL9bxj7PUGQm5akgWI62r5P.fUFqmjeW0Qzwdhj7JUGA..WYNLHL9bwL7kow9pNjN05wdCB.+SuSRt1pinScrL7s3L..qnrwfv30QSxcWcDcpOII2P0Q..ky181N6JH.PGvFCBiWGL1avVskL7vf.vz0SGGEbdbOUG...e87pDCiaGOI+6UGQmZaY3Oi7uVcH.vR25I42TcDcrmHI+wpi..fudNLHLt8QwdCNOtsL7EQx4qND.XoxtB1N6JH.PGwFCBSC1av1YuAAXZ4sRxtqNhN0Gljap5H..XiyFCBSCGLCeXclcaICOjH.L98rwQAmGNJH.PmwgAgoCeX81s6L7vh.v30cjjmr5H5XOZ0A..vryFCBSKebR1e0Qzo1SRNUR9fpCA.VHNdrqfs5XI4mVcD..L6rwfvzi8Frc1aP.FmNUR1Q0QzoNaR1d0Q..Pa7pDCSOGLCeHdlcaICO7H.Ld77wQAa0kiiBB.z0bXPXZZ6Y3CyyraGY3gHAf92ARxiUcDcrGu5...X9XiAgoq+dRtypinSsqjblj7dUGB.LWNYRtlpinS8xI4mWcD..Lerwfvz1QRx8WcDcpKmjqu5H.flcljrV0Qzorqf..iDdUhgosePr2fs55h8FDfd0KFGErU1UP.fQDGFDvdC1N6MH.8m6OIOb0QzwdfpC..fMO1XPfjjKjgAXmYm8FDf9xIhcErUuPRdtpi..fMO1XPf+gWL9MnnU1aP.5C1Uv1c5jrypi..fMWNLHvmmGXpcFhc.Vs4Kbq14G.F.vHkMFD3yydC1t0xvCcB.qd9QwQAmG1UP.fQJaLHvW16mj6s5H5T2RF1qw2s5P.fuf2L1UvVYWAA.Fw7pDCbk77I4wpNhNkW2J.VsXlLZmcED.XjygAA9pbpjripinSYuAAX0vQSxcWcDcpOII2P0Q..vhkMFD3qxNyvCEvrasL7vn.Pcdj3nfyiGr5...XwyFCBb0b1j78qNhN0MmjONI+spCAfIpSVc.creUR90UGA..KddUhA957rI4IqNhNleyrAX46bIYaUGQm5DI4VqNB..VNbXPfMh2JI6t5H5TeXRtopi.fID6JX6rqf..SL9MYAXi3Vi8FrUaK1aP.VVNTbTv4w8Uc...rbYiAA1nNcR9gUGQm5lSxmlj2t5P.XDaOI4OTcDcrmII+1pi..fkKGFDXi57Y3Oy31pNjN09RxeNIeT0g.vH0wSx2n5H5TuQRdnpi..fkOaLHvr50RxdqNhN0kRxMVcD.LB8pIY+UGQmxtBB.LgYiAAlU2dr2fsZqY3gWAfMOGNNJ37vtBB.Lg4UIFnE1av18sRxmkj2r5P.XDXOI4HUGQG6oRxuu5H..nNNLHPKNeFNt05UGRmZ8j75I4hUGB.ctSmjqs5H5TuQR9wUGA..0xFCBLOr2fsylNAv7weGT672AA.PRrwf.yG6MX61RFdnV.X1c33nfyi6p5...X0fWkXf40IRxCUcDcpsE6MH.yp0Sxuq5H5XOQRdkpi..fUCNLHv75hwdCNOVOCGF77UGB.ch2I1UvVcrjbnpi..fUG1XPfMKuZR1e0Qzor0S.rwXWAa2kRxMVcD..rZwFCBrY46lgG5fYm8FDfudOabTv4w8Tc...r5wqRLvloSF6MXq1VF9yj+qUGB.qftij7eTcDcrmHI+wpi..fUONLHvloKljOMI6q5P5T2Vr2f.bkb7XWAakcED.fuR1XPfEgilj6t5H5T1aP.9hNUR1Q0Qzo9vjbSUGA..qtrwf.KBGLCOLBytsjj2p5H.XEwyGGEbd3nf..bU4vf.KJdXj1s6LLx9.Lkcfj7XUGQG6QqN...V8YiAAVj93jr+pinSsmjblj7dUGB.E4jI4ZpNhN0wRxOs5H..X0mMFDXQydC1tKmjqu5H.n.1Uv1c1jr8pi..f9fWkXfEsClgGRgY20kgGNFfoD6JX6tbbTP..lANLHvxv1yvCqvraGY3gjAXJvtBNed7pC..f9hMFDXY4umj6r5H5T6J1aPfoA6JX6d4j7yqNB..5K1XPfkoijj6u5H5T1aPfwtyjj0pNhNkcED.fl3UIFXY5GD6MXqttL7Py.LF8hwQAakcED.fl4vf.Ka1av1sVFd3Y.FSt+j7vUGQG6mTc...zurwf.U3BYXf4Y18sSx4he6AAFONQrqfs5kRxur5H..neYiAApxKF+FhzJ6MHvXgcErc1UP..laNLHPk7.gsyCDBz6NZRt6pinS4GPD..aJrwf.UxdC1t0xv2xy.zidj3nfyiGn5...XbvFCBTs2OI2a0QzotkLrWiua0g.vL5jUGPG6ERxyUcD..LN3UIFXUvymjGq5H5Tdcx.5MmKIaq5H5TmNI6r5H..X7vgAAVUbpjripinS8gI4lpNB.1.rqfs6SRxMTcD..LtXiAAVUryXuAa01xvCaCvprCEGEbd7fUG...iO1XPfUI1av1cyI4SSxaWcH.bErqj7JUGQG6Wkjec0Q..v3iCCBrJ48Rx2LCO.Iyt8kj+bR9npCAfuj2NIeipinSchj78qNB..Fmrwf.qhr2fs6RI4FqNB.9brqfsytBB.vBkMFDXUzNyvCCwraqwdCBr5vtBNetupC..fwMuJw.qpNcR9gUGQmxdCBrJXOI4OTcDcrmII+1pi..fwMGFDXU04yveF0sUcHcp8kjWOIWr5P.lrNdrqfs5MRxCUcD..L9YiAAV08ZIYuUGQmxdCBTE+Y2sytBB.vRiMFDXU2sG6MXq1ZRd0pi.Xx4vwQAmG1UP..VZ7pDCzCr2fs6akjOKIuY0g.LIrdR9cUGQG6oRxuu5H..X5vgAA5AmOCG2Z8pCoSsdr2f.KGuSRt1pinS8FI4GWcD..LsXiAA5I1rp1Yyp.Vz7mQ2N+Yz..TBaLHPOwdC1tsjj2p5H.Fsd53nfyi6p5...XZxqRLPu4DI4gpNhN0+CY3O2+uVcH.iJqmjeS0Qzwdpj7xUGA..SSNLHPu4hwdCNOtsL7EQx4qNDfQC6JX69SI4QpNB..ltrwf.8pWMI6u5H5T1xJfMKuUR1c0QzotTRtwpi..foMaLHPu56lgGphYm8FDXyvyFGEbd78pN...vgAA5Ydnp1s6L7P8.zh6HIOY0Qzwdhjb7pi...rwf.8rKljOMI6q5P5T6IImJIeP0g.zcNdrqfs5XI4PUGA..jXiAAFGNZRt6pinSYuAAlUmJI6n5H5TeXRtopi...9GbXPfwhKjjsVcDcpSmjcVcD.cgmOIOV0QzwLiO..rRwGNAXr3dpNfN1NxvC6CvUyAhiBNOdzpC...9xrwf.iEeTr2fyickjyjj2q5P.VYcxjbMUGQm5XI4mVcD..vWlWkXfwF6MX6tbRt9pi.XkzYRxZUGQm5rIY6UGA..bk3UIFXr4fYXb2Y1ccY3KU..97dw3nfs5xwQAA.XElCCBLFcSY3gwX1YuAA97t+j7vUGQG6wqN...3pwFCBLV82SxcVcDcpckjykgWcPfosSD6JXqd4j7yqNB..3pwFCBLlcjL7a6ByN6MHfcErc1UP..5BNLHvXmGrscdvVX5xOXk14GrB..cCaLHvX21i8FrUqkgiC.Ls7ihiBNO9IUG...rQYiAAlBtPRNP0QzotkL7e+8tUGBvRyaF6JXqdoj7KqNB..XixqRLvTwKFeyZ1JuVbvzg4WncleA..5NNLHvThG3scdfWX76nI4tqNhNke.J..zkrwf.SI1av1sVFNZ.v3ziDGEbd7.UG...zBaLHvTy6mj6s5H5T2bR93j72pNDfMU6JI++TcDcrWHIOW0Q...svqRLvTzymjGq5H5X9sMGFWtPR1Z0QzoNcR1Y0Q...sxgAAlpNUR1Q0Qzo9vjbSUGAvlB6JX6rqf..z87a8AvT0Ni8FrUaK1aPXL3PwQAmG1UP..5d1XPfoL6MX6t4j7oI4sqNDflrmj7GpNhNlcED.fQAGFDXJ68Rx2LCCuOyt8kjWOIWr5P.lYGOIeipinSc5jbfpi...1LXiAAvdCNOtTRtwpi.Xl7pIY+UGQm5SRxMTcD..vlEaLH.C6M3mTcDcpslgiL.zGNbbTv4wCVc...vlIuJw.L3rI46WcDcpuUR9rj7lUGBvU0dRxQpNhN1yjj+2qNB..XyjCCBvfOHIWaFdvYlcqG6MHrp6zY3OmiY2ajjGp5H..fMa1XP.9hdqjr6pinSY6sfUWuVR1a0Qzo7msA.vnkMFDfunaM1avVskLb7AfUKGNNJ3739pN...XQwqRL.+qNcR9gUGQmZawdCBqRVOI+tpini8TI42WcD..vhhCCBv+pymg+7waq5P5TqmgCCd9pCAHuSrqfs5MRxOt5H..fEIaLH.e0rIWsylbA0ylo1N+YX..LIXiAA3q1sG6MXqr2fPsd13nfyi6p5...fkAGFDfqNObX61aRd5pi.lftij7jUGQG6oRxwqNB..XYvFCBvU2EyvWlFqWcHcpaK1aPXY63wtB1p+TRdjpi...VVrwf.rw7pIY+UGQmxVcAKOmJI6n5H5TWJI2X0Q...KSdUhAXi46lgGZjY2VxvWBB.KVOebTv4w2q5...fkMGFDfMNOzX61cFNZAvhwARxiUcDcrmH1UP..lfrwf.rwcwj7oIYeUGRmZWI4LI48pNDXD5jI4ZpNhN0wRxgpNB..nB1XP.lcGMI2c0QzotbRt9pi.FYrqfsytBB.vjlWkX.lcGL1avVccY3HF.aNrqfym6o5...fJ4vf.zFOLY61Qr2fvlA6J374IxvqfM..LYYiAAnMeTr2fyC6MHL+rqfsytBB..wFCBv7xdC1N6MHztyjj0pNhN0Gljap5H..fUAdUhAX9bvL7PlL6ttLbbCfYyKFGEbd3nf..v+cNLH.yOOjY6VKCG4.Xi4GkjGt5H5XOZ0A...qRrwf.r43iSx9qNhN02NImK9sGD1HdyXWAa0Kmj+2pNB..XUhMFDfMO1av1YuAgud1Uv1c1jr8pi...V03vf.r4xCt2NO3N7UyO3g14G7...7UvFCBvlqsmgGBkY2ZI4HUGArB5QhiBNO9IUG...rpxFCBvlu+dRtypinScKI4BI4cqNDXExIqNfN1KkjeY0Q...qp7pDCvhwQRx8WcDcL+FsCCNWR1V0QzoLOA..vWCO3E.KF+fL7PozlyUc.vJfiFGErUWNNJH..70xgAAXwwdC1tskgih.SUGJ1Uv4wCTc...POvFCBvh06mj6s5H5T2bR9zj71UGBrjsqj7JUGQG6ERxyUcD..POvgAAXw58Rx2LCOnOyt8kj+bR9npCAVhd6j7MpNhN0oSxApNB..nW3KeD.VNNUR1Q0QzotTRtwpi.VRNZ7JD2pKmjqu5H..fdhMFDfkicF6MXq1Zr2fLMXWAmO1UP..XF4UIFfkG6MX6r2fL1smj7GpNhNlcED..ZfCCBvxi8Fb9ruj75I4hUGBr.b5jbsUGQmxtBB..MxFCBvxm8Frc1aPFidsjr2pinS8II4FpNB..nWYiAAX4amY3gYY1s0j7pUGArI5vwQAmGOX0A...8LuJw.Tiyljue0Qzo9VI4yRxaVcHvbZ8j76pNhN1uJI+5pi...5YNLH.03CxvdhsmpCoSsdr2fz+dmXWAa0Ihe3J..vbyFCBPsdqjr6pinSYuAomYWAamcED..1jXiAAnV2Zr2fsZqY33JPu4oiiBNOtupC...FK7pDCP8NcR9gUGQmZaY3uK6uVcHvFz5I42TcDcrmII+1pi...FKbXP.p24yved7sUcHcpaKCeQjb9pCA1.rqfs6MRxCUcD..vXhMFDfUG1br1YywnGXSQam+cb..XAvFCBvpiaO1avVskLbzEXU0yFGEbdbWUG...LF4vf.rZwn52tcmgiu.qZtij7jUGQG6oRxwqNB..XLxFCBvpkymjOKCeAEvraOI4TI4CpND3y43wtB1p+TRdjpi...Fqrwf.rZxdC1NaQFqRNUR1Q0QzotTRtwpi...Fy7pDCvpI6MX61RFNFCTsmONJ3736Uc...vXmCCBvpKise61QFNJCTkCjjGq5H5XOQrqf..vBmMFDfUWWLIeZR1W0gzo1URNSRdupCgIoSljqo5H5TGKIGp5H..fo.aLH.q9NZRt6pinSc4jb8UGASNmIIqUcDcJ6JH..rD4UIFfUeGLCOrLytqK1aPVtdw3nfyi6o5...foDGFDf9fGVtc6HCGqAVzt+j7vUGQG6IxvqfM..vRhMFDf9vGE6M373amjykgWwSXQ4DwtB1J6JH..T.aLH.8E6MX6r2frHYWAa2Gljap5H..foHGFDf9y4Rx1pNhN0YSx1qNBFcNRFdMhoMl1F..nH9fX.ze7aVS6VKCGwA1r7ihiBNOdzpC...lxrwf.zm93jr+pinScKI4BI4cqNDFEdyXWAa0wRxOs5H..foLuJw.zur2fsydCxlA6JX67Z8C..q.7pDCP+5fY3gqY1ccw2PwLeNRbTvVc43nf..vJAGFDf911yvCYyrasL7acILqNTrqfyiGu5....FXiAAn+82SxcVcDcpaNC603eq5Pnarqj7JUGQG6kSxOu5H...FXiAAXb3HwuASyC+FzyF0ERxVqNhNkcED..Vw3Ag.Xb3GD6M373bUG.cgiFGErU1UP..XEjCCBv3g8FrcaK1aPt5NT7s.973ApN...f+U1XP.FWtPRNP0Qzor2f7UYOI4OTcDcrWHIOW0Q...7uxFCBv3yKljGt5H5Xemjb7pifUJ1Uv1c5jrypi...3JygAAXb5LIYspinScojbiUGAqLd0jr+pinSc4jb8UGA..vWMaLH.iS1av1s0LbLH3vwQAmG1UP..XEmMFDfwq2OI2a0Qzo9VI4yRxaVcHTl8jjiTcDcL6JH..zAbXP.Fuduj7MSxtpNjN05I40SxEqNDJwoSx0VcDcpSGeIHA..cAaLH.iemJI6n5H5TeRRtgpifktWKI6s5H5T92Y..fNhMFDfwuclgGVmY2VxvQhX53vwQAmGOX0A...rw4UIFfogyljue0Qzo1Vr2fSE2QR9MUGQG6Wkjec0Q...rw4vf.LM7AYXuz1S0gzoVOCGF77UGBKTGO1UvVch3G9...zcrwf.Ls7VIY2UGQmx1oMt4e2nc92M..fNkMFDfokaM1avVYuAGud13nfyi6q5....ZiWkX.ldNcR9gUGQmZaY3u67uVcHro4NRx+Q0Qzwdlj7aqNB..f13vf.L8b9L7m+eaUGRm51h8FbLwtB1t2HIOT0Q...zNaLH.SWuVR1a0QzoroZiCmJI6n5H5T92A..fQ.aLH.SW2dr2fsZKY3nRzud93nfyC6JH..LB3vf.Ls4g6a2Nxvwkn+bfj7XUGQG6oRxeo5H...le1XP.l1NeR9rjrd0gzo1URNSRdupCgYxISx0TcDcp2HI+3pi...XygMFD.Rr2fyiKmjqu5HXCytB1N6JH..Lx3UIF.Rr2fyiqK1avdgcEb9bWUG...vlKGFD.9G7P+sydCt5ytBNedpjb7pi...XykMFD.9GtXr2fyC6M3pM6JX6NVR9eo5H...17YiAAfurWMI6u5H5T1avUSmIIqUcDcpKkjar5H...VL7pDC.eYe2LbL.lcWWFNBEqNNRbTv4w8Tc....KNNLH.bk78pNfN1ZI4EqNBRRxOJI2e0QzwdhL7JXC..LRYiAAfqjKljOMI6q5P5Te6jbt32dvp8lwtB1pikjCUcD...KV1XP.3pwdC1N6MXsrqfs6CSxMUcD...KdNLH.704bIYaUGQm5rIY6UGwDzQSxcWcDcLSMC..LQ3C9A.ec7aNT6VKCGohkmGINJ373QqN...fkGaLH.rQ7wwqTbqt4L7e+82pNjIBeYYztikjeZ0Q...r73UIF.1n75YNe7ao+hmW68140dG..lf7PJ.vF0AyvwCnMmq5.F4NZbTvVc43nf..vjjCCB.yhsmgiHvraawdCtnbn32l04wiWc....0vFCB.yp+dRtypinScyI4SSxaWcHiH6JIuR0Qzwd4j7yqNB..fZXiAAfVbjjb+UGQG6+T7kjwlkKjjsVcDcJ6JH..Lw4vf.PqNSRVq5H5TWJI2X0QLB3KDm1c4jb8UGA..Psrwf.Pqr2fsaqwdCNuNbbTv4wOo5....pmMFD.lGWHIGn5H5T1av1smL75rSadoj7KqNB..f54UIF.lWuXRd3pini8cRxwqNhNyGmjsTcDcJ6JH..v++bXP.Xyf8Frc1avYyqkj8VcDcJ6JH..vWfMFD.1LXuAa2VSxqVcDchCGGEbd7.UG...vpEaLH.rY48Sx8VcDcpuUR9rj7lUGxJr0Sxuq5H5XuPRdtpi...X0hCCB.aVduj7MSxtpNjN05Y3vfmu5PVQ8NI4ZqNhN0oiujf...tBrwf.vlsSkjcTcDcpOII2P0QrBxtB1N+uo...9JYiAAfMa6L1avVskLbDL9md53nfyiGr5....Vc4UIF.VDr2fsaaY3ue9uVcHq.VOI+lpini8qRxut5H...Vc4vf.vhf8Fb9bawdClXWAmGmHIe+pi...X0lMFD.Vjr2fsapuMbuUR1c0Qzol5+uc...1frwf.vhzNyvQJX1skLbbronmMNJ3739pN...f9fCCB.KZNRQ61cFNR1Txcjjmr5H5XOSR9KUGA..PevFCB.KZmOC+8M2V0gzo1SFdkr+fpCYI4+ujbMUGQm5MRxCUcD...8CaLH.rr7ZIYuUGQmZprYb1jx1MU9ei...rIxqRL.rrb6wdC1psjgilMl87wQAmGdk8A..lYNLH.rL43EsaGY33YiQGHIOV0QzwdpXWAA..ZfMFD.VlNeR9rjrd0gzo1URNSRdupCYS1IicErUuQR9wUGA..PexFCB.UvdC1tKmjqu5H1DcljrV0Qzorqf...yEuJw.PEr2fs65xvwzFCdw3nfyi6p5....5aNLH.TEG0ncqkgip0yt+j7vUGQG6oRxwqNB..f9lMFD.pxEi8Fbd7sSx4R+9aO3IhcErU+oj7HUGA..P+yFCB.U6USx9qNhNUut2f1Uv1cojbiUGA..v3fWkX.nZe2LbrClc83dCdj3nfyiuW0A...Ld3vf.vp.G6ncqkgis0C9QYXaAoMOQrqf...ahrwf.vpfKljOMI6q5P5T2RRtPRd2pC4qwaF6JXqNVRNT0Q...LtXiAAfUIGMI2c0QzoV02avykjsUcDcJ6JH..vBgCCB.qZtPR1Z0QzoNaR1d0QbE3fuyGS+B..vBgOnI.rp4dpNfN1ZY3HbqRNTbTv4wiVc....iW1XP.XUyGE6M373lSxGmj+V0gjjckjWo5H5XGKI+zpi...X7xqRL.rpxqe57YU3sBvqEd6VUesvA..FQVEdnA.fqjCljOr5H5Xmq3+4ez3nfs5xwQAA..VBbXP.XU1MUc.crsk51aP6J374wqN...foAaLH.rp6BI4NqNhN0Mmg8Z7sWh+ybOI4OrD+m2XyKmjed0Q...LMXiAAfdvQRx8WcDcruSRN9R5eV1Uv1YWAA..VpbXP.nWbljrV0QzotTRtwkv+bd0jr+kv+bFitbRt9pi...XZwFCB.8hsmgimvraqY3ncKRGNNJ373mTc....SO1XP.nmbgjbfpinS8sRxmkj2bA7e16ICut2zlWJI+xpi...X5wqRL.zadwj7vUGQGaQr2febR1xl7+YNUXWAA..JiCCB.8H6MX69jjbCah+m2qkj8tI9edSI1UP..fRYiAAfdj8FrcaICGyayvSGGEbd7.UG...vzlMFD.5UueRt2pinSssL+6M35I42r4jyjzKjjmq5H...l1bXP.nW8dI4aljcUcHcp0yvgAOei+++6jjqcyKmIkSGeI5...rBvFCB.8tSkjcTcDcpV2av2JI6dStkoB6JH..vJCaLH.z61Yr2fspk8F7YiiBNOrqf...qL7pDC.iA1av1ssL74A9qaf+u8NRx+whMmQM6JH..vJEGFD.FCr2fymaKCuR1evWy+2c7XWAakcED..XkiMFD.FSr2fs6qauA8e21tV2xQ...Vnrwf.vXxNyvQXX1skLb7uqjmONJ373AqN....tRbXP.Xr4AqNfN1NxvQ.+7NPRdrBZYr3YRx+0pi...fqDaLH.L17AYXG71S0gzo1URNSF1swjjSljqotb5ZuQRdnpi...fuJ1XP.Xr5sRxtqNhN0kSx0G6J37vtBB..rxygAAfwrONCamGytKmjqq5H5X+mSxeo5H...3pwFCB.iY2W0AzwbTv18TwQAA..5.1XP.XL67Y3uq61pNDlLdij7iqNB...1H7pDC.SAuVR1a0QvnmcED..nq3vf.vTg8FjEsuSRNd0Q...vFkMFD.lJtqpCfQsmJNJH..PmwFCB.SEWLIeVRVu5PXz4OkjGo5H...XV4UIF.lZd0jr+pifQiKkjar5H...nENLH.LEcgjr0pifQA6JH..P2xFCB.SQeupCfQgmHNJH..PGyFCpGT9RM..X.5IQTPTA.SQWLIeZR1W0gP25XI4PUGA...yCuJw.vT1QSxcWcDzcrqf...iBNLH.L0YuAYV8eJImr5H...XdYiAAfot6o5.nq7DwQAA..FIrwf.vT2GE6MHaL1UP..fQEuJw..Cr2fb07gI4lpNB...1L4vf..+SmKIaq5HXkj4WA..XzwGxE.3exuQXbk7nUG....KBNLH.vWji.wm2Kmjec0Q...vhfu7Q..9h9aI4+wjbyUGBk6rI41qNB...VTrwf..WYmIIqUcDTlKmjqu5H...XQxqRL.vU11yvwgXZ5mTc....rn4vf..e0d7pCfR7RYXaAA..XTyFCB.7U6cyv2Tw2R0gvRyYSx+SUGA...KC1XP.fud1avoA6JH..vjhWkX.fud1avogGn5....XYxgAA.1XbznwsWHI+eWcD...rLYiAA.1Xduj7MSxtpND1zc5jbfpi...fkMaLH.vr4TIYGUGAaZrqf...SVdUhA.lM6L1avwDuh3...SVNLH.vrywjFGrqf...SZ1XP.fYm8Fr+YWAA..l7rwf..sydC1m9jjbCUGA...UyqRL.+2Zu6dPqqx3v.3O5XgNo3TwAwIq0AkFj1pfQrCcyOFJsCFbpEAGBcq3lKktVvgL3PwoRcoztH5TrCFEDBEGpXgntTbKzsh3vEDURyG264b9e932O3c+48tjvC22mKL+NdlUxDCKqTc....nOPwf..KlUpN.bfb0jbmpCA...8A1XP.fEy8SxgRxIqNHrmtaRNW0g...f9BaLH.PyX8jbhpCAOQ1UP...9e7ThA.ZFmJ1av9ryVc....nuQwf..MGkO0O8YI4aqND...z2XiAA.ZNOHy9aquY0Ag+w2jjOp5P...PejMFD.n480I4sqNDXWAA..X2nXP.f1wCSxyVcHl3dij7cUGB...5qrwf..sC6MXs9znTP...1U1XP.f1wCRxiSxxUGjInamjKVcH...nuySIF.ncYuA6V+dRd9pCA...CAJFD.n8YuA6N1UP...1mrwf..su2s5.LQbonTP...12rwf..sueKIamjSWcPFwtYRVs5P...vPhmRL.P24FI48qNDiP1UP...lCJFD.nasURNR0gXj40Sx2WcH...XnwFCB.zs9fpCvHykhRAA..XtXiAA.5V+Qr2fME6JH...K.OkX.fZXuAWL+ZRdwpCA...CYJFD.nN+RRdgpCw.k4PA..fEj+oZ.f53a7174iqN....LFnXP.fZojqClaljOu5P...vXfe7Q..p0OjjWNIuT0AY.3dI4spND...LVXiAA.5G1LIGs5Pzi8njb3pCA...iIdJw..8CGKyJ+hc1mTc....XrQwf..8GJ+Zm8kI4KpND...L1XiAA.5O9oL6Wp3Wo5fzibuj7NUGB...Firwf..8O1avYrqf...zh7ThA.5er2fybgpC....iYJFD.neZpWJ10xrsED...ZI1XP.f9oMSxQRxqVcPJvOlj2q5P...vXmMFD.neaps2f1UP...5HdJw..8aSs8F7CqN....LUnXP.f9uoRYYWKIeU0g...foBaLH.P+2OmjmIIKUcPZQ1UP...5X1XP.fgiMRxqUcHZA+YRdtpCA...SMdJw..CGGOyJQarYkpC....SQJFD.XXYkpCPC6pI4NUGB...lhrwf..CK2OIGJImr5fz.taRNW0g...foJaLH.vvz5I4DUGhEfcED..fh4oDC.LLcpLr2avyVc.......FpVNI+0.7b413CC...3fwFCB.Lb8fj73LqfvghamjKVcH......FCtUp+aA394rUa8A......vT0Vo9h+1qyRs1sG.....XhZoTewe61Y016pC.....LssZpu.vc5bi17RC.....vrR3ptHv+8Yy185B...yqmp5...PiayjbzpCQRdTRNb0g....1YOc0A..fF2wxrR4p1EpN.......SMmO09DhWq8uh......rSVK0TJ3FcwkC.....fmrMR2VJ31cy0B.....f8x1o6JF7LczcB.....f8vYR2TJ3U5pKD......6OWIsaofq2cWE......NHVOsSofOrKuD......bv8vz7ECtbmdC......3.a4zrkBd4tM9......LutbZlRAuUWGb......VL2JKVofa08QF.....flvVY9KFboBxK......Mfkx7UJ3pUDV......ZNqlCVofWulXB.....PS65Y+UJ3lUEP......ZGalcuTvsqKZ......zl1NO4hAOeg4B.....fVz4yNWJ3ZUFJ......Zeqk+aofaTab......nqrQrqf......SRamjyTcH...f1yeS7zehGhMH+O.....jTQNQjqBAlf" ],
					"embed" : 1,
					"id" : "obj-52",
					"ignoreclick" : 1,
					"maxclass" : "fpic",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "jit_matrix" ],
					"patching_rect" : [ 507.761638048772056, 74.0, 108.0, 96.0 ],
					"pic" : "drag-and-drop.png",
					"presentation" : 1,
					"presentation_rect" : [ 284.0, 170.5, 62.0, 57.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-35",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 711.590143203735352, 1026.0, 163.0, 22.0 ],
					"text" : "prepend set Audiofile loaded:"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-32",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 673.0, 988.0, 191.0, 22.0 ],
					"text" : "loadmess set (no audiofile loaded)"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-139",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 4,
					"outlettype" : [ "signal", "signal", "signal", "bang" ],
					"patching_rect" : [ 905.31145191192627, 1204.0, 104.0, 22.0 ],
					"saved_object_attributes" : 					{
						"basictuning" : 440,
						"followglobaltempo" : 0,
						"formantcorrection" : 0,
						"mode" : "basic",
						"originallength" : [ 44167.680000000000291, "ticks" ],
						"originaltempo" : 120.0,
						"pitchcorrection" : 0,
						"quality" : "basic",
						"timestretch" : [ 0 ]
					}
,
					"text" : "sfplay~ 2 60000 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-130",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 1,
							"revision" : 8,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 34.0, 79.0, 979.0, 620.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-50",
									"linecount" : 5,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 688.0, 153.0, 158.0, 74.0 ],
									"text" : "For some reason, cue \"1\" doesn't work, and needs to be stored under another number. Here, i substitute it automatically with \"111\""
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.796078431372549, 0.03921568627451, 0.03921568627451, 1.0 ],
									"bgcolor2" : [ 0.2, 0.2, 0.2, 1.0 ],
									"bgfillcolor_angle" : 270.0,
									"bgfillcolor_autogradient" : 0.0,
									"bgfillcolor_color" : [ 0.2, 0.2, 0.2, 1.0 ],
									"bgfillcolor_color1" : [ 0.796078431372549, 0.03921568627451, 0.03921568627451, 1.0 ],
									"bgfillcolor_color2" : [ 0.2, 0.2, 0.2, 1.0 ],
									"bgfillcolor_proportion" : 0.5,
									"bgfillcolor_type" : "gradient",
									"gradient" : 1,
									"id" : "obj-46",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 496.5, 193.0, 29.5, 22.0 ],
									"text" : "111"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-43",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 496.5, 132.0, 34.0, 22.0 ],
									"text" : "sel 1"
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.776470588235294, 0.074509803921569, 0.074509803921569, 1.0 ],
									"bgcolor2" : [ 0.2, 0.2, 0.2, 1.0 ],
									"bgfillcolor_angle" : 270.0,
									"bgfillcolor_autogradient" : 0.0,
									"bgfillcolor_color" : [ 0.2, 0.2, 0.2, 1.0 ],
									"bgfillcolor_color1" : [ 0.776470588235294, 0.074509803921569, 0.074509803921569, 1.0 ],
									"bgfillcolor_color2" : [ 0.2, 0.2, 0.2, 1.0 ],
									"bgfillcolor_proportion" : 0.5,
									"bgfillcolor_type" : "gradient",
									"gradient" : 1,
									"id" : "obj-42",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ -19.0, 305.0, 29.5, 22.0 ],
									"text" : "111"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-39",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ -19.0, 254.0, 52.0, 22.0 ],
									"text" : "sel 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-29",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 600.0, 446.0, 57.0, 22.0 ],
									"text" : "sort -1 -1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-21",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 699.0, 431.0, 93.0, 22.0 ],
									"text" : "loadmess dump"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-15",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 108.0, 497.0, 41.0, 22.0 ],
									"text" : "pause"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 237.0, 170.0, 50.0, 22.0 ],
									"text" : "pause"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 57.0, 174.0, 59.0, 22.0 ],
									"text" : "number 2"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-48",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 532.5, 334.0, 210.5, 22.0 ],
									"text" : "$1 preload $1 numbers.mp3 $2 $3"
								}

							}
, 							{
								"box" : 								{
									"coll_data" : 									{
										"count" : 11,
										"data" : [ 											{
												"key" : 1,
												"value" : [ "preload", 1, "numbers.mp3", 2800, 3250 ]
											}
, 											{
												"key" : 2,
												"value" : [ "preload", 2, "numbers.mp3", 5148, 5598 ]
											}
, 											{
												"key" : 3,
												"value" : [ "preload", 3, "numbers.mp3", 7399, 7849 ]
											}
, 											{
												"key" : 4,
												"value" : [ "preload", 4, "numbers.mp3", 9693, 10143 ]
											}
, 											{
												"key" : 5,
												"value" : [ "preload", 5, "numbers.mp3", 11947, 12397 ]
											}
, 											{
												"key" : 6,
												"value" : [ "preload", 6, "numbers.mp3", 14255, 14705 ]
											}
, 											{
												"key" : 7,
												"value" : [ "preload", 7, "numbers.mp3", 16427, 16877 ]
											}
, 											{
												"key" : 8,
												"value" : [ "preload", 8, "numbers.mp3", 18776, 19226 ]
											}
, 											{
												"key" : 9,
												"value" : [ "preload", 9, "numbers.mp3", 20907, 21357 ]
											}
, 											{
												"key" : 10,
												"value" : [ "preload", 10, "numbers.mp3", 23027, 23477 ]
											}
, 											{
												"key" : 111,
												"value" : [ "preload", 111, "numbers.mp3", 2800, 3250 ]
											}
 ]
									}
,
									"id" : "obj-47",
									"linecount" : 2,
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "", "", "", "" ],
									"patching_rect" : [ 532.5, 375.499948087249777, 75.0, 35.0 ],
									"saved_object_attributes" : 									{
										"embed" : 1,
										"precision" : 6
									}
,
									"text" : "coll numbercues"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-44",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 14.0, 425.0, 19.0, 22.0 ],
									"text" : "t l"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-41",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 511.5, 295.0, 178.0, 22.0 ],
									"text" : "preload $1 numbers.mp3 $2 $3"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-37",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 511.5, 265.0, 99.0, 22.0 ],
									"text" : "pack i i i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-36",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 591.5, 229.0, 39.5, 22.0 ],
									"text" : "+ 450"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-35",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 606.5, 193.0, 29.5, 22.0 ],
									"text" : "i 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-34",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "int", "bang", "bang" ],
									"patching_rect" : [ 496.5, 90.0, 129.0, 22.0 ],
									"text" : "t i b b"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-33",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 511.5, 53.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-31",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 551.5, 193.0, 29.5, 22.0 ],
									"text" : "i 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-28",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 283.0, 206.0, 41.0, 22.0 ],
									"text" : "pause"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-26",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 420.5, 193.0, 50.0, 22.0 ],
									"text" : "2800"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-22",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 60.0, 357.0, 55.0, 22.0 ],
									"text" : "embed 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-136",
									"linecount" : 2,
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 35.0, 309.0, 105.0, 35.0 ],
									"text" : "loadmess open numbers.mp3"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-18",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 43.5, 431.0, 150.0, 20.0 ],
									"text" : "<< sfplay~ numbers.mp3"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-17",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 14.0, 487.999948087249777, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-14",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 342.0, 153.0, 117.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-12",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 199.5, 340.0, 19.0, 22.0 ],
									"text" : "t l"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-11",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 342.0, 229.0, 51.0, 22.0 ],
									"text" : "seek $1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-9",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 138.5, 206.0, 80.0, 22.0 ],
									"text" : "route number"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-8",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 253.5, 498.0, 86.0, 20.0 ],
									"text" : "<< sfplay~"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 138.5, 162.0, 19.0, 22.0 ],
									"text" : "t l"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 277.0, 50.0, 150.0, 20.0 ],
									"text" : "from local OSC >>"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-3",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 89.0, 50.0, 150.0, 20.0 ],
									"text" : "<< from amex"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 429.0, 40.000009087249737, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-125",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "", "", "", "", "" ],
									"patching_rect" : [ 50.0, 100.0, 137.0, 22.0 ],
									"text" : "regexp _ @substitute \" \""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-126",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.000020523803755, 40.000009087249737, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-128",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 199.5, 493.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"source" : [ "obj-11", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-128", 0 ],
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-125", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-125", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-125", 0 ],
									"source" : [ "obj-126", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-44", 0 ],
									"source" : [ "obj-136", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"order" : 3,
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-26", 1 ],
									"order" : 2,
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-31", 1 ],
									"order" : 1,
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-35", 1 ],
									"order" : 0,
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"source" : [ "obj-15", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-47", 0 ],
									"source" : [ "obj-21", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-44", 0 ],
									"source" : [ "obj-22", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"source" : [ "obj-26", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"source" : [ "obj-28", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-47", 0 ],
									"source" : [ "obj-29", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 1 ],
									"source" : [ "obj-31", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-34", 0 ],
									"source" : [ "obj-33", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-31", 0 ],
									"source" : [ "obj-34", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-35", 0 ],
									"source" : [ "obj-34", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-43", 0 ],
									"source" : [ "obj-34", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-36", 0 ],
									"source" : [ "obj-35", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 2 ],
									"source" : [ "obj-36", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-41", 0 ],
									"order" : 1,
									"source" : [ "obj-37", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-48", 0 ],
									"order" : 0,
									"source" : [ "obj-37", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-42", 0 ],
									"source" : [ "obj-39", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-44", 0 ],
									"source" : [ "obj-39", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-44", 0 ],
									"source" : [ "obj-41", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-44", 0 ],
									"source" : [ "obj-42", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 0 ],
									"source" : [ "obj-43", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-46", 0 ],
									"source" : [ "obj-43", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"source" : [ "obj-44", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 0 ],
									"source" : [ "obj-46", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"source" : [ "obj-47", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-47", 0 ],
									"source" : [ "obj-48", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 1 ],
									"order" : 1,
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"order" : 0,
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 1 ],
									"order" : 0,
									"source" : [ "obj-9", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"order" : 1,
									"source" : [ "obj-9", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-39", 0 ],
									"source" : [ "obj-9", 0 ]
								}

							}
 ],
						"styles" : [ 							{
								"name" : "AudioStatus_Menu",
								"default" : 								{
									"bgfillcolor" : 									{
										"type" : "color",
										"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
										"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
										"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}

								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "maxscore.default.buttons",
								"default" : 								{
									"accentcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
 ]
					}
,
					"patching_rect" : [ 941.333333333333144, 996.721283046234134, 99.999999999999886, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p audio"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.701960784313725, 0.372549019607843, 0.03921568627451, 1.0 ],
					"bgcolor2" : [ 0.2, 0.2, 0.2, 1.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.2, 0.2, 0.2, 1.0 ],
					"bgfillcolor_color1" : [ 0.701960784313725, 0.372549019607843, 0.03921568627451, 1.0 ],
					"bgfillcolor_color2" : [ 0.2, 0.2, 0.2, 1.0 ],
					"bgfillcolor_proportion" : 0.5,
					"bgfillcolor_type" : "gradient",
					"gradient" : 1,
					"id" : "obj-118",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 544.548523071889235, 582.786868572235107, 34.426229953765869, 22.0 ],
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.831372549019608, 0.431372549019608, 0.023529411764706, 1.0 ],
					"bgcolor2" : [ 0.2, 0.2, 0.2, 1.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.2, 0.2, 0.2, 1.0 ],
					"bgfillcolor_color1" : [ 0.831372549019608, 0.431372549019608, 0.023529411764706, 1.0 ],
					"bgfillcolor_color2" : [ 0.2, 0.2, 0.2, 1.0 ],
					"bgfillcolor_proportion" : 0.5,
					"bgfillcolor_type" : "gradient",
					"gradient" : 1,
					"id" : "obj-115",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 366.703934376663142, 582.786868572235107, 171.533442136164354, 22.0 ],
					"text" : "EMPTY ml-mode~0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-112",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 478.5, 504.282852446289041, 141.212129538482657, 22.0 ],
					"text" : "parse_AMEX"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-105",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 409.5, 650.647540092468262, 150.0, 33.0 ],
					"text" : "could be used to simplify LOTS!"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.870588235294118, 0.6, 0.109803921568627, 1.0 ],
					"id" : "obj-101",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 350.5, 661.647540092468262, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-37",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 1,
							"revision" : 8,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 34.0, 79.0, 1089.0, 787.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-30",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 787.0, 55.0, 145.0, 22.0 ],
									"text" : "pid~9_8_7_6_5_4_3_2_1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-25",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 921.0, 696.0, 35.0, 22.0 ],
									"text" : "clear"
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 20.0,
									"id" : "obj-17",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "", "", "", "" ],
									"patching_rect" : [ 725.127655625343209, 622.0, 267.0, 31.0 ],
									"saved_object_attributes" : 									{
										"embed" : 0,
										"precision" : 6
									}
,
									"text" : "coll PID-SID-correspondence"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-29",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1201.0, 537.0, 189.0, 22.0 ],
									"text" : "/sod4l/pid 1_0_0_0_0_0_0_0_0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-22",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1181.0, 484.0, 77.0, 22.0 ],
									"text" : "/sod4l/pid $1"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-15",
									"index" : 3,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1181.0, 699.17021107673645, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"coll_data" : 									{
										"count" : 0,
										"data" : [  ]
									}
,
									"fontsize" : 18.0,
									"id" : "obj-56",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "", "", "", "" ],
									"patching_rect" : [ 760.0, 567.0, 276.0, 29.0 ],
									"saved_object_attributes" : 									{
										"embed" : 1,
										"precision" : 6
									}
,
									"text" : "coll SID-NAMES-correspondence"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-55",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "clear" ],
									"patcher" : 									{
										"fileversion" : 1,
										"appversion" : 										{
											"major" : 8,
											"minor" : 1,
											"revision" : 8,
											"architecture" : "x64",
											"modernui" : 1
										}
,
										"classnamespace" : "box",
										"rect" : [ 59.0, 104.0, 640.0, 480.0 ],
										"bglocked" : 0,
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 1,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 1,
										"objectsnaponopen" : 1,
										"statusbarvisible" : 2,
										"toolbarvisible" : 1,
										"lefttoolbarpinned" : 0,
										"toptoolbarpinned" : 0,
										"righttoolbarpinned" : 0,
										"bottomtoolbarpinned" : 0,
										"toolbars_unpinned_last_save" : 0,
										"tallnewobj" : 0,
										"boxanimatetime" : 200,
										"enablehscroll" : 1,
										"enablevscroll" : 1,
										"devicewidth" : 0.0,
										"description" : "",
										"digest" : "",
										"tags" : "",
										"style" : "",
										"subpatcher_template" : "",
										"assistshowspatchername" : 0,
										"boxes" : [ 											{
												"box" : 												{
													"id" : "obj-50",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 205.0, 420.90625, 62.0, 22.0 ],
													"text" : "print huhu"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-49",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 50.0, 414.07646107673645, 93.0, 22.0 ],
													"text" : "pack i s"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-48",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "bang", "" ],
													"patching_rect" : [ 50.0, 273.90625, 93.0, 22.0 ],
													"text" : "t b l"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-46",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 94.0, 311.884969592094421, 29.5, 22.0 ],
													"text" : "1"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-44",
													"maxclass" : "button",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 50.0, 311.884969592094421, 24.0, 24.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-36",
													"maxclass" : "number",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 50.0, 379.884969592094421, 50.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-34",
													"maxclass" : "newobj",
													"numinlets" : 5,
													"numoutlets" : 4,
													"outlettype" : [ "int", "", "", "int" ],
													"patching_rect" : [ 50.0, 343.884969592094421, 107.0, 22.0 ],
													"text" : "counter"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-33",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "", "" ],
													"patching_rect" : [ 50.0, 242.90625, 47.0, 22.0 ],
													"text" : "zl iter 1"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-32",
													"linecount" : 4,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 297.0, 195.90625, 50.0, 62.0 ],
													"text" : "Adrian Ben 0 Juan 0 0 0 0 0"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-30",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 3,
													"outlettype" : [ "", "bang", "clear" ],
													"patching_rect" : [ 50.0, 130.90625, 107.0, 22.0 ],
													"text" : "t l b clear"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-29",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 5,
													"outlettype" : [ "", "", "", "", "" ],
													"patching_rect" : [ 50.0, 175.90625, 137.0, 22.0 ],
													"text" : "regexp _ @substitute \" \""
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-28",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 236.0, 108.097739160060883, 184.0, 22.0 ],
													"text" : "Adrian_Ben_0_Juan_0_0_0_0_0"
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-52",
													"index" : 1,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 50.0, 40.0, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-53",
													"index" : 1,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 50.0, 506.732909999999947, 30.0, 30.0 ]
												}

											}
 ],
										"lines" : [ 											{
												"patchline" : 												{
													"destination" : [ "obj-30", 0 ],
													"source" : [ "obj-28", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-32", 1 ],
													"order" : 0,
													"source" : [ "obj-29", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-33", 0 ],
													"source" : [ "obj-29", 3 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-33", 0 ],
													"order" : 1,
													"source" : [ "obj-29", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-28", 1 ],
													"order" : 0,
													"source" : [ "obj-30", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-29", 0 ],
													"order" : 1,
													"source" : [ "obj-30", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-46", 0 ],
													"source" : [ "obj-30", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-53", 0 ],
													"source" : [ "obj-30", 2 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-48", 0 ],
													"source" : [ "obj-33", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-36", 0 ],
													"source" : [ "obj-34", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-49", 0 ],
													"source" : [ "obj-36", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-34", 0 ],
													"source" : [ "obj-44", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-34", 2 ],
													"source" : [ "obj-46", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-44", 0 ],
													"source" : [ "obj-48", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-49", 1 ],
													"source" : [ "obj-48", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-50", 0 ],
													"order" : 0,
													"source" : [ "obj-49", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-53", 0 ],
													"order" : 1,
													"source" : [ "obj-49", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-30", 0 ],
													"source" : [ "obj-52", 0 ]
												}

											}
 ]
									}
,
									"patching_rect" : [ 760.0, 476.09375, 122.0, 22.0 ],
									"saved_object_attributes" : 									{
										"description" : "",
										"digest" : "",
										"globalpatchername" : "",
										"tags" : ""
									}
,
									"text" : "p parse_SID-NAMES"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-54",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "clear", "" ],
									"patcher" : 									{
										"fileversion" : 1,
										"appversion" : 										{
											"major" : 8,
											"minor" : 1,
											"revision" : 8,
											"architecture" : "x64",
											"modernui" : 1
										}
,
										"classnamespace" : "box",
										"rect" : [ 1714.0, 323.0, 640.0, 480.0 ],
										"bglocked" : 0,
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 1,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 1,
										"objectsnaponopen" : 1,
										"statusbarvisible" : 2,
										"toolbarvisible" : 1,
										"lefttoolbarpinned" : 0,
										"toptoolbarpinned" : 0,
										"righttoolbarpinned" : 0,
										"bottomtoolbarpinned" : 0,
										"toolbars_unpinned_last_save" : 0,
										"tallnewobj" : 0,
										"boxanimatetime" : 200,
										"enablehscroll" : 1,
										"enablevscroll" : 1,
										"devicewidth" : 0.0,
										"description" : "",
										"digest" : "",
										"tags" : "",
										"style" : "",
										"subpatcher_template" : "",
										"assistshowspatchername" : 0,
										"boxes" : [ 											{
												"box" : 												{
													"id" : "obj-5",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "", "" ],
													"patching_rect" : [ 270.0, 398.0, 46.0, 22.0 ],
													"text" : "route 0"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-4",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 374.0, 401.0, 32.0, 22.0 ],
													"text" : "print"
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-3",
													"index" : 2,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 297.0, 496.732909999999947, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-2",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "", "" ],
													"patching_rect" : [ 210.0, 443.0, 37.0, 22.0 ],
													"text" : "zl.rev"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-1",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 311.0, 32.097739160060883, 146.0, 22.0 ],
													"text" : "3_2_1_0_0_0_0_0_0"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-49",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 50.0, 414.07646107673645, 93.0, 22.0 ],
													"text" : "pack i i"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-48",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "bang", "" ],
													"patching_rect" : [ 50.0, 273.90625, 93.0, 22.0 ],
													"text" : "t b l"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-46",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 94.0, 311.884969592094421, 29.5, 22.0 ],
													"text" : "1"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-44",
													"maxclass" : "button",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 50.0, 311.884969592094421, 24.0, 24.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-36",
													"maxclass" : "number",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 50.0, 379.884969592094421, 50.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-34",
													"maxclass" : "newobj",
													"numinlets" : 5,
													"numoutlets" : 4,
													"outlettype" : [ "int", "", "", "int" ],
													"patching_rect" : [ 50.0, 343.884969592094421, 107.0, 22.0 ],
													"text" : "counter"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-33",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "", "" ],
													"patching_rect" : [ 50.0, 242.90625, 47.0, 22.0 ],
													"text" : "zl iter 1"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-32",
													"linecount" : 3,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 297.0, 195.90625, 50.0, 49.0 ],
													"text" : "1 0 0 0 0 0 0 0 0"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-30",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 3,
													"outlettype" : [ "", "bang", "clear" ],
													"patching_rect" : [ 50.0, 130.90625, 107.0, 22.0 ],
													"text" : "t l b clear"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-29",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 5,
													"outlettype" : [ "", "", "", "", "" ],
													"patching_rect" : [ 50.0, 195.90625, 137.0, 22.0 ],
													"text" : "regexp _ @substitute \" \""
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-28",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 323.0, 74.097739160060883, 146.0, 22.0 ],
													"text" : "1_0_0_0_0_0_0_0_0"
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-52",
													"index" : 1,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 50.0, 40.0, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-53",
													"index" : 1,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 50.0, 496.732909999999947, 30.0, 30.0 ]
												}

											}
 ],
										"lines" : [ 											{
												"patchline" : 												{
													"destination" : [ "obj-30", 0 ],
													"source" : [ "obj-1", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-5", 0 ],
													"source" : [ "obj-2", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-30", 0 ],
													"source" : [ "obj-28", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-32", 1 ],
													"order" : 0,
													"source" : [ "obj-29", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-33", 0 ],
													"source" : [ "obj-29", 3 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-33", 0 ],
													"order" : 1,
													"source" : [ "obj-29", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-28", 1 ],
													"order" : 0,
													"source" : [ "obj-30", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-29", 0 ],
													"order" : 1,
													"source" : [ "obj-30", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-46", 0 ],
													"source" : [ "obj-30", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-53", 0 ],
													"source" : [ "obj-30", 2 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-48", 0 ],
													"source" : [ "obj-33", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-36", 0 ],
													"source" : [ "obj-34", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-49", 0 ],
													"source" : [ "obj-36", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-34", 0 ],
													"source" : [ "obj-44", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-34", 2 ],
													"source" : [ "obj-46", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-44", 0 ],
													"source" : [ "obj-48", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-49", 1 ],
													"source" : [ "obj-48", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-2", 0 ],
													"order" : 0,
													"source" : [ "obj-49", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-53", 0 ],
													"order" : 1,
													"source" : [ "obj-49", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-3", 0 ],
													"order" : 1,
													"source" : [ "obj-5", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-4", 0 ],
													"order" : 0,
													"source" : [ "obj-5", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-30", 0 ],
													"source" : [ "obj-52", 0 ]
												}

											}
 ]
									}
,
									"patching_rect" : [ 645.127655625343209, 476.09375, 99.0, 22.0 ],
									"saved_object_attributes" : 									{
										"description" : "",
										"digest" : "",
										"globalpatchername" : "",
										"tags" : ""
									}
,
									"text" : "p parse_SID-PID"
								}

							}
, 							{
								"box" : 								{
									"coll_data" : 									{
										"count" : 9,
										"data" : [ 											{
												"key" : 1,
												"value" : [ 1 ]
											}
, 											{
												"key" : 2,
												"value" : [ 0 ]
											}
, 											{
												"key" : 3,
												"value" : [ 0 ]
											}
, 											{
												"key" : 4,
												"value" : [ 0 ]
											}
, 											{
												"key" : 5,
												"value" : [ 0 ]
											}
, 											{
												"key" : 6,
												"value" : [ 0 ]
											}
, 											{
												"key" : 7,
												"value" : [ 0 ]
											}
, 											{
												"key" : 8,
												"value" : [ 0 ]
											}
, 											{
												"key" : 9,
												"value" : [ 0 ]
											}
 ]
									}
,
									"fontsize" : 20.0,
									"id" : "obj-51",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "", "", "", "" ],
									"patching_rect" : [ 645.127655625343209, 523.0, 267.0, 31.0 ],
									"saved_object_attributes" : 									{
										"embed" : 1,
										"precision" : 6
									}
,
									"text" : "coll SID-PID-correspondence"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 606.127655625343323, 121.0, 19.0, 22.0 ],
									"text" : "t l"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 787.0, 133.0, 291.0, 22.0 ],
									"text" : "pid~1_0_0_0_0_0_0_0_0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-47",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 606.127655625343323, 645.276591658592224, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-43",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 606.127655625343323, 606.978719592094421, 55.0, 22.0 ],
									"text" : "zl slice 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-42",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 430.627656608819962, 396.978723168373108, 144.148935496807098, 22.0 ],
									"text" : "1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-40",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 606.127655625343323, 276.191489160060883, 19.0, 22.0 ],
									"text" : "t l"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-39",
									"maxclass" : "newobj",
									"numinlets" : 4,
									"numoutlets" : 4,
									"outlettype" : [ "", "", "", "" ],
									"patching_rect" : [ 606.127655625343323, 377.744677305221558, 135.999999999999773, 22.0 ],
									"text" : "route pov pid names"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-38",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 787.0, 328.787231683731079, 294.680849313735962, 22.0 ],
									"text" : "pid 1_0_0_0_0_0_0_0_0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-37",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "", "", "", "", "" ],
									"patching_rect" : [ 606.127655625343323, 213.978723168373108, 138.0, 22.0 ],
									"text" : "regexp ~ @substitute \" \""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-11",
									"index" : 3,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 606.127655625343323, 37.361701488494873, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-8",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 213.0, 417.191489160060883, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.5, 0.0, 0.5, 1.0 ],
									"id" : "obj-27",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 275.0, 397.978723168373108, 119.0, 22.0 ],
									"text" : "s amexserver_speak"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 257.340424656867981, 329.787231683731079, 142.55319082736969, 22.0 ],
									"text" : "0/daddy/query~pov~0/0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 213.0, 263.829785346984863, 60.882978498935699, 22.0 ],
									"text" : "i 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 213.0, 295.744678735733032, 181.0, 22.0 ],
									"text" : "sprintf 0/daddy/query~pov~%d/0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-26",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 499.042554199695587, 104.0, 67.0, 20.0 ],
									"text" : "<< SID"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-24",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 439.042554199695587, 102.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-21",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 439.042554199695587, 42.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-19",
									"linecount" : 3,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 29.234044432640076, 315.744678556919098, 141.0, 47.0 ],
									"text" : "In case I know my SID, i can add it to the message"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-20",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 29.234044432640076, 366.170210361480713, 135.0, 22.0 ],
									"text" : "0/daddy/query~pov~5/0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-18",
									"linecount" : 3,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 29.234044432640076, 232.382976710796356, 131.925533950328827, 47.0 ],
									"text" : "Asks the whole network, who the pov is"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-16",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 29.234044432640076, 281.382976710796356, 135.0, 22.0 ],
									"text" : "0/daddy/query~pov~0/0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-14",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 213.0, 229.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-12",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 275.0, 200.0, 182.0, 20.0 ],
									"text" : "<< bangs when it connects"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 213.0, 193.0, 34.0, 22.0 ],
									"text" : "sel 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-9",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 213.0, 154.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-3",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 213.0, 102.0, 91.0, 22.0 ],
									"text" : "route tcp-status"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-2",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 606.127655625343323, 699.17021107673645, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 213.0, 42.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"source" : [ "obj-10", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-11", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-24", 0 ],
									"source" : [ "obj-21", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 0 ],
									"order" : 1,
									"source" : [ "obj-22", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-29", 1 ],
									"order" : 0,
									"source" : [ "obj-22", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 1 ],
									"source" : [ "obj-24", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"source" : [ "obj-25", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-30", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-40", 0 ],
									"source" : [ "obj-37", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-40", 0 ],
									"source" : [ "obj-37", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-22", 0 ],
									"order" : 0,
									"source" : [ "obj-39", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-42", 1 ],
									"order" : 1,
									"source" : [ "obj-39", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-43", 0 ],
									"order" : 0,
									"source" : [ "obj-39", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-54", 0 ],
									"order" : 1,
									"source" : [ "obj-39", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-55", 0 ],
									"source" : [ "obj-39", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 1 ],
									"order" : 0,
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"color" : [ 0.501960813999176, 0.0, 0.501960813999176, 1.0 ],
									"destination" : [ "obj-8", 0 ],
									"order" : 1,
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-38", 1 ],
									"order" : 0,
									"source" : [ "obj-40", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-39", 0 ],
									"order" : 1,
									"source" : [ "obj-40", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-47", 0 ],
									"source" : [ "obj-43", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-47", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"source" : [ "obj-54", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-51", 0 ],
									"source" : [ "obj-54", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-56", 0 ],
									"source" : [ "obj-55", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 1 ],
									"order" : 0,
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 0 ],
									"order" : 1,
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"source" : [ "obj-9", 0 ]
								}

							}
 ],
						"styles" : [ 							{
								"name" : "AudioStatus_Menu",
								"default" : 								{
									"bgfillcolor" : 									{
										"type" : "color",
										"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
										"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
										"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}

								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "maxscore.default.buttons",
								"default" : 								{
									"accentcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
 ]
					}
,
					"patching_rect" : [ 216.5, 591.0, 108.5, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p daddy"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-60",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 193.0, 630.5, 68.0, 20.0 ],
					"text" : "POV sid>>"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-56",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 261.25, 630.5, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.6, 0.6, 0.6, 0.0 ],
					"blinkcolor" : [ 0.572549019607843, 0.713725490196078, 0.815686274509804, 1.0 ],
					"hint" : "OSC (local) messages (BLUE)",
					"id" : "obj-94",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"outlinecolor" : [ 0.349019607843137, 0.349019607843137, 0.349019607843137, 0.3 ],
					"parameter_enable" : 0,
					"patching_rect" : [ 744.0, 425.0, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 283.0, 9.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.6, 0.6, 0.6, 0.0 ],
					"blinkcolor" : [ 0.086274509803922, 0.847058823529412, 0.094117647058824, 1.0 ],
					"hint" : "Server messages (GREEN)",
					"id" : "obj-91",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"outlinecolor" : [ 0.349019607843137, 0.349019607843137, 0.349019607843137, 0.3 ],
					"parameter_enable" : 0,
					"patching_rect" : [ 439.0, 439.737709045410156, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 302.0, 9.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.870588235294118, 0.870588235294118, 0.870588235294118, 1.0 ],
					"bgcolor2" : [ 0.696249425411224, 0.700880229473114, 0.696123361587524, 1.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_color1" : [ 0.870588235294118, 0.870588235294118, 0.870588235294118, 1.0 ],
					"bgfillcolor_color2" : [ 0.696249425411224, 0.700880229473114, 0.696123361587524, 1.0 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "gradient",
					"gradient" : 1,
					"id" : "obj-90",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 673.0, 1100.0, 147.0, 22.0 ],
					"text" : "(no audiofile loaded)",
					"textcolor" : [ 0.470588235294118, 0.470588235294118, 0.470588235294118, 1.0 ],
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"fontsize" : 14.0,
					"id" : "obj-87",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1099.590164661407471, 1048.0, 144.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 53.0, 188.0, 168.0, 22.0 ],
					"text" : "Load audiofile",
					"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-75",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1096.0, 617.0, 51.0, 22.0 ],
					"text" : "pcontrol"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-85",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "bang", "bang" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 1,
							"revision" : 8,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 34.0, 84.0, 1372.0, 787.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-9",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 407.0, 54.0, 150.0, 20.0 ],
									"text" : "- (separator"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 377.0, 54.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-8",
									"index" : 4,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 692.0, 88.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-7",
									"index" : 3,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 608.0, 83.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 256.0, 54.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-3",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 407.249999999999943, 446.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-2",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 407.249999999999943, 392.0, 56.0, 22.0 ],
									"text" : "tcp close"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-154",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 467.0, 261.0, 66.0, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 56.0, 136.0, 53.0, 20.0 ],
									"text" : "console",
									"textcolor" : [ 0.866666666666667, 0.866666666666667, 0.866666666666667, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-97",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 443.0, 260.0, 20.0, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 33.5, 136.0, 20.0, 20.0 ]
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"id" : "obj-136",
									"linecount" : 2,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 443.0, 310.0, 90.0, 31.0 ],
									"text" : ";\rmax maxwindow"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-60",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 355.499999999999943, 300.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-37",
									"maxclass" : "newobj",
									"numinlets" : 11,
									"numoutlets" : 11,
									"outlettype" : [ "bang", "bang", "bang", "bang", "bang", "bang", "bang", "bang", "bang", "bang", "" ],
									"patching_rect" : [ 252.0, 169.0, 277.749999999999886, 22.0 ],
									"text" : "sel 1 2 3 4 5 6 7 8 9 10"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-95",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 252.0, 133.5, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-90",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "int", "int", "int", "int", "int" ],
									"patching_rect" : [ 171.0, 100.0, 66.0, 22.0 ],
									"save" : [ "#N", "menubar", 5, 0, ";", "#X", "about", "About", "SoDA…", ";", "#X", "closeitem", ";", "#X", "menutitle", 5, "SoDA", ";", "#X", "item", 5, 1, "Online", "documentation", ";", "#X", "item", 5, 2, "Load", "settings", ";", "#X", "item", 5, 3, "Edit", "settings", ";", "#X", "item", 5, 4, "-", ";", "#X", "item", 5, 5, "Debug", "window", ";", "#X", "item", 5, 6, "Open", "console", ";", "#X", "item", 5, 7, "Close", "TCP", "connection", ";", "#X", "item", 5, 8, "-", ";", "#X", "item", 5, 9, "Sound", "settings", ";", "#X", "item", 5, 10, "Webcam", "settings", ";", "#X", "end", ";" ],
									"text" : "menubar 5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-81",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 260.5, 35.0, 22.0 ],
									"text" : "open"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-243",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 126.5, 199.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-233",
									"linecount" : 4,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 126.5, 231.0, 189.0, 62.0 ],
									"text" : ";\rmax launchbrowser https://sociald-ist-ancing.blogspot.com/p/soda-kit.html#SoDA-node"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-65",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 171.0, 40.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-83",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 50.0, 407.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-233", 0 ],
									"source" : [ "obj-243", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-37", 6 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-243", 0 ],
									"source" : [ "obj-37", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"source" : [ "obj-37", 7 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"source" : [ "obj-37", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-60", 0 ],
									"source" : [ "obj-37", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"source" : [ "obj-37", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"source" : [ "obj-37", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-97", 0 ],
									"source" : [ "obj-37", 5 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-90", 0 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-81", 0 ],
									"source" : [ "obj-60", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-90", 0 ],
									"source" : [ "obj-65", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-83", 0 ],
									"source" : [ "obj-81", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-95", 0 ],
									"source" : [ "obj-90", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 0 ],
									"source" : [ "obj-95", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-136", 0 ],
									"source" : [ "obj-97", 0 ]
								}

							}
 ],
						"boxgroups" : [ 							{
								"boxes" : [ "obj-154", "obj-97" ]
							}
 ],
						"styles" : [ 							{
								"name" : "AudioStatus_Menu",
								"default" : 								{
									"bgfillcolor" : 									{
										"type" : "color",
										"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
										"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
										"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}

								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "maxscore.default.buttons",
								"default" : 								{
									"accentcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
 ]
					}
,
					"patching_rect" : [ 1096.0, 576.0, 193.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p menubar"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-93",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 300.0, 46.0, 70.0, 22.0 ],
					"text" : "loadmess 1"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.831372549019608, 0.564705882352941, 0.074509803921569, 1.0 ],
					"id" : "obj-92",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 300.0, 70.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"bubble" : 1,
					"bubbleside" : 3,
					"fontsize" : 16.0,
					"id" : "obj-89",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1037.83333333333303, 1048.0, 59.0, 28.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 8.0, 185.0, 43.0, 28.0 ],
					"style" : "velvet",
					"text" : "3",
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.964705882352941, 0.941176470588235, 0.23921568627451, 0.0 ],
					"id" : "obj-76",
					"ignoreclick" : 1,
					"maxclass" : "led",
					"numinlets" : 1,
					"numoutlets" : 1,
					"offcolor" : [ 0.23921568627451, 0.211764705882353, 0.23921568627451, 1.0 ],
					"oncolor" : [ 0.0, 0.6875, 0.171875, 1.0 ],
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 87.5, 70.0, 29.0, 29.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 229.0, 105.0, 55.0, 55.0 ]
				}

			}
, 			{
				"box" : 				{
					"bubble" : 1,
					"bubbleside" : 3,
					"fontsize" : 16.0,
					"id" : "obj-45",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 14.0, 69.0, 64.0, 28.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 8.0, 107.0, 43.0, 28.0 ],
					"style" : "velvet",
					"text" : "2",
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"bubble" : 1,
					"bubbleside" : 3,
					"fontsize" : 16.0,
					"id" : "obj-39",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 13.083332999999996, 664.666666999999961, 53.0, 28.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 8.0, 65.0, 43.0, 28.0 ],
					"style" : "velvet",
					"text" : "1",
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"dragtrack" : 1,
					"handoff" : "",
					"hilite" : 0,
					"hltcolor" : [ 0.474509803921569, 0.694117647058824, 1.0, 0.0 ],
					"id" : "obj-48",
					"maxclass" : "ubutton",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 544.548523071889235, 1068.0, 52.0, 42.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 220.5, 186.5, 125.0, 74.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-46",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 555.548523071889235, 1124.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.6, 0.6, 0.6, 0.0 ],
					"blinkcolor" : [ 1.0, 0.101960784313725, 0.101960784313725, 1.0 ],
					"hint" : "Ping messages (RED)",
					"id" : "obj-42",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"outlinecolor" : [ 0.349019607843137, 0.349019607843137, 0.349019607843137, 0.3 ],
					"parameter_enable" : 0,
					"patching_rect" : [ 78.0, 977.0, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 321.0, 9.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-33",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1128.0, 1204.0, 81.0, 22.0 ],
					"text" : "loadmess -23"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-17",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1099.590164661407471, 1336.737704753875732, 87.0, 22.0 ],
					"text" : "loadmess start"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-16",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 3,
					"outlettype" : [ "", "bang", "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 1,
							"revision" : 8,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 128.0, 79.0, 1055.0, 748.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-86",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 1108.5, 461.0, 29.5, 22.0 ],
									"text" : "i 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-85",
									"linecount" : 3,
									"maxclass" : "newobj",
									"numinlets" : 5,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1056.0, 670.0, 255.0, 49.0 ],
									"text" : "sprintf %d/whoami/epoch~received~%d~%d~sent~%d~servertime~%d/0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-82",
									"maxclass" : "newobj",
									"numinlets" : 5,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1056.0, 580.0, 229.0, 22.0 ],
									"text" : "pack i i i i i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-81",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1108.0, 94.0, 150.0, 20.0 ],
									"text" : "servertime>>"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-79",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1266.0, 94.0, 86.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-77",
									"index" : 4,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1266.0, 13.000001072883606, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-76",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 1056.0, 461.0, 29.5, 22.0 ],
									"text" : "i 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-75",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "bang", "bang", "bang", "int" ],
									"patching_rect" : [ 1056.0, 414.0, 176.5, 22.0 ],
									"text" : "t b b b i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-72",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1161.0, 507.0, 109.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-74",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "int" ],
									"patching_rect" : [ 1155.5, 461.0, 57.0, 22.0 ],
									"text" : "unix-time"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-71",
									"linecount" : 2,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1125.0, 625.0, 202.0, 35.0 ],
									"text" : "1/whoami/epoch~received~12345~sent~23456~servertimr~34567/0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-69",
									"linecount" : 2,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1169.0, 354.5, 50.0, 35.0 ],
									"text" : "1613479672"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-67",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 1056.0, 361.0, 62.0, 22.0 ],
									"text" : "route sent"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-59",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 805.0, 590.0, 207.0, 22.0 ],
									"text" : "sprintf %d/whoami/epoch~sent~%d/0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-53",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 805.0, 553.0, 46.0, 22.0 ],
									"text" : "pack i i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-51",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "bang" ],
									"patching_rect" : [ 804.0, 419.0, 32.0, 22.0 ],
									"text" : "t b b"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-50",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 805.25, 483.0, 29.5, 22.0 ],
									"text" : "i 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-73",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 904.0, 507.0, 109.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-45",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 866.0, 419.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-49",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "int" ],
									"patching_rect" : [ 866.0, 461.0, 57.0, 22.0 ],
									"text" : "unix-time"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-31",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 804.0, 379.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-26",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 804.0, 336.0, 57.0, 22.0 ],
									"text" : "sel query"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-21",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 459.0, 346.0, 50.0, 22.0 ],
									"text" : "1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-15",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 93.0, 353.0, 50.0, 22.0 ],
									"text" : "bang"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-23",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 604.0, 336.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-19",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 604.0, 399.999999403953552, 56.0, 22.0 ],
									"text" : "tcp close"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-16",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 361.0, 39.0, 70.0, 22.0 ],
									"text" : "pingback~5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-14",
									"linecount" : 2,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 285.0, 36.0, 50.0, 35.0 ],
									"text" : "node-1-sid-1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 664.0, 448.0, 29.5, 22.0 ],
									"text" : "8"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-9",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 629.166664779186249, 517.0, 141.0, 22.0 ],
									"text" : "sprintf /soda/local/sid/%d"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-66",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 204.16666179895401, 441.666656136512756, 50.0, 22.0 ],
									"text" : "pack 0 i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-65",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "bang" ],
									"patching_rect" : [ 204.16666179895401, 364.999991297721863, 111.333335995674133, 22.0 ],
									"text" : "t b b"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-64",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 204.16666179895401, 408.999999403953552, 62.833332538604736, 22.0 ],
									"text" : "i 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-63",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 261.833330810070038, 563.0, 44.166669189929962, 20.0 ],
									"text" : "LED"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-61",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 404.0, 478.0, 55.0, 22.0 ],
									"text" : "zl slice 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-58",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 411.333325266838074, 77.000001072883606, 75.833335101604462, 20.0 ],
									"text" : "POV sid >>"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-57",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 492.499988257884979, 77.000001072883606, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-55",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 492.499988257884979, 13.000001072883606, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-52",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 204.0, 326.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-48",
									"maxclass" : "newobj",
									"numinlets" : 5,
									"numoutlets" : 5,
									"outlettype" : [ "", "", "", "", "" ],
									"patching_rect" : [ 204.0, 288.0, 819.0, 22.0 ],
									"text" : "route ping pingback terminate epoch"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-47",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 204.0, 182.0, 19.0, 22.0 ],
									"text" : "t l"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-46",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 43.0, 129.0, 150.0, 20.0 ],
									"text" : "conver \"~\" to \"/\""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-43",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 0.500000655651093, 61.833333432674408, 119.0, 22.0 ],
									"text" : "asdasd/df/adsfrg/svs"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-36",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "", "", "", "", "" ],
									"patching_rect" : [ 204.0, 129.0, 129.0, 22.0 ],
									"text" : "regexp ~ @substitute /"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-35",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 43.0, 218.0, 150.0, 20.0 ],
									"text" : "conver \"/\" to SPACES"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-3",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 308.0, 758.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-42",
									"linecount" : 2,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 471.499988257884979, 567.0, 117.0, 35.0 ],
									"text" : "/soda/local/pingback/1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-40",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 404.0, 517.0, 181.0, 22.0 ],
									"text" : "sprintf /soda/local/pingback/%d"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-39",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 404.0, 563.0, 19.0, 22.0 ],
									"text" : "t l"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-38",
									"index" : 3,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 404.0, 758.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-37",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ -3.499999344348907, 28.666667103767395, 127.0, 22.0 ],
									"text" : "pingback~1/0/m45345"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-29",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ -9.499999344348907, -1.333332896232605, 120.0, 22.0 ],
									"text" : "pingback1/0/m45345"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-28",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 296.499997794628143, 408.999999403953552, 74.5, 22.0 ],
									"text" : "i 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-27",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 681.166664779186249, 77.000001072883606, 35.0, 20.0 ],
									"text" : "SID"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-25",
									"index" : 3,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 629.166664779186249, 13.000001072883606, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-24",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 10.0, 478.0, 135.0, 22.0 ],
									"text" : "0/whoami/pingback~1/0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-22",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 629.166664779186249, 77.000001072883606, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-20",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 204.0, 478.0, 192.0, 22.0 ],
									"text" : "sprintf %d/whoami/pingback~%d/0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-17",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 308.0, 563.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-11",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 204.0, 258.0, 19.0, 22.0 ],
									"text" : "t l"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 10.0, 258.0, 183.0, 22.0 ],
									"text" : "node-1-sid-1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-8",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "", "", "", "", "" ],
									"patching_rect" : [ 204.0, 217.0, 134.0, 22.0 ],
									"text" : "regexp / @substitute \" \""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 204.0, 563.0, 19.0, 22.0 ],
									"text" : "t l"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 204.0, 89.0, 19.0, 22.0 ],
									"text" : "t l"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 105.0, 13.000001072883606, 88.0, 22.0 ],
									"text" : "ping/0/m45345"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 4.166673123836517, 89.0, 41.0, 22.0 ],
									"text" : "ping/0"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-2",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 204.0, 758.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 204.0, 17.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 1 ],
									"order" : 0,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"order" : 1,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 1 ],
									"order" : 1,
									"source" : [ "obj-11", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-48", 0 ],
									"order" : 0,
									"source" : [ "obj-11", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-17", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"source" : [ "obj-19", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-24", 1 ],
									"order" : 1,
									"source" : [ "obj-20", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"order" : 0,
									"source" : [ "obj-20", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-28", 1 ],
									"order" : 3,
									"source" : [ "obj-22", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-50", 1 ],
									"order" : 1,
									"source" : [ "obj-22", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-86", 1 ],
									"order" : 0,
									"source" : [ "obj-22", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"order" : 2,
									"source" : [ "obj-22", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-19", 0 ],
									"source" : [ "obj-23", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-22", 0 ],
									"source" : [ "obj-25", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-31", 0 ],
									"source" : [ "obj-26", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-67", 0 ],
									"source" : [ "obj-26", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-66", 1 ],
									"source" : [ "obj-28", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-29", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-51", 0 ],
									"source" : [ "obj-31", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-47", 0 ],
									"source" : [ "obj-36", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-47", 0 ],
									"source" : [ "obj-36", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-37", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"color" : [ 0.059509769082069, 0.501929938793182, 0.998454749584198, 1.0 ],
									"destination" : [ "obj-38", 0 ],
									"source" : [ "obj-39", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-39", 0 ],
									"order" : 1,
									"source" : [ "obj-40", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-42", 1 ],
									"order" : 0,
									"source" : [ "obj-40", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-36", 0 ],
									"source" : [ "obj-43", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-49", 0 ],
									"source" : [ "obj-45", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"source" : [ "obj-47", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 1 ],
									"order" : 1,
									"source" : [ "obj-48", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-21", 1 ],
									"order" : 0,
									"source" : [ "obj-48", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-23", 0 ],
									"source" : [ "obj-48", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-26", 0 ],
									"source" : [ "obj-48", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-52", 0 ],
									"order" : 0,
									"source" : [ "obj-48", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-61", 0 ],
									"order" : 1,
									"source" : [ "obj-48", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-73", 0 ],
									"source" : [ "obj-49", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-53", 0 ],
									"source" : [ "obj-50", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-45", 0 ],
									"source" : [ "obj-51", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-50", 0 ],
									"source" : [ "obj-51", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"midpoints" : [ 213.5, 357.666666507720947, 189.0, 357.666666507720947, 189.0, 549.0, 317.5, 549.0 ],
									"order" : 0,
									"source" : [ "obj-52", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-65", 0 ],
									"order" : 1,
									"source" : [ "obj-52", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-59", 0 ],
									"source" : [ "obj-53", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-57", 0 ],
									"source" : [ "obj-55", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-64", 1 ],
									"order" : 1,
									"source" : [ "obj-57", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-76", 1 ],
									"order" : 0,
									"source" : [ "obj-57", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"color" : [ 0.501960813999176, 0.0, 0.501960813999176, 1.0 ],
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-59", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-36", 0 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-40", 0 ],
									"source" : [ "obj-61", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-66", 0 ],
									"source" : [ "obj-64", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-28", 0 ],
									"source" : [ "obj-65", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-64", 0 ],
									"source" : [ "obj-65", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 0 ],
									"source" : [ "obj-66", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-69", 1 ],
									"order" : 0,
									"source" : [ "obj-67", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-75", 0 ],
									"order" : 1,
									"source" : [ "obj-67", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"color" : [ 0.501960813999176, 0.0, 0.501960813999176, 1.0 ],
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-82", 2 ],
									"source" : [ "obj-72", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-53", 1 ],
									"source" : [ "obj-73", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-72", 0 ],
									"source" : [ "obj-74", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-74", 0 ],
									"source" : [ "obj-75", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-76", 0 ],
									"source" : [ "obj-75", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-82", 3 ],
									"source" : [ "obj-75", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-86", 0 ],
									"source" : [ "obj-75", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-82", 0 ],
									"source" : [ "obj-76", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-79", 0 ],
									"source" : [ "obj-77", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-82", 4 ],
									"source" : [ "obj-79", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"source" : [ "obj-8", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"source" : [ "obj-8", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-85", 0 ],
									"source" : [ "obj-82", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"color" : [ 0.501960813999176, 0.0, 0.501960813999176, 1.0 ],
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-85", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-82", 1 ],
									"source" : [ "obj-86", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-39", 0 ],
									"source" : [ "obj-9", 0 ]
								}

							}
 ],
						"styles" : [ 							{
								"name" : "AudioStatus_Menu",
								"default" : 								{
									"bgfillcolor" : 									{
										"type" : "color",
										"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
										"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
										"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}

								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "maxscore.default.buttons",
								"default" : 								{
									"accentcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
 ]
					}
,
					"patching_rect" : [ 26.0, 930.0, 123.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p whoami_ping(back)"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-84",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1220.5, 523.0, 67.0, 22.0 ],
					"saved_object_attributes" : 					{
						"allwindowsactive" : 0,
						"appicon_mac" : "",
						"appicon_win" : "",
						"audiosupport" : 1,
						"bundleidentifier" : "com.mycompany.myprogram",
						"cantclosetoplevelpatchers" : 0,
						"cefsupport" : 1,
						"copysupport" : 1,
						"database" : 0,
						"extensions" : 1,
						"gensupport" : 1,
						"midisupport" : 1,
						"noloadbangdefeating" : 0,
						"overdrive" : 0,
						"preffilename" : "Max 8 Preferences",
						"searchformissingfiles" : 1,
						"statusvisible" : 1,
						"usesearchpath" : 0
					}
,
					"text" : "standalone"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-74",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 1,
							"revision" : 8,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 463.0, 240.0, 388.0, 253.0 ],
						"bglocked" : 0,
						"openinpresentation" : 1,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 0,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 0,
						"enablevscroll" : 0,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 9.0, 107.489563000000004, 29.5, 22.0 ],
									"text" : "set"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 9.0, 162.489563000000004, 29.5, 22.0 ],
									"text" : "set"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 9.0, 437.489562999999976, 29.5, 22.0 ],
									"text" : "set"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-3",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 9.0, 391.489562999999976, 29.5, 22.0 ],
									"text" : "set"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-2",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 13.0, 342.489562999999976, 29.5, 22.0 ],
									"text" : "set"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-121",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 263.0, 437.489562999999976, 150.0, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 7.0, 212.0, 54.0, 20.0 ],
									"text" : "out/error"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-122",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 439.489562999999976, 211.0, 22.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 59.833331999999999, 212.0, 290.5, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-103",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 263.0, 391.489562999999976, 150.0, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 7.0, 167.0, 54.0, 20.0 ],
									"text" : "INScore"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-104",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 393.489562999999976, 211.0, 22.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 59.833331999999999, 167.0, 290.5, 22.0 ],
									"text" : "/ITL/scene/RecordRect x 1.5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-75",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 172.5, 222.489563000000004, 79.0, 22.0 ],
									"text" : "loadmess set"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-17",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 267.5, 168.989563000000004, 150.0, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 7.0, 47.5, 51.0, 20.0 ],
									"text" : "AMEX"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-16",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 162.489563000000004, 211.0, 22.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 59.833331999999999, 47.5, 291.166655999999989, 22.0 ],
									"text" : "0/EMPTY/ml-mode~0/0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-48",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 263.0, 340.489562999999976, 150.0, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 7.0, 93.0, 47.0, 20.0 ],
									"text" : "Listen"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-46",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 267.5, 108.489563000000004, 150.0, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 7.0, 5.666667, 40.0, 20.0 ],
									"text" : "OSC"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-37",
									"linecount" : 2,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 108.489563000000004, 216.0, 22.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 59.833331999999999, 5.666667, 290.5, 22.0 ],
									"text" : "/soda/local/broadcast/0/EMPTY/ml-mode~0/0"
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.5, 0.0, 0.5, 1.0 ],
									"id" : "obj-56",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 242.0, 279.489562999999976, 113.0, 22.0 ],
									"text" : "r amexserver_listen"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-42",
									"linecount" : 2,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 342.489562999999976, 186.0, 35.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 59.833331999999999, 93.0, 290.5, 22.0 ],
									"text" : "1/EMPTY/ml-mode~0/0/1619013288139"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-39",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 40.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-45",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 242.0, 40.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-60",
									"index" : 3,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 277.0, 40.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-65",
									"index" : 4,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 312.0, 40.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-104", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"source" : [ "obj-122", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-16", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-42", 0 ],
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-104", 0 ],
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-37", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-122", 0 ],
									"source" : [ "obj-39", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-122", 0 ],
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-42", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 1 ],
									"source" : [ "obj-45", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-42", 1 ],
									"source" : [ "obj-56", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 0 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 1 ],
									"source" : [ "obj-60", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-104", 1 ],
									"source" : [ "obj-65", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-104", 0 ],
									"order" : 1,
									"source" : [ "obj-75", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-122", 0 ],
									"order" : 0,
									"source" : [ "obj-75", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"order" : 3,
									"source" : [ "obj-75", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 0 ],
									"order" : 4,
									"source" : [ "obj-75", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-42", 0 ],
									"order" : 2,
									"source" : [ "obj-75", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 1158.5, 758.5, 52.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p debug"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 12.0,
					"id" : "obj-69",
					"lastchannelcount" : 0,
					"maxclass" : "live.gain~",
					"numinlets" : 2,
					"numoutlets" : 5,
					"orientation" : 1,
					"outlettype" : [ "signal", "signal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 1022.090164661407471, 1281.737704753875732, 129.0, 39.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 53.0, 215.0, 164.0, 39.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_initial" : [ -70 ],
							"parameter_initial_enable" : 1,
							"parameter_longname" : "live.gain~",
							"parameter_mmax" : 6.0,
							"parameter_mmin" : -70.0,
							"parameter_shortname" : "live.gain~",
							"parameter_type" : 0,
							"parameter_unitstyle" : 4
						}

					}
,
					"showname" : 0,
					"varname" : "live.gain~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-72",
					"local" : 1,
					"maxclass" : "ezdac~",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 1022.090164661407471, 1348.0, 46.5, 46.5 ],
					"presentation" : 1,
					"presentation_rect" : [ 8.333334000000001, 217.833334000000008, 42.666665999999999, 42.666665999999992 ],
					"prototypename" : "helpfile"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-67",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1078.090164661407471, 1085.5, 130.0, 33.0 ],
					"presentation" : 1,
					"presentation_linecount" : 2,
					"presentation_rect" : [ 229.0, 217.833334000000008, 112.0, 33.0 ],
					"text" : "click here\nto add audiofile"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-63",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1074.0, 1166.5, 83.0, 22.0 ],
					"text" : "prepend open"
				}

			}
, 			{
				"box" : 				{
					"hiderwff" : 1,
					"id" : "obj-51",
					"maxclass" : "playbar",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "int" ],
					"patching_rect" : [ 1157.0, 1241.0, 196.0, 19.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 8.333334000000001, 264.5, 336.666666000000021, 19.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-50",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 3,
					"outlettype" : [ "signal", "signal", "bang" ],
					"patching_rect" : [ 1022.333333333333144, 1204.0, 89.999999999999886, 22.0 ],
					"saved_object_attributes" : 					{
						"basictuning" : 440,
						"followglobaltempo" : 0,
						"formantcorrection" : 0,
						"mode" : "basic",
						"originallength" : [ 44167.680000000000291, "ticks" ],
						"originaltempo" : 120.0,
						"pitchcorrection" : 0,
						"quality" : "basic",
						"timestretch" : [ 0 ]
					}
,
					"text" : "sfplay~ 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-49",
					"maxclass" : "dropfile",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1074.0, 1078.0, 130.0, 48.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 220.5, 186.5, 125.0, 74.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-117",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 918.0, 434.0, 262.0, 22.0 ],
					"text" : "/ITL/scene/version set txt Hello W"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-113",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 793.0, 496.0, 363.0, 22.0 ],
					"text" : "OSC-route /broadcast /7000 /audio"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-102",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 1,
							"revision" : 8,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 34.0, 79.0, 1372.0, 787.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-19",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 21.25, 291.0, 228.0, 22.0 ],
									"text" : "/s"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-17",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 276.0, 291.0, 19.0, 22.0 ],
									"text" : "t l"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-16",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "", "", "", "", "" ],
									"patching_rect" : [ 276.0, 239.0, 137.0, 22.0 ],
									"text" : "regexp _ @substitute \" \""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-15",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 14.0, 190.0, 239.0, 22.0 ],
									"text" : "/s"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 276.0, 190.0, 19.0, 22.0 ],
									"text" : "t l"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-12",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "", "", "", "", "" ],
									"patching_rect" : [ 276.0, 136.998047000000042, 129.0, 22.0 ],
									"text" : "regexp ~ @substitute /"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-11",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ -8.0, 91.998046999999985, 267.0, 22.0 ],
									"text" : "/s"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-9",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 276.0, 91.998046999999985, 65.0, 22.0 ],
									"text" : "sprintf /%s"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-8",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 14.0, 33.0, 242.5, 22.0 ],
									"text" : "s"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 557.0, 118.0, 19.0, 22.0 ],
									"text" : "t l"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-108",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 732.0, 178.0, 95.0, 22.0 ],
									"text" : "/ITL/scene reset"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-107",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 732.0, 136.0, 251.0, 22.0 ],
									"text" : "/ITL/scene/version set txt \"INScore version is\""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-3",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 276.0, 348.0, 65.5, 22.0 ],
									"text" : "t l l"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-2",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 506.0, 523.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 557.0, 40.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-98",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 470.5, 430.998046999999985, 189.0, 20.0 ],
									"text" : "<< from INScore (UDP port 7001)"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-97",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 323.5, 428.998046999999985, 97.0, 22.0 ],
									"text" : "udpreceive 7001"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-96",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 322.5, 391.998046999999985, 138.0, 22.0 ],
									"text" : "udpsend 127.0.0.1 7000"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-89",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 470.5, 391.998046999999985, 189.0, 20.0 ],
									"text" : "<< to INScore (UDP port 7000)"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-100",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 276.0, 25.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-101",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 323.5, 510.998046999999985, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 1 ],
									"order" : 1,
									"source" : [ "obj-100", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"order" : 0,
									"source" : [ "obj-100", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-107", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-108", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"source" : [ "obj-12", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 1 ],
									"order" : 1,
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"order" : 0,
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"source" : [ "obj-16", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"source" : [ "obj-16", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-19", 1 ],
									"order" : 1,
									"source" : [ "obj-17", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"order" : 0,
									"source" : [ "obj-17", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"midpoints" : [ 285.5, 495.0, 515.5, 495.0 ],
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-96", 0 ],
									"source" : [ "obj-3", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"midpoints" : [ 566.5, 328.0, 285.5, 328.0 ],
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"source" : [ "obj-8", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 1 ],
									"order" : 1,
									"source" : [ "obj-9", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"order" : 0,
									"source" : [ "obj-9", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-101", 0 ],
									"source" : [ "obj-97", 0 ]
								}

							}
 ],
						"styles" : [ 							{
								"name" : "AudioStatus_Menu",
								"default" : 								{
									"bgfillcolor" : 									{
										"type" : "color",
										"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
										"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
										"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}

								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "maxscore.default.buttons",
								"default" : 								{
									"accentcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
 ]
					}
,
					"patching_rect" : [ 460.5, 896.001952999999958, 466.166666666666629, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p to/from_INScore"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-99",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 460.5, 930.0, 19.0, 22.0 ],
					"text" : "t l"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-79",
					"maxclass" : "newobj",
					"numinlets" : 9,
					"numoutlets" : 9,
					"outlettype" : [ "", "", "", "", "", "", "", "", "" ],
					"patching_rect" : [ 460.5, 830.885246276855469, 335.0, 22.0 ],
					"text" : "route text2 audio whoami colour1 text1 daddy bubble EMPTY"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-70",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 402.5, 702.833333000000039, 43.5, 20.0 ],
					"text" : "<<SID"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-68",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 125.75, 781.333358999999973, 126.25, 20.0 ],
					"text" : "<< chosen participant"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-64",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 350.5, 702.833333000000039, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.865343332290649, 0.060312297195196, 0.959620356559753, 1.0 ],
					"id" : "obj-26",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 478.5, 446.737709045410156, 113.0, 22.0 ],
					"text" : "r amexserver_listen"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-31",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 724.0, 33.0, 100.0, 22.0 ],
					"text" : "readany"
				}

			}
, 			{
				"box" : 				{
					"autofit" : 1,
					"data" : [ 88062, "png", "IBkSG0fBZn....PCIgDQRA....N....4HX....Pf7hx8....DLmPIQEBHf.B7g.YHB..f.PRDEDU3wI68tFbjdcdlXOumy46ReEnAP231LC.AmKjnknIEHMkkVa.ZSuRwliS1MAylxxUV6xUQkR1Na9Sp8WIM6xwUhqs1j3RqbEyp133TVqSAraUVWhCsE8NChtXQaBKIRhlbFNClq3ZCfF80uamy4M+nwbgWjjskMkGZ9TE5F3qQ2em9667bNum22m22CvGf6WA8C6FvGfevg7G1MfO.eOg.8uGoN5Gm9OOkBnoD8Ig7ODaee.9ADe.A7uuhJUT3UCxf.4fvK+HPjZT3OPIfAFDJUV3LrGROt.+bKXPsZleX2bQ+ACt2ed6G+Cv6B9fKL+fCBUpP.KHvE.vB.nVcFytFipUY7cYFJFfn69Z8+LpV0B.LekyqVsV8BQq+mMls4MF2D1sHRhGDvJgQ0CxLsgH2APjaGbxGcSrvoODUeJy2sy06149s9E36466s2G4s++9t1Ghu6mK81N7Gf6Ae.A7u4fvRKIvZEIT6AcvVrOBBjvIsEYSLHIqAS2Sio++L91DquqnREwhkKS..q2XFQJ0DCt1W7ENUvlW4AzQcONarEsFjkMVARPDDoBfAMfwtNJ9Hemgl8LabPluS.d9OsFee5jyKAIlcVIbOAg3avndQKpuBi0.ipfumNDL.nEWZIw5uXCAlq+Ayt4l24yuT4x7xm6bukuaL.PEPXg4EO2EVwVs56n8b2Ac9tzD+dds58YP8C6Fv8ofl624kUq94FVhik3h06jA51CAarOrpDzUFAQqH7cxzE67oZATMFee5XUXlYDX7wcN9sxl5M9Babbzc+GAvdF3jaLxMcVP9DRr.FnflYjzIBpj7naP8CtR8VvKwBbdtBtvcHDUQ0aeN4idfvryJQwhtP3RHdFCb0FjaNKJlkWt1JLVF.nBCTEKtzRh0aLiXunujbjU2RjjKG4NFvFWMfhaJE0qulEy+6pQmOrAyrtEKeNKp.BmcNIxmRrvByipXg9smpU+9Q7v875+CFR3GP.+qGNxbyEUq9hvEVJGdkMGA5Nih3cl.5logvKAxAB.61AN4piqL1MvIuzd3xm96IIzKedQq3ztAa1KWvgMGisIyHb8lQP4ywxg7.xvPK.zVAhMRHNfQTyCgRNJZ9laCNLDnXbeafApg57hXIZVrFWEUu6Ix8DDjP.kQdW+6TDncfYwEAvxfwhko4m87h0ajilofOU7i7jbceMht7qKV6ULtAW6RoarSqLPZjfyk.4dAn4w5N9y9ECe7s94R9cFebmIEQT8h04Jn5acVvJU9dS.6ST+GLjvOf.9WGToBMa4EU67BRu82NUVzauwQ3EOMRZbJnObRvIYAIMv3E.hZhH+qibeXEF3z.Sd08wJOP32qOdafP1X6nzwM2Zfj3fgXCkibcErSZKrojP3ZAIR.CCzFBhXEzcGDgaO.BRs+TSUWTNSQzHUHiU6SBqBf9lJem0jBvtLr8rvlvvDwHtEi50XbNXA.iEgbislTNoqCgY6ZyM6I35e8BXi8dH+NuwaV.0SMF5dyQALo.uWBvqeHZWZ2std79Nm7ms4W56rq4wdhw3mHSQwLmcN4YOKvp.3EWeUKPUr7ZUdmjqxkIr1Z26w+GDqc7CHf+UGz7Krf.WKinVfqG3FEPxMmD11OHTIy.g+PfcbgfX.gAbxvPD4gnq2CsxGA2bwXwkRvxm66pGKauAP3AZQb68D1vVJi0QxHRyhXlMBFVxBqeBXV.KIgA.lPEhHWXSKLFOZM.LA.lXtbTsUqynBv7EKRkJWFDfcou8KnehtSQxLiSgY2yFeBWtbXJFqA9dcFyH5usnS7LzzYFFadIe4M9N6mqyqrwTX25mAINSAtTQvc8glSfrcKzo0FnWuqdCdxq7u828qt6u93ortkxYla7bhcUsEghAHL93lvtZCPUMvR..X4aS5t8y8cF06liddeII7CHf+UCzcLcZZfTr1M.wEABOFbkiCjZ.HSAvpPHAA1nPbq7HIv.p6LvbiNPcl8vry1D3nYYt2O66AFiwP1PM3jD1PIrIxZS5wvB.KrvPLRjLLVFZ1Bix.aVMvv..XnXWp..ZjJjWFqwKVtLUHWN5hGctV6bUHLaMa131TmKebt37KauvB21v0ivZqwG+S7I.hZK1e+wwNeicxT+p6dLzXiGEwG7S.g2LPkwGFo.pPMLIAHo8NP1cXbXbb6qOcz+wWYmVS+IgXBYJguSAGpWWYo333D+nnYKNOUq943YKNO8rmsi3o8Cty0fFgoXb14voamkqWeEds05es54p980as2WhOf.98G2oyQohEEG9W5KCha4i31CBVW.xTYfR4.Ud.RxvZEfhTPHT.hLvXGE1NigDNOtxvNXokL3buyYA2NNlcJXzYFzKzvoZa0hCMZSafddVSaEHBvZYXrBXjJjD.v1DP4aCmhcg4j5QMt2ost4psYf9N2wKedwY.rq.fIl6rD14SH6.fhSEwWZkmlKrxm1t7Ru0ABJL0TjtUKB2JzYu02XPr4FGGZyC.mbSA4PiCoKgjtVnCLHoYJvIRniafn3i2cKuFW9OamtN+LmlfjDRYrSZGW4vrju1ttIak6.ZVLOPw5herLEEy3NtPIhH.fhVOF.Hd311hEm0VrXQa85q7cMbN2uCwOra.2Og0pAb0qzVh8ibQ2M8AElBNND7xyHUNI7y4gzoRA+7dvMGfmGAJIMh2IGhZkBu51N3yU7c0IDEBC4BiGjLzXtcH+h6afyFrwtkMJpMh5Eiv1QHnYBhNvh380PeXHzcaAQ98fyoa.ubINxX9.2XdsZ0sUwErXoxTCeeJJyaHiF9UknREZyUayckQ2oybgY2jl4YgnXw4ua6pZUNJN111LDes2HRs4U2u.Zr2jvvi.dPCjEaBZr8gX75PL5APNbGHGv.gJG3nioCbJcoWYP0pMhiQXujCz53PcftUhTaFH1zX8UsUuvJVTunczjTbrYOVa83Fgwl3311tc0ljjTL.Ptbcdecnx9.kv7WMPXgEn50KgCtfuD6birH9RiAQ3XvycH3mIKby3AkRAGkBJkBTr.wcRPXitH5vcPP7UwA41CugHA326NgJfAHrvJzMcdZDlKGxWHscy0HQ6cpSVCk.iWLXoE5HB13DXrM.K1CHZSv3xfF+hPme2Yi9hwilcDSqKtg42FOiEXEFyNK8S8XOlrUxeraJ50UYatOl9Kbn9vlSykN11P.Ahu3yhL+7GSMziXvmqXct5JfWbokjCWpj3U9+SQuweQJu3qd3XnygmDVmIfzOKPNBjaHrTDfMF1XC3tLzMMvzIFJmC3LO5sZ+wd1l+h+6+eH4a7fmxrZCnu1VAwei2bkjkWqBipqvq76cc9yOzksGafGjuz9wlq8Gth9+r+IWWO7PW19Ye0KaqUZVrxqESK+02h9zy.fZ+PrGveGgOvDz+pg65AQT1.+KFgAZzCdCEAXIxQ3K7jdL4vfHFVsjMRhkLAI0EI8ZffaEbRuGl+IQA54uWmJTAzyUFDs1ZLVaM8hKtHF5gGd+atwS8Fn9dGfvFSh3Vm.I6bBXhEPkYanFbe3OXCDY1.QpsejM+rgSfee8KTeQMvxuk0X1n22TkK6N9adk0UQWqirJdMyhnlcqZOM+zytLN8O8nNoU9R+V4AJCCpTQrawhzZe6Xt0a3ycdMgEgCxvNHC1Xg0xfhLfcS.zZ.KCR..RBR5ARlA179g5SJOStbDgJX9MVvTpXcdWrlck0V3spPnp.0J+qv..KWcEtJ5KH..Pys0Yn8t3EkW+ZSAj45DVbQfkW9suF56qwGP.+qI9jXK6MDaEUqmtNJMwNvwcBkDiRjUADKAfEB.ifzVWmtrxaSXrabB8Nc+nY2yLNR8N67TbdhQUlpB6x.54WXgNOhtPxq7ke3dvZ5f3K1BF8AfCEvc38P1wO.tYahd5FnykO7W9S96qej+YSQO4uzx86.2GzbSLgr9Udi7a24FC1Y8nTaVSEl5id7Tuwq+T8d.0n5WrQAS3j.mdngvwcGgtPw4ITsJ5LwYoZOeHiltVD6YgcfPHOVan2HF5.APaIjRFvZfMAfijvF6AAhgMsFJuDHdX6pqBL+BKHNStbTCTDkpCFUO26X8bKuVQBnn.KtDvxGorlJUnU2ZBbxTdLb1.H9jD1c422YN5GP.+qEpx4vRZUuazFEFbSjZzA.YFgrwCfj8krV6C.PBYhfSZAk3llbCeEbP2qOZiK07A24JIUwUdqxRqLHbuqyoZU6JUqZQkJVfELX6hwfO0gv4guAbRDHKEirohfpc.5FGhcNdzi7j.X5oQUbcFnh.nLgOYVUiKMv.s9y273rYnQia540Y+blfCx27UcN3vWkJzDsdz1O+N859acv2LZn7CgKsZ+1wpu3WhvpKhesSdYDoNi8Kj32aGwPG.yMZBaCFHVANh.KHvwBXZ3BzxClnCfxce3LRCjY53YJXHT2mtwq3R4FO9snJmiPe478+wjR7cZIg8zNX7MXHML97gVb4V1K+ryYQvqv.qz+cTAzQ5J38EyB99tQTdOBB7y80x3ZSlzDr2YHq9Tjt8wswcGfHHXgWOn72kYbEg6.0zQlqgu1y0DnVBdacb3yOuB45PncVldpUzu8SzrXI2IG8zNMykV9m2vvXTCiYBs3Axavm8aqqf9wOqLJSqghzAmbR4m0J8Q11CgA2dZuvu4oS4sWIiFpn.kMta5XDMXKvN6AeuMwTm55OxmvtegG+aDUpdUFqMqr8KUiN0K7qg+oS8Okpmon5B8ng+J1lm5M69c9HHYym.BZbH8DfYFvBvQBXZm.DdE33+WfLS7swG4W3hK9yeX61a4Ru9ANzzSCT5I5ZQsZlkeKdA97JTrnOD9ogeRZHcbPh0BKhf0FB60Cv+o+Ghw3O+QumJ36kH2ueCevLfe+Q+Aota.hY.Xm6m0OT2axsu1ZOPT2ct4NTqKdbNoaA1FHg6.coritUpbk1L6zmr9C8gBauxW6cR9tCxWT.+av7RPRmCukPTTFv7KkKM0IbO6Wsvg1uQsR1UK1lw1qy.qw0PYZVTjvrEEsZkW9JIFOP7nHT7vXqa9vQN1SlD3UPpbg1nXPBBtIgPmrOTQuIz6aCZbByY79kOX40PzRkWFEKOqS7HKw3q9QLE6NsdtBd8hZFtaWU1qsYLW.11DHynfM9fkI.pV.XanRcI3L35X3I1c3S0NFyBDz0gLZI0IVRQ0jzwmcVf6FKTBimyEB2AgLtD3vgPx9YfQyP2pKrMafNWae7u+a2.Gft.vf6UVcuO.2uS.uK4.3dE7622QGeaoCz2aT4cZovpe5GW+ru7K2MkMN5adq1cvAWqA5cyrvxRjtWTgwG4fg+I9vMO4oZGkqy29dmYidaj49HSQAlYNtRkU4aqcxJnBAzOrEY8Gw9gFv0NDZyqtReAWu3QjuxyVTzsaFwZ.oVwnGBJdJjbvr.1SAY9RVUlzV3xPJI3JUv5ngJJMPnVzaqNMqcn8VE3jm8rmUmM+KQO5FApVkkz5e0mKHv4qvtcRhdxr4N3llRWoQxd5.C1EhNm.FJMjNIfReHP5sfepqhBCbs7ejSdvGZV+DftHJijFMVRIspSIs.cSzCKtzR8mEbpq5ADVBldmA5sNIR1eTXamAl..Si1Puy1HYmafCRtJFX9svI+46hmYSy22rK49Hb+HAjPkJzhkKSElYFQzvCK8bcE8DBJ5W9W1hVsLGYly2MukQ24gu+Rbhd2DOLCPOWEP0V+wsdsPBdAzDnX.JbBIxcFw.SWBm5QGyL0S3mTvcWaiN.KtzRh6H6p6k7cgUrXg4wpWpC8k1bUFUmW76fyPEl8+FJarKM3H6It0s1C..GGi.LGrUVcAQMTmmEEoIlKGg.fCLaQq4ORZnCmDb2SByMmFN1ggbP.oJBJR.xQ.Gh.KsfCDPGLLEs6C2YiXyd2pzgat4I57a1NHdohPqOrqrtYbJqC3AFnk1CEZuP5iGrJ+PaE3rykf55ihvN9fxXf6n8fWgCwXYNbnG1b3O0OdbHlsK..FwWR2nkfzoTDpWGiz3Rztm9zDl+7JbUp.D5y.6V+nH3M9QfduRfSbgMDf6D.S2sfv9l.SlBAOHvkc2Be4y1E3CHf+vBzhKsjnvLyHZTpjpaXZGajqKKkdTCRJiXsvIevFEyDL+4OudkKbg2xMpEKWl1s3cCD9JW3BVTsJvaecY.Dp.hpB6aWWhbEHvYmS9bGIep2L6IH7GcYrwKc4vmpZc6y9U9cjMJUR443HRasTT7vR36S6Vud+ADdaid2+br..f.3eoX9oJJVHNmHkRSCnzHpUdpXdf1oBso1aC53MmTTXVedhT4tanFRExs5NNuQTGOXtwnPr2D.sG.fcARY.4YfP.Pj.L.TB.qvRfSSQclvljzpw0t05W6U+QNHL6+xCuo++8Qs++dcIvuB0wMlKt2v1PyVwCeqWs29XSSwOymoU85i0.s55.iqERYx.yzJ7G8Gqc7CLqNF..g8yvisG457IvdH5vCQLZQQstJgq4pvM+I7fswwgY+GB5ClADUDBUZXLDHAAqTBJ0jf7EvqPDbOUa3LV.1INB.I+.2S5umf6uHfUpPElYFQO0jtIWLse.LYDTbVKyYrQBmnN1DxvGZB8Nr0AOTuEKWLY2h0skpWmusjrR63P8jRZrvPaoxk4keWxvaZQHlaKHP+YHOhvzelvKfphoamUNcIGBxQnSU.X65O.M4SB.bYSCeeJWpTJUKoPm2XMBAkF.mILjW4cJxX.rjDXVIFOTg3LxUjthCbThO7g.e3AZhwbxvi5kX8B0bciGUDa.fIQ1XWpiaL2HUHu4ps4O+IyyHtgGLuQQvlgfqqDRWKffATDHG.glfTK.AivQwRHUPaSCXKkzNX3q+Z6l2YjSF73e1Y5d9o9UkckSyY.P8Lcs.2BuHJXQkMwBehqX1cmw6hC8QmXKKSGyScLgEdCZ8xGJ.5Kstw.P5w+i4icrZ3V2BbvdAT6ajRrwKl0E1GYPnu4If9fSAQpggZj.HR2Cx.MLcDPjwA1fz.zXPN7gvo3VPNztnWgC.PH9.mv7dNnm8rmU1SMo6NWAY5rWmgh5hhrInHHZPADdQAIglnvFwgI6Pd418fRCdXIu78JLSNcCeeB99BzxS5Mrk296x4XpJm2St0K3EjwWNakxHdnGU27fArnNP8+xqXdppUMKk+JjegGVL1nQ.hXxIcJQlRODyUtLcN.D0fnPOgHaK.+rVttTRduKmKr3q4fcK5hRlTHxwAgGJQcR7p8rzq1VaRcXh4+bml5xVQxjDL9.ntwifYu9qKDiXajJjqg5r6kKxXl1RDenKDNtPHEfACkffJMfTY.EAHHxwwBIqcsbhjHjxxhTZNUJW120n8j.eFJvYRdTbYz4nF6Z0JZWFOkcwxKIRO9E4G+AtbxN3QPZawiHBYPCiAaCeQZigGC..++BfqgCtxtbZ.DfbTx02f1Y6GyAbbZX6T.b7vfSmFhrIP5FBnzfgDjPBHc.oRAUgAgZnAfJWJn7j3f+1ra0Obw8ODvJUnFwkTnWlzQwwC2pdzIh6DLEhaOIH8vBgz0pCiYSXKR3riWtzqGwpqKoL6DMro4Z+Ecsst4svGpzHB2SMDvDI21TvamFLhe5G7+5Ts1GESFZlgEH1CHORYklbCvFcJSTowevd68w2p84N++6cpzZ4jEWDnblhhgyYjvIvBzWSmMfwVPJHcrw1HJxfVsLWrca9NNeYokji9R+39NVmAEmDCjwWlCP3F2IqpWnvQaAEGjna1V062uANDaltApWO4ed5w4eVyVj+n8Eccp81fvpWBKhOMVFuLijdVHICTrABCfiU.gfgPZgxSCBZBZCwIJxzxgrccsFiPJXVJEFgWJStA.ZBfNtwLvI5magnetEd6aE859GY26Zogb5+TDgE.vz..H8Qud9j.F3MAvkQ9jcXLYp9uuucJd2Me.p8McIX0BvZEfQAxJAwVXgBjCAgmDVsCHsB.BPBIXi.bj.1ruuJzY22P.WrbYBtYcaV2NPXK4XBHONSpSvBuIHxc.RQJgxyPveDoRLhelT4MZG+C2g0lV9cNw49h72ZzBhW3Is7mbwgnbVfcKVjPkJh4wBhji+gR0IHdxvX7PVqZJGW+z..FKagkhIRFHD18JNp6FiN0+sa+44es8+xe8qE7La93wO2YmSh1YYTEbixqaS+DCqgqqMioq4JWYCyQq0jQkJz7UNupSiejL6K5MlTfITNpIHIUPoXekqqLssu.1RBL7vAhN5iy00yJ1NJdva868Zlc+XWwMZb.3knn5FOpvruHMy+h4D3S+kXHeLMTtcgR0CRkFRhfzJfLg.oLfLLAMyIAViIP.SnE1HMjtArxqS1w7BkCps3YyQu3yGxO8r92oy9rnHUAUnpm6b1EWZI3MSC9hW6eGelb+anF9AzXtm3sPL1NNGWHbcF3YAvbnw5qS+4++zku9UMLPIfDkErJFLB.Gqgkj.jKH+DHE.LyfBXvALzs0P1LFz.FH42WX54sw8MDvByLi3p663E0JrPbOwXVgZTkW5gYY5rjx5KcjPHfPfXORZSSJkmNhssNzr2FeqvM+sNyThuYPW0+y+YIIA+BFNG.djImTl6rOq6deqL9c2uSQXUmIIz73.lSZRnzfT.frLnD1RABvaqj7EUZVMTVIxO3LG7kwK264d7GWCbT9pctyYm+7mWelb4nO6W5KYPUfkPMZMbdQ0p4nC+e8Ayz9xcJxwxYLNhSRZLszgGgY3A.H8AHPBahURtggJVsm1A2JUFUN6CIU+u75Ytwu9NMzdCoHfaATaYL7omWBTMAk9iiQ2RGBgtIfdDHYFRiKnX.AQjMv.caoQ2lj5.FVaH.GRRmFoJLPiGdNutlvc0quNvEQadtT8SqoIlKGA.TCkoEwRDNGvyiMM.qfekWaVkegYT4TsuSl0Dac42nca8A0yZq9a+h77ydZZETjwW+QIz8MLvofAROMDC2FI15.17fCK.Rofvk.mw.o1Bygwvx.HrMrll.bOnzuuwCn.2GQ.WcUfwlP3zIPkiM5gTRdPxiRQtjCXgP5xBoR.h7gvQPDw4XvEM8LCc8WgRUW5GcBn3mQky9Fa0zTZ1Xda+7R00ylp4VQCE0FGyMkXFHcNAASQRHct8XsBBrEPyfRoiMIVCGEGJzjQvcZMkkPk60037JO0SYVA8ii2DXKYwoVVNM9L.Y7UaekvBJoZRkhlR4Jl10SbbofyobAQDXR.BLHGuTRsTjSXPZgg7ShLNtNr4hmHciPSTyhZE0UtFO5mDjrdDUYdPUSFIBYzMfo49vb3XPlTfnPIAn3jP.SnF5VDLcHS7gVAG2QpRsgJyfWevomowDeXmv1MFxNWvtXEbAaoUOJH+neF1iUO5Y.L6pE4ZnFOS357jhH5dyqMK.NsS.8.WXEKVtBVA0Q+3YFyXpbVzdTCFTFAsXOv5ahj8FF1NCAH8gBJ.WF1DKDxP.u8fHyUfb3a.djFHnw6a7.Jv8Y4CXhNTnihcLwrGaIkTBHUrU4AlD8qEDPvrPAqTAlYHCay9Xy.ul5XwDoz5+qJ3qu7+hOeBpUy3cfi3VWRmtWK4HgwNGONASPvlQ5HYoBZkCLRIrj.fHnXlxYMhwCZqOduC0iu+FIEZdqDefEtsGSuM39AQeAwwO4MD4FYbQb7kDnbwrYxHlLUFmYbSIlR4wkHGNsqOI7RK0tojFRPDI.I8f0wygEJWeFbQcDOSRLeRbBYo+zlC3..jISY6u6K.cX1AsUWA1IdrQivfmXejZjsAI2E5fCPzgsQT8HDrsk6sqgCaFwAMC3ndQFsdOg+Pug+POvkJLSgFF+NI6FYNZXmp1YwZb4YKJNdy72oexlq1l2b01b+0DtHd7GeF6yL9WI9odpUhepe0Uh+U+sqG+LU9JwOvuHznZE..LOJR.4HfPFWunEo2KAAI8.kZKPCdEvcuNz61.5siQ71DR1PA8dLrI6BJ+qAuodEHm9JiyM2Ga0N98rNbuGf6alAD.Pmv1ftrMNfrREXoCHkKfPJrfrFvjfMDYiMjQHr1XJRGyZDFBp2NVgwZJd5Ar.U4kOG3etuVcAHaZojGxnEkrZTvZHWoC.I.empJKCPBPrlklXc1jv3gsPMDEi5dCojnBtWERQ2l7Ud1hhAa8vh8a+snKgTX7Q0CJcTy33fGRpjS.ImWJghbdqqqwZ6Oqq1BXYVvVJMC.kBi6lUcheOJtQ9X2Fe1KCCpTgW9kpl7ru7KKesuc63vnA2+faPWGQ6QPGzf0sKBcRAXBSClML40FV+XX4XHDaX8m90K7fk2Xzhw8500XSE3Rq7hqaA5WVC+ih+TTmv8DtIiSSLGLe5U+RFbmxc3RBbxeC0p32vCVsCVyx0dEYB7FzhCRrXtc0X0GWuBJyXpoU35uNi4JYwL4M3hgAX2z6AD.D2FvzqCDNiBJLCfuBf5Ag2MgyHWBhbWAzd6u0Verv9Nx58l9auWf6qHfIQ9VhiizQwAVqPy.BgPHTNDHGogXvlPqTm.RnPLQ11fMcQnMdHYG69xCs4voQETgphpby5JoNf8IEmUDqyCVjF.Rhs8mLE.b+PWSTeRHrV1CLkyZo7RImJSF.Lc8aKcLQYTlVF.KNaQQwt0EsSlPjYvmT7l6MCFXX4fVMMMIooENbVgDBgfH.lsVyQyxvrf.XV.pub4HPLjt9dhjjAcT7wiOkX+O6AY5N6RyF9S8nOJ0x4Wjh.vj+ikZfNMuhxOdmq836iVarN585E43qOJ5bPQPJKbKrGnwaBG2NHe5FQC9g1UWTzp4.hjR05hW3EaYwr2s5j8l.3.iK0Bagtq5BfpLl+78qmguliCRhy.YbNDEkBj0BQ9.zsSLxqRPmLcwhK0C6Vj.5pvomfwGSkL2Dgby+Q40W9qsWG7e7aEB+8a.6PuIrdi.Q5gfbTIjt8fLeCHFnNT5Cv5q2urN9tHbh6mw8MDvra1lwosINNltPHZYzTOAYLFgQvtFgPIYljVHHKXjsXgml...H.jDQAQEQDenkLGPRYazZi3B4eUSotCPQW6Zpm7jOIO2keY6VWomJk1ySQlTFkHEH3x19lkyVKCQeNAgiVaFAPD4Xgzi.4Y0jSb2XE9bEoyO+4k0qWTzHUHO6ps4rwtTciGUb3TTP3XhuHq3GwmSasTAohyIcHoPBVHXFrkEHAVKHXQeYpYcE..jPPRGPFsU43JSmxyNP2AkCNwI4TKLewPC.LVKm1ZYDGam6mPFmeBa724kZ0Yq+rhMfY68vt+kaAciggYXfLSbHR8XcPqTcAqCvIp2qPAWco5qaWF.nA.d96JX.+AZYGB6hst7Msqh4.F+kSgFprPucdj+fBvtyvvnK.SmzvJLvantv52AxbsfI+Nnwb0O1OmSbxlGvN1d17kyirEayykC1exO9ulct+2Fm9Q7z8V9OZy1+q+e7o2Cger7vYZAP9HDjpGFc6.7LsiQ06jAEuug7AbeDArT45b6n33vLbKu115c6RCAS7fDm3kHjoY1Cjvl.CGQBpIQhMjRrUhwdPgvZICn+lbawjhLYTBc1LheoYSY9U+CySS+jBRnD8M6Sh9qJV.PR.z2Eb.r3dVemEBRRFhXXLb8qoUezq7fNsGcWBsZwGOdDKlCVz71++kwsLYHjyi7xIbi6BIIADR1BgD8c4hgAL8+cPDNJIejJQ+oAA.6Ag0RBWsPkKiR3llEgGHnHIQdFA2aPs810WjSTpks9Dvt0YbM3pYBQ3jGhs3sPviPv8ipAEnOSnwDp1I45KWOY0YWiW8SeG8o9VzG6Kt551kwZFTA.K+wSCYuIAs03fpOFjMFCIMKB1NHXiG.rHY6tPluIrCuKxL4MQXpz8tD1+A9HmnyLe3NwiMRHE0JGArJ7xWTLSpPGUfuy+ne7B5u9O6RcuztOc7A6mCvz0fGh0yOTayJ822KdeYoI79BBHi9krgE+DezDbqAOLJtiSutPQRiE5vVlX4fjzn.rwvX5wj6AD4dSkTdidsOb+YZ7akL4mLiZ2q9WxGF3p7ndhLAOMgVEMMd.Sbl7xPgiMPpDwBArDAo7nY+X1dmMZDlgkYolAEIjTXThMt4FV6S3m3nZtKJl+DVz0gKD3eOD1hHyHmfw39NNRkPKXw6rWjDV1CfY5H2hQRI0OrFBvh9aDYDXHrZVzKDV81FiJOahEBxWHHk1xQG8o0v2mlatPN6o2vTpdnFnbzxu3Y6hmeULauWg9oNYVZnQOAiZvBrFWsZU6QYGxQWt62F..VFmyBTgve7mvCtwiCa8OD38e.H5LBTIEfuHOXxCLD.RFRT.HXDH1qHXtDXT5fqOz0DftgHdfCBlRFLwDsrWr8l7B92fWOXljQcE1DSj9Q9T+L5GAkzqtZK..LyGOlJDlivBUj..kpWiWdsk6u17p38Egi3u2S.usvnQ0p7tKrPxzOwzs67MJnknYT2CS0jfXSlwf1XimMtsMI7fPgM5PNW1M6A6l2boewC+C9My63Yck4kw3vW8UQfMOkISh3ebKf+j8PWgiosuPz1Sf.gD4HIbf3HyN0B.XYlADDrjPDIkbWRYZIfpChzICkHDNxNLPc3MdKBsxem1erRyq0zvk93Rov0HfPBAwLfj.NphpPx6L99Qy0xf.QDaI5neWHDvBQjvBSBaC1vXhh01HojzRI0vX3aqDkwbcosAvY.vEAPo504Ee50sK2n.JtbQ5CMPNBHDad2KyzxKAwR..mC.KBr7xKgkwZLPUFm7S4fFlBvciShjMdL3XmFRYZH7Uv2W.lL.h9obEac.ro.LCAzXbDIOFb7GducxJIYSd.2DMl.IkpWiAJZWMbqjw8Cz0pUyraw+mnyjCzLE7o1a4R2rlunvLecpjWMNcztbuYqae1Yliet1Y4JXE7bUeqES3+ND2d.0+V+b8CKB3cRon68fKu128szKdIHn9wWyr3RKkr63ESrgiEzos3Pg0OiEjJpSfwr+ULI0+F8Zu+exdUVvF7K7EdDY3UikxV8DcR2fAZyg8z1Btv7i46x+Iaa5DLB0LMoZHfsEQh78UtLDrELi90gH1XX1vwjvoqvAG55PMLN1lnEGbb+PSWLADo7sA6sqEt2k.tC.9VML7Pk5OkZeCLO5ZP+IWgEfNxbTHt6qaYwc+agz.oiEjj5WPd2Pa71+PSOeexy0kP7mm6gEQuidyEBCea0ck0XrbUdATgdwihuW+PITlVZQPyzXNAd9Uw5.Tik6mzw8emKIPuFYQV8jv19AfzLEjtS.kGAhs.VCfUCgnuEvrU.Xb.CGXDYALogNPifjl02yq001KUyYvK1a1h6R0puBi5fe9iJU8yuvQczmE.YANSpqvMheCr6E2kS0ZCN23tX0F.q9hqvX1+VK0b+dIss9F+b27M81G6u0v60DPZwkVRL1i9npq9FYjsUdpfcjBiKQxXlmu775T+VepjW3fOexsSaGBfY.BEmm3yCItvJVfygOci4hVYuex1cZ9OK1YDuCGrfuQ3Lf9lqsO09O82S+a9e2v1+K9Y9Pljdsorjvz4fXjJsDBWo0sQDGXZXxK2igcztVqyAV1riwPCpSrYD.JqAN.FvILLlXqINLwnoNFN+NBoyVRWZmbEjMv240C5dRft.vG91MubJt7r8MjrqLhiq6xdBEqHq0XnDcB2CLhTv3Cp+JMIb6sELlsBIAFrP..Kfkf.rgsZKqiiSfUFfdl.DFad9ey0sy+YJR6B.fKfR0K2u5hcmG.5uOEBTA.kwRhFyMiXhae2X0idd4E40WFVfYjMFOPEl4ALKe4yczVc1K6.UiAPxsNArclDJjCN9JP9FPVCHMCN1AVsnuSqDFHjVXDQPvNfEtHrSAjryDHLyFaTa3a03TuHU9QSIVr3r7Z0KZ6GVi9wL7F6lWT+lYjcNrk7Ed4SHPueIAB+uTfsL8qULtorXHkFuTZMprg4d1WDuyZD46lymeuy0yai2sMLl2ZxceuUEAbOG+GX7dKArREJ8S7DNauUgz.1rMtpMSX2XOoPHsjUGuE5pbx0Buzmp4hnrt+5O.emBWje.gyNGA.7z9AzMBe03MK8z1Bo7CiJILGGvT6kxPSg4o+n+Uqn68uZE6ByCQvFvNX3wD6YLjm8Xjv0xgE1TmM0bLV8h13ezOx9tdZWkRAabfINV2ERQFaRffSBrrIJg0TThM89vMyM8Rqttz2tUT.ZCzQ+0tLtq4ZnBUo1BXh4xQGuYfniwi+QsJd40hhm6IbOzZnMXVjmi3RfQFRPJRZYGI02KeVKAvvxRBVCXCQfY1F2UaiBaBLvNPGs+OZXqjoVFX4kufEnJVDfVFKvKgxzmY9hzk5zWwJmd2h7klKGc7l4EoRbni4HoC2Kz1wMlwb8cxRMrHVD.acxrzAM6HtVRQKvmseGr4F2A02bPjb0IfUNL3rRnrIfMVHrVHsGMDoQ.C2edcsv.lYXH.chDniGrMFDcDEBLij9huzgNm8GG3XI4sEy.Q4Em0tbspl1AeJ4MVO1o1eZuzXmN4vgcyAMx.NIERhEvFpAwAn0.GBmhMPPwNnx4CQ45LVqHAzWysD.pb2h2z6.KtzRhcWqH8VqG+8MUe42kMHlamGokpWmWtRE7tr2U72X7dIAjVr7hpstXgznEF5fsRJ0tNORbDxwVqRH3XqwbPPOwFX+zFbrOZuJ257IUwSYPw4ILLjP5PHS+RW9SfrXqScY9+q+v+Cw0G+ooZuxL7274W2NG93hmY1woIR8ul1b01h50qKJlrFATCgRvgRvEko4UqcAaYTlAVyfh3vAm7ginXt6AaD0wD1ZGRGl2DGnzIgVqwlXH+dwj69YyEeKUZmc.1oyqzoQDv4rGMYysUPBph9YsdgYOA2MoNelQcY7m1Ht2CM7Att3JFC4jDw.fFCjMqxQPNNLSRIazVU+Lm0xrErQyVXiM5ftcXsYOuAbtExX14+jbAw3fhTEr.cAbA7znC8KcxgDCFBQ6F2TbZsOEqNMW+JOn8IGxQzQdnn0PfbZDx5A5Zx1LuYsZ0sydTQc5bnJWw803qHz7WEM5+UoREAd488vdMyCSihfSmCxzFnEQfrBPF.1RfY.Fl9Kh0xvx.FlgQXfUvHIjftcJPcyh32LUyW9O2gt9Yr4efTjTL.Uv20B.biW5a6bsWIWNr99ih1cmDwzXvHFALmG1dNvzJF.GBYpMfp80wAmXCTC6iAdzD7Xw77yt.VoBr24tPEP2dl0aiEKWlVdsYk3.W5S9J.49oh49U72YQgb434KVjuCQC8KqhExkiJf9qmdwxkwQjv64d9eyw6cDvkVRz.Sjp2s5Tr2g9mHnENVT.URqSxSVVwRaXbLWuWWiCxZ4k07t+ziVn276TI5B3B3gZ5JcBSIMYCrkF1k8Av+jAGwN93eM9bUedS+wvWD.qaqUCnFVu+4sVQa4YWD.0PiTyvX0UwZ3eGWCqv.GsmC7YQ72AHd7m8ki250NnKzGtEnjTHniBA.vJz.YBgIemdsiO.6uZOf6ryv91uIvU.nZnNOQpb7wimj6ryd7yH.dos0sGtnXK1Z80QvkTFoPIEPwdZMfDLwZKfP.lX1ZoDv1X1Z5B1tOHZS+Ll5X8WoUsqCyxGQdVD+JTgYg30aAw.YbUxlNxsRkUFo6wPNf8JLStxLT9NsvN9YrE5.bytsvSdxIwu6kKyGQB4KTqt8ibLO60udcK.Hr0YkX288frdFX6lED42O.IrseDZLDXM.YO5ZgvBlLfE196kEr.IwDRzDz8b.5nfLRvFPY8RIFSlBskjnaKVb302G670+Kx265oOF5EeZn4SAjuHDExB1OEXGGvDCau.n6cbjzdbnZuFDScIXkGf1tg.Gk0L2w969226+b+0Au6ZEIbs5BbwGjt7ICsOVsZ.G6XRfV.ttukpnvyd1yJuwWOuH5Q1fxO4j7Y.rMpW+cLC42k9A+UBuWQ.oi8mA28mHbvjX0Ih5ZeX1hS.xTPQwoIUhfYSrD57Y7DtQk7jwNoTeklxsgymv9G7GTU+a7y7wDcGVHitQL.f0F1wdkKtGixYvR.XMrLCL68tqvdGToVkiFU7bukWqB.s3h8Wu1xKC6VO+WJDn7diN5oatSRnDl.w.CLLAxv+XsirObZaxCMcW8l6uFe6LjGusEwWFkohyWjlnSN51ginnLhmSFwe4MFNLsS7AvS5JgvgXlI1psFZHSLbzwVIwjTHYPDEyVtKI4VBE0.bpcjBwM8U1lXw0zKeOk1hkQEwxweJBYLJXj9v2yAoLNvRDlF.BCCsqE6WLA0SRFPInxw.SJ2imEEopGIj7UvSYNysdYZNz.qhJDt3vRzsgKh2yEThCDVIjr.Nj.J6Qh0C.L39wvDIfcigfXXMRPrKjVE.SfXILRFBeiaVo4g7SXlG.sIMcSQBd8+7eDu82PLJZ25gfI5wfUdJHToAxa.IX.o.BGevrCrcNNR3wQhVA+7AHVGiWeynUdxKXWDkocKNKALONStBD.vEa2eVM.fUt..5jSflgpK+4O.WV8gYLcZ0.NVtI.ie1x1i17bna7jeJwlw8T0uzo4hAcMAoZyqbuaiZ2K9a3Lh+cEA7sGzTIZgTQ4iJXQ5wUd3D.hIUFjwZktBiFL3DWWqzjXIGOAvHtL6m1jN6Tlmu5bM+0ejCY6UcXStPVWxX6taQS8LA7ZetZ19aY.82MX6Kyrai9jw2Ao7nKdUqU6NWDWboE6SDWaM8NUO2QJtuB0rY+pO1h.XsCVi+z2p5skc1cRQmmdtYD2lrkM1kB1.3zINTfSBC.D3LIWyMlg6VQ4OyjMciM1nNRsA1.gAsXAJpSPNPruffDRvBqnGQ1CcyR6KEpCbFN09FVsa1R1dKVpub2..gJmWhuwC4kZ33Axl0VBrdLmTX.2L9ddNtJUJPt8EqdTRnocX2zGt2FwM9FsLsPzjsvn00X06buhaf0sOCNM8L3zhu7k1WrZpF.ldVP1DnHCbgBofCHAAMYgkXXXKrvBoz.AogUv.LAkU.WgCXhfIEfUXvfiXBOLcxCkwhaFrKdiVYEeguwF79a7IRiVxwfgNEP1o.QCA33.RqABSf.LrNJHR6CKmFhH.1LMr6tMhuQCvi2D.GIT6EN5qyWF..mI2bzEwo6a93mOPhvh9vsUVrqNELapv9oDMgMFoQaryI5fm82I.iuo4xuY+jLduMis8ioZ8efM47si+tf.RnudRtaseDmT1q0MSkI7gGTlIUQgRLLQTVRHbgMMA3vDBklDQZSncDUJWiP4D3lAsUdn0T+yelNW42+eqYzGwUF0qqsaWiYiGIqA+t03Zq.baSLphED8Uc+swYAlC.yAL2b.AE7ox.8cy88fa6x9UWEXwxyR6VYAae0WzelgkAHfkD6hEDKh6Xx1cHeGuYdwfirmvMwk1LUH7RTTaGMmau11.mI4aNPK6xqttEOGLCULJoj2CFt6Ui6Y55zpaW6tIcsCEa3gLwNojxDohcLJGcO+rbi7iSGjcT2VdYoNBunfzjebifYknRE9YO6YkuzW8Aybsi0ZHgkG20mePArOnxkK43J7jtrT4B33IrfnPhkGZX6tibLwVCEkZGJMuia9RaW6YNeKT8oz8+tdNaETQNwbmklaSfU6nifmsGfSa3JBIO3CWVxvJ.HMRXCXv.LCilAKYHNpTXPIRHHIbUIDxGwrHxs3TI2x4Gyfqrq80lLk7p61x9xK2.AAyjF5ViB1LEDCLHjos.jArP12DWUBj++ScuqwVYYWmI12Zs2mG2mjWRdYQxp5pJQUc2R7JoXaJYIKYCxdbGK4.2CxLHrBx7iL1w.pAjPRPvD.8i7iaSf7GCD.mG.FPJHYF+GmAEAx.XoL1IS6zEC7CYKQHEIQJWRUyl0KVr3kjWx6yyi8dsxONjUWUq1xxHVJS1+o6h3h68b1myZuW6056Q0bPQDnJFjOjflMEjwWE3zGgfO79XmViQqKdptJ.dLJJ06V3kqsL8UtcSF44kf7jlX3IyCY7zfxKgTRfQFgAomgyRNBcp9XrXqd4KEPAKzTmo9i8a9Juh+mfpk924wOsB.eOWn2kCN8jPedupVcpIXhqZB4PhIhInfCUUXvFxxjT1I7DFK2vFapGFmU9d9OsI8wq6y3okQRIo6Ctq+1+meWYGrFArFNDMoBgKuIiK4BPkZVrfkwD.XBfIL.Ym.LU0IvgkU8JC7RzDpZqm4658ZZkJZ87b8i7qEP24aXnu9IgDZ29YUDMcCrHuDhI.Habdf4Z3VTiwwzfjiXynxbnyR0.Pf2PgNq1wmoUCfV3Ueaq3lqKaB.sMjO9BeYmo7qjkd5D8RNK+nQmYlviQwFjvkqW0GDqCCBBG13xzvFy7nwOnyzokp40xFgJyU30Zsl8I8ub4r94MCrAKPxnEIRtQPT30rQwMrQFaPHnf.VggcDDOYnYLr1Tr9oXklPEMlxs50v0k609sFf0eEWa.BXcfs9ZX9k98cMdb+jtQU6h37iLQ1lrUqRlLRIC4DOqNu.xSPY.3Yn4VnbAz0MJyVkBhCSAG2ihpM.kmNc4pupha+5xCVXY5616gxgi94sX3TVfrZfoogYhJvTKCRpBMyB47pqRwB3pYfRMPrJP+xP0Yf5mDXbY7W8IFt6qdruZy95KWq.taciGSMR1R+e+q12f+n5gv2aFjemOBR2eIf7Yfmh.SJnwIP5cJLYODoKrMR+Luy8fjLpSWeyUiomSVIuX72CUC8mlmA74RCUC8j5yY0mZ7N1PVhCrrxrJvZTHPgFAUFSTpm.DVxHyXPLRuLArB5d+218vGV2CrlVrMVqyC9pQ.wD9Xt.DzrDlvGAN2fHhlHfnIJGSoNf9GNVKGLVOVB70nPWrVMetYyy6lm6OvXD.fnC6Y9XW0wemAq5eFR1R2.GxCuVIZm68THPU3.sXILzjpInLhu3l030tcSjvvTs6D8jhFd+tOrdiUWgw+KuN1+k+yyu5T9g2+wUxNqW9vwY9fIpERrn9KU0jdoWnep0T1MR1x2u+dt98WE8GeYyoChYt2kh7GiIE0ekpMzaniStgMDKDDGT2DqA1.PFCCTHMSj2yAduDKNeYIGk7dIjbBmjJRduDogbUsKt0fcvF325ysgYPsszWeiMb.KLB0l6XS3vGwrNi562v5QEuNBEnDxvvPEnWsnxKLHRAQLgLwPXrkMmDVcpCn3IOwL4LoasZCA2D59s2ROEyqRdEB1FLniCf3h.LgfrNPNFpvfTCP.Ch8fi7vaAnLAzXKz7X.sLXDg3jfsdyX4yubw7b23Eo4B6SGfZXTmlVb7oS.4QWCIO7ii7G+IfgqCM.vIFvi7PFMDd2dXPLCywY3tYGzoe3fxMC3Fu1h5Z.xORP3+ub7Sq.v26EoOdxYGGWeh9DqCTuKWcVUCTALzBPVQplC0KNObI4duN1FWejINOc9kZHc1dVo4vqycwmVafFXcrMZilztnFAbHiktZ.hlIBMFUBUiiQPIaiZDGDQbTjyXzbB4YZRdpfmHhZhxYINsWoxIBWJsVYqKy4jI94LT3tNcsq0j2ncaFqutzFsoMv.E2CXET5baBovDTt4N.3Z.3jQBdFGp8JlLMN3x5c25Q5492vEGdmebsEnOx+Q+ll46eMB.9Ac6Hgg07G2yxXjnol99vRw4MZz0CWWbm81S271P.tMvW60H7vQ1K8O5Rkl85zbF0bixUMujFWaAn4UMFmkoQJnH.D.nVR7dFBQtbuQ7hADlfYjJ4ooRZRVfQRimn93J+W7oxvu6FY23W+FHa9PnKsNn0a6Ac8tT4J2myGzPyNdVweVMXJWhkHifxjRQDrVon5mpAJTHNgn7wjm6Cpx6XiK81QyT8.SiQI3l2TQaPKrvxzV6uOZdkpxYGGKPHAjRf8VnRHHsXQbhOmiHJcN.cTXHENovNvg2f.lwQ.XtPBHQ6FG+bYg04NbDRe37H8QuHT20.ELCTx.xlAFD7tPPtJPMDjnCP53Cv3NCPsKODn.esmqorEOG+6odA9Si.v2aNwJ.7W4i8KNPmdtNiNQNJOg5qBJqBLW.GKlfpVxqDkQHeDQYcCqJcm7pkFMtwgtl29KHcPGrOZp6ia+za9ytH3qTiXzXTIDPwUhyhKEGYiKK13pjILhMrgYil.COfjrLIsWb9YTkwRnwVaFlowAIMJStvWdFMqpClIR3UZtJuYqVzN2D3WFKp.PmG8AvaYFfZzV+Sm1LU87vYm+ECrp05Tl0bhFmqZn5b4iRyuB+ASW+28bgj8VsnUZ1jhpeYFyWgwiAv9.cF3zGu6SxvC.vjmJ36MqrY4+k9M26YlUO+A9ZW4yZOwLW7excxpUelfErA7GLLDu.UpRIUSsjlPvMPLXLATkfVAvXAfWMFiJAFBjD38ZMVnYDJIkIWuv5ybTkFt9arxR4smeasRyl7sWcEt85qKqOyWdXTsOvCbc+FS.2QyqYbI0TdJ1TqjZTCXy4puVQVOpBkgOiD+IFC+HiI3uNbl4t60+2o9SbmtW58..VcEtQmNzQ+0FMbhIDDUMG13DH4iAXETdHTU.Yb.jCjQfPTQPovPkyYOhwAhcv483Rd882yYWB3nSiwn24xv08CBNZBfoFChyAri.wNfjP36NCTeYfxMg2OKRxuOB85G9S504BCOGwQ3+eQi3euWfR8O5rYJEOPySORE5ILoQppUc4ZHqNCSD48RNyXf2D1gswcJW1dZ7b1ws5ui+1atstvxuFs5J8wsK1BBOF0n7qABAUrHxEhHDEDSw0pVJxDw1x0HKG5MlPxDFAlMArlaI13.4FkKYFi6zynwwYTPsIoAYgYRpOu97h1I0PkFeYyxcqIaf95mGWbb9yS2c4D6UmmiZ9AlL1JAQASlGleBaTNip6yIM26xSnzSO1mfUlLA4+65+X2qpL6mZJI0XHq0xW8E.tObR3VSKXtbEa7H+JnDlE6par0OJlXuEtkYaeHsSXd.fsp3ko4XZVxPSXiMP8Qplm.hST.njZHPVp3wLQDIJSLACXhPj2S0LLljr1FkiopSMY4RU9he1jjteMWkl.Ma1gW8sVAuwq75tq+Os8YmLjteZRdU1niTxdYujNswlUG17nBrrJNXBxUXSH0blwXdTPoRuS4FSb2q9gl3fO3+fj9eu+O9t9u7WdYyez24Q7iAvuzuzUv+Wa6DT9RYH7RcQlcePlI.xqC.ATXJT3J.JKq.hA53PHIBT4LPk6ATZ.BL4nahfCft0V.K9YJpM9TAiIfS.xNwBLbB.ZFPQgvDkCkxAa7fLNPAJXNAhGfmH.RXYHjEOLSpcUn.gWfGz+tF78ikFU+zbGvm6uc0Y6I8xmebVE4I445t9bR8oYWBRZchQDYIOT2YpZdLEO48Xi8.IRNaxIOM42uvmGvs1pkz47uv0PKp6RwTVWvHcrAMirPs1xwvJLaqU0GPgrMtDaBJQF1FXXMh4nLxX7fXlcoY53ArfSxAPJ3vI3nx0USw4A8u3M.57.fkQM5923Pdq6Bz7Kb0f4dgZAZoYBqYCiM0cQ73zHcLGPgirPbDxbjHpX4DmkSRW3ESyyyx85.aZvo0G5lO2MrKQSLgWl7xL8K9YMTkGNT14fN5lars198YBD.z1nIsiIS+eC.nNUgAU26QEqRgPDO.Dki.ww.pfmtaf3AjyyHiAnBYlxRLERVpjwDTNplob0qFTZw4+PC+Jc+7t46+G31Ymcza0bEB2B781FtEGM4gCF0vO53tGo4oWib1KqHaVU5WEH0vrImPkQTfsGGF2g3K8vpWdlGN4Kc8CtxOWd+81aOW08+CPi5OIX1W7RjC.YWsFUc3c7HtZJhm8.H3GBe+p.oWGBL.QtB08V3hfNnPFRP5C.9LXqtOLMNFJRQXeAKEiEarKArMlKrOsG.pC.HFBJaAHKHKAxVTnHvgfXBjg.7YfgCbrqHqAQQWm1H4uTQXBA7w9w8d+6Wf1ONfdCfeFhDlt6tqjtv0FYmv93xPvnS78bhad3cSZXDYXiSAchHg6Cw9.Sf+I.cF76+uXiBoH..2rnD4ztyCuKG...B.IQTPT4XccMbqB57HVBwVFjwfxr0FasUKSVNlsggjInDYrgAVxvl.ixJ4HKbfCyICKB7YR5YjxZp5pnZXPnqWVfqQn44l7994kno+OYtfEeoFkCWHpbj0D6yFGJBGHVWnOwazzLipdVUOYnwZZ1XodsLoRvYhOOUrn7fA8lWpVo1vTHZxILUeJfdef.9SLeE8WdwE0uxRa6W+4YDBcKbqhdThN5FODdrxKXabIttRTUwg37TDTHbTDDeD7ZC0P4JSQDjnh2stXHEwlppLI4V.J1FaJEU0TJtgINspsTkp+66O4rWIuQsdxM282QtE.Zi0kG+k9lityWO1+c27all7j6Nxj1qC4FLonIkfZIqILmB8iQPTea4oNqby4Oox0u9oK9gnwMBerCOriBzwfWntw8gZxyBfRFGM3Wql9c9K9docO4ROA4xc.kVA4mE.uqIn7Hvggv6.TuGBxfjdJT9HXlXGXuzeM3lGfy7iwbMkkWHQar3ahtnKcP1804vUwO7t2.fqqfr4.9L.RAXFTn.tjBPDnQF3XCPlGRpCpKEhliSFI+oe0+6nEWXQy.D+iiCh+aMsg38crQgdYN9E9E+PGdz2pxfrg9NiNydehsUhhCrklPR4RnWZR5fCtG0+deuN8wGra56YKe87lpSKglzIYgzSbFFAAFP4lx1RVigMbDYBCISPbQvWPj2XKaMpmHRyYiMGFiADkE.UDJOURG.oDxcCqMQhMH34t1WD6RGglXwe9ZgkmItZT0r5VRhSkffzAoFQ7VIarAJHBdPLfwxD.SLbrVRIelBqIMPGPiFmZRgUxSMDkdlgh7mn15C0n500UVcUcy0W+B4WfV6bFL..fs1U9T25SYN5+aIJKMrBHoh33HgTi2PdlOGUlnDIHRgvfXFj.UTPxE.WQATwSJTiwxglvnXaLEEWghhByiciJ4LSY3tww4MvWxswtaHuA.nu5W0uVq0xlaoqc1IUmMK6v+MGFL3gAPFQYxHekImyGFlkGO0GwM4Kc07GMJO+R+R+PWoNczs1ZQFa8l7me4R1FK1vTNxvmAfJjPsRRvhyuQ9VM+u7XvW9sQuzPHCXnC8PkKC0GCRTHoBT+Pf36if4tCruv2AgK7CPeyI3jrD7ARTr0V.KuE.FC.fCxtuB7hDBmwA6LmAL7H.cF.DANDvTqH.zma.fApWAzgvT5L.6XDepzDC4St6Ilwk2+Bal6eq+Lfu2AA.r4q7Jd.LDqcqDz3yLD6itkp5BmpoisSE5lbxQIGe+Gm9ve2z70PG8org3GYzlZsTS9A8NhwDWhgwXPrZPHwwwjgC8FJfMAwVSPUvVCX1BhBMLKQrwHfsopyKDqNi.JfDmOIiswYYLdl3us1B3Seipz8t6dXVZo.KMtjgLUMQlx1bOqgNNa3HVnbCGnpInpiHVHaLG5gQcYAPCs9LODO6Fdxokyz3g1pgxkWPDft.lgT2t6pWoNzWtVWYyBj7q.EJR8ExB3gnoJokCJOYdIbBWxXPfWUiBXDkJzxIgUEPYlIQALBTXOeV7bo0ffGJ7Eh+DEvvFYCrRPXDEXHxNzx1ACs9ntA9mqZhqutrQa3VdgWSOXu8bWavuK9u9e7MBW9Scsf5WNDUqBYBQEHeeEi9Kb27e1WOei0OWiaV6V.GdGZvuYfZmNmFPLExdxPkn5HCK156jUsY13u+2pzQG9sm9Nv6RfYzQvqu.TashVbLHAHqKnpOB1qrKry7NHq+g3jcKDCfeilDVXqmpT2EAe.0FLPQpOAz02GQi9gHuaYn8MPbkAMNFfHHdOHSOvUO.ASsG3Y2GUlaD9TwTZOOMhG7SEF3+yh.v2advD1XaEXwbbs9CFOQCyi9nkM3jw3dzH+x+JgtGhOs+YcsH8orh+cY.8iytOse9BL3gLF1ffPTfRjkAYXKYMBBiIxPfLwVhXPLSjAFElPPPTCaI1CjmSj5ElrzOZN6ek95TWqDg4ml6MLgKUKMvjGFJBBDM2H5PRnbPRtgsr2VJTsgk7.YP0LVySU0mUj4iOi68nmDwGEFWZ5Z4XgRBPCLz60ZlOJAzEciW5bD7W.ntcPGcgIpI..uLpQu4iNiBGWFEdnGqFKoLGJLe97ECPBHQfZ.fxBD4bAWijBLaRjBXgvg.hWDm57d1klAGEqtHm3Lixc8mJWN7s677u3s95xV2pEge+8nOL.8p+1k8QFhXeDOA..ylGvBNLuJUdoqo.W2A7EUrz1JVZU8EpuguY4ltjAhoO6HKqjjx94pdjb3vN5G9ytzvRU4GbusadLNk1CioYPFpTPzWjBU5Cv8v3rdXn6Tb7to.2zi1sYzpEpt8cvR.Dxt+SMfmc61UwYiFivK+PvwFneWKR6qP6eEHCpWTUU+..9QvT6GfxKtCnq7.DGOZ9niYf0zEt5228G+mtyy4dw+DLz2m2+etw+e.i3aSn8pLPLcMLMlYgPsZCqTp5YLvUL0lbjc4u42D2oe+BZgbyaJnMHzBz4uWpqgBlzOVrE8EZxDBVKED.ffh6pvfhabiEfsfJZLspPLjHER8.4UUEOgBpH.jCjohNgnZWuWw2NSA.54iHHMH4LhRi6yVpDaJ6IBBA3AKdRYuRDoDC0D4g5cBD16QH3fHwmopaLb9d8s8F93nQ8CF08kKkWqhWhPCjMoSNIIVajb7SO6G.zMvMkM1p8SWL5ZiWMMK4k5wtAmYhCFQjlQAl7fXaFAnhW37L988gNaHuw.wmCJyaXumADNiyvnz9xvry7C4fwCGbO2vFgG5v2cWYyadS41OyNx..3laqqckOq4Sg53O7+3+frWp8alt5qsLg9io+nvqR+kAioc1JkN75uAV5VeBtT2DcqsJn3zM+hMk+4+l64d4Wdde1GnFmgLfTi6+g+x6luVqNTiEef+kWDoc2Mdz2Z6vA281AGgSShQ.yHsbFBGmfy3Ln8b339YOkUJEldCArJtcmaKubsAzRm6giG9R+AJdMjiu5xmB1vfpY.GLDN2CgjMIHwBQF.yjO.7B6BLwCfIrKJ83rGGMizndcM5i8IEbBzacKvq0bEpfX3Ou1z7zMJPw6omuYwO1zU+YR.Xazl1AsnMv1JZuJ+49jW1TaxXNpb.mZYplgIa3Lm+RS0PW8R1VhncRS8q7VuU9FcdEcsEWlw1a4w4m+6EwX7MXmBZBErUAyZP.f0BDE3IDQvDAXrfBCAn..iAPbpPpQKXhfQ8ppDaThTwFWRrQwJZ.T9Lu985lnEXKMCurKh6bXBUqVNbkEXBI3UQI0A1ppBBDqpJiHI2wVKPtpRfMV3.xQHTx54yFc5wzYOYf0O3CvQ6Br3RhFMsno44xb44ZJNmyZE7Q6bqr9cKJy8Ve8zkZ+8Nw6ziiBkdhlO1FGGEVNL26APJrLKEhYwS0yZADXkLPXC4.Hil3spOPg5GqB0KaL54Fj0OWnQa7ouZ5stEn0Jd38ivt70PK5p4k4Oxk9UgO9Cy+dqOv8JWHafsuUvm6FgQRzQ1SNvqO9c1QjGcDiCFYvviT3tR9u0u0amuD959c.xa2F3M.DrNjMvMo0t0svgMaRu7h.23wgok+MNyOZP4Q..g3LoWud9X22PB+U9U0c1tO.Nmk5miSyMa0Rwu2NX101Tmu4JTqlcX.fM69lBd7Kkg4wIH75NPlCAsaM3NoL3gQHKICkl6HTdwivX2.zraN9ro5J3QX1VszMt815ZsVm9TepOU3AAgb9G7SIGdXtqM1Buw5PQaPX0UXzrCivqRX0wd8U1z+2Bq7+Y2NfKgsUzF3y+Z0nzoCnnPCYsLcgz54YhxYhJQfxSMVRTMLw39XW9xxFcfaM.7F3cYgvfvqpmxIJLhh.QBifmrjDDZThM5SkzjhiVCi0RfcJIdkUmn4NQjbwFRNJLJ2FWMiqzHqRXjORdWEFC.Xg7xbmFFZ7Xf7bfrwpRgpZrr38AJLAEsSRTkTUgGvAf.aw0hHjHdinZhOOiIAhQRR4t61EmlTVmb0bor2+zGTG1rIsxpqR..y15bYlnPubD.n6zZmwu32b1tlnIN0HoCYdbYEgJTCQDDhAohb95uhRhWAG.Hrw6I3yAqdHfrohFel2aNwXSOsbozg68uZuz2ZEvWe30raWohrSqc7qcymSAxH.fEBFImcBv10Gnu5xKxuJ9l7qu0gL9NtIdmQm1Px5Tp+S5YSNnuY7IcBPuNL5MHAf6C7On2NS7AFhIudx567uNe8M13cwb61aqq0pEtC.d4OCjZww4yEB2AYeaEXazH4qociGSG14+UYkUWE31qxadqVJdVHhs1ZXisWRWq05XgZKquVsADVZSYcrgfOe6jkPK2N+4SzGmteLdmsCgcbHhGHX5SFrzG5Ob3NKszEEao.0SaW7++EZtBEDDxQQp0ZmUld5yzUWcE8MvlxabQvW85FXrDZ1QQaHOWlCuOieVD.puKcfZy..iLFph0xIbQvWECSCNO3yXX1RfDlPEC66wAD.vsKr+KrCZQWPlg9g8EDG6AMsqTkTGaicdQ8hBuHFijqrHjHNvB4DQyTMOUHMUIj6IPNDFkEEyIASTaT0omHMSMNml9zIskvtzA3kfJFZ7wgHOoj5kHU8UjbADwFsfM.d.npWrPFqpmgRZnRFUIojlkNVRyFb9hCoHtRFWodLmEyz3CMT4oKB.6FGSyFGSOUTkpUvti0.tf48.27lRxW7O8rIT6Qpa7QBkV1OXbrnwVhsfIRDVOe+OF.dHNO7dkEm2jmY79bMQD4Dn3P.eGQCNcTvIiVc8WQF+e5MBNdug7gmDn3fmdHlmJNQarNvgOLMe1y8Lv8tV+f8.hwhA0Q268BctyvYk7ypMta2xoCGDijwAvoJrw8fybDpVsC3INA5bmf67eUOztcNPGYsVEeeWXi32oee8kAPwY41FyE9WRejFyaaX6yOndp+O7sucNVcUrxsaRa15ozf..Wv7cnu5hE+62X0U32X8M8z5qK6.juRaHu8NWQFG7vL2vhFlV9EPVqkVBsZsF89g4yae6MkKe4OmSDq58Gp6sWj61mmJJ5roBrhfvRDfCHozOQUJ8momAbsVsntwwTCigN6LCWwvjOfnALQFCwFhHlIlIPAJzdVl60IjKgagN+d2Tag0HflzBKWi5hDcVzTvvNNzrVd0RlbSf3Tj6DmWjggRNQdyHB.APC7L7if2OFjLTH1KDS4AkpmE1n533F0FmvAIXTddjl46ObnTc+95VnK90Af3YB1LdznxHLupx4gEnMI2odFJfAjZTfBekvqPEmJVsvZIkjPwkko.6ixUs7DSMztvLSaqsXfz7E.bNfd.xbmOWcQADdupa14C8A+fpmUalxODYouCGpVxjOCXaEJvFXXXYC+zB.nrgDeN7YNMOwKNGMRxjiUQeHLkefGziCRdRuM9Hej71.zj1DNqmQeve9ck8eWtBhkW30LUC+tF7OeaLHaA+rMZna7FvfGb+JnxfYwrg2fSFei9NbIHZEWZPDjpAfiIXc4PyFC00GQTWvm1A96eeXhe.t87mdiU+nYO.ix+XKdn6v98U.fWt1yRsr0vAY6nWs1oT4yJwRsP8kqcBATC30.vW84mfNbUf0ZsF1u+g5x0FT78bKv51uqpy8U9RWx8le059+p8FxAO4d5DK.As1Q1X6apO2NWmarpEkE6ONGm6Q8q+Lel2.PA1Dn8yjsv5mKnX3uYAh5mosgX2tKxymzvhGWJHtrwNhYC7.kJAxvD6nhpPZUUGqPMFhmzxzC1tIs8FP.Vhuvdr1eq95RnuhkZhoiG4rSMkqBRyU+.uOQEAYhTJfboDLlTjkKh5GQR5wLSIJYYIpbnyVtZRT0nD1DjjO1klOLyAjIMRRzMZ0QWFKS8YWwjWBPddYjNBpIr3uw9BVT3EnNmCVFpWMJIN0CHYhQ8tbgDqljDCmyh.aBUch8Yt7kLAMKyILKYLSolhNlWOOWmKr.5SGfhfvejUj+27uZ7fV+VOHoW0ZAkr4gQlWHrBtT.qMLFpLQZgOKA.ELohnjL1qIiF5R7Gop4gJE8Nde46QYxga1YiQ.PWsMLgvSY8LZQv24Ujscap4KbHGt++iA3CA7Kb0OI8v29Wxr3uvQkeP0ylOe7fWhYyRTd3K48UlQ4n.kCXXf.Xb.tTfgJzgBbiSfjdL7GME5MSDhp8n6tS4SvTI8S+DSqubQ5DOczH44AWc0fwbiSy0n5M4Cx1UAVBytVQVCasUA2OA.tC9BXI7FX23wTb7XBMWBsV77cl5WU+76to7ls.9EaU76sw1P25lEAJsaCpUKPMatBAba7JqW7YV+ugTJeZ.15uOUuuEHcanOaU7uX7ytc.a2lZlTmS1sTPejWpVLBM0DNrDHLHAAkxHDCPA07lnXwHpmYhyJGXlsUSdGbKcIz749J2AcTzDxk+vMjr77b0aySSbtPWhOGVG0KT4JLCgTSPFoRORFMvPZhFVw3xB0rp1fTOaSyx7YSFml2gR80Sx0uS+9methEeteybupvA3GWLQlIdPpp9yAUtWJ1ETbP8vnj.EBfOGpKMRgM.AvShY.UoxPThZPIcIZhI3BA187em544+sjBy5x8+us0w3i+y+CPCdvDWNt6Lg7MrwjGPmAJGIhx.fDICZdpHYCGpZ5gRt69NWzd4Z38LljGc329M6gsVWZ2FbSrD6FbL0+jPAXkm9qsxpqx0h5vHHkCpWhGlOjG14vR1ZtYqU9vaL1qejbT9CRgSLqipWUMkMvvBLVO7VFNO.5Cf9DngwPSB.oJjCHLphEcWvwmTI0eF66FGKue67OW3uLcAK2O97+V4JM481qB..NDUP0Pf67cAv0AJMtttyfun909A+N5uwmYLc8JM4cpmRWA.mVujLaskk01cK4lae9BMmKyGJNup6KtL+CqWhyxtu1tMj0WG5ScmoKvK3OFrgR.pt5JFTa.glUUfM+QNS3OqB.UrSKtyq1yjte83.OUcb4jPS.wUpmSAAGBaTNEO4jZ4oJmiIyxfHbfMTSirtGf5luNfuMddGaE..a1QO4e3GTtzzoNLrVljllAII0IUDKkwpvrOiY.OoofjjDFX.LkiRiPswAFMI1Dl1ueZdm3T+bIIRZudxExOPUzWqXhzyLhhJhN4D.QkAEV1xp.hUnj3TVKlXIo.uIDHxqDyvINgUwQjMwKomF481RdQlvm3Kok8pD4U04bRWuWe1hw79Lt3997OyMyVoya0YyGM+3yZXGNiFlAQy84HQgTqfsIdn9Qvmz06GO9XeFcOUB2KwW9g4Ykexve3adB150cZavarCn6OWFM4AARmsumrC9E0MN+76EoC1mdzIywkGbL08zqEd7tS2XvYGcUGMwKA63WzRgyfvHFlRodDQBEJfCE.qfLmGvJvXUjGovMz.jMKjwVj1KGiGeZm6EM3nua+rW7SF5tXmefhyEewDv.IR+ySF52qq3JO++gz9+fOjIXuQ182OvzqWOHYL06Dl.F.tbrLyLK6ii+uwe6uwdtY+D+qk4B6SWO9TtWvX5w.Wn+au2IYsHU0sjWbwkws62TWem0HfujA+Kl1L+7gL9J.OtxPOV90DTXYauOuzCfZCHTuICzQPqezdB9yHMgoMiu0isO4JWItRX2R4AUJE3JEEYGy5ndvFkSlvDZbuyPZZfymOUtR17j.mQNwHyFVxgO+htc9JE9V2SETV..rsV+EVxmeuocQwYYwMlXfczjNLgDXMQjMlnRUXVYPHyBeMhIYRs7TlLSsIShpM43wItrJURcSMrH36N8OW7cNe0s5FnX3m18hs9vR4lAZ4JLrADmk4MV.PtB6dvSpZM1hTSgJd1ojxJaMRde0kTxJA7DtrdAoi8MxG0KxUO2KQmyL+4RRd54GNHqnGjMRRz6zuutVqVDt0snCa1jN2y4KDRo68JIqbs2Ba98Zt+voq3EJXXTX3iDvkYUM.BfWP1Hed5P8LwQO4wGgCwg1Sv2Xq9swq6asF3sdL3Ea.bH.d31Ozu8NqHafkTfBczrabLE4tJgytJxylh5dzGJ5I6F0b3IitQNnEQv3YHBkLF3BLCDObHAwp3JbTCvfJvUMofsBBJwvYJCXMvFr.xRdR2Ng828IUFeofrr4v6dNX.f4BuOAbWbxPmGl594l4+Lz8reda+6mG8N+4GV+I60qJTe.xBIjjYPtC.4Y8lXxDDO0fqgUGBrZRiO5w9u9292oP9M1dyK14Sw6Q2ftn+dJ1RdkUZavO30hvUhigOqziiFYgxDRQFdxrI.qM.X8BJm8LxVwaf0wZwioJAiogwioNMWg.174BB+ojjTzlNeEgyuoVmP++Y1wuckX+be33RSfHaTTnFXLBB.oL4yTxPI7vCFq9A88bTkblhL4btmrixt17Sa1XscErw15ZnfpIafMTztMZtcGAWuhOzDmL47VWNhRHwXbFhhqQjo94NsBJAiNuZEU8whPwk8IixyoxI9n9C8WD7Ur625nM.sCVWw8VRvZsnIWHxWtt3BhC7..bHXwSF.OHMSAGnA1LgLPUhIKSJwNUXO4qKZXeJanLcpiaNNcbXR+gCyK0ib1fLYt7b8frL8hU8ab9j4cNufDG1rI8x0pQkmdZy0+M+MwnVsxun0D+Slole1mza7F+eZdHtrbBlNMJJvZSS.hhARyiTjMohNHCm1MoYmsx92qy2v8ow2Swxv7IV5Zl81CXb8fhBuryJZKL6Seo7vlMoOVXH004vfrWRw.Ocz8uVTutGOaZVvhdT5xTfFGYFRLmxL0SYMEJxzD0qpFSfIFvQHv6QngfKVPdHCMLBT4oA4tL7zw5IQGEEhQHO2OWXHcPVlNW3WlJbpP.feczK3eHc7wg7Qeq7nG8MNrww26GbEbR2YQlDBMlgHAvmv.TBF03LDcZm6c6oN5dMtT281q+v60ZMAaush0g72l.2SnsAe6IqB7CmDAkZf350gubDfCP4Tn4cwbwOFU+A8vc+1tmpBhaushUWg6TAbcSJc+vqROnVmelsC3O5v8WvCSVzThg0XhLBHq2GxVSYxI4jgcjSX1laPxPik8BEZSxEVr1JVyGcYvWGMoMaCci02Pt0ZfWaMPaey000WaGc4E9R9O8qTWOHNVpFXbgmZ4KZwwydYLzKpwq5nDnMJMP5F6Ub7+S9uSmaKy1YSc1y+bu0pqvXUfE9ACnFu4VXIrs+Rs9LtJkql4j7j7DafaHfKICR9.FHCvZn.KSJXgXCHC.wNXMNRMdPt.Olbxj7PYbdZ2TNphqztc8ihAhRRzC62WO78LscQpvG1rIEUuNivPdDyT4Owm.qAjeq1fu45ebYsktk+KLLU6eVs90NHdXf+4YyQt46p86tsjX1QqNwFRoktFE8K3sg0mmN9wYHZjQ67Mtmr+Nqn.qhad9hmqcqawnYSF.HczTRt+C3yO9LyfwUCx8Cq53xMHakIXKYTO4MXLQHEjOQsDHKwHmT.PDrhRmCND0XALVF4kXPwUAzoPVdsLedvnNwT5DATQwn9SdZvWufOIEE9e.a5EZR24rRO5aculO4u9tW0cxAePLp+BvGXgFKPPHTmEhjAyv9H+jiPVmGiS69v6c7UdLt6kOC2Xoj0tUK+F27lOaONe9z+W4sr3a+8qA2CdAvctFxJeYHSLML0JctAxkBt1AH9JairpuCtzuROr8w4qrzFxrsVGOt1xTjIfNzLSQqI.vy3yDJvOUB.aSqU3RrO6MSQoXSKpbngIhsd1DYYkCHSTMhIgLrkDSUJNLPMQArw3H.hCxX1Vi4Rq0jwFPzaAFKtLuc7Xp4a0T1oyFJvZRZuEwb.nm2q8qTTcxJlm+EwLuWsduNWdtdP2LsQx+8JvVXoli4VMWBcSJoEEQa...9LMFSIMf1bdnSb4nLSb9vPenAIhu+QHVDWj3FaE2XFdfrbixArDFDJj3THNPppoC7NI2jEOYkjqTZ336dhIKu6gxguTeEc.PmNOM3+uoQZudRD.RCBnQC+FBvMAdskMeoE.150+cTrzWRFdD.lAHJ2R..m4rTn+TB9dTIoJkXfVJGTzrdJ8DildhQG+j6oCtOzs2YEYGLqBzB38ngp..ndOevC5pOZu8LCxtARRcLSbfmB3.CoLrJIF.uWgHh5FqvyJbEd.Ea7jw.lXFNkgPDUHGD4gf6WFVDEXaXRsLUFEogeXmSDfw3ic4+wlQl+QjaXj8ncFT4s+KemY5dm6rn6rCeIL9rEgHWBjg.S4.T.TN.PTHoIfx6ijgG.W5TX.UFmvO.Ob5i2.KMBsaqOcWvh+y4222xfClqD7e2KAe2kfK6i.N3pPNcZvUhJ9jjG1oO.1JgvVBH7z8PyJm7xK70zV+beNS8fwzQlh5UXAv98q9iLm926AfsANOAw2y3hxVYhHXBHankLLnnRVhC.YnpDaHFVKDikBXvdNfMTNaYlSCX5ZggzZsZQas3x7xSW0TAUQXXI5Uqsr7l6dSo6t2RZr3hXzybn8m8P1MRRzF.miyzeOcolGRsZ1gqFdU5CGTfav7vRZ14nnO4OqjlfRZ+4gt45qKq8a+amGn0GEWMUYxlLrjMxbZVz3AmE3xFYYMi8hQChhUeXjPFUKZYTNxSgy0ehQWZINwVKN+x28s8y1pSQvGNGoK+XFq0pE9JEp1ru8p2lesZCnkatBc69P675ap6iUn2bGH.c73dcvRnI0ZoN7vgM43YpwQ4eb5rfQR03Poycg18dan6iUN+275XGLqtAVRKZ5v4AesaW3IBnH3G6tqr45+Vd.XwG+RZP8oXCkyLGBlhD.0KppvAw6f3yyDeNojlWXBaEXDgrVKaffTQJnHk.B7.NvlwgUlhPwJRnQRMciaCAXU7xu1mgJG3oytWPvdeq8l4v6d2EGc5Y2PRcu.zpSASPLPYAbYCrAAPxBf3InZDfDAmVTI1zSTLZFG55xvaWOE+1EaMsB.u5EU5D..UsXXRcX3KCw7RPvKCx1.TXHnPSAeBk.PZD78FB3FAtYOfC58hexoLWqQHmIi0G6fzvejhcR9UJA..f.PRDEDUrdJtnJ9OSkSs3ucuO68l25OIUo64FqfhCTemTmBD.kCHCAhMDqDfgHhMwrRfX1RFKTk.YXGwLwoVlqcNSENr41T85MYTdrsL.bNq7qETk.VFu9W8ldfaIqrcymdcrI.vNuGAUcis0as1Fzhu5xT8wWkdwOxoLLyP6+X.zuufwWEYCJBB6u+VZmcJxapwgG5tSe+3O3bUciJSiip123hRCQd+P2QONX33bSfgozRkUNpjvVQAJVrzON0mGVIc7g8St5z8bafaKW.woeRFWXca5ashAWtjYuGAZ6lcDrwNx1mCOuMvMkasF3MVCXGrlt9MWxcqkVyl7jdDtTgaMgveUsKh08wgZwtcqgMvErv+hlMCbg8w0nVMpKJVzZyaV78+GuAn+m2aaM+C9gXNNfCPYhg0C.u2SD7Pc4p3cNAdUTelxVCaThCLDasFhE.DnTJTQXGfJRUqWih5fZl+kjeu+JbGr5SKD1cvpZsWad9d6zqzI6bvBIc67RRh6pfJMEhqXgVOAbU.Th.aHndEh+bWV0QHOoLFO7Rvk4Q3vDDnCuhGCe35sxT.4M.j2nMvaTv6IEyOqA3zFv3tBzvO.b9E.UyC6Tigot.xF.IuF3nxPSuFx5cJr9cwf+rG7gaDx1wITkpAdf9XXVjKKojtSmM02UzfKF1K5xO9wRo9yyasMvyB702aOPZi1TKzhdO6.R+SvxzuO.JzYX.KChsDaB.wA.hwx1.hTOnKjQARgpNPY.HRINK9L5f3XF319ihS4oLSxCXGUE8APM7Yh6HqsCnMV+l5lqcqyeq87qfUNOfbyh.w0vND1..uJ.vcw9GNAw1TB3D0zIfxq9HEHD..cZA8lmWQruxG+i6P619Ya0J+vlMoYa1jkjcsiydG6oO7jvztCsw0Hdb5DpItpPAgJGDoUqBLJ0oo0l1U5EhyG2+Q+DASomabAK4qMfdvQobXsbJ+OISwsA.VE6fNp1Fzq+ZKykmtpoddeEsQ9Fqui+Kbslbmm.bo3w5CJkn6iWR2AewyEX3sw6WfGPw4NafyABPwtuJ.PyFKxg8NkxRF.XCTIupv1Pm3bNIWX3sv4UU7hnNWg5YaDhYFfLjgYBHGArmT1Cm3gw3zP6XsdzP.7mgq2LvNZmhJK1Fso0WGXwRkBxu+imn2QmbY+nwWGZPSfRVP0Xv0.rULfhLPCIXHFhn.Jf3XnCh.mY.6EnCOCRuNCE5nkvRCnywQ3657X.HqhAl6WCpzDT3TvxUAUc.L0TvSTfsV16ALAPypALdZ.WYjQbzvD1NQI5zy5KnLPu7RZ74Go4YF+HmA78ExS.sYr7qYvmtNeio.Ba2Ryl5mitA.57keMYq86qX8NJv1ZQ+I2VAdNi2TA.BPfBu2SAVwTATXYvAgDCSA0gTBjbdO07IFuynNEpiYnTJoBX5RyaHzbV96ZFSCOKgrbNUpVI9rGmp6+sFxab3JBV4MHznFgtwz0ZWgeoOcroTEKOZeCm+Ey86s8H2F2toaiMWRZ+5q6asFjkZujVoSFCTE4UejlMHTS5VR28M2Rt4FOqBeCEquttw6duQnca2xKrfYT2d4iSs198SnIu5zzYtpBy0UNbBc3wo5ByCjBH4cOz+BniVfUw+NowjO8yZByI+tAJ.vu2lPWBftPstA.dx2+QZ8abCrVqVzRXasSk0jpY2mFDdUc+s5n6fNOUcuW+BlVzt.mtWXEWW7ccQUXuXw1M1.X4FuJlIZdc+9tzLiOwRCG6DqC9TQyAHI.JrpGJfkHnLg.hUhHvLIjgfgH3UlzLhDU3.SNYstbeUEXF73jG8zhirCZQ3FW1r6eV2X3OZBzevLHKcV.tLXa9+Or2aarwYV1Yh8bN266G0mjEIKJQJ0RpYqQpmpF2ts47QaO1Kogm3cR1oAb1DpcsaDD60.cGLCrSBLPLP9C6BH.INHFaLFrKP2XSVa+iAXYg.rX6AaFazaFI3OFOqGFOdbyxq5VMaI0RjRrHYQVe99w8dO4GuEonTK0yzNdSlc297CQppJ9Vu2668bumOdNOGPZ.RzvE6AV7x1CmcP4b.NG3TBNiFA1..i.auoA5ToSApXmyFuKtyia1VQf5mCXTQPTNvdZf7LnxL37i4pTE.HGjTFBzPxwnyxz0u+2JNj1mhrdRrJ11MosfdUkla7A0w9v8Ab0UYr+K4U0NgWR2szCe6D086VAk1sC9VtbtoKWwg6dFC9u7BV7VKXw5a5ZfMNhGLOVpfEb.4MkuvESlnZdqWHfxiHkGH036.RTBSFGroFSbTBXSL78SXw2XTjSCfgLS4Kjm6xGP84TZHaojdi36+18o+UuUDP7rzbOWIZ6gmgl5zdpKtfm2jSp77zbfTMwiZ6auzy3Fbg50FdskQTiFMrnIbRyVOnlCAvU2.T6VP1n4C3kkG4mxw+rQCY8rEvBZAKRNKsebYB2uqfOw7.EOir3kxRsvL.n3V8jc1p5idcdfbjUEONZPerjzqnCUA51YBWM.oEZiZ3nbWsNVZ4kj68cKIGEguUasra9EmkdBJeO36c0UoipFgSJmXiBZErFtk+kL4y2YHhe6ckA6uSpaXADYBHXTfDAb..o.7TP3fLlYS4HQYYvB6Hx4rVq0NR4RSXgIinJ2E9SNPT0L.+2gfx+yrYnPpIAzDH8yP3.iOR6kGlz7PHMDFPyFHrj0uBEBhigy.vNGfS.mHYo+vjQU9gvGrTDdlIwbTwEpeJ+MCWMFmnY2..fRdLL5wsYsTMf1GJeF5IsfKQPhAbc0.oJ3hRAXGzA.tSS+u9a9bo9et6I0qCfZa7ncBgGRNRA7wsSLgV0oSctRZV5DFXQPZ+gdUzw7g6tuK+DEco8SMyMaZRbJmr+hyYvBgFzDV7HJfaflx0vJoO2m8GMt774RPhUBJ.34CXEHZEfyINaJaRGYhk3twhseL7nHctYhohEL9klP.hwTpgz8t+8gdhJ.NkbvA.GLvPu2FoJDTya6aW1CRr1qPTvN2SBbTgP+I04SGxg81M0ZLlC3bEN3GIbgdc9stczyz8cSoF+LVbhBqb0UAmY9ZVHIdHkgGu45BZzvh0VKqQQdU.b4RDhA.ZKqWIyz2S5a5GPdTHM8jf3TuhxEplUk5+Ysa4Z.HqhMdfxTC3tViq8fMH.nF3ptUVOqdLyFQOLO6bhwvCMVOVwqwC2PZpWJu4ylTYvMRRuOFYtIR5kG59yIZVC+bB7BA7xIPEBnySPoHvNxwovRI.hyIH04LCDQbfU5A57y1NX5ydXtYKa5YKHET+pn4Utj7cdsEU89ZMklu8+ELt+rZTrWHhbLrJKXUJrpT3wtwAEAPRDPIprv9mBhhAgT.ePp.APEvvOvWWvK2oelfv4uTN++N+zuj28pW27PsHckS.lYHr6zER2QPTNvZBbNFpBi4PlzPHCXP3.nJ0CrWBr.23F+p.kR.PKjqSEoxhqi4dBO10OwGzqtJg8ed8ryjyeTTuPQFEFh19wVhlrnwwdNqkbo5X.DbZrP9HZDhv1u7BRiWeiiZaviWS.AnoTYl+msA48SsgtD+B5DO+rRF0IPXqXYNCFUfFFkFemQ1D+XtXPZPNcJBU13zT2917RfwSJEkZirdRepizZ26CWoeVc9buPtgw8lD5vxQ8UE6c+bEHqKm+gl7DwgI8SMI8SOLJpy91DcGxWNX6SeoCW5e560+Z272I4ncAazHqG+0X77vCYt3S1mYAGmSoUIL6Xyve8r5fD.33RloYSfUVAeTBBywR6qInzhB5UTVIyjFowGLYxOhBVCbTZgZ73+Lm70nGUw6nWu0X+6qci6Zm64d1jpcx2oc7TuGT8Ji39EfuVADPfYBJMCVQPkU3mB..AwQvJ1HvNCCHorh6qxWZ6hyblsJegx66WLIIvPx8ZkgDnxQGxS+Kcdp5u75ba97ZHC7PZpBPxn2MxPvZEnfCNwBk3fXHPVBzHnQJoUoD.XVK9JOh04Sx4MQpe9JNc3zddwEJnPsZxJ0pgcvxtq03mwfYueBN7L6AY+aCSuMgXyAwEBWuRPbDbc8gbX.rIC.W4dPO6sfZptkEmzM8tD9yOCZgZxhy2AWu20kNsu1iNmB.bLc8jM87P+9x7kOaEewrm+DZDfSkjKYTnWtgovJgNqBVI0wAkrDxsKSpBboSOMc9ynjf4VVt1INR+nUq4NUp346mXij999NGEjxZxiLRpHwdtHiKMvWGkFmFYz6FM0DSjN44JZovPqpvP6vtCb2r8Mcekmwy6rvY9c1ouIn7+Srp9TAUJuPd+shlte6gm0YFMmxSOgmhJZRr4ndp.gHkMJxJFyHIU5p3vNPz22Oh1NlJs8KT+WY++zUww0gaiFHiIq2nJUYgRzRUqJmr4M9DV.+.o4IrBnA.PCAG0YUWYEBMaBTq1QeYOtSjd7xUfCqs9QW2Spf7gI+f6q4XSQebc8miLcc4kVl9+9cScWz+z8ZawcfYqxfLSBqvvD4CGG.iyCbhhRSUjDvLYEFINhSrraf.WjUQo8PXgsxUdpMO0kmYqK873vD3mdfYnja72oW4TRUXNBnEP7LDntBriDPiDvDALj.ExfBH.PYqhIAD.bZxRVAVMTJKaRh8f.F5bj14rVixorYM4vS66S+Ye2c4c2+6Q.vg+1+9w3e9uTaXt76hzAShjccvN3Bvc6o.46AJw.a+CgP2Cdm4uB5puML960cnXPI.biuqferZn3VWRvkV9Avd6Q1rTC.5HSKdvNoqR3lWPW9SG6qIJDlg4Dlx4wJuTNANi3RRiLHuh04Hh4DJzKkMvmJhBxm7kNia15qIGej9pfVqNn2b1ttNXln38sG5kWGYYOBJvZ3AN.Rh.ikLIrYPb0I7SO8YurMdZO2vA64prWj769Fugcs5WiV4LeQGRts7S9FsreoE+Q8512uX7g1YzA5yVb57WfgddR4lfnzPkJwi4LOM4.HLhE.jHD5IRvzwizEs2BpxS3a9hetW5fyMaWWmvPZGz1gM.t7KVh5DFRWF.y9fSB+nGEySt39j+8eDYZYBPjwUn9SpFy9+khfGDNvG55WG0oNKt.eyaNspa4tpYGTHApb6.b5bf5U.TTBvfYQZRY3FPjU6SFsOo8ULabLGmpnDKSoiXENTElaKuJSr4TOyTu2TK30waANF8hM6zNwscmHYE.J8TdR56rqnwyQPmKFI6O.tACAhighAHqAHgx38NMCmR.6KYsN6Xwg.xgXw5hbvjXcVaphsCbCUcKLzNLJJMMNIwgtccca96K9az.qrBzMazvBTeHd5E2F9ysArIQHY+8fjbJP5.HlLTtqJcOHEdKP7lvZO.A8M3ViDTq1wsAuYaWWd.lSe34U8QoNXCTkVA0yhv2hKvSOaQOm+.eEIApv3PFQgo9jGwBRhctPeNETJwFCg74XCFx4KpHcQiqSo4LnVMYopUcyVutbEbE7arvh7b890kfy72M5dg+rVDEoJollF5o3PNCtXS5ac86NxTIb6zfR+qsc1rrCatYVER2tsfFMjUVabfPZW0gVqP2eFI2nt1YRMpyqX5B944yn87mgTtBPR8H2HNyjDRfnIHALzff3MoIE4MViNZfN0KuLXh9ylpWbhDDGamMqkCh.ee5HfA+PMmiOTogbhNn6i7VenJb+.oP8ukT79vtOnUwpzFnJ84NrLuYgA5sixoNX3.CBh6fB0H3qXD8NQP5FSH8T.bYRXGmvNRzZkJ0xzfTkNYjW.1yqPg6DL4TaNUsycySW+YtekmxaTdSpCULNfAtYauoakU.R56KXefKg4baiKMDtC1GBsODaOPVIimci7PLoflY3ER.9YAOgTDHA.J3RgEl3Xm012n4cihy2troP2btfjf81195+5+5xpK2ldlO6y4MZ295Mmayz02toAouPW3eoMAitvev8PbuJ.CC.qSAlpKBd58vDyzFQQG.0nXDdfC0Nmf5.KNejLak1evZ47DhtEpSGUk4GWkAepoUyO0PMYzZUog9AdjOjf.Dk3I9JwGNmMUHhRA44ARES9dVxy2I4JLgN+v.cb4xtmy2mtW0pRs1K4VG8Q8ps4Nc+8bw32KdtdEkqhkwkK8hYXbD.2ykHGl11Ez9WV96zFXkq75YDp9ICBxFPPsaKW8psvYO6s86bOuIYlNiyHWjBvEHOuJpbTNsV7bFkxkXYHN5HNhQkq.HwiEi3IDHXUFqP8SinCbLE2c67GV5L7Hq0JkSSktdYXRrRTjfORcH0GUIbU5jjqz+tpb+z6RwQssWWMvzu8OhE3mzfW316Cu5LRRLH49Cz9w2SDtrhbk.nbJO3oY.MSFu.yvfxp1ENcv6O44lcqy9Yma2pOqLpW56aGLTKI1PoRTj75abE4KWCbTmbROrtLK.NkTd38UmYODu8cgIZZ3YNCvfIfUoPfhfxCvQRVejPM1oJAPrNHJGRUi.vth1eK3JbeOuBGh82O95uwaHKcgapmBd9px47S6Gp+O5KLu9Sc6lC9cuVyDbw29PT0kh61qK75kG5QJXxYQPtgnTtQ3h1QXwPC11JXtyQX6HYwEyh38wnp+IrwqNKa0eYTYTHgEyHU30APXQEqCX1iCHghI1yi4zTFJhrFmyZhEsV6.oDHhXLoNNQoRRUbLSTfmGeO.fjjiqt4U..FSUbHbDg1W0g2ngkZ.yItmDY0GvmW3QMIpA.dwbxUuJP9WTUHDdyYhsKXSoKxjcd.mFBKhiYAJVDeFtDxkZIvDQdAVVG3.bJqwDpILEovYxUR223rowVQbC8RmNMM4XrWhLp0+DQI6CSNxkW7nk3x+thHOFZTnAZHqBfBEZ4tYqZtqgFOHPa+s+eKdtsew8r1OexnN6ref9fRVQxSNSdRg.Ok0WQ8XX2xVZhAwkNSt8m8G+R6NyS+r8bSNWZb7gBauuKJos6znn6da9O0sVcPn5R.ucefqBTCvs48MI2+7O6AHdqaCVJBSeefQ4gRE.CK.J.mFPkxY86AK.SNPVKLtgP3C.i6.T51AS5uSgKzteoYaYJseS0oufm+9mJeXt7I9dyCt3DkoO4KNmYkuxH2lu4+KtCuc8g23Y6FgMGr+olZHc+8sx4+Tkbd+3oxDgvsPkVtl0yhzc+EKQE2pmbs5seXxh53o2GH5Z3Kmc5WFSdiEAv52OUhTraRNmy4CqWnyocoVqmuVbD6RhU1DmXibNOyHGqTLJ3wGA6pGUZ09ZxKVZwmfIYOFtS7HZd6wbSS.hzqnz5qrJeFagIh5Yep3ApmwjXmSLTQP.NiXEGfXIxYUDb9jPDQT.ARITFkfQrl8D3J4q34zATpBH0MJcfZfem3gccu9liq+vGVw66mIjO399Qyk2SFwQ+PobBEwwRC2FYvb5gYr7FMbauJh9hS8R1tElb3vAm+.w36Gkj5woFOFFsF6vA4lQJOcmzJmya3LehKMb2dHMX22S5peaJv7sk7c9N1uGfc4pfpNtZ76s05R6qAoEZgEPc4PumezMTOSaXcaBd2.3F4ANcNXFTBDz.VBRfGDiBD4.wI.pH.ytPWXS3k6cPwI17zmt3dmag+pzJy9axehKcFuIG1S6R8TB6XWeCqSMdpo8C9oppjW3E9do+Kd2qmdiqtbJZ9OTt+3w9sVGDtP1y302ZLQPUus.zN6jue..ZwGHQ7ygdBZCWtRks4mImIWgBFPdIHMv2X1Qo3g.XnRbfHkGA3CCGJ4zZqW4JVU3TYgOlYpM.pD8GKyUcI5npK.9mi..1H41Ra.7AH8U.bhjh+XG.MaeMI3R+26UXW8DowlSqSwrDQ4rLSf.YsfPpaLi3n.jbPfGDmunbLILXBfXE.H1S6Qk0dXNGPOiSeW+.Ve8+Y8Dz3ASfG0TX99Mg9fwvSNQ5ikG26+CkJliSBH0rNnE5rHsYk0I4JiwL4QRiFx2XUj9xe9Wzc8+xvzRSKbtRcXXxo1oOSSN4rXxImECs6IkJ7ccV82xdZ+2xdObOAXCTI+4nozskexnbzXJcA8di0Ge8ybHuF1PZdCXVbtENXcoHA4FLj6YyRDtadPog.IJXsNnDe.IERPev79.92DAm9sPk4u9o9L96Oyk9CRJctea54P0fyTHQWblBTas3RsLFrWDKG5wQdTfWTNpnhoYqtkck50klmDUTmz5rwfX3GjoxSJGq.1IWjr058jVnsfuDvrSLmoT0zTJmNloPeShuNM1RrNwR9PQhyQrDyg4iCxOSTvT5H+JSjTcNkU4DoqyI4sV4581RlCYmrB.ftscqCfduQqLPNmgqqGZw3qNtrPFOzdrJhJYVO+7tbAEzEcINe.FDbVmiTNiPN2XFACJH.BfBLf3bfHwAhXPpLMQkl0JOjiAJwBG1cDQWCWMCynie3+nfK3IHiwL6Xku5m.RdO7CmueJm+vibjkH0GSTuWpnrHVhvpW6Q23TPiFxqm861UJTmPz3.WMIvA.3fC..tJpfuNMmMyUj+i8OGkyqJcALBo3b3SfaCDkSVG.kleQZyWY8iS8y3M.cqu8pQKN2K1Yc0yADMYL3C6AI5df0k.4GBEmGjUCnhAWrKjf1Hv+NPWZSvUae+eqmN5+pUAegBK4aiRo9okc.iv6MzxCIRFDAb3gQvLTnR4YxD3PuzyQipVkNANnyFyOpUOeDE8QnjnFpRsPaoIxpx5bq8VlYtvoSFZ7T9vp5suBjtnSBqpU48XTfcJni04mHZhSEFgPc7jyDjNRaSCym5hGk5vfAtYa2RVtJn0APu1UcW8pWycrKcqrFMNazGQ67XMTm.ZhpnFUGsjMPsiRz7wOraBfxdi3.kVq7RX1C.FLFJRvAEXF.N.J6eTj3x513JAfTJPJQHBNlDGTNmSDxl3XShlJiHfFMjUvJnI9+Ske307ziww3QIT9IbeNN+uMeBKJOhsw..pVcIZYLJi2T..5AAnJtJ.J818odastTcIPsuFjZn4I99Z3VeaD8Eu3KsaKt3vaC+NfhtEzil.jqHTpPvNFZNFgS1G5h6C8ncwoK2A09GmLtGiiu0d8scRVfPbM0vs1QMryg46ueuhtdCB5e3TrxEXrtRi7KbIs8B+sTmoZ93UVYkzJQQx0WdYYL0f7vIX+w61AvS34pFngTCqhVnNd.Tk..ZY61qPBfhM48Hmx.NHv4maZkKgobdZqW9v3bk0w94ii7lTa78hLI9Fmsejsxd6Y6r4ltUZ1Dkp.JrSM5MZVTtJ9lLVoJ+BqTVk1QQdq7BB.v1aXkas3d1ey0A9MpsJMJwmfehfVscqf5RMrgz.YOb2o5RTQ+PkK1RGoogrTuhwjOgCjvvMt9qGefG..XHfx5MDLKBHkHNmXSR.RRjnd91gSC6q8ZKxa7JMs0PsSBcqOZJGez.a8ObJiUz9qQpOdre9ip2tL+KGqIbB+8eU.TuNn1uIjp0.gkAvrMQilOZrBZH+FmYY626tiF9+Itn8a3JN.Ro8fGBwjdAPQLxKV+IMopbt9y9z5A.CM2pQcB.TCrJ9Ra8xdlHpP+67tkNb62Yxt288OkYvtyjLzTxlPr0lO1PU6Z7NyN56zqc5mbXmyG9t8wYeyjm6L8bW9EOGc8kWUlsc8Gjy6ODfL73DRvw1x+A1wZou42Tc4KcI+9lBAEEc3cd2D+z9Di3HDTIvVrXRLWHLgciRhmJ0cDihcxHFtF.iZPgV0ve3EeE5qN3+b0b+BkxW9Tw9giqwuHiHQsctacizT7G1K4KyV4ElyHS5YjR61y0tPU2FsZ6.tpqAZ3d4uyK6MbxubEI87016cj+Vc2AeJSLUxYsDbBQrEhUHmiImkHmSwhCDogvJqSqbVVAGXRH.KQjULwCDI5cKV0+OTOUu0+uw8yOH229F1i3CxUVaM9gvJ3SX973e6CemvOLSV9+2UZkSb+8WCEui+aW8IkKzwxI8o9DfAQVc70n9JfpVCT65PV4Jvch6EZErF+EVbAN5vc3MFLq5OxDxsbJBmMTgZ95B4UpoOUDWcF.+PiUggo+Q+KShq80CM0QKayWdA9YuvoKaOnyrwsu9Yh2+8OaZ+CNqMczosoxDoohxZxEYnI2CpotKB3aOU0CuyoO2ev89Q+BaOblmUkLUgYM6OvX+d2cjc11yJM2n1iFrtGc7+A8Aj9fu4wuz0t5UcyVspIXZl5mjCE7hLpYYN1AoXEk0o0oIgcSqryNl3+zMce0O3Bzi8gZ6K9SQe0AedE9rCmNsOp5BPggrGYRRExpkzQot7SXiG97989GeCt6a1dOyOZtNoe1nplOMZiO2EOC0+FsDAfdE.DPLkHJQLQNahHNqhDqP.V.iQ.rBXeFiM+Lq8jAvPDlMByVADINQ6bVN0ljF6RihhODortkcRLC6O+DNAqCZbkguR85n4G9BxGLte.pW9Pe.7CyxOfJeOzX7jJcsFWVZ0viGD5qf5xC9rYxZi67w.MwFY8LBo0ZqPWYEvnVsG3FRiMjlqugcIrrbgy2ixA.32ygKEXN6m9rwOySoUm8bIrNX.evA2DxgjEOsusEx4pC.b8dpa1ephpQ6OG5s2yRIQOiy4UknvIrDkyoXxB+TH4m.NSEL7fIiu2tgG55S6rQ21yetY5CrSjNjkm6LShuG1whLVj6glyN4FKOt4.839tGNwe3IirE1Y4kcWnPAa47b5gm12VPwz.qSJMQjavvgl67t2zz7A1B+Ax4wFnl.rBd8AUTntYxpmRe9B4cOMDdBFVhUr3XmnbvNIgH9L7gilQ24s6bpNu8cpz4Ovn682KwD+Er6JcvWfIzzsTu4jykWaRSjHSpY.LlQNipnXc9DA.x5nrlgNXPBzffCNPPXE6HRK.NHlL9r1YnQVSZWDaNLtucPze9ald9uTJ0YqQOlFF3+VU9gBEz+lFoMsv2e.LbjR5Cvm9WFMVYErzW9y3c1bk7+4VQq6dX16TbBmMcsT6rmM1ty0sxev2ZOd6+nPF4.ve9N16fxxm5S56zmxirgmVMcXHqKMvhe5MM3q9skZ..sWQG0uWYDbv7HN9YHws.oCxqCxoDsOIZ8XQC....H.jDQAQEEfi8gk7PZ+hjqWAazgxn1goa9cLi3yLn+SWyyzI471S6eZZ1pSwiiR5IlF+9w4Z.5UpCp4p.qBPu5ig5rmscaYX0pN.XJTvmA.J.fACSbCGLHi.a+PLKqAZHH4kHb1jxUNKcgh468r5P0E0A4lj0LQFHNqSXHNKrISVxMrPHcPZQ8NClM+6e3tI256dixs+RSz0bI0OmZ0aspswUg6eveeJYXL2GjriSn66RhBcoIkIkml8BAyZ.n.XVX.KT.rRbrhbrVKhk.nXQLooh0bHD61VBakbn8f+pe+ee21ykva1ZarFVguRCHWCODPr+AKXIO4j1+CEJZ+Mj7PVN8HlUd7G5nW+jAgA.XiLJaGsvZGqDN+hknu158jWXkWvu.lZhCOvM4vz3BtTxC.neGWheY0nseuxwJsIY9msfklJIwltaz8+N4rnYS22nFvKO6Kp5jLKc+dShjsBc3lugbLBkNfCvvaOEBFbJnCqHLmSXOchMui80BXEHQHFVOv5.NgDXUO0n9E55u+O182+1+3sCp8Ew7QPtGBQuaeapzCW06iMA8CtF3Dt8A8NUWhVo90PK.zbMPqtAjWcrixu5pfZgqfcZujaVrLJm9RRWurtUT4zTIHJRdbLK7IDBq7VdXTgRHHYNOe6BfoKpz7EXEUToACBByDHVIfXGwoFxiGnE088Cob88zI+quto+1cpDELwgFfkYz3mw16W4WwDLLsu1K+cUp3hQoiTtjz4IOUdn0d.ZECBDA.VDlA.obrFNV4DKzPLoFwI8cBZKN+aAk+62YW2AeQuaI528TDZBrApQqf5nYiGTVQmXA2GEEo+8IktGm7niuGJuoB.vZfQsZJTn5wHcZkmOmfUts7pMuhoUiLkvnCKyu6YmkK91gS50e3bfzyE0SUIYjyGBHPTRbeLzJw8AvfzTZPg.6gIASXwxchw0Z3PCfWuUc570lVlnnGNrOxJsSrJUuVcMNTmCoGNMDaUDVND9rEPIfCgiUfUBoYCqXi3nDgPdOD6OswV5zcilp7z6+o0w6cFBSe2rAx6bQf49tef4D4n9Cw34D.7PQPVe4R8oqikvxH63+V0uFd0wKzZUGTkEVjqE1l12+aSwc21ML7Wi.x3wxiC1xCO4Sqg03qfl.0VUUr3TSH4ryoUpKDD3NuWn9zZ+vIHeOehUPQfbL.TfX1Bi1yorTnPBaikHwwc5eAcm+Q2Js8uVbvQb2Odkc9evzI4WqmXO28fmVCJPb.Ij0aVxJkgC4Dk3QjnHhgiIgUf.rDrv4Dx.xaHoj8fXuKB7tCYb26d+Kdyd+8.Hb+rAy7Xdp0QiuO3IZOVz57XVL9eHJOJ3JXrvhLJ6oPvXh+w4KvWKvqpK6TwMDTaE0MRPvcJayWYS2EHi6hgEo4LFTvjn7EmiAgThvHmg5CG2yXc8GMH+dwIwdXuyI.eyA.+LVzbC4VqVyMW+83suZO.rLC7p386VVgjoC.5UDjWYXK5CWfEP4.4SPwjncD3ThHxoC7YmwWYiU4bHnbTZkBGdHBl6v.cvSW1fjDocXW223Qh5s.PXMvn5RDJ0mvQzRX8qIxFPnFvo6Dt.cYrI1F.WuWQAUWh1YbygLKoo8ybPN41BB+bzQTkNZ2VdbXibUrJ0Ac3Z.zc+LExO8r7oSFwOCqjKFDfy34KkUdrh7TBSTFu43.QFHhuBrFhHBasHDBllhUyiYz6+8du98+eeWdzYPaAqB5W3Mdc4UwOwvbetysGefH9ExabNuHm0LzD6lCT5DDoxCPgfbiONDYMRSlMJszmoz1f8uiWAbm7Ew8PHN.25WN8x+2dVeL+T.Sde7lMeywTz9GBUQ7Qrjh9OnknIcPMN1e1cEDobnae2FWARikVlQK3iPaQr.cAOud0SGxeBlbSRdAdhHpzDhgHFhPB.EIhanH7HlS20ik.ecHk7idg6ge92Z3J0aY2rSjb+9yJ.8vhnDUGyRaD2U4qG3kDMxGpTO3JPPzBrj.wRvAhDCoPJAWhEtDBhwAm.mywjU7Nns1+v6OTeucC4J9cMKTYS25uRiGjhk0NZCmpL7FQ.EABa6PTNAkVj.V2I.jF.nS3BD.vrgGE0xZHq8X8Rnq2co7im6JmdFINpaVua+IDR9Fng7Z30PKTCW9BEySr8zJO0BLIOMqbUYMxAE.CiPDCPLX.3xxSmnTNG.IHk7MIRYMapVNuc1t+Xk24O4ti5f8xPJyKtHHr9ur4uZ0k68TWphQmJwJiYT5gbj0HCEASwraBQPQ.UHH3QPbJsMV6YFJ979ZO2cIs6Vg4os0EMcNn88h+leyknmthOy5DRe9ZnYyltrpheMdmpUoYOQEQ7QjTk9XoWQAnsCIixl2Z0xhwssqFq9MU3cVnDpXphQCN2zUGb4PkZAVk6TLGFB..QTPbJmENAv34AKqPJfj.GlfD3Utjihdt.5bPceb1WXzB05ZK9mYk6bgKf0u1d.0.5u2yHAjwkPwF3rIHsuA5zrRYJQKfUjSbjgSHMEyrcf.yHqXSigCwtjDC.PmtLeA3vw8RjiL4bL0QhvCXDL5Am1qJSveRG511gpYnIRC.juPdF.nb5Otz06yPCUpi6UcV9YOdG+6oRcU51cbN9NFej.Oh4WuB1xdwe6Wxahb5fnCkoco3o.nSQjJOyhV4YEBVG..CO1oTYNCBfwDIOoHn0ZjS7rSVpXxLEJjqzDel7d+aNO.ZtJu45MoeiKdN98a7GXekktTuE+EuTrQzQw8rwwwROGSSAAkrVTffMOojP3bNsFCCCod4Jw6w5b6DDZ2QkmZaN7cF7k15mwt7mulFiYEr2egbBVcUZokWlqTpDgvPpRoRBPFagsR85erR3OfB0.NAWCG2gfd39kGsT8Zg2ylN6d6XtncP7kCCF9L9dEpp8z4TAZhHRbNgsINVH.l09DCQ6QRVLtobVinfi.6IoQ1Pygc82at4rCetOepb2t.232cSWyV.0N+Sw8hPBPXWH1CfLpDbI9HcDANHqTlbB.GCAQv55IvNzJNSeP5CfJeeNPk3GZr.iae.Gw.DGo7UPqfNfgmWlBnM23f3c.NdCn5fzm1+zTreLWh+DTO+OuX4PpJyTDyTAhn..DyG2eELcBCIzpNs53HVsJf6CliiFter4VQ11l54FoJGm5pn7nBr1iAbhhrh3xZCzBCgcLbJBjEjSXNivxcNVQdrF48z1RJOawImOe3hSu.O+lKfp6dSFo2Dagulr50tlqw0vPrxaYvnJFHQC7lPUNMMMGbp.HR.DN.DDnk9SMopmS45REM8CX2.L3e0vkuvui4UuvRLJGq.OAAyH25Hqu3c4RYUEOvIZUVGQWemXMF9Xe+9PkLkvOXoNsxZqw6jZB047lIWt3yQ9AOkVolgC0kYsRSJ1QDR0dD47UDY.XEH1SHkJKjhNmRohA6qSDlxMDoxvA22EO7r4SPggF+4F3vpaHMazTV8VeYyryN8nc342E5CuOj9SByvRYW0bDHMfMkrbLQTLAaWRRFjR.GPJ88E+o22uxoFL4DCR2IN7nVJ.I..UayHrLCc.CUN93dBAk7.WW7OGgnLKozc85RC6Tf6DYUd9WmUi5vcTOEEVtLMTkArKCST.UfrQ5TzlITYg34Git52b85ePeh.Puh9jpSHCJVSrRSrPrRb.DDAfYQbNhHGDANjMMRPFyliffPL.QFkH1.P1PWhxGXNuu15ucxuHdKakZqSu3qsH06RKwu5UuFnFepTT6s5h54R8Y5vTC6izXFhViTiOrNGlxaPoboCl4Y1KYTkPySgV1ZU+cnWsZaNqRMhAbGJa4xNoa11skNOlBw8IzGGNZt3iUDeLh7vAs5HgPsZJ7mGDpnnJ993TLmeZRibLQZ.kRbBTdDCOH9fbVK.IfHUle8DAhInAykUdjySa65En6nrIGNrSXOzpcRqlsrn4QtLcU2N1+9Cg+b6.e86CW+ofLnLbnDLQd.ZFrgDNVDJJERThjFcfS72hxO0cQoys2j0mcvm3msW5vACbnM.VcUh.vKG80kehBAxOhojS6hk43TZVmu.mVv.XQRWAsCbn85B1.hdJ0TzvnAJystYPmc6DZw7dJucXt77JMWlT4BYcX.YhhjQCxkzqiz8rEl0Brioxni6ACO5BOZz88H3CvLaIk0PD4xvjo7gdRg3x.QMILA3fSbryZ7sow9tAC7qd4P82XUjLai0cq9p0Tn1HZwBEUXkZt0p2x1Dsr8194i1JAlg9ihv9.2nEH79dJLMKyUSEeq5+ow2JiAyjMVELV4nPiOtnHMkbymzysH.dkGt647C35rOVdT43vwO1DT4Hnks5pzl+wUzAJof0ISwZoJw5Iz994XRTrVOFfuDSr3HEflyv2qfLkOHYgRP6qHk1OT4omTmilRzdknNrp4Fscqz7ADkdCzvUauUhacpSsOXu6.5fRPNTAW5bvjNIrHDTj.UpwH1QLa6C1eKGW9cf+ScqBmZxClYtgoCGLvUYbp3VodcZyNKv2dm84q+W98.t4eo6BKeAT9L4jOU21tJalUUGarw3p7YL3Wz.2ACu2L7AuS+vdakT1wtvfRNEUfUAgFlCyyJuRjAgHsqMpepxEVNdzViBsUvQHXX0wUyvCLE8l2DXg5h3kmSEmDIBYf.eWVaiU.X78csZFXwHFFvT.ygrxwZ8u5KcF0WsAR+BcxIWpPQ0eQnuBgmQU8yTkqcy+QlsW32xUZyjzM.RasOxxuRx0IzsurcdXQiq3FmWJEVXQFEJxvuTVtorwBr6JHcRYwdPNBPeOR8.9wJY+0QpCZ8EVjA.1bs0yX3sV0oCQfdxob4UjLgVySBkeQRoTLSB6qbLCWFvlHVoEABQNG.bYkclyk41leNs.1i87PnWNoDLbnIpOgF.egEWfqs9pxQOCqiV1V7B8gZxsQvS4g3XCjcGA2nSAjVBvBI0LBZygVJrM3BuOBdp2EyTeqA5b82c6HWZmHo3V8jYqmU1UU2tLeve0OM2s6kU.sQo2ddGtfu6q06lx013Kc7ZlE2dd05KskfYqKZ..uDliGUTmXs9JVG3bdJOwWa.oT1.h7xqXU.LPSdARPoJjtYqDfZHq0D7.4gOcyJFkRhXEOzZPpXEeAX7NZY9lJrhfiI3FG8EFBm0TWYm0xNgSEDNvBUL.vD4YUbgBpW967cbesd+5xuD.de.DxITG+RLptIe81+5NT8Uk5saKspCfFaHqfLZZuQqinh8SDlXLJKj31.AoSlc+2ssCn3CM39HWTterbr7pqB5EWXQt2zEU2D.EBqQX0VBZAjBEKD7HFgDoCAqTrRCVAmxCNhfjEfQ.ZbJ7oLfFRv4.QLIhSXEf1iHcNxSGf.RaU..uLJQUFERagk4UQVj5ahq3vO0ZwH9G4PDO+VXOWJhl3.XGLEbQEg4.EvnXH1tf82Ezo1ApK0Fx45g3Nla7Gkj0EUuN.PFahuy9vN4TOORmu.ULsC2qRUF+kFj68FnVB2EWqdaYE.r47K.fEIb8dPOPUgL5TRzAjmxiHNOwgEX+B4YJbRVGLoxoJyPoQtIsJPVcw7Z0t+19Ti+quaFeurzxLvUOIMEHyL+dVuvxIlH5PSpaemglxZb9LY8DlInBxNDzpfCDAIyjBYbQNJV.aJrNqeWQqaOru29C1MMd+cE2n7.4BCoYC+Jz+xAMcX.bYGyMK.VAyVsNWIpjfRknk50St1Z0wiMsIYlEjER3ioSiLyPa1dc4JajYhzJ0yHj1wQ+8Io.9wAg4CQpWGTuoKpZmG5Soxw6FdF6q8h4jWowUr89U20UohHjhTDSJnYPJ3XEbj.mHhjo3oxVy3fvJGFGFcBVm.Ah.Rb.h3f3LvAOMRG44lC8jNiIZzVnNsBViZhq3Pyq3vpeyQWDmY+st4EiF1d9cfMWH5NJOh1QA6gVnFEAWwtPOWePIIX6utAq+0jrtbzxYWzq0HqR4WaM97abA5fcJDb3ch7wn8UvwDFoRg+ERv1WJYiuPGCp.bDUYqw1.nGfmQSwrPHvm7CCXJvm0A4TTPAUP.oLVKHh0JknlbAk2OVsI7t3uMv2X+jrb67HnNcgJa5NLc1Aol7sMQ16XLTd.gEFSPNkOoxfJM.HwlYSOP1jKDmSbTJ.2kzp6kjnuc2tQ6r6F45OwbIIlJItbg.m1+4Ifm+w+D2GiysYnrR0pnxBKHcpW2MteIHzInh9ipKsiBRvqtJnV0Wid4WbAUmvPJegBYMtjZ0bMALef9HvGKenhfLSO6kG5xwA9jmUOaJYlYt4jW60VDuxa9+k6oe5ksNlcrlDhYGqxd9XsYgYgD.V4D.VXEDmMiWdElgSb.BDwIhKkbFmy5LhkHxlPcyX4g0erfBW.tpah4ewQm4BSmzOYDe6+Bga2trOtSfFiNi.SjAc5GgmoWJt1+XYokZRKuB3q17Zt9XdBXQrNViwKu.ieu47tULWDGbXUjr+jHdXA37XP9wfvgHg16t885dwkRhWodnYy4Azc1rizqeEjZLBPHzrOAubD66wreHq80L7zrmGfM0oJUgzgkYkn05fkJIK0Ix1+sijwLZ3wmBz7JWw8Bqc6QhQsiIMMe7HPZe1o7oy4bXJkiynCirkxYacA.Iq3zSXkzC.aKj2M83Q2J4ubv8wdGF+mbPdSs+AIxrU+fOnOBmp..CGmGy7ddBvUQbgahJ3EAVcU4DzIvCE7nGDZ7UoWdgE3NyNqtZPfxLNWN1vPyJqrB9XkvO5xlginhpprKf8YkSWVaz9CE4KsH.d82zY+oWxpRIKX1RLx76yAx4bDF6imCLxhe.DVofCVvNHfrvA.qHNwPNwos1DI0IoFGrtZXCoEpiZePZEg..Veq2vB.6J0qSEqVktVaj.+1L1B.2Zj.Lqak67OT9xK0jdluxK3o5jRetSen8e9W80cqiWC.Kv3ZEBgZ6J.pSCazSAamSgznIfjSClifcvNHB27vtsuy8S+2zw6EN6np5OoSmu5NRozmBCzNgTJgXQfyIjnDRfXMY+j0YTaK..xAjZIJUyzrOih5+1O14b4oveZxsJ97626VALRBSBBgAL44rHmkj7DCx4DXLPfAjwAGbThxS5q0XaDP2HTwu2PKu8WL41C9bS2WZ7MfM2+oKvm1uL+3T3NRpbz+WoHr8MQbuq55TEY84gwJemn109.H4uSXHUQonCOTwb9Ll0VF5I4KTvN9Z7wxGAYa+yQkUi3xtPxxV15BjYzjxuPfB696mJlFFvTJyvvYgUAh3.rLYyRKETJHPf.E.D.lfXYCDaL.rhXHqyoMFPwhfT+.sQetINJvKOlS+.PiFXs0.ekMfzbiUEznNP1wBtk.vrnu.zGq.fpKWi8hFxHGPtoRo4vHZD1Dm+7U4aMrPIP23RPYuD.cJ3rSBBE.rdvNx.WmYfzUiCuMZuYavCijgO6mJQe+QOmLJojAldor0EYFRZfCgMVCJmuyq.qbp.VQDIvlXR7RJOiKMElTzM1161QYmDrVcBW4gGjMuxUbK9ZemQ8GTd2b43DJLuHVWnXM9NhpPBytTANiifUfMULD4MhXYecN9V9UTWOv2caUo608abi+SLeCrJgUZQUepeYNtvbJKyTdWV.MsiAKPdmS5XsRbRhCHqpMP4uMh6ApRzWWd8MNhZCVkOYAi1BskUwpGqDtS61t7EJ3lXBsavPkE3.DkLvME7kgO90Xer+eOAg.vpI2VTIUMamZRx6D27NqKwnrzgkoyi+DXFjDQd3Pmk6.EGRFIPbPYciKjZ1kE4bBDbieFxfHQCsmEFqyAXRcNdfyX5RrtqJvFEzkdXJT7QD4atjZi1s4Wa6Vxq75PpgZL.vOEhjNXSWSjkFpZ.zFMVxUGWKE0.1nEb.qvsv2RA0OgOB1cFL7c9j.xmB5IKBuPO.wGTrOriHX1KOb6aQ7tow20L71thi5Oo0pKcteZYFUT518Ob3g2iwg6Cms2fBjBVkmkUdcYU9Rjv4Ha7vX.LZ+smM5u3fAIKNezwCrkpVklcsU3laT6gpbf0ek2vBrRDtrUB94YtvjddfoAp.ZhrJUHC0ONqQTPLBnAJV2weBZmxSIaOwoFt209I+zlUAnqhqx2sWNke4s7iSxojzy3zSLgqqyI9Gn4sdOhlY9T6oyOvF2sqC.360qmb4RsoeoKTT816AHMZHu5XkuSVo1YJg0wpXUznQC4Z.Nr7xlYqV0gxksi1YO5oKW198t6csWaiMjmPRk+a7hY8eeQZ0pk8zO+WL0lLBQpIRda6gRzz9R0Pf+tyA5+i8ZObpyN2VJRtgyZrhvy5bRQPJehHQoYGXY7bqEYQQWIrBPn.HJsQ4YFHhriXo6DTP1t7LAGVnv.6QMezikwtOH.DJ0mJGTV8y8Sbd4ke8utccrsTD+Bxqiq5NISl2.PdsE+sTEO7Ol5i4DfpnIpBbw+G8.+smB1hyCzcdPENEzhB9VAjyG5Te3hrHJIDC6eZHoCPRv8vdyt69uUtgZzB1JO+KPGLc2Qz5vb+Cdaam2cKQLi78z8IQuE4kKjRMJYTOWRxck9m5fOc5uco7zF2HRdcroaoU2fl8y2hQUfWdgutb8dEEr7RX11eEo4U.9UunO4evP2u0+jtcl4+rboyeo72yurKvESr0.3DlFFQBafUULcXwx7fhSzKhCrQM+IuTxpYTag9tnIMw4lSQoWVoGHLJLIh3JBbNI9PhBx0iO319l2uzcsmrSF8K9hPsrJlNyNU4lq.d4lf.ph4WrD8+C68tFajlcdlXOumy46VcirHYQ1j80gSOyHwRd7HSIqwVVo6wd7ZIutcb1fh6Z6rQZ2MaKCaX3.DrPH+p55O4GAHWPDjC1Y20NFw1Aoogwt6n0dyhIYZ53axinkhrIkld5g8M17VQVEYc66x4xa9QQxlcO8LZjsPPfz7.znI9phE+9N044b487997rwxOLkxdGjvFMrndcWspUsQXn+PrzJqv0GlHrCCXyI9d8Z.fa7AjvmD1oxqS47.kejJ.s1GljUb+wO3UzWdMO0m9hSI9e3O8U6W4+rqdeBQJyfLsVGyDKHREHEJO5PxGC2PuvDvBPRFjDfjrTPZnD6SF2594vsyOpc8QOmrq5rV9pybEIvIRivpUsWekEXb4KIdqchDEmwJjiq4ete0C34SeEW4W9U3arv6r3zA.5k7wEUzo7LyWjWcYfBibfuSmL5fjop.kZDHjgv2mQfxJ7Sk9xLPDqcdLxxLixLMIxhJByHdvMNoVbkU3qN6rtmpTI7FkC4d6zF8VO0gC5n.mQ.w.gdDRicnceyGWdd8m8rJ5bQcjO+46RSe2uDeCrDNk+md3CmeEZxvhb4j03owWB0vR7mA+AvblI4OQrh+29uY+N+1hcO.UTDRCDX28vPSSoKAQeG1a6DfOuAGWCm0j.Pd+y7mIm3L.SL+7X7S8CX8y8CSFWQfcAPJPWCyEOcQW1tC3n0OQxwBf4mYd4+gnAhs291hI9Z.MmaUtJph1nH+tQBqgqO7bCGZI0LigQG8323gYzwi3krOAKH9Igmzrme+Ao8BniKj8wTDl9oP6+jWyYqrCe5mdb5E+s+Wo2+m6eZywlLlS6H.rruErmz47rFWD.H3z.HFfrfEJlj4AFlgUrygTRR64EwOvym2nzon8J+rIwciARGuqbLY.gc1fZmMAuxJqfEZ.asUWhm8km28fs2c3wocQO7I+IliplDI.V9IFjsvo7olaCbTVfUHJhhifPXDRmSnfP4CkDPw1.OqJLvRDYQpFBszDv.E.mSAbZ.QwgGDe60VyUd1YwnaBzF4y.tfEIGLjT..jzBigHTIJG+SKlhOW60bnMvc72C.KgegYlm1H6d7XGp50igU.BAUcsXZlqBF+e8Ghjs+44vof6m0Ki+YA.ZkwssFdC+Vnmae5obgjmJz8Znr6DBeDUAyQ2472Q7me2eDr9Y.lbpeM416dVkuv2q0CDBfHjsK6hCX16qwNunHtT+WvU65WGKtvBV.fW82cYt2GaJHdKivbKHd9yuCOk2lb6nYY.7DmID.33YCQC9Z0AcsidCmPRENR1dOw0YFOlpQeB7tsz02qemuW.S1rIW5zmlS884AiZb1jL2hqrH+e6kuD..9oN+5biu1+ht+X+b+icFmUwtfPPNo0RVNiG0ko8Yaeo.8fibLjANRJLjJm0ZIs0w64Ef0CGWsQXQ6tnfqe91sMVu+DB24+ErdvODJ6+R39+Yqirw9DzhW8etXM.fkAhauga001jAVF3Wa4mT.a..vFK2k8OOb4Q.sB.PMfQCCcoa4kJCJD6zF8vPUxJvfbC0FeHXnbNvfEzvh9UYggMHIwp.Fd.y0.bwspvXNXwNPiU+Z.XlC6rDhm5v+GyA51Y2l.tEl9VawyLOnO4mLl7Oj7Afike9LbONA.Keq+T2Yu3EwacvyfmI7gOPE.vYgBuEl.V.zzuGu3lORnh4a.3twc+blklqoX1KMazJuQPIn2pDrpvd8JH0Y8IiMhiyrZs0jNRPRbXAU+ox9T8w0udBVXAWikfCKssoBf2KfmW773xVfgih0NJ4aSm9Fu2uL.FdP9Xn60dnT6+85DpuCvwpIcZmNtTOORBfA866PC.Y0TZpO5nzUvzBbiFlF2rV+KlM1NIIFQtbpX1H1lDnryRE4TaNXi8EBCSJuTQ.RkA1XFhXADsjdt6LxXzF9m0zIZs+jzuwC9Rzye5H4XOaQ4psW2zd0Dz4rygasHvy+C+Q8MpPuhmV5U.B4m3.latdOdsSkowsC03bGnw+9eG8i5nU2vkMwUrScvNhEWsDPMf.LqMeBR5ret9P0KFrLCP.3Dr1RPjQBXMdlTKbFYLrh8gS1ClvTzou8XKp9aib6cnykUmlc0WilC6PyfkHLGny9idQg+8xdGu+rycOFyAfWCXCrLiaAWHlG2e9qd764jy5brpb+Xo5Ui5.mGPUZ8yj6Aqt6Xo81eFoKaJV3Wv4FQBaQBbAK3vLPt9G3Z24.Y59aeuQ1crm9bsaU+0G.bCiajzqL...B.IQTPTEZrJ2DAbWTwcm6BW94lFGQ9N48wIueNJ8cqW+IOqE.dH46net5RGYDpuqAp46Wwa1sK+bCifBFr1Zt5Gdcx2PmsZYwmnUd4kZ7Qr247ud20m3bYXVzpPtz6D3GTzjZKDmPiHR047IKS4oD1SLHWNNVorwx7lt4KJa2Q2uye9qdtT9JyKu+jdpcJ342kBnuxu2WyduV03UWE.6Tweq6y4m5o5OZ51AEcjKLom1oOPjgb7.b1VCPwQ5Wo9ub74lYlzk2XCKZzfafFtqubU5qL2bzpyA7oGymhmthA5Bw6tWvA1rcZBm2NfyUDLqbVmvHD9bVrm0RFv41ELsAjSsOBjom2uq6jVT86CzfWDftdMHVYGfeW.dou3sX7M.M+yMBEO8rG2gK5ecDu7aVfqsD3EAPMrLCLKvxqcbX+A.dGjtZWWf4pP.2vgpUom+tOa35e8jx5sW6ToGr2YQRmKXYyz.g4fbDBbQFTIKfeFbCFfrc6.Y7dXPg60J8rqOxYlXmK9idk9KWqFvhugcWzgwb+DuqjuGlsDKhGUNzeB3jjuGGOISm46iwhqLLeIaeB0T33A1FaLHJonQ+TNwW..89h+5t93WN8NaTT+0xx2Yo7cUGHy6C1FBcleB4XHsFXi0czRMjYVnco2sYyjqe8Whqc0elPDXTmUXnQFvpeu01Sby+XG6+zmWctSOo+fwRKjySMVbS4jlvzJjwjOs2.Gh08GqP3Aodp8GoT59STHb+wO+m7fqNeRBtxUzuxq9p1EZrfCq95Dlqh.OCvyMom6siOcx8uaw8RxJ+lvHcvpOMbcFEHy2XgGXkf4n8gRbaDL5c.+zMedW4XycUt2g6Hg2Gp46BKBF0pgZ0pI+z+W7iGFar4DddAkjBUZG.QAvxKvtO9ymkz9WwK9SWX2jE+J+N5ZCkEb7DHeGumu4e4YEE7GWFM0uHeu0x719M2qxfGbmyk0amYoz8eJVqOKX4DPPAv00Ans.wV.gAl9Iv1IFYwsPxdkPZRtCFDKtEN6VW7GyO9VKdAys.vlqlPybraj9rOotKu+5U0bI9aKI7H78yjw2iUYYqDv7H9LvD37gN5L.h+7wVwdmFWy.bY7QAvGcpphNcNSReqe2a6ljZIL7rHlKaszy.EAKPubssqhect1K9h9eSNNxWKjSDILnfmaku48IWw+QiR4JL1niXKOhyeLlESXxnJr1NtUmkiQFjgnWoP69vg1p.8dbl21IaTZmGnJ0Zzykq6u5u3uX5VUqZV7KAfIWDE6UkS+v+ot7O8TYO07OSa3epa1ZGpGN31G.c6o.kFJfVJTNmMvqkCQ2F1vai1ka+M5byDfJzSh.9ti50EW5xWVLYyJB7hkjX8RQ7NTE8txYrw1wxxPDagf5AmP4RHO495Ddqxm5Ta+S9z+RCV7hcM3Vcb0wPWXpFNNqTN5ymledfMfuvrSQE5s0nIqu1rYcu6brI6BfvTvKWYP94fHRBmxAVXALNXYKHiOHJBNJOftL5c2wfMy6fuEYUSdwsv0aN.KrfqMttXCrwgjiGR.e3re0vbXgiO+m5uWKk7jjvlKwueiD52GgS1dPmTonuF.voi3sJF61F.2qT.RlDXyW3bzWdlyQE13x7jUax6rRE2RH0gpqyG4UiGskn53xhYvMouLdV9W3pKJVS+x9sxPv.vvKyW66mk8+4pS6MxruTYVHeVPYOkJHZBqkJHETdVqxwHxWI7.HNQHnIXqNA1r15tlM2WT79rWvskgQbvyUlKO6rNfWkmanPdhwjkHageOG8TbrR7zt0t4nYcaVruKdz6GvjmTnIXrVNS1yHJta+jSuGtPv.b4UXzn56ze.eOZ7PspUocvETX1QBvcT95TtbVr97o8DOqIimlM17VFRg.NjQwRIuaeFqs21FQhRs64+65M3t+61IdyaMoaN7NSN1ZUqRsCCoHnDasWu7cd60lxNXqmV559gsj2TLxGBUdOHK5fHfgiXXMN3LNHLLHEAj2CNYHrcGG1jRHqYB5UX+3aUp27Wb1jkAbKhU3ZGKF5qhgDtUHfUwb3H4Oe3F4degucyD99DeOdfanG2qHtF.plcOF5Rt1xInukcWF.PTbB0k+YeFz0UjaaeZdxWD3WMIwsUVEdmJUbSV8glgRCzvNz9u+73W4y8hAawTv9CLdoc5auYwRt0+qdKae2+y4JDctQHcxSAXqxLJIkPQfkFHEJ4PoRQPrOCNOm1mLtrI.rilzyEJ20oEdIYRJzTZlw0y+baZmc5Uvbe7JhJcfzu3Vx0F8+Qy4J7Y3Qx+B5C5OS2DaYkSmiPbKDAAFjknoRmJyOQmV.glk..p+jmA7cubalaN4jaNRvdq4Jzcm9i5myalzd7E0Y7rBRTgTHfcti7mHaVFlPm4Bz8ASgN5Tejnsrm6ij9l+W8114OomzC.Tutn7ryJRymWbvszA6s5sJmsy8OMahOKHuIgrXdHJBHxCHCAfOf0.HYBVGACQP4MrBKXeB.Jvt7v3lBYab1AaUn4ct0OXuKU+0iWB2vMWiE4UQsC6PrHV8wdte7596Ze65d0bouqPbNJvMeOFQ7IN3T0pfdsjHNveTWGutTvdSHL4+vxTYNBvGA104hHOBGaLNUJobddtIAvN3FtZUqBb8q+Hetijqn.4AB6RlM6Pl+OtYa8q7a92iN+m3oxKEQkcdz3jCkfhhHAQjfHhXxMrf5YlbffTZoPoBru0ZEBI4zwHq2tvDjiM4jESv7Hdtmsh3oC8kgNVF3IjiNqUrQ5uuMnxua5jXpzbUBXr4lXfROLBvCx4N0Di5R6zz8lc+YFVHsUaxu+WBZ85zNuQdgLHKraKdzAsEmtea9o.SmWnnoHAWP3AgDDwLDVFBSFG4zFaVeQhHjFjKHr2LSphahJ1xw3P+vtNwnAVnZCpcXUJ3fSK24t6GDu48FOaPySasYU.EjS3UP3TE0jPAvNIPJ4.CXcLDLAOg.FAClbfEZvALrAFPcKAKlA8Grd7cr6dm4tfFqVUi++4cv+dwYCG57QOZQMOc2B7lnoqToJhsxdINP8QbAACK+qT4jCIXsZAUoRnjVy2oYS2R2XX8edjfYETpjXqrqy+o3Z5O59iFq5IzW+O5NYs59KghOy+zbC5iQi64JGDHxQdJoPoXlcLSLI7.IXh.yLDhgwifUD78HgSFAorBIgAVi1kPcx782a1OY6jKj+gOWi..eYfXj.fTuHw95Cbi0GXrROrjc1BVzyKlQXL8lcuACrDvJ0+Nf.BfIuPN0N2wOJMgKaR0yvvcNlnoUNLB4wdjjXGyDXgjAHAQvQhRLiorwnUqllCr.Iu0YJoqz2xEx74Znp6Z0AlqxknV9aRaAAwsa4k1syH1jrIYVLJTQAjLDR+.CwYP3R.QNXGpBcjkUfMJ.ASP..gyAfTHAClJBXmDrYrACFjaPSzC6Twz.fq8t7b9AU8920wwsmMNRLTpC5Zq.d0pKAzD7NUpyOWwK4BxURrUXnnrTRgJkHo0vjrusMl24AOvtzK8R1gJu9BNb4KizwGWNHLTV18I4M27WSit+twybydzCt2uDY+X+CBTeSjyAdrzA7XRBQdJHX1A.lER5volYlejA6HKI7HAwfTxPAwSHBndpB3Nfj9mx+TDvNG+t07.NZeC56TXzh.SfwPpsCGL1P4GLw4yS3kY60uoCXXY7tD.PiFu+If0pVkxfuJQXiflK6fnh03pHkhQfj8AIjCqkOg.NPRIbT.4.IUlLtDXZR2.SqzTWGjueml8aa1zaedNTgpVE30J1iR8JQ9sNPDG222jMnHqyFknb4gJTAubLQjxWnIgHg.xfj8fEdDrJxHE7vQwzDDZ.OmCPPPygfnQ.oJA3mq49E8PujzZGu7yO.++UfeTeR.WC.zBGUXzMvR0AcIbYdxpU36UvmJ9QB4JiEHa1DHm0xK0rIWGutDyUQTa0qic.fMLTlaPfRUPaaF9IoW4i84GpCfW8et5GnXNUXAJeZpWYclaLquKmxxhGF4ClIXFN+G..D.BADRB.d..rW.wDYiDRdzvHtPPTleGuNG+LjZUrnOfFJdD.3smGetR.EFyiKaJ5r1cYqErVGw+.GZ81u1I1tx6aB3NUpPAojPlZCXlJHjbQmkxIjPJ7ADJv.BAwP.A.IbNOIYAfvYoPG6JZszH.HBmJQh2dnfItJZxUWDX55wTGYdZcWnJte+PWZVAqEEbjR4nH.3IDNmmibrPXHoKiYB.NEFVL8R.gEP.ADr.vYgDDrROPpP3D4PWWDxzdX4vmH4a349U+cb8q83WXEvmLcz9.7dhGR3dLmQp1gjuiBNypMpR6.fEQSGpCT6bU3lkJgfw8Da+0zLV.n5bUDU52T7xyOK+6ditLJCfhC+7Jmjb7QZM+7.hLofjpP3bkXiojyYCXmjfyAgj4gpPaF+vaRI.KIPJFRIKkNljv4bRgfzgrfCbDQRbAzQuMG5qXosEGLvXCTknTSGdFiiC2qmq73dbVW3zwixYm6dbRRDu1ZK61XALzdzN797QKSi2YC2w+64JVj7CDBuBBkTwdDIjRIfTAmRRGVDkCkUP.vBofogNPKKjrTnHeHY+.ORgIGWTQlxS62iAVDUl6geozcekzz14YxrdVGHiQ5rFkwpErwBoQCxjIXsQwVqjMVE6zRGLBGLBFFAfk.Hk.PPfDLXA.AIRbRjIE3h9uqjm22K+7IcbCuaGAweCNCvuWa+endcpAd3Irtxi0VcT4g8KLeQ5pnHgFM4cVroCc5XK2sqo48GVdYsiR3l4a51X4t7RUaxky00rQ6Xs7fCzuxq9p1S9YFjSHhhrRkO4wNVgiz.ki50SV.hYPVFDyPRLTjCRAC3XqUPlLgzFyhrANV2ks1Arava.2ze86wabytt1o4cAEhb2cucL6bOgd4+22I6l+oF8exZwFcbzwjuW8UW1tvBv0.0QCTG3vAcdumA7DFQR6vPpH.x1GbPdgyZg0oHCDvBwgRb1vGNlFJa.fYV.1IHxRBofDDHHIwYNCPyUp3Zu5+aLvhn3LySqhXTVFRnGfQE3xb4RrNN1wpTnEJvBvFExP.aHMKAXiUAFd.NICqZX0R6jLrA.NECqlAGXAyZ3DYv2WCeqCWDXwaMLKWNZonC2OXMpNVDMNrye8STxQW6I09bXmnqggQzCUAU66vyA764HZuSv.3XOSet5.n5IRr82EbITgVZ0lbsUaZ+FUpPKuwv5y60V95..XNbCFKzf+h0qqqUsp4P2Y9cTACdAJV4wZuPuTgzpgP3SDcnGHHAwVhIACgBBR4HRxFCSrSHsYoJFdjPHxHqHNYfJV02j8u8KA2bXUL1yeQ5N+7S31..F6.22Xkugcou3QypCT40qHtbRDg0V18wZb70oGVH30ghqehYAeBFzIvv8+cJeepmOnnJQ1zAooNqMKsmUy.VA7bPHXXASRmSHDLfCNqUxtDg.NFjuEARmzGtx9L2d0+mXL+qf5eg4j9ENflC9bG6Drqjw5WX7TN3Tcsc2deX7GEVNBFxAkmCvy4nH3XIC1C.A.Ve.VwfELbLcrzzv9NvoYfTC.J0GHLFi5aP2DfZ0Hr3h7CGS9ggj4jG79QKY5cajabhqWsJnE+NfD98AjumHdr1GtAZbXUm.t1xqR.KNT4wWD7byALG.eiC6adTEpiCq5jqgFG6yDO9eGYHyJemIJmXfWjnmxWjIjHmicDYXljVvD.DRH.wLIc.DyVVZRRE5DqhgfkdxDgxa+rCx5cf1jUqYMwLqtI+J8dEWkKBfmA3bS9i6lr5+MLpuBpUsJ0..MdoEbe0u57x0dr6qSVsMJz.LduR13S.SN1UXTWV+lp9h8iGvl9ZHjFlyKI1SHHv7vxwvwVqvkN.rY.KHapLL+.uhk55K4jDQSGt5qfOyK+hCmA9i.LVxnt6XS4vBQZUwoFHT6rOfdOXSq.NqLDoDbdLD9NnHGnH.RB37XPA.fX3L.rS.1QvAAX1.Vj.Rd.HuNHfSfehAOC.FatiHVbM.ZwCoX0pch1hiJ1u2IG8Xbxipu9gcrpcRCH4C.vgCzv3v7i8v7j8DC.crf4dc.5n8Jd8Gc+i.MF5iD.Cyszq0nAtxLyKwxKipnlaAr3iH+DoCrtfhHCL2iTpNfyRrYoLrPQBAKAwfTN3TrSHAaffsLYRzJchQpScNGqhkVtMQtcED5t1ajn+uueSwTW7miwxK6t2ryx+DelSQak84oxylHpg4n6iRxexDOwnu9eM8JXMWvK7gr0t9WGGpHeOR4uoH.lOrWz60Hxoc53Fcrwz8yI5WnB2NtocW11eLXDdFKwjJhYHEB4vOCSRhjS6vv0uO4KZIk9amaDuc7KgNyTZM6m4G3EUAkBjNiOusGPKaLWR+M38BeV6niGE2r7YZ0e23sAu8ofdvDPPRfHBdgD7JHfRBvJ.qbnt6aY.JAPmPvkI.RcvknAysfPtErbKnBhm5T6a2tf5QdNOJc3dDxWsG9hOI79LGY9.7cHHLb1riSHgC0kUTEpK8aVQr+HeH4nmVP0NyKZWDv13y2vMO.MOJKpiGp70E1nKGewRF29Ehk9h8sVZWxL3.FCFksrhE9BBANRDPDII3jvYfvpsBslU5TFZME6r7tIwt6KDXijX2AseyXK.PjNhlEeAZw0dMF+NeJTpZIIpTQ1JNLvzyjqkR5Gu0jRa3Y4whzwdeh+N8qcu6kz3b+qRqehB.PggG36Id9OANYRzVuN+Y+betLkpb2BSvakLAjIGDRwGjoMwcmlkbdommvwBlfUXS6SHqmlD5lxvn6kaL4sm7oo08Jbv9sV+2PGTIvyKxS5FQxsswtRxTpk9V73im4LWnXRt2NeKb+xahtsp.2fQ.b.BJBpPEBCkvOT.VAnog4ApNyAvNX0zvzSqmEtzdPFtNBF4sQTz1HjiKdtblyD1gKrAvR0pAr3CYXKt3vJ2350.89Mer+.72.79HnTzg1B2K17o8R8iTIYwdQ4UxrDRjrECdfzV4BeNyO5+x+A1a+W7V1k+iSLn5C+daIbC2Y14mRa2Le+wF01RE41FVtBXcA1Y..Gwt.hg.DjvwfX1JXKKcVovZf1j4Z2um6t85aeKCItiGa1GucG6Ej.2A.4NeEwbK+xtUmcN7id1H0Vs4B8FzezjdpJrIqja.6efRYkrXe4HxcywA6c95et8uQi6XlDM44vJr5Ru9qKOxzIO1q6dnGO7vFpFM3AUqpO0K7B8yMwXF8yTvzuieVbu80lzXmgyFQ34KbVKHVKYCyfk88T7FJun0xUJ2c8mvrSV1WI4S7giE8FwSNtHPrlK0c.OBUPNTa5Ko07dSlZl5BE5r0aMxVYcJNBzchFJQq1o.YKAoHj788Ymjgv5fls.rEvZfwX.kkAvcgf1FdkuMxe90vnS0DmSGeqo+5F.fZkq9NdNeOS55O.+sBeGUaj0qKpUqlJc+oBxP6v1OHMLlbdjUoT9gBNmOKDg5fvP8f8xomp5Gy5Ocp1aLc7slq9vfwz.X848MP2LV8oJevHS6ssP4UTBOEAmgDxwIkWDDBwgJzzvH9KDLQNCHYWcJswtaaeKrl9MwAXSSur90xA7M7h4V5H54sAzENeEQiEW0c6yVNHT3p3r3LjjOsPgwT8ovA64rBgnkbW88cZnBJ5advEOc2eka0zr.N7f3eDme8IQ9NDKtvBVTuNe0qbE23m8rt8mpnoWqLWbOYFrgiZXkOXVXcBHDRsj76IBza6kyeSpfnkxqa15O3K4tx39dsAiNrh2wY4L2A72xlx.OE534QQYVd5yFkc2KTYucZ06sgQaftaJjNAnr.x0snzJ7sNAXsyB15fKw.SW.DmAX6AQz8f2X2Dgm9Nn3LaO++oC5t7m+iXvWD.0q+H5546Wh2h.Xt2OuwOXueOBdRjuSrcmGdtfW+PWHN3o8FbmRgw61IW685FIzcibwbfAPETbDgC4sBa9LgRlIxYx76oLg4JjVHTxyOyUbKWGnVipzZKCr64aau6eT4X5uqt0olM5NdJjY5zOwpIMbzXNKkCBmGrrfMByPOpFwLSaab31Xqz29GN1amWRkMXBuA1ihX4XdmlKgGfw.v4OeEArYE5zw7TB19b94hlLRIKvNxOc.AyllSY01QzIjXyU43tsP1qgYc.vpNV8vdOHdOBZzfaWspqLPpWkocSNHBPDjkbfcjjXWfvQRGjNkmJMr.5qJJOXzoccJdNQbIslmrxjhl566VEEM.CPScW2cRF0A77.3WEC1TRw9RJZxXyO3GKWuUreHyFeUULhuaF39Nj1y.xXrlA4X3AXYGLVGzFKL8yPVRW3baBuIdKDdt2B9yrINyF8W9y+RGKzS0QCZ0pfv0wPV0huKOymHnKe2bEoe+TzOOl78jqMR5Hu2n7ryJRGeb4j8yoZcScvAaZyY6FWvzWmyE6B4.cfmRobIwjLHzw9tL.UpmHTqkVcobrmmzhRAk3yiOW1hyumc9kA96YCn+fMFXdv1iz8C8C5oEi3OnK7zl1ooPnlDN2HvQQNMHm0YffyHoqiTwOHLx71Pwa9Qj8NXFHyl1afHbJep3tQTrGX3cZ92LYWwcmqTzYrxIk1cmUHDOqumeIgxW.gTZ0LoSrkS6knRFH6zc+rcwHxCt2XkRws.otd0FTM.fqCBKbjGE8d2lt3BK3P8577ybEb5oNeuxSBW+.QmPqPYSHgLjc9RmYzIkogi4E6GzMEniMsSBiv4n+7NvVNYMyvrwddzNrLMzmG.xMsk69MSb3bY7SMWh4Agg5M9qmHEFYJba1At8ZxI8NCDIQPDQHEN3jLzrCrTCcXWHB1ClSsNDyrCB76i6fiIebcHVrJnYlcdwFcKv3WFn5gdCwJGRDWYNvWC.WakG9P+9ZlumDdr8678SjuiwiGk8gQAEON4ynK528dYAIcyEYyNHeb+zbDT4PPg.BrJXDeoLe.77hXgTJbRo.YrmL.Fceima.KdP+wD4wfAXZjrLBM+Lxt7Oi5Mb+28GfA6+i+homVVL0DEps87ZYfsDHJGANfIPBGaHmKiCn37dXeUQayy+xhN+F2Xq3ZK0jqcly3OVuDYqIJhj3LS6nDdwCJ4M93GLoGzmWEkbFRkaB+bx.UnxoTjSH.bFALFYTR2Aiit6OArganRpzE.P8wqbIu2JNhA.dluZSGu1xNbb94c75CdmcZZ.rLdUab8poQyDZO8ztA.Sb7KGGUvk3Oh87E2ydxLTXXFrekGim+PkZJXu8rE6slCqB7JG4nQ0qavW9JovUoCRdPKLXq6BkJbXdnW..i.X8Y3FUCHSPvn8gLWOrgsGDalhejlL.n50AciKeIQ6h8nxgwGeOTYNflqBtZMPGQ9duv21Hf9tDjguuj7cDNYp6chknuSkJzoJURLHLT51y2uaOQT+8SJX5ay4oBhfSFX88UQgBoLLf7yKgJPRjPQjiEFIacFxpFXU53XxZ5KxPDN+SOt6tW5NtVK86yepy7FtO65ax+V+Cua14+r+lYd+PWIw2jtsoiT36ajFVHFL.f7XdjBryKz5J3mYFWFpG8hZ2EtLvhuzJ7+PaapYhuLWvLtMiFmeskWygerOjmC5xf6MoWfbTJnPnenuTFPVh.ffbBOIShPkKauhneyQAOZtMiLd.0ST9E6IjMSYak.9nFmCaoHTqFM+K+ED.CCq6R3FtiVpZMTk1AeI549xM31eg4L+XycNK.Pqr6w..alDwsWaY2q7R3QNalkZzfWpdc5pW4Jx1ggT264SwQdzN.Xxl8cKh0b3gtR6w6Kkq2.eruL5u7aVdK.QWfQT.yAD9CRHnLgvB.hBN3MtFwHCsU5yjaW85qmpwhK335fvkujX4CIeE7OGAbOzNIhwkuDvpCmE7ZuG8gd+bzCMZ7sm.+AXHpUsJUtXQpimG4uuR7flIdIaoyYRL4HVjiUT.w9pnbgB+PI4mGvOuhDpCcYWGQCSvJVh7Rx5LrTm57QXFlTmhhcyFCeQyruvzzu5K.5uuA7W425ejowuUM8UwWP7xyERExtGssNh53EyS62iaGMK+ZKenjzWuNc04thrreQZIzfmPVi6KKyJyKSva7gl8x3ebNpXLTQJgxiHQPf.BofYhH3DvBmyB1AomyZxAam7XPRz5pYBtDtbeUV2Bt8lsmc909ycW6FfuwkecQsepm16leEafuWtPtq1STJjQUq4+3m4ERl5pWMqc61lcVbQWuMWhBpdQwbUpPSGtOA.7wCKA.f88G0c+YmG35KiEWo9iLx+UuxUjarwzdalD4Ee+dRWpfFzw51przfu5YLXt+ZKVcQyIEqoa.H9E9O5LpOzO5.deS93CNXOr5s9V1VGLIic+3.lh.hQ4ow.mu+3t7az2sZq0sXXlSvnA.ptDOOtD..Bw8PRxvY96Bb798N4xNOI9fy866R3PkiqdcfFCEEZ..n7khr9vW2ueDYxxwhv.h8TdETxvRD4EJcJEfRX.yCUgOhBHVHIRQjzywBf.GacrraZ9vh9i+gGK4K+kg9yMuuSVxRkTWDm6erun5hKZWYw431QWA.mCWPmmfFTLz.H63LUAMZ3ZWcXZic8qCQ5+rE4HueVWeUBuwxcGdli+EWIy7SOQegh6QJaBI8L.PxNHrCKvP1YcV.VxDQfsB3Nvaa+oUiiJB0EZtj9otQctV0u.cpewWvazGTIp85bIeuAiMXurxNCB8bVNHm2.w.Uq98Gceuh46+bWY9zztWx8VwOfelSGw+eeXm4ivVY2iKiHTd1qJpM2v7s7T99zs6jW9l+UY9asiIWqatWtl647PBQH0ZgIMC6mWi3tZL9O4f426JIKiOlA.bwYlm5TMVNVgYvXiAa+QNCegzYco27S61o+OH+fNZ9Vsx3MuQWGV5UOpDWNN63AvgUwvRXdbIftEXfgElbyCKOj2szL6CveywwI5wg4.J..pbI5ZXI9jCpkJHB8xTrEArPE.xyyKuRDUhnvHISgL6IULPBfsEIHEwTNRnxK.jfYHf03g5IsNPC..f.PRDEDUrANVHCB777+vuvYUwW8p3NW9MMUpzT.LL4lp9ElWfZMbqr.bafKKlY9D9jZD6IkJkZXAToxknKiKg+Puk3tcb1IjgG+5eV+tteq0GuSwI8aq73CHob.IHI6vQI+MIDvJIgUH8SQvnIPzSCDg9mOuPs.tNpUaNU4xkUa0NenoW136deyLIcvz5jrwEPlSjIvf8Pu8klsYCs9jWzeqzwOKOHrpYByY4z743zrzi22XI817o7+IH3eQpiWIpr+ZhAMOGsUxLpja1M+s+K2dztaEOV6s2uDFXBQpP.SfFYbBRRyfNMAAi05lyL1tXiuZGFeLyhkiobyLJgS6I1z4wJ.JUVlvbUnmqru6BYv8Q6mwnZSdwkdTkVC3vzd5nLrn9RGuejKiu8xMwGL62e6vwjvW+RBTr2v19pfNZufCjRBcHglHIHmGwBkJxS5mGHLRxAEAK7TL6XGL8gw1FZ1WnjPBQH77jBa.AgvpDvPdpzTmM12FVwqvO++0xabimJ6ZUwPc6oRSAJUQTa14wJ0aXQC3Ju7vY4Je386bXE9v7hSh4lSB+HBY2iwm4hH4K9kMqO2+4.XXYTMlWL+a8WE209B4aI7nVdAhNffuMyQNqXXwIPjQ4aSBKUrCBOUa3kL.5hZuBZVg4lSVtbY016jy2tqqvtqaOcxd8lyjYlVHkEfzKv4XQVrIqyCbSXhYO+QjCxmap3xQ+mvlrHGPK6XxjGtIaInV1JLvEPfuuv32QJ4P4d+08xem23Mmt4M25oh6zcFLXv3HMIGXgDFuTXEwvDmBioGric+txd2FybNPaT+f5qzv04LunQknQeYLUJnhrjJOrxhjw2mNxfoClcV9PS37IdrJOdpNcTD5pVEzBqfmX8.dDErVUPK9AyR92NbhfegFMXbkqL7xBhDPHcNgT4oDROP9Rvxb.HPNzQ3EfbFCCXgB8APDjjcXJqwNgizBHrPaR8cC5342tnxuPnrApiqsPCGt9RBTbdF9fPXLcspf.Z3dRAGqAFlauWtREWkJ2SjjDwS9Qh3H.9FqtBNxdpCGYRGNvkRB29BOUSoGMA.6wFxBl8IAXRRCj9T6f741BEFea3ks+TYCFb5a01pxkOune+.UxV5nNO.i2uk4LFMdZRfIE9AJgRpDRHYskSRSJYrbh5aEr4fwC6TZpm0Hp3arIm1kBf6biafIymmN0K7BtAYYLPeD36K19aMsZ6uRyBO3as8jsWe+KlrWqmC1ryBaVYv5bfHID5LPHAvlAosGbCFExV9.9.y72GMZ7I5MG9J1ep+Iqhf9EUCbVNvwrIKuKz4XygMbsCCOxDNO5K520HRdDI7xW9Rha..fK+tRtpe4KMzlpqtD9.R32EPkKQGYM3GAmHiXPBlcBgBDBjrTAHAHR.g0PLPHbbAFrSHfOrFlsVKzoFXyxfoeL4HMoyThXtEsexnDpVcnK3s.b35KCT4R7hMWkWDWGKVuhDnh3EqVRVbDOwji3cbo0801h4u7M9yrau5+q75qrhEqtpsN.Or1EA0.M35KC6bnVFFweeUf6ARONDDRcLWDFguf..w8Ud7FA4r2IXTusRSh6rcyalrMVvoB78Eq+07717a1urou3bLTmU3GMpmGkWE5oX.AQjjMrvn0RSebpctY5Ljj1OXB0fOxemD6ctycLMuQSQ6MyTK0NOOe6NtYKulamJUn8+KGmt8a2M+5e8MNaxt26YPR7rvEdFvQiAExAUV.Hq.L4AqyGN1BiMObBeX6GBy87.mIwTy7fNc9mzc8NcrkK+mPcaqYq4Y3bUJwMSSY.fbG1vsV6YEn9qSX0lLl6IWmXGipfJVrGcE7XK07n8sfgy7MyQKcBW5CHg+sE9mi.heGWli0jzjJXxSX0fXFBmFryyBX.fEvJKvx.wvrNzp.bRl0FVOHE59wP2OgYgjY+PVR.q9sbL9sGlhkD.iEfEXI5pe0upJFS6Me6HutaD6EuWPfGj96FK7kJRDOfcC50woUmyDLwuf4o9Q1Mavm4b8Z7INHo9BqvUOrTYZfFN.XtzT+W1QjmumTIhAi6qSoHapUIURhkHIKV0JmQu8S6PqUaljdTvAUlN9x3VYAatFWVHoyLZY5TAECy4ERJgDRhfzYIgikBRnBxxvHo8PkVsSKE8fvclsba2RK7RVfZLvOs.muBVdwW0tLZvn10EkKeNU66EWBa087Hq6bfjy.YgBvOe3P4iv3.YYHbLrrDFV.mSASlGR5o.kZQVSM.xV+O5To3GAwAuvKYyk2yA.jlkgSo0L.vVYY7gxRvPTC.Cip4gEH3S.UtDMeoHwJY2iqigUlFNTAuQiU3iHgkCio1IQ7LE6Qq17CHe+sFGOf1PXFPDPF.LfcBxYcBmgXsgYQBfPoNbolfAxAmvxBG3jX1wYFmynYSZpywZFZmQyVWm9VGtg9wG7ktzq+5xAm9z9Q2NJHPpCSxahBcx7hTjyp3.AQRIXKY8Md4hzdkGy3RjCldrJtwwo0MvBGopdG1OngqW1U5+g9vWXSJzqcJm5AqPZSEjUPTnzYyzoowGDjLRRqzUOLvh..pseaO0a+0iCZ2B4By6JkufpX.SQDAeAQjiAw.hg5nnuPPFOmSDn0Beabh3vn1y.K5.powcahii93hyJZ+wEQHMYLjkdJX4ogjFC94jPjW.AIfmkfPSPxBXYFdB.mvAs1.NvG1cmDtNOE3nVnur0ewJ4S+gmquqbmNtiLbwfCIc6zsKu3ILlSrv6yzq6IfKgJzRnJ.dnaMcDI76zOqO.OAbXTnOBpbLqxOpk86pEJgQ.gxkx.g.ZgRHNdKEJLrhSgSmB1oSX8fDmoWriyXFF1JCUYo5xY61VYPXh6PEXeHpWmtvEtfRmE4m5zgd9tbiVLLuHuJuWnHJHTDP9PBKL4LNSOWISPBmo8jN+Bx.2cMdWp9q6VpwK8Hxewxe9OlY4506UqZ096ToB8bEKR3v.S7lc6xS17Wm+2uxhbc.7GdTl.A.0e0e7AX6uIHjJ4DBVsgIqgTNMongR7BAFD6.yP4HIaTdrNxisYLexFQFXgSdSQ37ckX+h4fHtLr8FCtzhPRA.YVfrgtLL4.DVHELbjhX6g9mFGpgz4.ZU.XvTf5NAzaj+tesx89gqhTL6PQ34M61kGdH9M4kdWjk.7XpwEvgqtr5PKJFH5c7KrDZxGQ9VbEvu7rOJw6CBHy2g3js+Y2iAp7HRdhMO6TAdYxvnXxRdfD9FqUXRHgR.hy.A0gRdBC1kIYcbpyDmX08hs5zTKXqkh7zDISBB7SGImm9fasmatGNHJcoKeYwf94TiOtzyFhnrIb4zgbNgTDRJxijVIwJA4wBgOIhJ3Ior7dhC5qbCR8Kgc7ah2Pi50cnQCfSTTwXnYtBfg6t8cjGrmLafNbkUp68uXfAipzfT8PdQGcla.6NzOfYGHRbnniQLIEov42S445VXTuX+RvF0d228NgCpnfLNGLCFAHtHHcH.KA56.6XvBlXlUDCvDX1wLKs.RKHh.Ae.mB.E.bE.iHzun+ewJZ8O0rI12r6vBabxS33NO1c.g50o427KKKfckocrzlsj7GtjGu5mxmWbNfWvuB8NuwW4w9bpiM5di2wy4GPBe+gGoZHNZEDMWhOJRh4rVNv4bsx4xhJSw19PRNmERgmMkjvYXmBRglHRvLIjrYPpkM8sNahSqGX0Ch0.rVHTIJQoAFQXR3DGnO.q4NVnspWmd9SeZYGuAJiQ5SxfvnhlbdRYjwHB.rRcFHkxvvB34CA.AZThYmVoOX2fVcVMnycWMAKtn9vmn28s279.p+YS.b.4Y92kJG7fCLayNuyXs7jf4.ghFVjqjzwVXk9TeoRePzHd6WPj0ajRoY3BuSu0aHpSvjHAnPHz4f05C1.3XFbLC1vDjNxQPxPRjD.LbBO.MCvPB2Qw1jB.DQPR4PGs2Dy3Kvg9L26765i9yWWbI.QZmNd6duC7dquQaEa6PiTtDuIHm+anrekjS6t+E228CMc.2LCO7fgeBmDQiarjq1SPJB+.R36M3iV8QUPCIcCyDoqsB3ZGlkI..lRV2nkC0NsJtuJUnSHVvNGoDdoZAn3gY+hkjrRlxrNwpGj4xhGXEttNA0ylZnTU9BCrAkFfHJc6Xk93LgBCyC4z74kAnqxoS877K3ygxPH7hb8sRNCDrf0N3TR.l.4EoDBhDtX3kcPO+jsuke5NqHAf3wNtq+FA0mpPe6aaBi2MQevueG0CrNwLryMEyBI6DLIfk.aHIzf3Cj9dMKLN1szzAcKNBouea.fgdm9IqwqKgKKVJenDttRPZEX3fRpgzIARbDzPxRVnHPPxDKAABjMUxvifSpAfEDDfDdPxgP2I.gS3o6DRsCCom6v+Xu4weWenarVsJgeiWPsDZEfM9KCg9yFgta5AWln61Vy5OHT++Ckkg+ClTLoUeQ4tlQN2OFW6kuFcbQIWsJgUN4Lg0OLxmMPspflqxknU+tjeP78U3v1rFM.W65fdytc4muTItsw3rEs5QXSr0FJP+DXDRNM1wNiirII..f7jLDFVmDayR55HSWWZ21Fc5.izezTuQKNXjyNQrwixVCKawI5S9bEKRCZ9WSn.DJTQYxn+eYu2zXirrqyD76bu22RrRFjLHyjLWXwJUVRLjJKKJ0RxRdxTCJ6VtaWMZKfHGfw.y3dLPIzVvc+mAv.9OrBfYZ.iwX.FuAHALtU2.d5FIAF6FRnsraMnRBHKKAIpRVtHkpRoRkKL4VPxfL1da264L+3ELSxrxpzh6FibO5.jYRxHxfu28cO2y12463AJzmzjmRGnxDHtTwA.I0Sjf.CfADyNkRyJx02SkF6kdz.yhMWTuQdtYe6kml6mmRLliq5FO6twWUVfvPKH16tvppvrbrRABJjo0hizRJKna3Xxl0WH3vfY4gprtNzIGmb2p90nk+XqxafaRKh5zFKVWgt..Y.RlC4IVNFvofXIshIkhgFJQSrPJFBqoLXTfiHPgVPLk+9SM.oZnYEXEsceE8bHulemIqmKuLgseQM9ysEvw8Jin9iC64FC1wKCb9BvloAobHJLAlZCfpTOzoT2a6OQe7pCGtFro3E.i0WWNqx2YWQWY8VRyFq9ze4ep7zkmhWBmLzN2odcAYYbEsIKabepZRFkcDINXYjpcvlvYIwdFC.bBaUhjFGwtnDlxFxBOjMNIEkLwgkpFY8n3meg8rek2+MdJ4CXW3NRyf7EaBAVQPxXRxXxkpjzLeAVImg+rVnCAILHq0BjADOjIofSswqtAArwau6m+.T9..L88SkYvb3tCZm.+IOtSO2sqNFFpzTUJfUAkDILTyJCwL4hJVybXwYSO1O4HafyI.2BqW+ZzKVoO0.MoYWZAU7w6otP2tppnJ5pIKDjBwl.JKED6SjnHn0ZHhlTrFJvBQDYIE4HG4.3DBRjBbhBJGAm0BXRfXr0qmiqT.fc..50K2Ufl2TgWsZAjt6THcyK.93KAW14AYlBPWAjyGvAHYwv1+PnwNfoGBo3CQv3OD+ERG.flu.3UdKU.yWYOopg+T2O+6trW617y66S6TB1JC.LAdxXWJNK5P+jTXCF5nXsu1iyavGjyj7hH5LNKVwDWRDaZZsx5nhDm7bKrmcTKvIm3B7KCfa06yIyi65RR9nbeJ0o8RbJOe1FSb1PPt3xRVrUXqVLw.IgfzoNvtD.qENGb5hkrTjdjh8acaZKmlA3NYOxSQQz..z2OUl.UcMi0Iqr9QO7dYiu+k0YdpwqnmrDQUtLTEKQpXqxEV9nLMucZmnugqXouF8bUdMpQXDcRv00hBo0yJPU0IR2d9VTn3PHdcAPDjDGXmPvSPd0dHHJBjAJhHmSCErhCYDDGANi.wV.d.7K1Cdi0CSOHofm4QmrcNeepVkJzpK+JZ7u2q.F9ZmCY6+LHa3yBkdAPAyAJrNLgUAbAPrZH5HP9G.X2Fb2yAGFGTpAlPA+GL8V470SvSdx1oJLOvOUw6GI4o2ejBZ0Bqr7xnYiF3aA3ltdcwUxI0truy10Oa2.UZkgtXxqnudeOyPgTrMgTt.wZhEBZIP4KGeXe13uu0lEFUXA+rOymaEGZ0JW46lPkC9afWF2BaztM2K48Y4xhMnhWFDJi.asNA1DvoQZFVPV.fL.euDR4FRoCyDOOk0OnXloTe2UNOvseatkeDRq9AP6mlSP.9gazlegkpvKFE5l309VY290.98u8zr4SCLy6aRcoh20nO7XBwuND75Hv9.A2MEAyUWgz6K85UVVGqHarQSGdTBRpyXLuCQnWa3R6AWeg.HMTPQZvfDMAPfE.GzJFVQbD4fHVBTFCs5X3UdKXprMlY7id9Oxv3x9omw0h+iqNsAe8nPj7clFw26ciz3FPWXNH0lD5ZiCuoJCSEubLYyBDw.1UAt9ZvcFGt1SB0DgPNmBA0993qN2g3C93lR9mhF6e7kSC4umhbFkPztsKXgEjNgg7478cEubnEcCxvPcJEOTMQQspeeCbwLgLOgRMhILi8LUkYlHf28vnzwK7vbkuQJdndaEPaf3BxKBfO++tMDb9CsyzvlQYlDffHg0Zx44kjkoxFpkrzbKsFC.osBmjwYIoBmVv5Jpx7mv2M1rfa9B.2L2H3O1iRNyVqkOtnWDqK0VqAUC.qi5zD..3ywqUqAsvf6PA9c3zjtDvdHwDRAXFLLHgB5dG2J2YCdka.dYrLsxox5D.XT4a1GEPazmZSvcDTpPmPgPLFxozLAE6XBjkDjAkDIDXHPbPYige3NPGdGTn1VXZcOup1zo+.CncRys98s9q7ndqcTHty8mBQ6eEjb36ARVC.8XPAMHnAzo.lL.BfcBfifjBHoEAOrH3jhfiIv8Df4ivsmc.9fW2ALpn9OM7Y+SkenkefaNa0RVY47E4lH+fuuU85D.bOWkJVjEltmojJwCDflzgBpDB3Uik82NjK8rohqiSlGPV8V2hu4MgBKrjBUKnfWcB5DB9iyKkde4257Khazpk8mY4+z3pWtnWv3ETJKbwQ5PmkLhPFM4TVKfMCRhk3zgJGk4knCGKMrvrVksj6W97HebjOhjgeaNj4sULmn705z8N2nLIds5WW87yMmNoTIcmNcTUBxa5VerO5gwvEtPaLXuNXk02RdRyDMwM0qCn2n3PFicti.u48nztUIvjP5KHhNvwH.1LEClHhIvV.NgzbrEJ+9rm5H3O9CPwyeWTd51W3C4kV9p8jZwqAf0vN3eA5mVRe7c8JfiN9bH4vmAfuLf+z.ZOPpLHNG3ANXAkWrQK.mQ.V.I0A3APhO3AK.WWAJytHYlcweQPF9Gd8LfawMazhVbzHndi1qJ+T2O+OqR9ZYqQwT+DJqqB73B12nAgZ3rLU9gqK3e4iKEvMuITMWXIEpVWgviTvqhJm5OMD7pykm3gRSrnrxm8aFO1+n2GM+U7YS4AopLJTR0AFeUfSyZMf3hEd3Pky0qfqWuwFFTxEMyEuX5sitsqQCHMOo4sOwEymlq1+.XcdyIJeBdLYnNc85JTspN3vCUCbNUxfATsBEnDUIJfYYfaZAtTYS7rnF1C.agkwxmLzID.P2AKnJbdXPECc4OjD41b9s5sI4OXPllsrmSHklrUIR4CIQI4yuHPfbDoGpP3dvu38CJO98JLyk19bu6p8a7g2D07+2Qe3IeccIcBsR2Vtrt+dDZueHjzyAluDnfI.EZfop.34.fEtH.IifHJfXEDq..GHiC5pLPR.xhlDtXA1tWFdO76islNBaT0gEaw+Fefq4cU+JJmae40l6iyKVOx05Vq9VCv6ep7Cs7VhVjSNjqEDJGcSD.jSFkYsPK4IYyNAfP8qQnZgGq7YJn.oIfdXqsqfBGlw+ytxUPwr4wq8m0NYMauL7QFO9c798BKOtoPXXTnenxDervGtMms4lHCGQNzahz9SlDcPotY35fadZlSnwitGvIWym4d5sQLsPKY4QEJsIZgZU1lRJ8gUtCmUIEc5DLAJoqS.0P.Cwl53wlfktLK5QPA6osttFtCeY+5LvWWOUgph+6b9t6NwyeOyCXtywoCEW1kkrCpCpaYBC8TZl.nLgp1GpZG3Un1NAAkdPoIK7fwu5XcW3R6yAEusJn38UyF5qA7Qu6eN9ge6iTvMrHR5ME3zY.BBfpRLnRLT9NHV.XAbJBHCfyD.l.oEPEk7ona.AxK.jtJbGeNj98mEF4H7fmKtYS3tpeEkuuRasU3BtHY1m.Hw+vH+3FiveuVVdYUyFMn8pWm5+FUdzZV4q1SdSnW5zIqn90Hb8Qe80yW3Vo8px52HeMbCzfxGlKmAJX4R6UETYIFdkInCDvGKPMF.hfxjPAa3n1wUn+wnD94.vVlXdsu5NQ+0STJclOxXoSdkfD3qzQ2kc8FXyvCroX6.F8icX6CxPw91km.mMil0uFgmVMgOsEwSxF5oZDYfSMdxtU8qQ+F.3V8dcYarlKYx2MB7mWxoNvKBfHzw4jhJmnONSRRSk8Z2lW8VmPTSKSKdFHbcC9dS8RtYvPsMZR5BuW+zYduMNb6aWJ6guQb+dG1e+riKLs1saEO34qfkHSXpW34Nxq5bsKLwT6O1ztCqNkqmoDEkDThGt6Ln1y.7kO3gYS3eIJZ22GcvFNOjdbQ3FLNrwUgwi.oGBkAPbJ.qNehvwBXv4COPNelfqyHHVMfRCnX.n.2sFh6MMLIO.eOygqrRSzbw0sKTaAj55IsGzk2pW8mpxTqQkm3sP67uSPV5mfjSe683Y3vSRztKuL0rQSSTXMyz1BdlvgFqgHSAQBMWgwhIY+lu22aFAj8H76dZFK35WSg5sUv+RDBORsX8Ecqu7F12tDhQ..qurHq2xgFfebGvmngKP9Nu5a.rm9Qu0Z5T4p5DYBu4jO+eS2rw+mV0UZdsUOfT3cBmYSNC6Ga+3d.nlFegaCGt2MjW9Ut1imnXU5Sn2ODOWexRRL5mY.xQkPiFqJXEfquHnaAHaOKbu9V+ar35Kioa+3hc+u4IqO1nSvN0fs7w+6Bc3m4Bf4I1mSBLX12afyqD0szjdY69cJ0aXmY2xFUHv2K1XsVnzFaPsIG5MQk9SLkdvruqBwVXyPXlCcCcES7EfeObq1+I7yUYV5M9qeOdHIIDYCKgrgk.YCg3DHbFjXCXVCRL4Y+TI.fGAZmQ.2wofX0fzJnzV.xBIMDt9UghCgyo.Zharxh1WZ1Our1ZqgxmGxzMd6J95xzeWgmzOAKOhHcAP9z94UtlF.Pt0pL0BRSbS0JnNgsqPO7hyD3MfKscWUogQ5v.OkwTfcHgRQR4A6Hd8a1norBZksLVlZgVmh5PVEn4hJTxng+z5F.XiFvs9n8WmJmE4neZiFDtyBJbXUEsSCKZcCFX0SJEQdV4uKn1sgpt95JfA.HAQdyIgi0kwZ2g2dajM46YRNXLeU6+lANupcYb66vevQjzTE7GI+FuxnCFNQhKH4.5+Z.XU4sZD+8lJIwHqiOxB3MtA3kGcJcC.ZilqIqtBdRpcftFdEcgqDoaO3A5H6VJyL+uIE8tLms4B1QDnziNouYylnW4+X1s+VN+zYU818mUpTalL3qclp1H+zv1IGMmxYxS6aZDyA0XdhJEsIEbbOzyM4job0rLYmpoxPTGCSSwz0ap5rAv1cfBwIdvkDBJKDr0Cr0AsCvES.NM3QCOTklyyVrhAzxnHN0.NS9HEU4.LV.QAIyGoG4A+YTXu5DVoE+YZ1jwdkIzYZAXw2dErSgx+GI4qi+DiUveTxZ2i1PcJhzM+6Q9luvHBMfzD2jVGKpwE5pwCqGt02VW0KfqwRTMN0TdHQl.eEm1MN1aG8QCS4CyNZhivE9x8WYypNfFtkyyIQtEQ+KQ66qzSYHMBBjlKrjZ8Qk35Z3UzqB.zrt5BUqpSp2U296TwCupmAasPJt7qjf4gkV8VLtQqSxLOsLtF9fW48Q.y.f4Pe+7QT80PcZ0O4myUd4bVQX0FsE7IWWZhFzF3ORZhUv0WFzbQebMpVUAWffrBBv8eL.y+ArF9HkvSImYB415zVudxh7u7qXvFOq2pOXyBXvgEQYt.nBFnM7XmOHchOg+vq8Q2cvzK1NEarg6Djj7NtxuD5L68Q29SAf1Hbh5R53V6XSiT.Glzk9nKnpYYxNooBvQ3bm.wrC.RPdu98bUpP2euppnc8n1kFv3Ha96QCEX3Ag8AwNfrD.i5L60YN2SyGsdnFsrnIPFBJeAZOFLwfsBTFBRY0HVSPvJqvmrI7Gzh8OoK+nlxb4Ipy0Iw82rAHTstBdQzJ.tUVtNg0pYvvopfJIS043jYBCvzddpIAghLCeQHxdrJkD4HQ311L8ty8ItX6CE8Qy3+yOr0WudxxqBPsZIx0ibokLt8gBSMDVbm03VXMYQ7ZlMVJ1fElN74VrPXnx52ahw7x9G3UfGNHzNqmUIiE2OxuO9P+58w1+ZCw89ro.sjMvmRl81c3Kdkqblwy90AvznAsRqa8nIbatbCoQSPK7BKoB+HQDtO.bA+msCQejB3x.Tqyzkum7BKqt17+Z9C66U5nEciYuzDSl1c3TFmdLlmHPG5yAic9gklodmv.ytZ8yzo3GaldMWbwD.fcB+PpfJGhoBB..n1saKnZU9bwwbR2749cdCK1VFdpescdhKzoQNnqK7cqBb3sPiFkvFuQcGbiaAGjAUfEtXAj1.Q4CkAfLBDlGc3ij+8.3j16jL.pB4MaAaHP8EnJmA8vD.WFn9btmE.OZcImotU3++nzpEeJt1A27lOd5J+EW3qq9nYWo7t.0Fre7LYb34CBvbDg5FOTUojPPvi.zD4XApAJs9fBE48H+Ba6WDaShZqce8qt+rKUIa40ttPerOl6l2DI0qeMpc6Ukar9xxRe5Wzr1JEJL1bkpM4blo7MIiqblhUFKHzCoEyxTEPb.xxrQEsliFVW1GuCbf3+O+vpW3k5sB9LwnUCtoe5S8PnmVIeuwJfWdw0jFa.pY85Dt6IuRDP6MNiu82K1...H.jDQAQk9xOJIai.DyHeuwruDzK+5PZsJNCBxa9O7WOvlMw3Ieu9SUHwaVFIy4QAyQYI0XwDpLANu.yPhrGFcTvC72I5NEmp38qUqlKIMkCzZxZlSknycyLHIi2+05w6U3gt2l936oKKuLsz1aSQc9RDPSFkZ.DWKC1ohgdrdPb8AUrDHeCHeYjqlN.9we9JA4veSkq3Qg.pR.jG.OlCZ6PXS5AJKFlzSZ4I5zWf+8dSf+3KzIkaZQrtzb8VBV79xeNtD5z6hAwGmMsKQdVim9BFsbNiGMkwCUMAHTqUJQXMDRKrh.jwIBSnCzSq8nyiL9AY1LS4YwvZeyzAW8xyK3duBtwM9XNfUw0dkWQ2rYc8q9+y4Bquvfo788uLRbWNU4lU.MlH1.EkUvy3BjBJn7zwpD6QFcZ6Da3tvHa5Gq99enk902cEbo3E23UnsQEZok.NALJOsa3S7JrUqSLN8zAg+n3Kd5w98zVHwohAbY.06ozELWrvlbS.YwQSZzW5q+0Ma0a7R1to04L+KRvMOzAWxqPoYgGpBHdPWjUAkSA60MKQpjb.4R535FGTJREpRsFixzUqbdDEvhLD.sePWdssdDM2+Cu47VsvRe5kvG9p0U2E2hW8y+hBFLVJTC5A+4ZCktM.zP4GBxSfJHCxIDlVVd1OAxUmHeBvifJDPWj.a.zQVP5dP4eDrCGfBJK5++L0b4E8989foT3CFiwR.MuSKdkUFQk1KNJdvM1fdzW+ecJTSbS0rKsfpQTH0ovBxKiVtWNtf7Uw8QDJDx13oYGdGPgKGVfFW4gRFCE54Ckx.vNRABZgIRXRTZJTanRBQ0rhpB6vvriS2DyLSFhF39fWYNY4a+JnUy1Bt67lNsGKnPDWYXP3LjhmmT34HBWvwnrhz9PCeBZknzPaXKLnuxoqigndFqpDXP13vL3k95ecaqem6vXEfWZsJz4GcCd5r32B.XYflMxGYZOWkJmQQ5Du1.x6pC5oXD4GFW8M.svrKAxefiJDcYpIJoKekIHb6kcc5cwfjttJC5PSl1K67RFeQRQmmBpLtpPo.BJMTFQ6UvCJiwFybZjzNsm49EGybXP+.dfiEmGQYpbEvirrrPs6vq8IOC0QL5B8zwW8lUNE.r1R.ggs44aWGsJ2SvllL3WsK7uzCAjYfqSQvYgPaI.OIO4KJALzfbzissqHPDkCKMK.m4.jXnqzAdSd.739XlGXWLZMJY3jAeiuZYcmg208r8KXuyqCK.dyEh++JWIbQTmpEkOOE5TXZAs.Hrl6+wes+095CxJXTzDjVlySiyaBfumGYTFXTFnEAJQHkv.jlf1.VQffRB.KAhUTNGNeXXvjqbLFzbLjht65lcoJL1CnfoheuHLlPxzkqRWz57tnwiNmxyTy3q7UJPNqQyLTZEXEDkHTXZLFiYnQl3Hf1c5a10eyKG+w+mUM8Kr2CcelU6IMOKZvvxXYBMaPKM6Bpy8dqpJ54Qc1VS3BOdsX9ImTpN2bB.vKsvB7qe8qmWayabiy1n3OETxbZKkmIILAS6n5Wttpcrk.9fXui7MRrKL9HtTVjLNasSnHohxufmxWSJsYzvEUQBK9frkSF5pZ66UHv57cdpTihNyF0BotmxFzkokvKp8tPpIy4S6pSkpa1zsAdz7gPN4BWd+qYwxfnVPvMaqvu8bVLnzwvK8AvteEjsWYvwgPhKCDXft.AkuCPqASJnT.PQfECj9FvQJn5lAfdPraAc4s.lYeHXHV6q51XwEMk2ZGZuzNpnTBesu8ltZE.g816QGVzrIvJa7D2RO4IhOt7D+DSlP+QQD.7x3iw02HOJoOI.ikWFMa1zb7suXogchmLcfT2P7jj1uhNuWRAHnYqncVRKBTPfnA3GE8HSJ1xJaZZQWJUOnfZtUnjNquq2Q00IxpqkHW6e87FUsvhp8iOmmu5YnhxyFnvrhXFSoDOsABoULT.DCVQf0J3rNQLB7cVoBfLk0ZO2vsw41JJLtT8w57w+D.egU+luolpsE.PsET.m26a+Mi8TU0Z+TsxdDQEzDE4DIxpkMsdh1djLLyWzd0y5UdtrQcI+YObtAnQkH9M8b+LJfI6okHuBx1duKAXZ1MjHY.oxhHcZjySDafuGGnHilHCHkRf.gHPibqPSj3YSIeNCZsmREI7Y9ktWhKm7jxEJ2pWSyZ0hK.03EAzJnyb3BHE3WOBad8Lr7iGhnivgzIPSB3W8gN74qLD6G9PnmPA5tAf66CWx7fL0.WkftrCpB.JeBvOOodRpOjXM3DGf5Xnq8cgYp+Fnm51v4e.1c2DfVBJrj7fuSfs1BZ1TQI9O3XN0dEZwoaq.Zf5KtGuxJSKMaB.zDqr9YltKuohSO593zu1euQZbSP2Z8Vn0FPdoequtNnZU0NcmJb+uexjGef8x1gwWPonJjwD.EA1A3rJJOf6bUN5jIl..kuyfADKHworYpZrXdF7NU82H188u7hyyenOhNY5ECzwssk48vE.v6R6KWToUiKr3SZEPdT8BzfUD.TiNvlEslfV6QdjKabW5fKzaa2wYwQCCCKFadlRLZB6hqbFrPS..Wnzzld8Pwz3wBKcrKz1OS6.o7By2B5x.mk1ks8RXxtqnLHYlFS26ZW+5tUOcnUMFAOtFqhmlR3YT.A.dvsayGhBBvcDcwmUhhyO2hHHDzBoThx3KJsRxoxrQKhJ.BZhfnDhnz7nqdjj0CztY4IhoYiFzdK+JlUWqhOdCTDT6xHwLFR1qB3AFPkrPWb.Lc6gmIrG9p+RQW3WX1rO7Ma5VY8Urm.bWLpipWoQ6D7aWtCPAfjv.HYd47NiwB3UBpPCHRAX.TALPf.kIC1TGj3AfsO.pvuInw9aQh2Cvd2sOv+HG.vRK8R336+m5R6AGv8EumuBguKvfh0U.6ArwzXoWnl.z4wMw6aNd.4QWyOV96WVBWFzWbgkT2u7wpW5E+uUOzLGM.dZclWXTbx3IcsyRR7TZiIjTVEj.R..yfHjaMTAHJ.VoHIOyzLH1IL6DPVnHcIigO23SqOXhIzcO2OqWxbKx1Zy5o6zUWvnwzZk6BrQMo1n7Ewo..SiJxlhAiQ8JKKJR.HPJkw.ISSEnL2TYYCNW5.0CIJXO0DbLVrNMptim5YQCJxLvTHtdffjxCNzqn0o8TJklyxUPc1L1kILOLxYGFwVxan+8Qb8xyoGwXZm8YaNSf+lJR+aRAbKTdTFgVWFi9XtDOuTQhi.T8fw+XU.BUdpBJOkA4jJHAFDKhPj3XGrBKrFPhDQ73QTWHSHE.WrVn9A3Cf1XPQDs8TPuyLXvwy.a5TfSp.X0HyyBkY.blNP6sK5LwNweCyQ2a54GzrQyXbyFOpNiOZS++pFV7u01EuZ06B23of81Gr2kgovrfBm.jWAP9JfRNXFKEjZ.TUOF1x6AdvCft72EEqsEpk1G+yK3NIdz0ZTiWpyuBJu0VBvGDsATCl4tpzCNH+b7I2CXKv6c0EyUnZclSROCrsdhk5S+5+Duh3KCfNgQzkl9cpskFSWzOP02nTvWoA78.RCUZS.YTJkQxs4MR4S3Q0QTk2J6PD4wqLLHXyqLDQZiQJTshWwBiyASTVa78IUAhnCLjQWfBTCPfRTd4tapElgHDHHtQY3NCP.A3ARoHU9NbE6oMryKvKzTT64EJAZOpjRuzrUn0N6spfqUmFaReUYcpAFsOQruQo8YEoUZnHFBHM6xBclvRLmFXCCC7LFecTAuyl3k5WKm5KeBdP8Dw.rLt5Zsjz+azRTg6IMv+.ImV9ZIG24WKqydkGhHcmrLdWOgKyrVwYxjhvEADOwAABrBIoDnCgh6PDOPq3TNS6JUL01kYIafhlzSQ8bJJ415h82Ymygg26Yfc+mA5jK.lFGJIHGmlrC53TPbW3w6fnt2O8P4dGe65agqWmqUYgjl3InMh0WWv+CMRPu2w936NUDxRN.1AaBDeQPzLfJTApPMTABLUiAJ0A556BuyuCxZ2FEO2QXo2HBKVma1ns.zZzBYSfZMYTqF1qdcpQ6ag8.vab..mjPoGTQM+7EQ+2XVAXq7GfmHW6ZZL8zBVbwmVoVdj6N3mPsFdlSqaAbsqWmQPAAgEkJoNtL.5XElDq.BBo.TZP5bRrVXjaYBJHibbSHRAAhn3GmK.mX.yb9+WMQjFHr..J.jl53rTGCmwwVmPjRHBBnbFCBDx80EB.6n7dIcTn9itFT.vDnHwo0NTPq7Hs1CpRZhVnV3Sp.B7bUnJAJhAQ9FRoUPyBoYuGq.pcFxXJhzgV3mTQCOqxufQMcf9jYSxij0PNhpeZhA.nMffM2z04RP1BqHsFkguU+r20N17yOL1QGTRqMEGSLEboZmO7IiwSoThHHSqoHkQeLEfs7JpuueMy9hvCC7GjpOdfqX2tbQ.jL4jZ+rJ9IGYqD04nKZb6+trlgOCPvjvqhATfKO5NqFHEP5UCTuo0bzTtHOSzg2a3wse93hyOoE24NmJP2ViFdJ2Ba7oVMa8UZ1ai0alfCdW8.2qM73J.dgPUzCJsApRVHE6AZ3gnasN3b2KFM6wW65qCfagEq2VOeob.2d2Aqyn8d7FinTuZK7eufQqO82NPkMbH0e6CTkwWFWCyKqdhh0hKZva7FF7FuAtRuW0cafmfNy+gWNIc1++gcTAA.Y0asJ2rYSWwRyKF+TUmNFAIFGaEVrYLzV.p..HIu0x.LXTCoPVQoDNWWzJiTK.yZhHC.kQr3DPVQq0HwZ33AN6jkFZGLDHpWgLarx5bBq0rPrRfLR4iEBhkX3xa5SRAHPNQIDDoXmBFOOE.zdkBzFCozS.U1hu4a1kVBvSoUlLRghJMYEsPjFryHdlbBzzBWLLv22GYU7EHFUPkRTjuldyz7LvSsaIvHEv0AjquG30WA7ofiFVdUf+hu2WK4qzaPmjKbda164hBlLTo7HsIHHkHQCvw9ELcC7wdlBXyBio1rxjpiz5dwX6sy14N+N7u0B2QEFFQe5reW46+c9Y7S52tlq+gWPQYOK77NOLkCAMtEppIfHBRpA19FPNOXiTZUTICF1kGDs8gsc8F6tES2qd8Q7qeK7oewkzWcRneVcB88l+ZRz6sm7m86dijOyZe5Dr+U6hPOODWRizBFnCLitCiwcuWDvWMCG1hAVVMc8ULuPsELWLnfw6XKMnZQN0uhK125Vr9hxdsA+581RdtEdQ.bGFXczoeecTTUE11hM+9e4.rzRFrYm.7vGVDFS.RRna+WOLEkld.BcCwAyGCr1IyFfbk07Ljdxx9SV5E5okJ6eLE5Q+0Y+c7zdimMARmfiwFMcAKrfDTcffpgxwaF3bYorhhE.FJB.ZMTJHLaAgXQzYBjLQIPHvBIJQDMAUQBvC4NjpIfLnHFfMBoIIqCyC+ZC36zIVlrVsLlcYDyLTJgA.6xU9XwNxJHfn.kOLZU.DDsAPXgMF.qyPDHsmefpPwbszSH2qml3UjH1Gj1P5LGo0ddZkBJRfjE.3qfv9gBihJMIHnBclOKBPj1qJ8FwEpO0rfdR+.dK.t0pm8D5Fnszn5Jt6kEj8k17Y68enxLaN1zUXOO8POOLEKp.mPIDgiJTQuW0Ko2s5E0GayhGVMaf6a0qm7ovJXopeb0CBOR80V4ainzmobuGbvz1j3YEQpCcPAnJyPJP.pPPBAXz.dZv9dv3YXxVMMESkzY+Y5bu6ePohy2a5OPc00t90w0wsvUmD55EgYfZZZQ9XwZM7uz+hEkO8FeR26+KtD1ZqkTae3yBryyHnTQGTkD7dpkszu8z7ZexbWCuYiVTmZ+xlqVT6aGDoqEVfBG5b9kbzCfASfK4dtJsoWu2sDfWD0VXAo21kk2Xaqr88MZzuhG19aUF6uZcj8f5fKLNFlFB3ofFwPU3XbP48PsO5tv761AsaGcJRikPylJrxJ.4VWO6CpmnGx9QPdb1WygP2SGUFibOdTQvx8er0i07V9T0ms0MVWvMAes50kmet4vdQUXVRcNarSogSXQHW9sAQrvHCJjHJsS.DlH.RoEPd4zx0nzwHLDEbLbYYVlxjHssSDyq9G7WIXssje1+f+kYprgItLjnMANRqxoucGPtOtp7qQWNYVKJHJ.QDQ.QPxS5CClsr05hF5wybR9IdJhonHXD2WSZiXzNwk.FizWYqUHQKoCrrxkwYVsjzSjvy+DePqCociUepzxHvHKfsdrl4YdSqi0ErwdbiEmN69abnE2ykV5WofLV0rAbJsWlUEHYbleUZP0YnNgmiOpVs9ocbNYmt4frdEjyb.tuWfzYmw7FbvNkGz9nISiyp4rRAvDAAo.IDPpGjSJVdZNNNYs0IFQRnhcOp63tGbPoK7yLsImMzdCZhpETsKFYJnKnpqFRJLFOqaOAwEjSto4jgDFLTiI2UC8LBJGvyLyiuOE.ZkEWTOiuRaGzWW1Tzjg7IFgsWjTqHTq6AtSXDMc3BTG74vdsuNu523cQ3uInD5MXLv8m.87lFtYu.D84.5VEHK.fH3nT376hfI1CbvlPiMwEeOacA4K2YyM+4hPqVBtFTXwEUnwKCrxi4yRB4CJx2pMJmb8O589lJ6wK8hunF.nSiFmo1T0VXA0In4XkkWdDFWyaL6QoMWjabBzgdrzDMnUtwM3ou4MU6TutLFYcGQRrRY5AfgtLqkIKo8CHPF13ExDoEPrnnQ3WfTfz9ByFVQDAAf0RFKp9JR5vR1wwCxF5mYsXsZTSTi9paYSKGxGoH0ANKWjz1JJEUP.TjxPiLc7HHCRLD.vJhfyIB6jDmU0wlg8rGo5T8HaT28HWwpu40yxWsm30tF6DOqKMMMypRbYBHELRjPNAhJS4hhSrRbOaZTh0PtzdCp5x5w7YbAsEjliJ+vSKDBCdahqnEZIKCvXilxOO1A+9Wsm6bOC2sPYjwGa5E0SYrpHtTQuLyTTzLSOHMXv.GNYLg0tsr35PvBs4O6sVS14g+uTH83AEsCSqXyPAIUQ47P+PARRd9r.q.yDfyAj4.oTBEj4rdJWDB75k5+v6Rz7+bOjpV8OUcXIi9788zYFspa0.IEB56Bj46EHnEj0vZVbsxBl9WDi4qIX7DyjE3vxcbk2J+d+kekqoaTG9kRF3W1HFecjF.PKjXTkIeklpp2iVrTc0Fa+.5bS0iW4OZCAu5+q9vwmG1iemH6tuSX6MGnRUgZ5BfGK.F6nrLSVHlTnBh.G2E1u6cg2XesMko91.e8cAd+YnOHLntBqTmOMhfVdzSh2JQx62MEciGQU+TyadS0J.XoNKnVasy6UtlQgASZaWZ.uw5.WFkTicuPsWUVJbw4buzKt.+Y.buLZgWdgkT.8Av0.VdUBsxGHkO4u2UVec4kVXAY37Vdh9S1uqu+tQGksqMkOOItRB.Y7gn7CcZcnPZG3TqxkEQFek.u.VACzZQRXQTJSjewBsoh56kDgGNrmq6s+VcS+MuRY5m2+RxJqOHZgFAa4av2ENRTZbIRAesVkm1QE.mAxwPIBDMAVDFvPLD3DG5pCT20H729fCi1L8MP+gYlLTKUvxfjFOFX4WGuLxvenikJoHUEmkZUIQL6hSzfiIBL6fvC5XybQQNaTWqXKGC+nrKM1YSNHg7wy8aU76uoxP7jOeyiIbk7G.qtBv0ek3nqOu0WSCO24TzQojDjjJ1dcbX6t7IsNDdDPqgznwZbqV.3WrFAmSiTeO3Jn.RxYoL2.AB4.yLfUiQHl.JHffBTPBnhYvFvVmVJ5SzD5uAA8QzUFLtoXPfJVGqrGYcnHjnrBxm8VegGGO6pq5t7xeVW+gOzB.L+UBcKTabdu5sw0t90zUe1DurXiWWeecAsnMjQA.DqTpXcrxm7n5dUTc0ARM+9Xm8ORszh+FAq8chmBGauJ3nk.nEgekY.6o.WQxsjOx.hPiRbyiZF35fOBPQDl1OC60b+qTeE0s8deDlpBg0ZP4iGs1xF.Xw25X+dDuWdZPB2qbYyjuRU+Gbnew81raA3h7fRDzOR..tmWBAOhPHmb9qVdX6mCCu10utfa0hQXDgR0UHrMiFPVFKiMPNLzdLfkyco80u90koe1LdpEKMjBB1wY81t+g7bvY70PB0FhfVyJCyjxPhVmWNPMXs1jOKdLf0ZkUzbOkuocgppM4Nlcd32sSOr5FoSr3nrjrlN4n5tcpTwyXYH9EDufPDBR7UJkVb.NFJWJTD.6TLSDYEBILiXgn8MJbeGb2o8+9d6dgftt09vUc3E98jkatn4O2+RzU.v6..eB.7kB1vc32c7z3fhwwcFRo8NvwC1VkM7HnDEOLy2MnCa6c7XNbjyBUgz85qypicdS0.7sK4Y+fT.OQdzGvps9XNzBt7tPdi7MXqrR9qOhZ4.vYJF852.RSzjV4fXGJVNAbkH3NNANmCVl.wiT.Af3HPPCQaATVvBAPIfBF.pxfXuwShIlA1DC1KivHWHRbgL3DoSRe2JeyUsqz5jDb7JZb3b56sglpW+cHs6xheQF612ou.ppuH9T3qz9+c7y8AzxzbJG6ztB5bzIwRAwwZ4fCA9dkBjM5bG6d8pyU5cc+M95TMb78W.IceufJ7NgpRYP9YPEoAGo.rRdpwy+qGuFR.j+DH46+Af7FAPcg9Xreija+ZOHAu64Y7E5I.0ob0o5zJnszD2jVD23jxV7HurVFKS3FsXr7pO1B0Muo5g+MUJ16u9UqmNvaR3pNFDTF1jBvNPCE.b5L.UL7xNZ6cB20q+bGT1OrSqVH4kwFV7hKoQu5xaUbKil2dxzsaKmat43AERhiF2+ndEcOzDISPBkQJYBGqJqrbnUjGsOSoLBTN3xrPbFvLYcNYnvbGkm5.eO9vItXud3eaTDv5Vrw5Xcbc0u4U7oe+O2vNG9yNILSnzyTWWvySUTyJemkKIPYbofbYLKBxzFJFJY.ow.QPOwgsFFQ68FeGyw+A0AtTAe5yuxekfWXMznZUsUanR.XGUB8Q3.4Kk9x1Ri8uJc3VyqcCsvs220F0aWEY2QxNR6RkJtdOnjEczNnq5fu1hc6XWM3o2UEuUxOrJfmVNMZONqqIuYzdj+iADfEAxlyB5vdfB1GN9PXsSClJAkhfpDfxigXEHIRdrSVAHlgKcHHtCnB6ijvdVab1gtpBlrrrywNWhqq..TsDaW4q7URWY8kE7RunAu9ECveJWDcOr.rjWay.MB.19aXbPRy1bpRoekysXxO26+WI6aV++X769c5q8FHFIPLFp.k3R3tYr63wRXLD7dsqyqdqqyic2+o9Qa7pyhn9WEb5yBUs5PUJFpJcASdfHCfMm2YTmo6tx+Cm4gz8lGtiX3046At79v+Z6hK8QFhq06wKbq19TwQeSZz39MGMQnAAr9IT3...s3MeMuG9m0oze68297n8gOKX4bf6NFTEq.mqDjDOvVBBkAxN.1rCfkpe+ux9at2lu6GL4+SeoCIjEs7m6i4d4Qwrr7Hfich0uEQcB3VnItoZkartflv9w+nuWneeS2yubxNgCj.G6MjTz4TPlwkhwYJqDIYFPNAFvRFKricVWgD1520YkCHAOTYra4W1+3vPWBZthEsZwsPta3Sbafkvr7Zu57crO+k8xFqbAG6YzNNEflfsbgzXnbVHfPLybePTWih64DbjikcDIaerVZZ0x4Mx8I0lKTOEcIUBYT48H3b.3i5eI5aL1eZ1Nu5+cTb+Hjsy2wBcjJp2.9fdZG5XbH9hNjUjgcNYFdna2cq9zlXWusxONJf+nHmRIskfu0Klhe4y2AG149v4OIbXBPz4AE5CcUMLUxf3x.mnfjPfFnQZRDDaWX4cfpx1EOW0ie1YCsCG.9b9En6WD16ipXmz6K688pyqdCH3klUiuUZEzdyIfsxTPnIgpyDHqeAXYFn3PnG6HrSxA3Hu1e4zewC2ayvz69I9+N4SLWAaGxnmwWoS4DI1yWZaqHaLn.+bU9sn1MBMa7WlLFRN5Yf63E.LUgDJP56fDZGMR0D.MAwMpdWmLJ4T4w3JoJPIAPFTCooWBzC2D8uxwnyuSe7oZB7G9Glul0bZfQs8zdnNcMbcB.X5Sid+adSM9hKnvVvai+vAUQm6bdL7AKfrCemv5eNXTEftfGTEAXsBpD+7Zm4RAhOOR6+LHBOHNNoR7lW86MIdua25OF8Ow8877.rLNcbfyhYoYWZA5EvB.2AXsU5x+EK6EM6LgsKOEj3XY.akifScryxyPbRM1EGJbpZT8yYmEQVm93zT8tDz6nBx1tbEdmfhI8JLeOK9m7XpkesE97p674hnqe2Apa8Z2SZ8e5JGPy7+IvXO2.sV1RHyDB4J3rhuKIUABwHLXfmR5p70c8BQWJK9n3gdGgncyFuFvTdoR7h0nNqslrzRKAyEKSJ0XD.f0x7uDrX8Jul6vu5tIG2cVIqL4gtcQ2AdBhlzgdkcXWOFW1HvCxt21H.a3vhucyTj2r7eoT.kmxWSX4OmCnYW7WRaAt3XPM9XfAAUk5vqlFlJ4n5SRsHKRAQRgJoCD6CAWXSviuWkoNt2gQGjl1FRsJWh5DddBnA..J7sJqwuv+GA3K72VEI+0WDY9WD9yMMLSNEXdRvwk.wLH6.vCOB1rcwvr6ijjGd6sd18J5+a26K8AONau5+goWudaEJUWkO2xSAPA75+sSp23Kl4i8ypgD2EgK47PQdfcwvNHazDE.4JdrLBnwRduHdxZAQ.VGDJFfHvCpCXOORTeerXNb1V9S0VMQ4TZ8Gzm+LXEFqzbTDdmL0dOEXeWYQM5Tn.5jTDou9rXv8uJrCtBD8kg1eRn78gJTfYr3blgKwGbhFtAkgKEfhcfSKigGqgaK4f69QFfkekHjOBlezywlHmwzmEuAUawsnxG+wnBYOjtK.9vWFH3OYO422+RG8Bu7rIEKJ8Fbj8nDAGRI7dYI9i6rIkbIPCQHgUYVqtW2d79sOJYWbfqMTxQy778GXU1zEFdGt4MgZsEVRUsZc0RgGol+WstmmMyaw6Lu2K9v9zexWLIhuDH...B.IQTPT8W83+3+1e6iJuvK9.SYW4gskPaxgEP7lZ+.cVZv4iXdrA5BgCJOld3t2NK8de9cbMqERU1usJA.nD.9L.auDPXOfYd2.DkRypbDfFeZ.7YtzWf++50Q1p+kvA77z4wkDe7Kv2CkXfEDbudBv2bz5z5xOp835+kRA7oAspb.IuLrv+5cQg4uKPMCRSRghSgnpAapIeXXqRAYbfJbLLz8Pf4Mf2Tah2QgtW3pcSWn1cXztAdcbc7bnB5bmPp219zW3OAAXmu+jHd2EP+68yBx+c.IoL3DCTU7ArFHN.Wj.ou.2Q8AF9PLL46giq75eq+SO+8dPukN7E9+k5dWisxttxyue689bN22WxKIujEIqWhpjJaRYYYyQ1xOllTYbOvNoUiLSBqAnmjYlLCfbfcBl.z8fN.CRn3WFfIX5.Dzoa.KjLAcPb+ghAcPZYLc6oUZUbZ61VSK5GxlTxkppT8fEYQdI4k799bN68dkObHKUR1SRPZKCzquPhhrtWdOm85r1605+iuvWs+0XC+kG7MD.xUst9LQed08cCB4tEJQxd0PRGGwN7IeViyRpDEHmHygYn7Oqh3Cm6t.ABDjfJuCcz.7cqfs2HTrcAd8OsAdc2b+ipZpmKmAteZlVzbB4eyR9d+7n7VCBnX4hzc2QI4Nmmzi9n3kyhobIzkMDTJjfhjAMdcJAUr3SEbMivJ.ZAo6P368jDeXeNdu6yl0Od9u1aDu9W9ulEPVgUXYVFNoB7iFkb4T.7WuTc+u8lqZqwbctm44RF6iLbeu2djqS3NocRJ0ucg7C5TJLM1oAUhtuqUxtwGwe9fiWJpfqV+iR+I+IsSVim2s9xnVZNTuR6xxhs66dsJo9eEZnZaJq6VhP6XFyu7yj27c9m9Oqy28Bez9LnVSNyPgz+1AT7+CEt2wh6uQJm+uaJe5a43+zG9vDEbU80meF4bGWUCWiZfh02FpUil2rfZjK89+78hu37L326X4h29FdtywrK+xRetn6SQCAtEOJOB+Yrl++WieQTA7mJVXQr8uzka9C+C2+Vwa2zwws6iOtNzqXlCfGlhVRgbGSwguGmYz6boOwHMm9ICSFuzsNoMuqvRycUZ1ngrW85p0V8wC4fGLDCZMCo7IvT4Sh3u.9XMtVIf2l0MxTERbDzo.RaGhqFRXI7CEvwFUyuEtuSoyl7Y9L3Z1XNY15+NJFsu50eq+H49+neICGqxi+vx36ODXKCRJJiKy1yDSVWaEyCy2vIY+LOnLYOcTqb3ihQBiQ7QPZQ7cBneA076LkJuYLUnVqf6+HW0ZbZkuGFKc0qpe0WsVXy2rSIBevDzqyz38SixLJAE8DTALQfITiVEhx3QE5vq8nJ3AqfxXw4CHIdLHdZRu8Tr8m330+yrGvxK6dDpSofq42Fn1lyot27sz05WWUNIR0IJQ1XyFdXEY0MVlklijhW7YES4xwC5E1xeb4HYPRTJlPiEchRrwMMcaW6nNG++12M4KPScsY2V07yuhrzRnqMyKpg4YyaUyuIvd2tdvu6O3GDc369iJNX+2tP+VtvDY9JIy9EFhvg6ipXBCQJerb8uz9ks23O9aYgukvlq34O98utbUthe10Q1lEjoleM049hnYRvNHPpWtuOcqHgQZ6IokvI5+5mKdce96.+9bGYblT9zbs+Rkz8nweYS.++Su4BndokQsBKyK9BufIW0p5l8Rc5AEO9G7Zt2cvaEcH8SKhOMff.PbNDsiPSRtmHnyy74lt8kdtaGO9Td026lanfUjLqg9JBqfvUuplqObNR6WGem4Pk+iS3ENCJqNaKeXv0CThiLpu3AuESIEjOG5pigp7iiM0Rbyi25l0ZtZ0uYLqrh70di4C3.z8281doPGEhQgjF.1bHo4IK6Rg3MnTg.pSzfTPoAwjk.pzmrq.mJaa2priDhO6aTQd5bc25MeU+.29RlLTArvBJVKCg97Uuplq7dCoeu50UUmTqZtouHIGbFLISfpPNLQFBJpHLmFUdIyleTZDIDiUixqPBzDD5QGEfprBuUPnJI6bA5VtM6+z8ew+qdgjWFbrxJePceUs75KK+9rndMpnf0gk+b5W7EdCE7xjq5qqI8eoDz+enMI5utOe3QV0vkRIpVXPfNP7huSeq8u0kF3Go0UjO2bypJ0sqt9EWP+GLcAIH+k0awWhyTcf+ce6RlF2No512syDtVxYQM0DRUojxFIZyft9f28Px+D6Sgf8erycoil9W8K2c5ey+1t0d9m+mpgHxUwvryZnTc8suMXx8bJWbNAvaZDqt4lwD281R6IBk6WXZ+hmX+.25KfZ6WFYQfUx5F8eoR5dz3C6lv7PrLN2bnVn90T4pNhNtTdSbxSpN2modRuiFc+2rwt6yOHmBWzIT0zIDD64ogmag.0POy2UwHeeU28VWMd8B5u1KL+IaEZc+pKuL7RXHItLtASfR8XXpdNBGKODlf6XvcjBYff2Ch2i3RQo8XJqPOlifQJBbN78Rw5uK6Fc+4m5EFrNqX2Nee04LCat7GaLt8ssBEC8bPPJfMqxmnP4CP4BPg4jKoRFAbjSNKnRc5khLlw4z3SA4DNzn0NTk8r9q54WYVAtGo9iEtzk.5CKjILry2bFMes2PO+7mJMfWLvP9HTMKiZvDn0igpXHAkDByaPGpQGnvXN48UBQrAn7BgjoDbogftfPj0hMo.oGOMg2+PLezGr81S1cokVRwbmP+qGgQ+qvJvUmS9h6Ls4nI+R5wlwnJM9cjs15M4yFlKWNsSkN3egqZqeG+NC53r7wQwUvxyfUUVYLczwk95lO6+4+xAS0SBT5RpvxhK9N220rmWUY5B528GXx8S9y6Tcm25dSm1t4iiO2iqCFcRiQWV6zVijqkHIMjjs1wDDrU7tg2uyVpGLY9q4W5pKIqdkUeHr9dXx2nSGRNIX3moj5luwVR9fbtF29115kJ49Qu5lBu77bcx30x0.NkXBeXEenl.dZxGygpd8ETWtRGE7GA6DPtfgk3dw9m64qYey+IuraoSr15S++tIMjkF9Jr5hyZpUqPPPyxZxid1Rs0eN56GLnf76WeA0kNbjfaT83Rbuu+njTnNBCSX4hDLrBQ6QhAqRexYwxlMGdORffDoxjkv7EP0aX78SwczYHwLx9a9ih+h+q9h8a05Hc2wrpJAopRzvR95CHLpCDb.nOBHO3iPYzY.JNveRiekLYOz+HyBTzfRiHZ71.jXKHV71XRON6I1yMmZmAutGNhMdsNdnLrHL+TUTk2tsb4WnhpY97pmtZU8a8Z9bcu+wkv2ZHr8GAbUILeDl.M5.MAhl.mBchBEm.wOQgyeBm77JBPmQk0bZDWd71QvkLJo5J2Zc2QSmyF+XyLirD3eHj0NM1XC47uvL9J4yfc3Vs1hYmLm9h.kM4zTLmd5hf0cF+OLda227veq3J09uULA0Mwo633fuACcw5lZi5MfURi6HMy22Vq1yGz7cBCequ0sl3d+f28IRNp8SpjtyDjiIMQ4FxqyGnLlj.QFRYSFV76LtpSm5C1Vm+HMIgGuU74l6CnaOWA+Ks7lBKV2uYtX+d+jF4R1+yjqvzyqFcx4Ro0V8W8mrQ5xyNmdwt00mKru7523k.d9etVw6CFenWA7zXQfqC7iAhGssutYeU0gxIzcC0UuJpqbkqbpxbC.ytLLyKLuYopgl2MRaNrPnNe+TGca32byMcarARmcdQyMd2ZAr+MKR2dCiTaDLUJBQZ7IRlebFCmduPb.HHNO5.clEkkpP5FgqYErMFEcyQo8tC08t2+n92ZF6VexumqZRA+9CUvM2bn1bSR3tG2EU79nnIhZBHn.phNTgV7mdO+jupBTmRNsrspJBJqFwlCwEih1nrMQ2I8zds7G7066fuqasUPXokfMav5.KrXVkuKCDWqVv9CTQMOLMG19EPYKhjlGsyf1k4Id5TfAYXuDQDkjgIYi+jmG3UnbYSFPmpQ6hPhKBCJguegFMBiJuWQatGC8parg6Qz5jSXmwJ9WdkUjkt5U0P1Vh2qA9G+iDklKz3+nlb5Zp95gHUk2poWobxa69CFDG7qZf1dfz9s6629fJ1oFeeoxdoVt0b93mcT8c9w1hG9tsNWR2AOEBOI5nI8dohyYBCL3UFQgGkIxVvkzYThaVoWSWudMievNe+sN76tx2rCORhyhu1qYFu9r4izpJ1cciFUn0HtgSK5ckMchy02xie3G6u8+d6ux+rsZVF3yMYN+bQUE1bY+i.EvetmH9KrDP38r4kNlXUSyXpZrOkiNuZ7YJnkqt96Gw3Ktfdi7MT4MSplRYUCikBiFI8ueAYoqf+JfvK9hF5NPSieXHCNp.gRIDeD5PPJ6QGQlQblBOTtSOo6HRpfOQgs6IIgMCwdPATRYZqKzY+9AvrTbxtxs411poEDpWmKLUC+cTuUeTGuO97GfQpixDgoROHxBoJ7IZvoPYEjfGk66dDiCouAEQYePK1.c6cvdytr5qHr5rrFiy604ykHaT7yR1iwfl4yqxo0JqnzQEz5jvHCFkIqBqUAwfwoTJAkRj.DQq8bJhkEwKNQKhQINQiDfBmSkojbnQo0nU57kz5B1P86MGSXYVVCK9vgyuJWQfMLEqi9xUPu81WlW4ceS+YA+4qlybwoBMnJoF4vX9UZsC0t0sb+Na78RWaywEVcc+KeBeFVdY3kVAYUVmEt5+iAa+81Z3iOn2zXKdQhxOtnkPQ2CkI1qTRpQqcXPqPhLhDll5M1A8lHsUmQpdyB6r.ek1eUVzcExTorwqOadc27S0psbg9GJWn2wxD19cqj1KNp2gtXqM+ttd717LC81u88LslnGw2Dbr7h5EVbQNQwy9oYpxeIiegm.Bv8.B0wJOiQRXaU278Uu7Lyqex5uGs8udkNpyEcdUNcrZTLblZYEyhdTp8O4q33e0bVT48YxSeeCpbJb8AcaAeNAYfBrf+zNQpxjCAwARrBoCXMJ3HMtNFPBvnCvGoZ1up7zo+cD3OhGjLhT7h2VMV92VZNz2cPq1i+.jKrMDdVT4pAQQPjG0InewK.QJdehBmSk8daTnx0GS48wDbGhcawDqOfysrl0lSdes6e0FBrJrDBKtn5xUpnxEEoh8dYz53dPgh1NAUSPmKA0.KARfxXUJkm.sUYLoDXRDkxqTxivBckFmKDcPjJMMPP4PLZOnSHrXBQ4RGZ3HWgfVe.4ibQ8hWffFtbppbO92rE5M9l+OmaPbTfKdTs2LoVm6S5e8z6Debmuq8q7exEY7pcztc5S2ampqe6t5eseuBxh7R.ekG5U.qrx07qvJxRW8plu0qtY9FaoGwaUmgfhSPtQGBSf2SSmyejXzCrZiwoMJiRoCPEkyEaJHdpQyjQtPwIG9yL7YOhgmMYoMuJ.bvaVrDAtK3SbOsyxEbtnQ7nJIhXDm2mFGTOIVIDE09OXv.eqjQ3drkGpy30gZUpHmn3Y+U2DP.H4tx4BqKPaOtDgtM7uTi59Marl7pO7o9.MVS9ByLOer7g9ZAiqwEIkR2yeSx8d6SckU7vRIT5YFfJb.hJI67cwBhETgvoNnkNPwohoVlDJnPrZ78Un8F7cUHsyvjJQdUpyG1ou.WB3KQFCA1.3UoZsV8a03baASUFOSi2UC+g0.Jh2zAHIa7CmRPSOHdIC7i8yiRkfo5VnKsAAm65nO3AbqWIEGAvLt4AlY17p81rgeMfkW3DaC3ZPykxqNCPbRhe7wGJc+Ohze+GD0giyeLoC5pTD.DfRk0aGUpRqRQqrx6Weu038FkVakf.T1TPoHUBhZSXoiKOju6G8SEMnesR1GjzTfLdAt3BP6NOotYyAge2dmKZ2Bepb1ePuxpnJE7xHgR3zAtv51Acp24e4MJd7+5+vuS++KloqaZsRj1yKCq+r5nKbQVjKRiRc8MKj4AHKs9bpUA0246Pzgu06Tw5qNDtnJXJVDcXQzZGRXBlvTwm5xbTVsRoUJuJmST4DPJhDORhRUYPvnQmsZKyW4B00+VIUzMt2wCmuXgKHnd7fPFGEQ5vPCjg7XAUUUW2jjmKzzjq6pp1VxMl84lKOEK4jlf+Dol3m0Lt++2wGFIfOBpO9.woUuFzIqgC2565UqtDT6KnmcxupdSlk4mZfjYfiKIe4WYC2Rysh+q7rKHWD3bca4+0+c27C.J4U8DM+.bizAmsCDFiJRPEoPG.97fxADm0.FAP6TH1SXpPpGmyhq2.7o8QqFfIcP4BCrkof+AIIxYhPAyR0zxRG+875yrauh4+a358Vm8Nb7O4mj4w78tHJaYjbdv.JIDQN8I7d.KXs3R5hN2ADMwFnm7GVvbl61eus6.edM2Yp.tPaN9N8k81rfaMtlGVQVICFLpNesJ5w2D3YxLxF2SEaGauR8qNTtV8NL7.mUUWD0PHl.Qx6bhVTBZunTdQKFy68HAHToCLdaZnWPoTDqE7oDTtI4JcvXmIpKWtSRgVsb60ngGxLFSZTWuyfAgqNXqpM8aWio+zmoquWsPuoXRZg.eRUkKnfymn5wnyd7VAOdy+q2Sc7mK7hc9B0dpAOSkoRdhACba2cGa4jwT.zrv.oFyng2Pu029aGRZ+7HtbXJDfCHnqfNmGQbJs1o0Ah26ThSi1i26hR89vDm2HDDDsUOm9N8Jx9oQpbCUTemipE0dOyvLV53AQgioPWTEn3ToLTrdwaQ4rTMHTMg8LrO57CJOStzyU23gd+TZG5OuhOLq.pNkKTYjZ6QzDiSnWiBTL6RAP8nMS5qYj6x5cJ3oZNGO8LVlbC2pbU3u3JoytAR1YDfOfDN.yLaJae6VLXvgHR+Ls+LJq85nHqpn48R.wqwOHCqlhKEjT7hEu8.PcDpjd4x8NIM3h9KOXf7.NO0FLPhGLt7XO++wIG8VuksbqmMcy6z6Ajd9eDC1c.1CN.IYJneUjfxnb4PRixDYJUB5.KRPSBJsKgSdKBN2FQgSbyOe+VG8mvy6VhqJqdg5Z5UOXnIG3WamWI8Cf5EVe61x5amW8EKCOwkf3zT+vS4rSLcZmcdP3dc6maR52WiIHGlpcRI1gREhKPYLdwoyPIfRqTn0JbFmRLdm0FfuWDBNzENHn5vGV9b5AzpkiM2zs1IydbUdMMMHByCFl7GbQJ14wUQlGK0VZTqDjGSdDUNq3zJRSbPReh3Pjn67skv2Mvkqw4zkaUvXFLzHwdWOxp.t95L4EhLW3Nvc1OuhRC7nsNRG3vHVRJzGMwDHVPiWYTfRIoHdQ67oZw4BiEobOh50qqJe+NlhtA6lHUplW+iBRCOuTrrH9RhWxIRFnb8.9TuHoPZpWKdU9RELC0a5zgGqVtCF6iD0Ip.weHli7g+VPeXRHe.dQs7xZt1hQbu6Uk1eupnM4oehFajmiJlvVyzEyhcIocxp6rbBrxo766CZ.lByLik8x2hfiND4nCQkqClnRPPHp.OpBBDJHNAkbRCRbAYPEKQf3DDYejf6ggconpU0KcyjO+TeCo4s1zuW8uhZOfwazPp.99exOopv0GXYrhMI9B2j960EemVHbATQiiwLJhpFdagrs+ZhgfivjaaBl3lD7X2XBSk67Q51qwvMt9.NAgFKU5G6OZfUZaR9oP8BrrhcpnHWjh2A3RPQmShl14GYxN86Ml+fdsyenz2M.offKW.JEod7XBDTdkVLnC0JLf2oEmyoctSZbiJb.ZywlxCcP0ZUNxH8i2qQC+ZW4QjMiekmLhjd0nq+wn8deLP8jJJOsDZFVqCC757dQWLU7JCACz3MITfiIH2vzyE8iRePteX6paOVkZGI9g743sru55c7vWlkJgpDKAceAKCUZ.XZg8313R5QZ2HL5Tz30gJzJixKZi0ANqS4F3fXsfJWeJelNjehj+jzmH8ypNPtTRjFafQIo4z5nPsQg1fjcp.T9TQaSxlLr2Kg4KnJUorobsw8EGerfnN4D84SSka2ngesOn4z9yg3C6Dve16Wd4k075e5Pt6NCQ+adNLst.1ACiWk0YPew9Dt+9nF4AT3w2iI9ac.6N2fGUCUN4.VYu1KsoiW+izmbkOjjvsP4mDe5YQ5VDQrnMIfRPIJDmNisEoA36Xv2JGtXKJytXF95TY36wXwMmqd8jl0V2u5FqKePC4bo4liwq0P3Wal30eENj2z433AVT0OfbSWFc8JPZU7GmOaTGQIXhZAU1mnp6hancOaZqCWqwqL3jDMErrZ1Ma3O7RSaeyaz78ADZ.lmWvr9e5dZ9RU4tSlHOAQpdFipdQmezm3fAG7iBO1TLrgumeG75B35TAehg7ptNQ6v4MAZBbVQoDsR7NQ4iCztdQdB6JlB2WBKd6fx0ajazg5H1iSW6ZW68552xulgMc4onYDh6+XXZ+wvKSKACGoBxq8gQhmHQHzfBC4xqQaMX84QamhjVlCSR0+XckjGOTmNSwBI.rZ18TlcIjMWYUXzkfRS1BStcY+quCtlGf6vHjNQPNTglTcfFmya7ooAtXml31gL3HEJULkGuEAS1CIu60OruLxIW+LgHJkXAk06HBP6R8J6.7IoXsI38VufHJsVY7nCRiSLQgYfC4TiX4m2wGFIfm9G5OEvcAduJeM5NLpGbVhR9Hnjmf7EFA+IxnpJLAcxQDz79nu80YzZfu9gT+GmvRqZkUVQ3pmf2qqfWckq34r+4CPU4.LceW7sF9jjqy.DhDoQTAYa8re.RrBeeOttV7GO.ucWBpbCJMwMH+E2gg2p6prjkq7SKjQOzdl2XCA1vweu47rZsi3MNqm9WpIQmUiNWDhoHtjPHELpTT45gU2YHh68YJbid+w230OcKlpk3p58ntZGpn9I239mb1uG852xp0IuhOWAEij3m6Q96ok2KiNS8zbSpaUb+jsOtevOgdIVbcNK594UVcrVgBWrR7HNiRYzJEhWDarUbwIZJrit7YtNkO+MFdxK0XhGuP+gppcz4QdiVotlmucEh7mA2fyA5IQETUTg8EIpGtPIS6qsJDIDwoQoRHLxixVjj9SPRuC+KRd2FS1KeqnvQOdrnNObH2OTWhVZCK6LSG9g0Z.UuGwGMA5VZ79w.iVBKJdmVi0h15bJquu3h6ACN.cwcCpVbupmajVUdr6mzekBt6NQOGEM939pA4RTcBb9DsQJ5bp.aBJuU7ooJe5.uKMw1KcfskXC6UHIHVrgVFbR55GRwGlU.+YW4ay4BXmsKyfaNMtVeDJplCcwKPvP4ILuMCc9NMo8lFWuogF4wtgkRipnKGylK06kVdE6KM6rl2I57pm3056jmeMmZquYLW3evAXF+VHt.jiSvm1ArS.Cph3ygjDfueDRhCw0EIoAh9tnidGBptI4m4cI5INhJNKq7ONKI3QTSrklaN0d0qq5b8JpEntrFWyyUthmqd0D99O4QL9TAzvJXCzTnXHdqFxAlBNhOHgcZmbLMj+Xdz4IsrZUpqfF5031Zdn6OtLvqexuyNdVXU+7e1WPfAvr4UP1VP6A9yL9im9j+Ri1l78uqei3ds2sytz4cdLURuynk1CQbbYTRfyJRfAwoMJMVQ4ssxmKbWconaUbho2rx4er6l+bkOrdY58Xydfas+yVwKf5JKgd0+zixQbXchO7wPRmDyvg36awarH9TbImbdUqFIUg2EBFHrfGcfPo7fMtbmtMF9OKYnJpjIL+Rucye50HqrhGVQ3o+Acn6H2AWubXuuCWSGJWMUfIOgjyXEEdcLjucZPtFp7idufbi8SFelo2djORwidy+Ie136.rX9WyQZPZ61E5Dj2cjwPaiwTxZcgtTkwZEs37o1j3Dah8vTm+AVQsmsu9X8vI8Ie1bJ2qd8et18ySiewNFhMmSw8NqA66Tjjd0IW5EvTZZxUaDJLtlvghQIJ7CxQ+lEIlgwwQnh2Fauiobs9zNp+byghpUMOQnSyzEfWaAjmeEm5NK1gyLzCvGq.WezgGfO37n0mAnJdJfO1.lTT9lPv8Pm+534cnzY1hmr3gL7lCdDUI6zNt99R9Ve67JHuh4lSX4k4D.RmvxKmMw5U.XNU1.zWkWbdzu3KNOy+jkgFqAyLe.sKKWC34+M9FJ1+hFRRzryedDEaU.+dgPOEX7n6kR3lwrVZ7wENz8I9G9WWxFJeVTz4j33XasyOT+Y0iDO1L8O9duU+COXyAMSa16r1V8lRI5Q7JWdiRGJHJcFtP8g43fvRl6T4bkt8zOyL2o3Sb98iFq6foXmzW9UdkrNUeUzKs2rEV8e0uUAR+6LJJ6TDUYTXHMIFKtTGtXG1AdTtLb7JNKFQQPNk1nCPE37RDjOsD9jItWu824qah0ec19eWKnEdyWOgK7o1E2LvfisHOHgnjovwvlPoBAnDWP6.S3dQQUuSTsoe2JWXz6dt4O6Alqeq3St2wT242vUu9uYxwweli52SuSfQOpR4ToIpgrVIO.dgd1T8AoIAaGa4tXksOagAGLTT4tEK0wR5M3xUtmZ7rwP7W8lC36q4K2IRQnM.SZQz5pDTJhB0cT3LPP9frYkkq.HA38Jzo4wlVgf37.bI9so9ytPHEKDhDIDd7irUsm2g7+ZOD2dXFuMA01B2f2FSgQQQMjAkHsoFIMAR6fD2.s99P2c4SFzl+dGGy+Og1gqAqSdE2tgFtHyO0LxLyge0SGv3IR62RWE0Wo9BpEmtfY6R+JlnHsQoRT6KQRRxuhKIos+eMcbe4e80E3OLjvwCHUEx3ACQqu8DnCqdBlMSwWrG1w5vjW33abvbcO9ZSF+3T0U7YeugpGmj36UxIbYX7KSRgZCmbX8Y5e+2t3ACdvV2OtUmJBo4MAZsJnpUGpTQ5tp.S6NUlp3gSd4fCGa93VUpllVxz08a+0ekGB6rMlcVS+umOGUtcArMJitREPUBSg.zwVrNAWJ3RxHurOEThEkwq7lPbwQlPmWGJhMWZEr1QImsF0T44uwhA76shCxnV06GoIeYK24057qN5YbudkOdxtNNjbGdlnp1wxEkTAE5bFSylKRbC..f.PRDEDUWcgINHZrGa6y7Qu3N0movQcJ46u10ybSK4pX1f9pFW4JCN+Ety9Jq8cEmVmDq63b9QSRonVo0B9thxrSwh5aI8jaWXX6tIij1KWxqYu8sqqtbk+aNYM1u4OeRHdj3WrU.WEXRfAwPnXP7FPKXhRwDpPYzYVGsXQWPHrPJdmBmKBQzLHm+FM9s8m2+EJRTPHNkEHkqslWcZ0pcymxDC0F8nslMIeiV4pF4yEWZahpRRZAJcNEdmEFLfzNcHz2lO005AeaGYnmG.EKcUMydBC.VYEY0keMMu9zFl39J3hPtCbqW6V902XiG5Y7KM2lpYqumh5MzTB81QA5ff15VlPM.2Msuue2BtW+939FW+EUj9KUfV5p32uFxd0v1YBxM1YQFYHxloUB5hcQWoMRgV7f8az3ZirSillixQtAW7Y6Y6YLpZ68GobGbcUbko70hVvtWg1oEd5KlbopmqSq8Js+A6DlOsWRTTAkpzDUSAHHJUGos1olo0fbmuUZwZamlu3EkGzH4gI1Bndm6ddUgIuglHiIiPyRlSSIlrNKyfLLG3cYxsg2BZkKiSmn0JWDnMHdqJvlWxkVFisD0I2rOa8f5+CdM6ZWCV8OqugEV1wZqXeu29m29Gdvxc+6WdwzeOyrGwv81q3jcpkuxwkj3TcPtn3vwtT6IuzENp7kJzJ2v6F2oQCOqrhHWECOyWLXt7Gok2XdYta0tao8p+fdsQFzz1oSG8PtTIehyQ9PUekwcXoJ5cl4Sw9yTqc2UuxSkdiSVS8q8FyGrNOzHf9qdU.eXrDv2IQHpfGafC0IR0m303sFTAJLQoPXJAIQf1koJZdvZ8T0XW9ewBFWgfndIRTwPkPZA4Qj7aA1vxtK5gaIyAvrylbzA13fbQctapOXhv.kJrp7fjTKCmKkye2zElcE+u1KLu9KOKBrLrxhZVsuAlSfq5xjZf9FlMIjsSDpcaGbMgIQXt4Tmhem8pipVkaotb0plF4vDp0p.UAU6lV5Vsn+NGmj9O9q+GmtzbWUsammKjiiGlAacARt4kH8AWFI9bXhFAxkGB8nMonBiQoSxTQt8uCZ0avMx+N276TYGpiMJITGeueO8fiOJzWdfa1+l+gx7zwsc6x1Me1emdUFblD85EB62oioZ0LEetX3PJWPGU9nx9y7QOvaS9+z2taWIWz470F7Su.qZwHGgijhYnTHvix3PEjQGKmRdO1evIXIU7f1AZq2qbZm2fXMJuKf.qQUxGVcbeP8oKE7zSGY3hgp0t40Mr1N+Ln9yJ9+Ctybo+dLiieoJIS7Qq2QkzKLrPpt5Xo97iWOQOR23KL9fj279M7i2ng7Zu1BlrGBFXHHmlwg4t0Skj6yuS6i2WIM20ztwMsgC5jpSaMvaBM17UjAm+xkFL4G2LnncfbpZhKvCAPxW6T+o7miwufq.dEOy9ic3pkPmp8gN8AkfOMBe+BjY3m.FKh+D9zYRgfXByGS5Q1OasHS9HSThQGUTqRgSTOZVVmQZzrCw+vCus4xBLmClIAxq1E.F.LP3EgEl7ee4q9O54BGJrhd4EWHdkqsHPeCSLU.oEMnbBEdtPb6Uli1IOIWG1cqD5X6QmmLl0ym9uc9OleroNvUtdcA9F3OrhlJhog9XUpOT.XiDuamVm2uzbWU8ucimMbq+0IUHIdR7aOKRmmBc3ShJbRBJVhfxgPjfR6Nwi6Ebs83ZUm3cTreGyVeumK97yYRqLdjqeya5mnTofGerxAKBpEuXc+dt99iO92H8e5lUhKu8WIY8cpnXx7pu3HQp9UaphKYTiblMoT3pzn8+W5b.wWuq+Ge8+Kee2xdhKA23fxVF4oSwTLEu2hXbf2f3ynzjRTjA5UAQT3PiwoEQIHpTuSKZMXBTdkNzFMTN23SGRsoBMwkhLUFEl8wiTaN6WPwlur5DbuoTmXDqavFBbEKO9xtgRVLtAMzUmnqd5Kda1s++8xSLN7fj6JKRcnNpJU5josoAs0DFZvTP+a8bOG+5e2IGjmebpMrZK2P+Xeoez+KxW5S8SBel4xa5Yc1e+0WOId9ertVwp5EVbQYsGcdy+6vbU9Ka7KZrfJr4pVtvh8H+PGgJsIt3XhaFg2EQTWOJiBAA6.v1uCJUahFtI8GuKEZ6isGRWYZIDkzAnbxce3ElkYY0JrhHKihEWP+Z.W6Zq3WI6FoO6oZ.KchBe8xWQt7aLuIuYLUrG4ZWKA9FUTm8r4za0Omlg04IQUkf1iR5t0I4spP+aoIcqXBzs3cFbHMpcvctU2iuS8Q6svhss7BuHcp+JBCFPpOThcAB.2NosuZ0eCMjnN7V8KQqClj328IHdqmFLONlZ4wTpOgkTnKEhJJipP3U3sm.qtA0wt2yR2cC4ACe3e927yd7e++6hae60209q94dtb0GIm9STKmFLpwMCqGeni3pydWQckm2tDWUsJ7dJzvx0UoMuths9lpZyTPAv25Z+E9Mu1qHmn.TYHY5G7Ga4vKA7wBQUJAZmhK1gRz3FXPmpQIBlvLDsAJz9Hzl.7XEcjEclCtpCTCz5vV4JD0oz3ERGcxhROceES4kVuyi6n9I9hwxnYNTxxmpV0qvKAprtashe04PlYl4k1iV1TKeTPtfB5X6z9hQFaqVM7y2trP0BB1.OgCLXxoOWtblq9LewfW5q+ToyB1W5u6WLj+tjG6EJfQYHWR5K9r+xpEe6uZbsJ+VpSm6mBD44Wyc52+y6DhewCFaVQ3hKNfCGYW50+cgd0HtcIboUv0E7ZADOd0.D6CPpbGLisM0uPaN62ytd99LV+AChhx4F.IuQi5+zaaYwEzTugdQfFyw6ee6KSlHps5pvxKSyash+av7oMu0590XYEqCaUupgRRFC6cO3wH83yhsYcb8Jg1nHXjTjA8HM8PN91aQR86QW1ZsMe7iGet5tl4eUa85s04O7gpgFUop7f7406wPl1uyMqQqe3EYPim.I4BnqLB5n9XJ0EJkBEB.sI6bUVCt9fLPPhCg9OFVY.cN7Gyae71e++2i682BRdlmbX6XZqZHuGbFO9iDrU7arYCVlEzSQS0WX92yk5VeE3k4Ik0WXIgNeizYmYF0lLKz4ETvsduVteE7uN2HkO0kSIv0Fr6ijbFRaWBoeD3RILPPBUnbZrAZLhAkxiIuGUwXIcfw5SvAcyUPum1XNHJeg9lTuqnwKlQS7ely00u5hWyyZjAUwr6SYW+puf5gUf1.YoMPXF.SrhfgznRxHcLEfastmqfmqhvy7EyTe7vXC1JdZsi+T8N8kVruiJQNJl2hVI3y6Q2T86Tug9oV8ul8ji0bZRH7gPxG7K1Dv2Cj1KdsD9Fy+.JcYMGuM3OX.IIiRhMDAOl7IP4lDN5VTXz2gpO1cX3AGwraXW4o1TF45mu6jsZEtQCrq76Nd1fbOA5VxxnodCMUqZvkSVZl4EX8G5w7rLpkqufhuBrYiUY0Lc2zyr+KTrRCYdPsdnNGzZJ58NOE1ceFfKfnyiJTHXXKJC3amCwGic2cH45aht5eAea80Wkp8VZokbMd1eGKUGQWtQrpSPhBCvMaYd6+z7E4n2oNI25x3bWBJUBUjfVaNQJCcYt3iSgzWgqKPWv2GTokPSHR3PXSmld2Xx27qO8ge0+mlOYLsUUyUzSh2QyaX2.H5t2Wc2+LTyM6ZZ1bbJe74TYr6.N2kf+CoJudikjU17EjMW+TCEe.PcUl+TrxoO0WnvDIT10f94tI82pFoGMF93RDlymY62AfRz3KkslRz9LkM23HsmgjTknS6ljqzCBKEtW0RC5ETV6ZsiVUqXpziMeXmWeeXH9ClDlkbl8uYFSgomFUIERj.IvFmXBJWAm7Z8UbQT3qHz7VVdkMcOrJ1yuliWagXdxbB4rAHQB9HYtR00xKLugUV+Dz5mo.4qjMao+JYEvrOGmZQVP1vVWd4AbsEe.E6DPyAI35OLJeXVEvf9jejlTX58fI1Ci6XV8oN0fKUeqev+P6dargqwlnY1EUb0kz7nXVrTcMlJJbsOMoTwJHqdUzMmYd8SNJlblXErvIulKAq.WhxAaOIFL8Fl1+auDtlebD4iPP4wvTzCkRPJ6PUH.egRYBratQPRBQkbHAoGwFlca+4eltGlVPZQaeNaG8jAkUsB6qZ1nfY2s5mG2wiPZmK.LM5pAny6xjpBW.RpCQ4yjPitJ7GJPmLxP5soH4RPWJDL0o+Clfz36t81zs1iWRQ+cszpk6kVcS6lqrjZVVUMGnO6ycVS7EVUHsHvhzvc4GVYdtAvxWHVtFs492oubC5.TWxli4x7drA+11R09HG2ML7tjb3XfbFzRfR4CPSHZSHZsV7JCX.qWgEGVwQp1hyzCU3ddUkcvTXeftarYOWk1Q53AImR10e53mkEOuBBu.fNVgNmFkRiJ1+ve1ow0VyyKLui7M7r4ltSNSIb51qe90bbUhYl4SYzxFLwJhFSyn.KiK6i9xpMYN0xrLeXjD9K1sflgnjra9yMmhqcQKsqrOtQFPgfHhS.CfylxQi2Ca3.511h0ZXoebHytp8zj3NS8BpMIOyO0.U4M.dsWyrViFxhargZ7+hU8arQCOLGEl5Wi027UUKc0kTqtwUju1LPs7QFquhLUkcTv3BqByRC8AS7jAcwVf3sGmj6NKjdYBpWffw6PvHVDqGe6.TgfTsM9TCJaD57SfJ4w.69Lv28Gay2+I3I7USeGIX7XoygME3SPi62RgMHjToD3GEz0.SOzgVPY.qAeJfymAT7N.C.eWIy.JirnJ2CUUGhLLwGOFo6WYyucyC3yWSNk8BrxxLKKplhufhYeUNZ+19NtOiNu6+H0cqMiNbPASyG41x.fgNNwGMQr+F4a53NUD.Vh4X1S8mo0tl+r+yep969S5u+QoC+t3Gurx1HQ45ME9zpJwDf2X7RfVDsFqMfXQgU4vY1G0v2ibg2LpZsGL4GIs0vexCRae2H09CLRZyAev16qTrrhUlSwJqJv3ORWty9pbq08L5BNBMtGtJtUi2eR7JHrX4Gt00Ses489FgqfWVdcgWXdnZcMUhCvDqdIx5ovlLm5Cn8m+bM9EQB36wfgkWVszbyo1ai5p0d0JJJjJzldP8dvndJ0M6B3lCD3VdV.36WNfzoBXuF5ElcQ8ZKie0Mthr.HKM2xpa0bJFeoOm9n6aCet6kKWKhx+fu8Eiro1.cTMemiuX+O9m8uY+6gIdgEeszW8V+t9AUaae176qqku+CuYrIyBoQFx0pHpAi.xjH9QQGEipXGzEr36aPTFTJWF6AJnQrYR+tXGGeuIfb2dqeRdU7B+88My+OWcFvOhMUkjtqzNtpv9VAaMMR9HPhPY5mQBXQgxqQRAenNSoFTBNMnyqxF1VnGBig7db1B38kPUL3MeqiO0JzAx1N9UYNYCdRnv7TNYO0PiXzuIOco+z1MGtsNoR6zA42SbZKARSsxZqlqWNW91Uc0OtE2sKKTvs5ZOTEtYoqdU8dc6jZBBZa6WcqN7DFooePfzsiRNdbrRAAUjHZkBMdmxh0zGI+QPssI2PuKCOzcld1oN7o9zGDe6Fq5G+7Ko62HTxUu86s3doqZ3aMSNfhn5Vff+IgPdvIImUsbuOCU6yVe2jLH.tVJO6BP9HC9bB2Zcu58ed+rpmUqqYwEDYk09Y5Qe.YiZH+cERq5Yvv9WJaQ6G7AC+UxsfBORR3pYNnjPy4TzutvTU7rdaAtkvxmP2iU.XEgUAV30bLdfPsJxZa9OWV9qrm9SO8WzXKEX9S595t8t+m1WrQCyceiVkabyF0iO5f5o1ACiOnf0dThJW59caxCF8nI2exKdwN09X+lt+f6+q6d5KlonyL6pBKsjhUAJmyftTA5mqDlxA3iETQYOTv2yfqe.ROC9A5S.PfGU0AXFIAc97njgPoxw56bh3I8E3A7+.Wb7pxlspHgpDOAkhI3I6i9cRv2KEIviHYxnsnLmXn5mPVXiBUNveJSNLjQ12bJTD.4Tj+oi+I23f3aemzhQtBASUeAOrlbEtheYVVs85KpitvGU8lVStucuVS9F1i+nd2wW.o6nXamGENzk5BA6FS9aEW7BuCiLhkwuaW38CF8K+wNvUY6IGTnbk8u2OnPR6H0gtV56EL3FS3iaNB1jBJTJuKepWWoGEGtCb1iH3bGQwRGjexn8m7o2sWu5+t9KO3VplrDiWuqmMa7dUm92LTdx0bB5s+z3aMNlvxXFRPk63sLk2dUR2dhIdxiT6d0AbkMjEVXE6hKhepcP9xu7GHAYNTjuuhv9Jltfg2Xdk7Jq6N0rLkGQ09nRGECJHPKGz5QW29gZ7Kxsfd54+DIaEVF7sVNqyiY+rG8WeYU1tetlmMmSn4p709Muk9KMdXv4xgYOfJg3em9GZ99u9aU8v25tmePitOlmnoEc9QQ7Es1zXams1i9p61uUx6NHcpcnz4Ze4Ytb7aldWYRfkWbA8JatpvrKAslUQP9PzUBwTUAsjLoFLQiGExfP7IF7IZTFP7oDTLydpTEMnymCmIjcSjwazPZ1nlrW85pQJgt1fZxQ6E6Y2HKlgiQOVG762CUfKiZntrDckQib5NoB.cAvEvIzWNaP29POpnTBKjfYRKah61GAznut4r80mb8UVgUj44ELjDW9sr8ltmx9Qwdvm.bOFhaDf73sdnSGD+t3iGliembDnxw0ep6wUuZaNgCbqtwFxRfepYHsckQ67wKju+QGO0wG7NGua6a910RNb2Q8RuBnBzp7ES85bciCGscZ4Gqa9xUFLzYKG+Idl1Cl3y+NoiXtm4rUC461ZUGrTFBSVdYM+Qeoxr6QSwfa9D3N7IxTbtgKgzR.eaRt0V3U2bWSt2kQd5GvgyNnwZyw2XslR4GhqzUNcwVlS+VBMQUzDADVVwRyp3EJHxidlx+uauy1Xirq6y6+Nm6Kyc3vYHGRNbWx8UQsZWENqjbDsr7aIjJwFP1wJ.4CjsMsnUMEvJvF8asHnevXzf1ujh7BJbSQi.hSBJba.YQMRjqiZiZDYicrbjo7KZ4HsqVwkbWtjK4Pxgbd8N268bN8C2Y1k6pckkrcjzVqG.BRLbd6dNmm64b9e9++4AfZcFedqO9eOi20OFhqm1DEPRQLh2JQto3AhbZA3S3Mt3X3.ZWyv9ApuzN0U0+Ai1aiUdii0Xsq8PgsM4QjLmwNSJgUR2nPhhhBNopdvQMIBReYkqSysO9kaelmTMxC+uIzKc74eM43yIVXowAFGhzZrDZvH6HoDVHrsPXIPXKQ5Fe1bHriIDpPHxJVfrC0HrLD4J5ZM2CWdXy2N8xpyW6ejYyyGaKVnUQn81GoWUTZWrzc7DCaPXahqneAwxoXBvpiHOq7AiuEDovt+cQjXODtF3aXmM6ueXTsjlq9WF5Tpv3PwRA.3bzgseQVdDZ7xOBX8.Xk5DXkY.DNtHSFq3Yl.aTU8Hpb+DVdTjQCw0B07W7KsB26Rs5DcRwbEJvzflb0L2y3oUmu1fp96S6uWOQM18R6tassa53ZEIEx9MpDCGDMzgC7xMXvQOUynTtnOzmzQkI7kMi5k1ppNg4vt6JtV.lImZJ4BOcNO1d0SPqW6gHX2GjP+wP1SJHAwZ2SEMQ6beXZbJr66EIY+uD4FubPeenvbW7p5gIqo.Kci6g2c4mg4LXssINhosEjIiEoRDeNgAWN1JygXh2ANpiaJXN+8HdO3b.6fNWfWOiUt83M+uUILPM8qG1u4GtvCau+FxgZru0ILhT2mQGbJsVjFvRqzhnHownk8YDVdnBBnxls11XUev6Ma8RkWH.ljhyuflRSKnTd3DozXp4iNrAD0.gI.DBD1NH6EjIDn5QgUnFSnDcjjn5IvD0F4.svYvpXaEfn1ABFvzb9NdL.ICLztcDt10vxacBkChPcLLg8fLU.RuHjI0HSAFoItXd6XtKp1FTMswT0ESKChd1FqgKCs7g5l0W+RQmx9TrdlxVOxGNqLdj3rVaGscun15Hz9RODR26Gx4AF6XUBHLJNXOsAcqjnajBSqAQSHJ0U3B8r+It2mTsJE8i6ytwVHFNedA4pYBNlazfC8KFYkKWyTZsLaxjhPOOgSlLlzd8qSNjvrUaOy8ejZlq0HvzjAfZVQw2b4TbXWWwUJeuND1HC9adRB1+gvPdrSLLfASKeL9BzMcPW+vnZN.3UACWgj85ew8qW6hfpawZa.wbcbVIf3.yTE8DrA30Rf6weqylr2EIev6ADvqu16eLfueRCofsRjv563iSqcZlt4NMOhNr2iIbrFRjvOAJKYj1SozIMFoiwXkz.8ZiLwgIn9IY+Fq2px.qy8LKEmeIMLELcdXtwLDrS.LRUjM2Bi00P3dTDV8A1tHSYPXovxJBAFhpYgprCQkcIpJXasKxQ1hHQctmSxzTF350QVLFOihyG3SqjkwYzWCcidQUdDncOXv.dQHxpQlwDWWjQFLADOKUcCrMDVwAiQiUtx3N30P6TElI7K8kv9e6+Tji8K6FcRZE2TO3CzyEYugH5JGCi+XnZmCYppH5oEh1RL5NdXnwfACBmjH8RAVCCQGictvVq9+4AZL8ryFN2sHGC2RhIq6VnxakKmvA3LoSK.nhWUwv.WKn6qbZfAnpyKKR35Iadt+bQqu0PNT8jYHpwQP3buH6anX05NJDTAXTsAGKLtCfsoOD8NBRmQwDVEomhScjPtXbc68zEPL83iaAsnToRpYl4FpisYVjLVRIcC9V2Y+dODumLC3AiD06Dx3h.Ca0V3ue+x+hEtrcqsOdZiwKmv1MmvKYRLQXBzJUjsVabMHSaPlVgcFKhh5mH+gIpc5c2015M+tWyvFsTiLBM2P3rMVCrLzd.j8cRjdCARODVfvqMXGgsQfppFirNl1ahQtJZ4ZDHqSxPyMQ7.XdXRJqW3Wi17eI8NXe3kIJLMplm.r5EgzEgtGfPPpiS8rNMQwRZnAiNBCafVeID1WAWyl7FKWCvr3XSq+RKMGe2GbRCGIoUgIKYU70ZmDYyrXBGBL8iIxCseETM7AiMFr.LHE53H65ZgUuIvxqGLoF.+18gJzYqb4DSOMVvzrU43DdFtNID.toywqPAwvcpexGLSF4011SBqwgGZH80BBLvmglV+ZBU44EWakm2t5lmJAQY5gHxhL4P3LTRvaeh1qMQ6EgILDAgXkHBrShclzH7xhzjFG8d3mT18blehmXBqLYbr786WuzRktIBlXFTlYWD5N636Cv6cKAsCD+HWE5Mv5050btLI0Ke0V5Mdi5lnfrN9MDNZkzQKbDHcTZKkkIxRfUBChDFjILXrDfJApHOrDNDJt4Ou459GG2jJkqhlQUHwCrDh.PWQitkG5F8gkNCBmPLdsPGzBSiJXkbEH0qfyHmi1NWA4dMHsmg4oqPVCyC0GMsXrrKGWA8SOac9denqBGOAB+rn11fZ+6GSsgvpdD59THcDnCimETGnQsmFSyMvYzWAo62mjicgIZt+NKRGW.Zt4ziMKPtxVzWNmRCigyU2FS4dvn6Ais0MZkC0fUrHBif3+FSrmEJCfjFD8j.oiMF+ngK+exzpVK6j9kjm7Q9BQMyMtNquereQbGDpnriMlbqZCZ8Zu39V6t922lJKyd4GMp+G9gCaWsp9v.U17GHqu9UrBqgM9GwABiKcIbUPOsQ3qPHsvnhUWNgiD7hvpGAhDtXDRjtZz.yOkjBvxdyIFwZDwEIpy4nW3lEwqkvPtdMjt9MFC7tbfWNHdOm.9NAkJufYzzSnamvQ0p+bVpM7BMAsaqB78ihDApPakwjPKbbEF5IVJB0AVDoLD0pMQ9AzzOrdEs9.t8SrtnG6xt5KdQfSPCBuuKi8dF72yFcUvznMAA8AIzX40DnA5fM.2WCmLkP5bYRq2m9Py3wC0Gti2ExTvDogyWqyrhyMilomsAeuO4Uwbxe.9MMDtolH+if091XkRB1BLJEcDRHzMZCxkwc7uMNC8pXkcqE23dawANb5YmA8RmCqA0tlkWFCo7LzxH6XPnFvViLoAYZh8pvtROiLd4jBSrX8IcAoiAgiFasYtkly7O4i+fhke0dEmcrKHYvSREPeSxzvMFna.DU77DoWeG4VuwqZUYiywP4G0zummNquu470pYF1aYS1Am2L3gBLal9Uiv9ABvXpgVTEYEWHzASaIhDFryXGWvuRPPHFq1XDAXKBnkeHaDo3dAJUR.vppsMWoZnNtzx.XVYGyM8NO3p7Bl2s2+G79GB3c5h9FcvEg4J.vh5riMQzHY+bh85Y3FAgauWv96WQEELhQ3IvY.jtYUF6jZSjPP6FtDFpocTEfJXG0.Sn5l5LJVz.yJfxFlbIA0+5J19y0ffmXMb6KjHwUIxbDfzXkIBmL9faSzs2GgdKRDrCmnZSN4.BxjQxFU0LRmP2muiDJkNsX3b4jcMxD.licawEGbE1530QTcMDqeXX+gn8dIQZCB2HrRzDRWG6b6iUeWCuitB081gqd4CR951XYL9IM3GnVbQTLRugX2aKZ6zNtF9jZjozX2GXrH1jXPSmh.CSjDSPBPXgHY.59BY+32aK2rpi7KLrt8ftlLggllG3y85BUUr6Iwz4yK5obY4VWdIITlduu7QmXDuP.UkkWlgYFH23j11VbpO1.FK2q07RqN5tTa60ocy0PsaZD0xhvsMxd2GRoQ3KPUMDSjO1ophL4dnk0Nj1u0l70hX33uCU7SZxkJg44W9E0j+2JNJ2jSr.4IlPVzb8Ulb6PmzVryfRQ2102hWwOQ38KDv6DtozSpiYbJfwXvSWS6bonlgsptc6FsW2D4OfUBYJgM8IrsbssjNQpPswnZiJrF3bEjtqRB6Jzb+vWXpIkSkeAiXl3TNAVxvz4ESNdQ9O7a9ocqu2ek7q8ResV+t+qYEZ7OXKBqcIrEdjXXEIxFhcOgnqFj49+Fsu+y70hNwYNr96c9mxz2Y+D5EWulYg7kMrz0IghJKurN6XOuLwG5xxe32pkX3TsES9YOST6iNFoP..vE8jDQAQ0cevJa+xCW+pat41sV478ydkygLHEzi.7BwdzlDNXU5ch8Qenp7qlpIe4uZ3aV2P6fZ8ZvqbbdwlzIjnQZf8pUQ6sGHShvwF77vxNd1NgRfPYgITBhDHjRvK.qgpg6vMvM1bKxbjjlLTyP3gt9G0YRmVLb9os6IWY4NW3BxOcu0EWsPAwRyOOpToDCNzP32aupQy+Qil667UinXQyzyh7S9gdbauT1VVt6JspC1e3D599qSVc+ql8xnLkP4mDc3XHrDXmvfUuFzAgHRzBgYarN7kwM6l8njUGbyKDtIEMLdAnTLgobix5NIYOPNQcRKlDX33L617Vxl9YzY.e6.CfXt4.F+YMTJeHa0nAMyUF+scQfUj1QYqCtGQvtGBQKWhzsPY1Bg2pXenySuG90ouTkO0m0Npb2kbzEEfoyOC+6ejIsS6XY4jNoyw9Uype7OQa8y+7+QAK77au6K9spJndFCC9yqyZ6XF9g9d5O5uvQsdvG4z18lrW0xNK0drrinW7olQevLH45K2s.lBSOtXpoxI1XmPQhc9dRF86wv2aayoVUUe4W7ZA0WKpVzdFGiFQfEZKgLvaHa+7O983GTOa6EJtvaRyPudCTAj2HiNvPfS.ZcCjY2BQOqBZWz06iHavNcaPDeLDFeGT07P21FTsPvNXO3lDpqvXaGPQLsehx5DY93xqscc4VscL0uPUMLhyhei.W1ZPWBSkfJISP3ltj7PJ7NV8993mn4G69Wq4wGtpd5om1N6SrgAVjLNsDAjF2nT5L8ljQsZqSeHG+8O7CcYVeaaZtVDl8qCjCc6DwtKrnINCrF1C85HG77ncuRS856UhYtdR5O43iKdpe64zSNNBHOLGLAouQ+PmeOOPZ5XVPuGt+O3tKBHE.wnyMiLKHpvDlE4OT8Lir9djAHYErrZGApVV3umJJzSpzMTjbMb8VFY1UYfgt1g941u1EG4bQSOCZAXLEPxTSJWJ2bxTolTdROWKLtlJ8DE0Snk9r8Hzqc5ioVtdfXispZTsGW.sQMTZQu1tzLUnw1MgbcOundO8Hl4drNUkQ97xIpLlbrrdhsxUVuPwhJJhFJEwziaOxf4XkAgcdkzR1Dk88LJOzHUBaUoUysesuuILiRLVtrxS5oDG8r8IFYToN2a.O1s4NzlYwhbSJ.3oqggNVqLqsUDGu2pn6acZ6dAz0rvz9Dn8SideCcyeTSjAkeKDQ9fUYLlKhv4p3R0I9TtQKNGlm4CuXzjuvuiUxVGA1DwhKNhCuTT+TmgQucVTpzz1KM5g7HJUC7G5Z6++Vt1ycwQCl7AqzlZCZc3i7ux7Cu5WUAecU9LIM9XQEBn5USnt2L4fOgdm09KGSgJihn0qiZmQwDlAsvBrqg8vqfbjWGa4ZizZms2XiOtOcuYTo7hE1ZIfIEKrE.egaSf8hCXz7yufdpol78EQB8tJB3TShLc8Qb7SDXc55qo4G9TArwmymG5Q2IU+4Ug6UogzTesD15TZcjiTD4KS021VtGaaUJZ5MxNgaPs.lYlajztOwDVbDW27IFMtsPkTCP1VdgDrulcpqRUdQ8bu3z.aIF+XCKCpcXQ3fUE8NxoYqFqne4qlCXPNS5mPrP7dKE7+7QbVw5pN6ersDCk4UCoPAMEKZJVDcgoxo2vutoo2mSLX5o379IDI1osI5PsLGerg0IytrdtkVxrX4h7G+jS59K1mShKueXhx2aan.5CVrnlYwhGYRG.Vbm5p4e1EMKb8xH5YUz3eXc5o+qhyfdD0NfnFMfpiRzt8C5DwR2uaSD1aiv4ZHRrJN8dQ7DavCFzp2SWqaTpMK7XOVDEJngos4uKQZ1q9wHZ+G.cs6AcsgQUqOHzFiUEL675f0OfWKcyElO21Sm+kB+xy2Y43kya9lLiNNMzlyDW1SyCer7A7xsqvd8cAbt+xjHnOHLY7vToOZm8HJbGt7J02fOa.2Xk.wjsyLpfJeQAYGyv4qYfxlE4Mqn0OcQLvBZx2Ic0dOD20P.MfXogGWtW3lVxdS5zNoV7nO3ZL6Orj+LOWwfSL64pntT+M8Z5ut9PVB0lqKRlbU09W8u1+hW6ZQEN0bhNRSwMF7BBFrWKTo7HLvgdRnQnCvuYHMrT78WHhtlAZgwkSN9WPr.4X7RyYHC.SIt5KuqAdXwJqbR9H48DSO6rxkqLlbw+q64sCq605M96veXUvi+IOo94fqebzUVdQM7aQhwRaNVYOQsptBpBUpGH5rjUM.O4STWQ+GUD5prKaSzz4iSIrqGA2wlPhmqEx.wxMZoWX9IMcMPSnngyNUKtzf6RxCIoQ8VHT6go1QP29vHT8fwVik29HSsN1IVGGu0IgtLmX0pezOZF0YRmVrP2xHqXQC4yK32NYRpt2gP07LnZ7vHbNA1CkA6TovDZivtNFQJTsCwdncYNa+4xy9cyjl3f1LamVhanwocpoy1Lw2UQnrI0GbSRYKoQjlrUBokcDkFuaHcAP7zEPTJOh49C9CDvWTvRiCI8gdO3nmtGURbalf3ncZ.Aytf78RR3cMDvtHzQZhpnE8zuxJvjvEVJDPWZlyFRgBwRZ2lve3S70slXjQbFVss7XVCk.4mV7zSe0PNaov2TqsHRDaY0nwzvfenhqhhNKSE.JVzrvr4MStD5xiijcieom5TeFn5oLfi3a+ssrVasOpEulwCYy9nQu8zz7nhlq2eq0txHbze2KK+XGqpZd9h5ElAMLCTnfY574Ek8FSt34xHO5JUsHyz.EiIqe3EiXydiNSO8yxkSFN2LzwxjKHghFwGdwnyctwEoRkSNWoRJp+ckwI2d93HHuvio3y+cqy24dMXkqNs1YcLWICgU6CoHIJWE8bberFcO5occ3ps68P+YACLzUMIpd+VU7lVM8zSau0KUVVd1o0k9JtBJWd.B17zn7ePrL+b3LZJjGcefVHZ2C58SPz1GkvMpRTPYjC1f+cmtMvACdJ21yPrPAAkF1lKriCYW1v.OWvot++7neo+Y8o+7.S3MtMYxXcEfWDHyZv3meCCrPDY+0MTxOdycKzclua64TdinquDFl5GuLy5mF3tJBX4sJoSL7QUIWsYXXqd0NspqNvs5tI2K8ymGAYdbENIDXgfvZZtE8iQ.FSixZ5yIDqdMDoUzVDQixZluzMWaY.wUFv7BnLGYfFB3IAfWcWGwpkZXy2yMI5v9QIFB0lGhn5oAs.KqFDo1Zs+v8JOWVpLwiOUMJrfOEQ2YVA4jSky739Y3bQvD85aV7fet6mLjfWO3yb1Rg.lOOeWm0GAmWR+qI1zoo9ryrlhRyDPgBwoKDKKfbhNmMmlO0xZpbz1bEm1zu2t7CtGK7b8Hpipu41eDiLXadDe8HsW1Jq8hxdy0mrNeS8Y7yZprLh5q3Ia.P4zRDa2GAqdTPcDH4.n6yfkIDgaHFsBiVhpQRB2YXXmigI6UP8nqQ2.ocmbYnBEDjOufR.rGXUxP+Z8QF3hpJKioVtIE30RrhJg4UbSKWZwMXiMf1tc5mN+5Fl7z2B461Fs3a9wt0.xcfwG21um+TD2UQ.meJzitwZQmdqSXRzVI1kc0ywtpa6SdFz7BsTjtt.uxZ7SZn7K9lKHyRkTjJWabiBQs8App72bETCEI4idJqGd6dr2j9nV4R5j7nl.1QxqzSBzsNLMei6mnpmAS8Sh1e.DIjXGTkn8uLByqgexRW3alX0BO03QEoTP22+gKW1r0Ch5PWHsty9tna.hH3xlNybafYspjyKw4sxz2lIhrQl1PkAZBmqJkJci1hIRKfojLadwo9JaXewxOi6.4NjY2G8TM42HeDO+ngTI1iIl3S4aVb8+HE.mdp4USw31USzVr1Ztpm4YeF0z4+TBFcTypYWVyyjNARkG5l8gV6ANAn8EDsiKBWPXDnBBPGYfPazgYIRllf1wi0Jdy0X3Ma1M.KsjYhO0XQKd3V5Scp6i977zCmcVybKMiY57Kv7Lo9B90MY8JqyOFrwnIMq7rqZXXLPIXbhOmu6bU1byOdWAfp6eeiN82UhN56oa.8cJLcpT0Bc9dW7.6m6N87A35h3SmBw7M875HCdcyDha44b81nBuvjV0CpjHpov4R63q1Z+rQvuGu5h85s+Kt6P3u+Yn809Hf4rXIGEgLCROKjoahQbMB2qDz5Eom+1W4wexu05OJWr9SefN8t6mYZfo6pEJcRbXwYKEx3myg8UCiTbXDhCCljnpnQ5VEiy0v3TAiSKB0somxQ7j4zSRY8Jq7mXW6U+Scyc3AMegemGI3STsr1yqk3a0IYj6pUZddsDttGW34sm7MTILo2otZ4kWTefDZ1vHe2dPc4Gh1u9mATOHVoygygjXMPSDdgHDZT0cIZSGh1oJ5fKg6f+Mz6i8+kKcuaR2Z.sCdSDv2pG+NQpJffRSKXt45Nd3sM44NkKxuaQ.uqZFvtMJEea13bi8u8V+7OfX8b6vMkqp+ve3lXToLhQ6UelG9oLm6Btx8eoWqGZbkihez8A56C29FEwPV3jtAxdDfiD8tGFcsHB2bO72o9e2+K+ZO2KR8CdsX5VDooqKnFFVNVM2dpJSH4TuXZZz9HnV6rDFdFT6bbTUxfpoBoydHScIj8+5j5C85noLdG1mcsBVXf407m9mF7u7abJy.IOh3AcZIFYjQbjx1he0a4BUqSz46xPbRUMSCuVhb4lTPGgB8.MIFL5.DgAXZqP0DDIUHIBjZL9RLsDXZGgIJ.UXDMu8m+8cZ4n2wkody8KD22THdlu2gju2Of6pHfuWiRk+hlZq+6oaVtYzwy9EMqrxIYsKXKP6mD0lGBi7HXb6GSZarrCw30Fgm.DIvn5ASXenZbLDIt1tW9AdCXsa5tuhhnMrPrtlN+BZQQzTnfj+iGyiDu7H3K+4PW4CiHwowDlCcid.kFspN5lChY6jXBafJSSrqqn5oB4KGmele4O6ECLEtnfoG2dE.Kq12l67WCkJlDFFlz32YFxBEPzw+9DLJvFJendETgMvDYEW19FMhDxXkNutK58jnZDfvTABqgk2seqB2I7VUn12kQxdqvGP.eGf4VZIyDM9mGs+8dZqWY8SB.at49PjwAUiLXb5EYpHDIazIWKAiBDVAHRDANAHM8gxKGgmJYrEjcKytTDCOQ8t4inf4mRRRUFp9sNNnNClfSiLygwJiBYp5fBzg1n18Hnp0h1u9kvTqLV99voNXTGMhhXLTJpwzia6daJL0fNpLdLwq9s9uiCRxJ0zrmecZDsNDrKlV1nTYQFzKQh1wBLUfEppAXZ2BYpqhUXY5YsvtlXyOR7VS9dqd920QLeeQ1.bWCJVzr3Hqq9H4KGtJMzqtxJvdQFHMnrsParHV7VhcfbiPGKwfJCFsFjf10A5yA6yZwzeQIyNamWSGL6A5SJTPvJXSjNCl1GCUvw.qrHccvt+PbFtENizBmbJjI8PJF.0tiP6kFhpeSmSv7vs3vuTDS4x4zUqV9M8iueRS4x4z0p0qoVsdMKu7h5x2hmHbpL+OLoF9GTkjdWAqdNOlDmGbWlHwFHj6fky1H7VCQxWGQpyiU5kQWsL88Uh5tuteTsw+jzEc2F9fY.e6gqmMHTrnYN.97eWIMSHfQfdBiXuD9P6HL9dX1OE5TsPX2lHEHMdDscRL0EHc7grMPbLCKMjKIhLO923wi9Mp+bQwRt9DxWOSN4og3ZbqtiCN05Ei+gQXO.V8qvZnVXkUgnyd1rBiP66ipg.0lGB4dGF6pqxJqvz+J+Jh4NvLOhhEMyVdACL4c7h8VIc4yiX1YQrTt4EkJW1z2.iUewu9meE1LSaTM1Bg8QPG1ODYgvK.q9phTrEZ0pDxpG8W7+dsy9I+q0aszyHlN+r2482cyju2tDwehHr2NEZ3cq.v.e.A7GaLwDvh+ssMb3FZdUKeRL3Nn2dGzMuGBqj.rASsXgbxR4QzddnpWCqTqi8.WF5uAJKAWA16u5bR9WLtE4xo+KyjTlz4F5UJZOIp8bvX5AokKhd0XmMBq9MfkLtl9zZrRGhLoBoiMZKGLthUG9AECUYL4jSkqSABOOCmGStbSF6fP2AjN8DW++U6.Iq7FoqKHGLwXaDtHo2i+y82B6Qqfi4RfpeD1tXhBvXWEKqcX+fJryeVi0F+2mO1HiaW1aBwvLm4lx9k2Gf2MIb2J9.B3aebScRK97KqY7wEP.jqmFH+HWF82eH7W8dQU+vHMo.qdAsMJjXBi.mJ3k+UHwnuBAd6RiciNwTiXBG8PJnkXtbkk3kQVu6m2vkMjoeAs7LHRFhnGE1IkH6wIVrds0cTiMIBOAVoTXOv9nObUBaEQqph882R167IULErv7E0ElZRY5z0EYxj6Ms8iffKG6ohGTqT5PTWj3iqXcnSEDLuleyoZyeReaitVCDo2jn.Kj8nIzO7LAp1memWocbUlLsHakkM8t9hFxN1e+z6bWJ9.B3OtX7kLSRN8v4KalqzQawdGZS7NyaPTyQQ4aPa0GFch3kkYEEOqP+mCY1KPUy5GM0tAqkYM0pzVu55KpgwsajJm7u4+10r8RYEuj24VxvfOfhDmnEtquCF68QjXH.OPDahKWWQs0FHgA6QpRzt6QplQvnweWmBVX940SmGwSzg74bvYY6.GmbBBSZvsyRx5Z8a9IMcOqvr.K08ET7wTTnfNtzvxIndZATCVDN+D.mMmAPt0RyQ1Q69Nzsnf5d52+rM9.B36bz4P8KZVfX+MexBuP3BaoqQvYtDYxEQT6WG0NCitVRPqQlpENY2CRcELYuBYtZ60VMshIyYnSg6Bv49apXs1EZ3zrUMCSNoEKTTQ1+wADlYOTGcUzUyQ3dGGSnGpZBjcLrUsBh1J.89ZHwdXObERre.IyZ5yaX8By+rZ.FeIL0x0qwy6xF3329qtC32h2ppgMQmGaI3FVURWYn3fA642JufkxIlj4YgREMS8ElTLZ55LxomjRku0yT71helIPLe.A7mLX.XghOlhYmsE+dGsLqcz5X2bcjCMDhnDX4DhztIpfZDHpCUaxpDwzKaXZfkVxLc9aDDfc1bOYiVByoRtf0EAEW7qFxIdx8vYvKg+NoPua+DreKja2CFkMZiAoLDci5XBtHB2qhH6djYr1jcrnEW+YUcCtQrHbrfdpoljzoKe62CXWMLsKNX.YxMonFwZM072oVj74ik4eJaVX57caidqi94OdAe4+u.e.A7GGz8t8EKBcy9hYlQSgBs4q+DQrlcarbqh1URCZSONgbnJQL1xZF+.Q.ryLeegbSJR6W27s8bzRagQZINv4ZUzvGIeMd9wtBVC1FSicQT8HDV4PnZ0CHLHRUGqduFxzWgDidYNTe6cpQdiVWbjm85dtWWTrHTjozTrnoPg2LwXhaSVCc8HE91ozclYISmJwHVBPAFMccQVuVh0ucUe9OCS9fOf.9SFhq1.nKIrXWOpmHldVelqS13uKvZXttGXTnfb57EEimaRwT.ciH4I6ca82Q6FzuG7o+4G27z+FkrXFzL2L5mFpUb7wawUz6hu6JDDdHboWTRMNYqSO2SYru+JblQawYR29hi7RuIx20Q2YDuMjs6TZ9I.i4GUo6TDtN4CnquRz0EpFMccQoxXtC6+6m4HevGP.+oBLyh0MU6ffg4lQyryJmboWPBw46xDidAwXYeJ8mZrut7XYdbY8fKaR2M.G050vvnu3uLAs85S+zY63puuvjVjtt3oAd5L4DKEzp8W441X2W6kq1dulMsq2PnsRds1MG5UqM4iVx+YxdeZl4w5V3w21utc9867DwuHlx4Wvj6GwrfwpP1MpD8J9IMcmAbtCV1O2EmAK+zB2UUMDuOA2T17SwhFSAD2lD5VPgWvBxI463JfKBo+iMSN9b5e8mH9b1lf3yYaJfa0XP5RlMcsPqtyV0w2C9Vasub2MCE6V0wLvHtlhyTpq3D8N6Z4lwa2W+sebSgBhaR5M6LQWgolWBD6CG29Di+mYIf++.DsJzYJGOrhJ.....jTQNQjqBAlf" ],
					"embed" : 1,
					"id" : "obj-22",
					"ignoreclick" : 1,
					"maxclass" : "fpic",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "jit_matrix" ],
					"patching_rect" : [ 724.0, 70.0, 108.0, 96.0 ],
					"pic" : "SoDA - icon - transparent.png",
					"presentation" : 1,
					"presentation_rect" : [ 8.0, 7.0, 53.0, 50.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.059509769082069, 0.501929938793182, 0.998454749584198, 1.0 ],
					"id" : "obj-12",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 129.0, 1045.0, 138.0, 22.0 ],
					"text" : "udpsend 127.0.0.1 8889"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.059509769082069, 0.501929938793182, 0.998454749584198, 1.0 ],
					"id" : "obj-6",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 793.0, 381.0, 97.0, 22.0 ],
					"text" : "udpreceive 8888"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-147",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 1,
							"revision" : 8,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 59.0, 104.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "int", "int" ],
									"patching_rect" : [ 233.0, 248.0, 48.0, 22.0 ],
									"text" : "change"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-9",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 208.0, 296.0, 223.0, 22.0 ],
									"text" : "1 whoami sid-1 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 233.0, 47.0, 223.0, 22.0 ],
									"text" : "1/whoami/sid-1/0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "", "", "", "", "" ],
									"patching_rect" : [ 233.0, 169.0, 134.0, 22.0 ],
									"text" : "regexp / @substitute \" \""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 233.0, 133.0, 223.0, 22.0 ],
									"text" : "0/EMPTY/ml-mode~0/0"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-3",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 81.0, 379.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 81.0, 39.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"order" : 1,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 1 ],
									"order" : 0,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"order" : 1,
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 1 ],
									"order" : 0,
									"source" : [ "obj-6", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 805.25, 565.0, 100.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p avoid repetition"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-141",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 744.0, 229.0, 147.0, 22.0 ],
					"text" : "print NODE(receive)8888:"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-138",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 786.0, 354.0, 190.0, 20.0 ],
					"text" : "UDP 8888 (from control to node)"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-137",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 125.75, 1070.0, 190.0, 20.0 ],
					"text" : "UDP 8889 (from node to control)"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-134",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 1,
							"revision" : 8,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 461.0, 160.0, 945.0, 490.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-9",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 856.0, 508.0, 150.0, 20.0 ],
									"text" : "sid"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-18",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 9.5, 623.166666999999961, 119.0, 22.0 ],
									"text" : "1/whoami/query/0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-16",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 109.5, 564.166666999999961, 150.0, 22.0 ],
									"text" : "sprintf %d/whoami/query/0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-15",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 109.5, 524.166666999999961, 56.5, 22.0 ],
									"text" : "i 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-14",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 311.333333333333314, 524.166666999999961, 54.5, 22.0 ],
									"text" : "i 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 668.0, 146.0, 68.0, 20.0 ],
									"text" : "<<POV sid"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-11",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 612.0, 145.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-7",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 612.0, 100.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 741.0, 435.166667000000018, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-8",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 586.0, 435.166667000000018, 150.0, 20.0 ],
									"text" : "selected_participant_id"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-6",
									"index" : 3,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 741.0, 100.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 799.0, 506.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 799.0, 565.166666999999961, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.709512, 0.700452, 0.700662, 1.0 ],
									"bgcolor2" : [ 0.709512, 0.700452, 0.700662, 1.0 ],
									"bgfillcolor_angle" : 270.0,
									"bgfillcolor_autogradient" : 0.0,
									"bgfillcolor_color" : [ 0.2, 0.2, 0.2, 1.0 ],
									"bgfillcolor_color1" : [ 0.709512, 0.700452, 0.700662, 1.0 ],
									"bgfillcolor_color2" : [ 0.709512, 0.700452, 0.700662, 1.0 ],
									"bgfillcolor_proportion" : 0.5,
									"bgfillcolor_type" : "gradient",
									"gradient" : 1,
									"id" : "obj-3",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 148.5, 145.0, 29.5, 22.0 ],
									"text" : "?",
									"textjustification" : 1
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-2",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 109.5, 145.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.709512, 0.700452, 0.700662, 1.0 ],
									"bgcolor2" : [ 0.709512, 0.700452, 0.700662, 1.0 ],
									"bgfillcolor_angle" : 270.0,
									"bgfillcolor_autogradient" : 0.0,
									"bgfillcolor_color" : [ 0.2, 0.2, 0.2, 1.0 ],
									"bgfillcolor_color1" : [ 0.709512, 0.700452, 0.700662, 1.0 ],
									"bgfillcolor_color2" : [ 0.709512, 0.700452, 0.700662, 1.0 ],
									"bgfillcolor_proportion" : 0.5,
									"bgfillcolor_type" : "gradient",
									"gradient" : 1,
									"id" : "obj-78",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 437.333333333333314, 524.166666999999961, 40.0, 22.0 ],
									"text" : "sid-1",
									"textjustification" : 1
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-118",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 279.0, 381.166667000000018, 50.0, 22.0 ],
									"text" : "sid-1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-116",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 341.0, 381.166667000000018, 81.0, 22.0 ],
									"text" : "sprintf sid-%d"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-115",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 341.0, 345.166667000000018, 55.0, 22.0 ],
									"text" : "route sid"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-114",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 25.5, 307.166667000000018, 209.0, 22.0 ],
									"text" : "node-1-sid-1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-113",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 295.0, 345.166667000000018, 29.5, 22.0 ],
									"text" : "1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-111",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "", "", "", "", "" ],
									"patching_rect" : [ 341.0, 303.166667000000018, 135.0, 22.0 ],
									"text" : "regexp - @substitute \" \""
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.5, 0.0, 0.5, 1.0 ],
									"id" : "obj-106",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 311.333333333333314, 706.166666999999961, 119.0, 22.0 ],
									"text" : "s amexserver_speak"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-105",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 539.5, 670.166666999999961, 194.0, 22.0 ],
									"text" : "0/whoami/node-1-sid-1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-103",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 311.333333333333314, 641.166666999999961, 147.0, 22.0 ],
									"text" : "sprintf %d/whoami/%s-%s"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-102",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 311.333333333333314, 600.166666999999961, 143.0, 22.0 ],
									"text" : "pack 0 s s"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-101",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 373.333333333333314, 564.166666999999961, 89.0, 22.0 ],
									"text" : "sprintf node-%i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-100",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 601.5, 518.166666999999961, 89.0, 22.0 ],
									"text" : "loadmess set ?"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-97",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 594.5, 629.166666999999961, 112.0, 22.0 ],
									"text" : "0 node-1 sid-1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-93",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 373.333333333333314, 524.166666999999961, 54.5, 22.0 ],
									"text" : "i 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-92",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "bang", "bang", "bang", "" ],
									"patching_rect" : [ 341.0, 419.166667000000018, 115.333333333333314, 22.0 ],
									"text" : "t b b b l"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-89",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 437.333333333333314, 450.166667000000018, 72.0, 22.0 ],
									"text" : "prepend set"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-69",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 25.5, 348.166667000000018, 102.0, 22.0 ],
									"text" : "node 1 sid 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-67",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 341.0, 261.166667000000018, 55.0, 22.0 ],
									"text" : "zl slice 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-60",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 22.5, 271.166667000000018, 209.0, 22.0 ],
									"text" : "node-1-sid-1 0 1619013224596"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-52",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 341.0, 217.16666699999999, 252.0, 22.0 ],
									"text" : "route whoami daddy"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-51",
									"linecount" : 2,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 22.5, 232.16666699999999, 201.0, 22.0 ],
									"text" : "EMPTY ml-mode~0 0 1619013288139"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-45",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 305.0, 176.16666699999999, 55.0, 22.0 ],
									"text" : "zl slice 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-32",
									"linecount" : 2,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 22.5, 191.16666699999999, 187.0, 35.0 ],
									"text" : "1 EMPTY ml-mode~0 0 1619013288139"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-22",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "", "", "", "", "" ],
									"patching_rect" : [ 305.0, 133.16666699999999, 134.0, 22.0 ],
									"text" : "regexp / @substitute \" \""
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.5, 0.0, 0.5, 1.0 ],
									"id" : "obj-12",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 305.0, 100.0, 113.0, 22.0 ],
									"text" : "r amexserver_listen"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-132",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 109.5, 100.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-133",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 601.5, 564.166666999999961, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-93", 1 ],
									"midpoints" : [ 750.5, 510.0, 418.333333333333314, 510.0 ],
									"source" : [ "obj-10", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-133", 0 ],
									"source" : [ "obj-100", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-102", 1 ],
									"source" : [ "obj-101", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-103", 0 ],
									"order" : 1,
									"source" : [ "obj-102", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-97", 1 ],
									"order" : 0,
									"source" : [ "obj-102", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-105", 1 ],
									"order" : 0,
									"source" : [ "obj-103", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-106", 0 ],
									"order" : 1,
									"source" : [ "obj-103", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-106", 0 ],
									"source" : [ "obj-105", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 1 ],
									"midpoints" : [ 621.5, 504.0, 356.333333333333314, 504.0 ],
									"order" : 0,
									"source" : [ "obj-11", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 1 ],
									"midpoints" : [ 621.5, 496.0, 156.5, 496.0 ],
									"order" : 1,
									"source" : [ "obj-11", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-115", 0 ],
									"order" : 0,
									"source" : [ "obj-111", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-69", 1 ],
									"order" : 1,
									"source" : [ "obj-111", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-116", 0 ],
									"source" : [ "obj-113", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-113", 1 ],
									"order" : 2,
									"source" : [ "obj-115", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-116", 0 ],
									"order" : 1,
									"source" : [ "obj-115", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"midpoints" : [ 350.5, 373.0, 808.5, 373.0 ],
									"order" : 0,
									"source" : [ "obj-115", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-118", 1 ],
									"order" : 1,
									"source" : [ "obj-116", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-92", 0 ],
									"order" : 0,
									"source" : [ "obj-116", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-22", 0 ],
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-132", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-102", 0 ],
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"source" : [ "obj-15", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-106", 0 ],
									"order" : 0,
									"source" : [ "obj-16", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-18", 1 ],
									"order" : 1,
									"source" : [ "obj-16", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 0 ],
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-32", 1 ],
									"order" : 1,
									"source" : [ "obj-22", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-45", 0 ],
									"order" : 0,
									"source" : [ "obj-22", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-51", 1 ],
									"order" : 1,
									"source" : [ "obj-45", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-52", 0 ],
									"order" : 0,
									"source" : [ "obj-45", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 0 ],
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-60", 1 ],
									"order" : 1,
									"source" : [ "obj-52", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-67", 0 ],
									"order" : 0,
									"source" : [ "obj-52", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-111", 0 ],
									"order" : 0,
									"source" : [ "obj-67", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-114", 1 ],
									"order" : 1,
									"source" : [ "obj-67", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-102", 2 ],
									"source" : [ "obj-78", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-133", 0 ],
									"order" : 0,
									"source" : [ "obj-89", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-78", 0 ],
									"order" : 1,
									"source" : [ "obj-89", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"source" : [ "obj-92", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-78", 0 ],
									"source" : [ "obj-92", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-89", 0 ],
									"source" : [ "obj-92", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-93", 0 ],
									"source" : [ "obj-92", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-101", 0 ],
									"source" : [ "obj-93", 0 ]
								}

							}
 ],
						"styles" : [ 							{
								"name" : "AudioStatus_Menu",
								"default" : 								{
									"bgfillcolor" : 									{
										"type" : "color",
										"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
										"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
										"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}

								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "maxscore.default.buttons",
								"default" : 								{
									"accentcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
 ]
					}
,
					"patching_rect" : [ 221.25, 702.833333000000039, 99.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p whoami_pid"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-129",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 164.25, 971.0, 169.0, 22.0 ],
					"text" : "sprintf /soda/local/listening/%s"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.735483884811401, 0.070231787860394, 0.910786986351013, 1.0 ],
					"id" : "obj-127",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 164.25, 930.0, 113.0, 22.0 ],
					"text" : "r amexserver_listen"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-124",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 793.0, 461.0, 97.0, 22.0 ],
					"text" : "OSC-route /local"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-119",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 793.0, 425.0, 99.0, 22.0 ],
					"text" : "OSC-route /soda"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.709512, 0.700452, 0.700662, 1.0 ],
					"bgcolor2" : [ 0.709512, 0.700452, 0.700662, 1.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.2, 0.2, 0.2, 1.0 ],
					"bgfillcolor_color1" : [ 0.709512, 0.700452, 0.700662, 1.0 ],
					"bgfillcolor_color2" : [ 0.709512, 0.700452, 0.700662, 1.0 ],
					"bgfillcolor_proportion" : 0.5,
					"bgfillcolor_type" : "gradient",
					"gradient" : 1,
					"id" : "obj-78",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 164.25, 702.833333000000039, 35.5, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 284.0, 138.0, 61.0, 22.0 ],
					"text" : "sid-1",
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-4",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "", "", "", "", "" ],
					"patching_rect" : [ 793.0, 535.0, 68.0, 22.0 ],
					"text" : "regexp /(.*)"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-77",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "int", "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 1,
							"revision" : 8,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 479.0, 194.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 464.0, 372.0, 19.0, 22.0 ],
									"text" : "t l"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-12",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 464.0, 453.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 365.0, 239.0, 77.0, 22.0 ],
									"text" : "route symbol"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-8",
									"linecount" : 2,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 493.0, 250.0, 50.0, 49.0 ],
									"text" : "symbol Max"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-1",
									"items" : [ ",", "Adrian", ",", "Ben", ",", "Dalma", ",", "Hanne", ",", "Kathi", ",", "Magdalena", ",", "Maria", ",", "Mariama", ",", "Max" ],
									"maxclass" : "umenu",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "int", "", "" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 464.0, 414.0, 100.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-173",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 365.0, 325.999977999999999, 96.0, 22.0 ],
									"text" : "prepend append"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-172",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "dump", "append", "clear" ],
									"patching_rect" : [ 365.0, 100.833308473770103, 118.0, 22.0 ],
									"text" : "t dump append clear"
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 24.0,
									"id" : "obj-170",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "", "", "", "" ],
									"patching_rect" : [ 365.0, 159.948062196914634, 209.0, 35.0 ],
									"saved_object_attributes" : 									{
										"embed" : 0,
										"precision" : 6
									}
,
									"text" : "coll nodeID-names"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-168",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 365.0, 65.0, 120.0, 22.0 ],
									"text" : "r populate-PID-menu"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-9",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "bang" ],
									"patching_rect" : [ 162.0, 153.0, 58.0, 22.0 ],
									"text" : "t b b"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 201.0, 212.000000999999997, 35.0, 22.0 ],
									"text" : "set 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 85.25, 139.0, 29.5, 22.0 ],
									"text" : "0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 139.0, 29.5, 22.0 ],
									"text" : "1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-72",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 212.000000999999997, 74.0, 22.0 ],
									"text" : "settoggle $1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-70",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "int" ],
									"patching_rect" : [ 50.0, 100.0, 100.0, 22.0 ],
									"text" : "t b i"
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.694118, 0.670588, 0.670588, 1.0 ],
									"id" : "obj-17",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 162.0, 100.0, 58.0, 22.0 ],
									"text" : "loadbang"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-73",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 50.0, 39.999977999999999, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-74",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 131.0, 260.999977999999999, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-75",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 50.0, 260.999977999999999, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-76",
									"index" : 3,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 201.0, 260.999977999999999, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-173", 0 ],
									"source" : [ "obj-10", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-173", 0 ],
									"source" : [ "obj-10", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 0 ],
									"order" : 0,
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-76", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-172", 0 ],
									"source" : [ "obj-168", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"source" : [ "obj-17", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"order" : 1,
									"source" : [ "obj-170", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 1 ],
									"order" : 0,
									"source" : [ "obj-170", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"source" : [ "obj-172", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"source" : [ "obj-172", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-170", 0 ],
									"source" : [ "obj-172", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"source" : [ "obj-173", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-72", 0 ],
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-72", 0 ],
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-76", 0 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"source" : [ "obj-70", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-74", 0 ],
									"source" : [ "obj-70", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-75", 0 ],
									"source" : [ "obj-72", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-70", 0 ],
									"source" : [ "obj-73", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-9", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-9", 1 ]
								}

							}
 ],
						"styles" : [ 							{
								"name" : "AudioStatus_Menu",
								"default" : 								{
									"bgfillcolor" : 									{
										"type" : "color",
										"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
										"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
										"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}

								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "maxscore.default.buttons",
								"default" : 								{
									"accentcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
 ]
					}
,
					"patching_rect" : [ 41.0, 732.666689000000019, 91.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p umenusetting"
				}

			}
, 			{
				"box" : 				{
					"align" : 1,
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.2, 0.2, 0.2, 1.0 ],
					"bgfillcolor_color1" : [ 0.392157, 0.556863, 0.301961, 1.0 ],
					"bgfillcolor_color2" : [ 0.244319, 0.299304, 0.229028, 1.0 ],
					"bgfillcolor_proportion" : 0.5,
					"bgfillcolor_type" : "gradient",
					"fontface" : 1,
					"fontsize" : 14.0,
					"hint" : "Press this button to connect to the server",
					"id" : "obj-57",
					"ignoreclick" : 1,
					"items" : [ 2.0, ",", 2.0 ],
					"maxclass" : "umenu",
					"menumode" : 3,
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 41.0, 664.666666999999961, 25.083333, 24.0 ],
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-55",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 1,
							"revision" : 8,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 459.0, 147.0, 909.0, 485.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-3",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1015.0, 170.166686999999996, 83.0, 22.0 ],
									"text" : "closed"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Geneva",
									"fontsize" : 9.0,
									"id" : "obj-12",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "", "", "", "", "" ],
									"patching_rect" : [ 929.0, 137.0, 139.0, 20.0 ],
									"text" : "regexp closed @substitute \" \""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-21",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 929.0, 63.0, 113.0, 22.0 ],
									"text" : "\"connection closed\""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-16",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 778.0, 354.0, 71.0, 22.0 ],
									"text" : "tcp-status 1"
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.282352941176471, 0.776470588235294, 0.125490196078431, 1.0 ],
									"id" : "obj-14",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 778.0, 308.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-11",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 778.0, 255.0, 83.0, 22.0 ],
									"text" : "sel connected"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 176.0, 170.166686999999996, 69.0, 22.0 ],
									"text" : "connected"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 176.0, 63.0, 64.0, 22.0 ],
									"text" : "connected"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Geneva",
									"fontsize" : 9.0,
									"id" : "obj-10",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "", "", "", "", "" ],
									"patching_rect" : [ 176.0, 137.0, 157.0, 20.0 ],
									"text" : "regexp connected @substitute \" \""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 462.0, 354.0, 29.5, 22.0 ],
									"text" : "0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-19",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 835.0, 170.166686999999996, 83.0, 22.0 ],
									"text" : "cannot"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Geneva",
									"fontsize" : 9.0,
									"id" : "obj-20",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "", "", "", "", "" ],
									"patching_rect" : [ 749.0, 137.0, 141.0, 20.0 ],
									"text" : "regexp cannot @substitute \" \""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-18",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 749.0, 63.0, 138.0, 22.0 ],
									"text" : "\"cannot connect to host\""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-17",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 632.0, 170.166686999999996, 83.0, 22.0 ],
									"text" : "disconnected"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-15",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 366.0, 170.166686999999996, 69.0, 22.0 ],
									"text" : "stopped"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-9",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 546.0, 63.0, 124.0, 22.0 ],
									"text" : "\"server disconnected\""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Geneva",
									"fontsize" : 9.0,
									"id" : "obj-13",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "", "", "", "", "" ],
									"patching_rect" : [ 546.0, 137.0, 169.0, 20.0 ],
									"text" : "regexp disconnected @substitute \" \""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-8",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 524.0, 354.0, 71.0, 22.0 ],
									"text" : "tcp-status 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-22",
									"linecount" : 2,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 366.0, 63.0, 172.0, 35.0 ],
									"text" : "\"stopped listening on previous connection\""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-6",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 524.0, 406.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-50",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 524.0, 225.166686999999996, 71.0, 22.0 ],
									"text" : "fromsymbol"
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.831372549019608, 0.062745098039216, 0.062745098039216, 1.0 ],
									"id" : "obj-49",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 524.0, 308.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-47",
									"maxclass" : "newobj",
									"numinlets" : 5,
									"numoutlets" : 5,
									"outlettype" : [ "bang", "bang", "bang", "bang", "" ],
									"patching_rect" : [ 524.0, 255.0, 221.0, 22.0 ],
									"text" : "sel stopped disconnected cannot closed"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Geneva",
									"fontsize" : 9.0,
									"id" : "obj-46",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "", "", "", "", "" ],
									"patching_rect" : [ 366.0, 137.0, 147.0, 20.0 ],
									"text" : "regexp stopped @substitute \" \""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-2",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 69.0, 406.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 69.0, 63.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"order" : 4,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"order" : 0,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"order" : 2,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"order" : 5,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 0 ],
									"order" : 1,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-46", 0 ],
									"order" : 3,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 1 ],
									"order" : 1,
									"source" : [ "obj-10", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-50", 0 ],
									"order" : 0,
									"source" : [ "obj-10", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"source" : [ "obj-11", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 1 ],
									"order" : 0,
									"source" : [ "obj-12", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-50", 0 ],
									"order" : 1,
									"source" : [ "obj-12", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 1 ],
									"order" : 0,
									"source" : [ "obj-13", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-50", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-16", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 0 ],
									"source" : [ "obj-18", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-19", 1 ],
									"order" : 0,
									"source" : [ "obj-20", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-50", 0 ],
									"order" : 1,
									"source" : [ "obj-20", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"source" : [ "obj-21", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-46", 0 ],
									"source" : [ "obj-22", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 1 ],
									"order" : 1,
									"source" : [ "obj-46", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-50", 0 ],
									"order" : 0,
									"source" : [ "obj-46", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"source" : [ "obj-47", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-49", 0 ],
									"source" : [ "obj-47", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-49", 0 ],
									"source" : [ "obj-47", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-49", 0 ],
									"source" : [ "obj-47", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-49", 0 ],
									"source" : [ "obj-47", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"order" : 1,
									"source" : [ "obj-49", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"order" : 0,
									"source" : [ "obj-49", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-47", 0 ],
									"source" : [ "obj-50", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-8", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"source" : [ "obj-9", 0 ]
								}

							}
 ],
						"styles" : [ 							{
								"name" : "AudioStatus_Menu",
								"default" : 								{
									"bgfillcolor" : 									{
										"type" : "color",
										"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
										"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
										"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}

								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "maxscore.default.buttons",
								"default" : 								{
									"accentcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
 ]
					}
,
					"patching_rect" : [ 88.5, 467.0, 81.666667000000004, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p tcp-status"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-58",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 1,
							"revision" : 8,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 59.0, 104.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-3",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 232.0, 155.0, 58.0, 22.0 ],
									"text" : "loadbang"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-51",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 50.0, 230.0, 41.0, 22.0 ],
									"text" : "t b l"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-50",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 189.0, 72.0, 22.0 ],
									"text" : "prepend tcp"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-49",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 88.5, 144.0, 29.5, 22.0 ],
									"text" : "off"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-47",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 144.0, 29.5, 22.0 ],
									"text" : "on"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-45",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 3,
									"outlettype" : [ "bang", "bang", "" ],
									"patching_rect" : [ 50.0, 100.0, 96.0, 22.0 ],
									"text" : "sel 1 0"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-55",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 50.0, 40.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-56",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 50.0, 312.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-57",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 85.0, 312.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-49", 0 ],
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-47", 0 ],
									"source" : [ "obj-45", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-49", 0 ],
									"source" : [ "obj-45", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-50", 0 ],
									"source" : [ "obj-47", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-50", 0 ],
									"source" : [ "obj-49", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-51", 0 ],
									"source" : [ "obj-50", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-56", 0 ],
									"source" : [ "obj-51", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-57", 0 ],
									"source" : [ "obj-51", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-45", 0 ],
									"source" : [ "obj-55", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 103.0, 342.0, 61.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p tcponoff"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.301961, 0.301961, 0.301961, 0.0 ],
					"bgcolor2" : [ 0.2, 0.2, 0.2, 0.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.2, 0.2, 0.2, 1.0 ],
					"bgfillcolor_color1" : [ 0.301961, 0.301961, 0.301961, 0.0 ],
					"bgfillcolor_color2" : [ 0.2, 0.2, 0.2, 0.0 ],
					"bgfillcolor_proportion" : 0.5,
					"bgfillcolor_type" : "gradient",
					"gradient" : 1,
					"id" : "obj-41",
					"ignoreclick" : 1,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 81.0, 381.0, 41.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 281.0, 111.0, 51.0, 22.0 ],
					"text" : "tcp on",
					"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-34",
					"ignoreclick" : 1,
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 103.0, 308.0, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 323.0, 111.0, 22.0, 22.0 ],
					"uncheckedcolor" : [ 0.572549, 0.560784, 0.560784, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-19",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 455.0, 375.0, 118.0, 22.0 ],
					"text" : "prepend tcp address"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-21",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 455.0, 346.0, 59.0, 22.0 ],
					"text" : "route text"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-15",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "bang" ],
					"patching_rect" : [ 455.0, 258.0, 32.0, 22.0 ],
					"text" : "t b b"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-18",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 496.0, 258.0, 61.0, 22.0 ],
					"text" : "set 12345"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-11",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 549.0, 313.0, 150.0, 20.0 ],
					"text" : "TCP port"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-14",
					"ignoreclick" : 1,
					"maxclass" : "textedit",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 455.0, 313.0, 89.0, 19.0 ],
					"text" : "12345",
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-9",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 289.0, 313.0, 150.0, 20.0 ],
					"text" : "IP address"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-44",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "bang" ],
					"patching_rect" : [ 193.0, 258.0, 32.0, 22.0 ],
					"text" : "t b b"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.694118, 0.0, 0.0, 1.0 ],
					"id" : "obj-43",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 193.0, 70.0, 58.0, 22.0 ],
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-40",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 193.0, 375.0, 118.0, 22.0 ],
					"text" : "prepend tcp address"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-36",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 193.0, 346.0, 59.0, 22.0 ],
					"text" : "route text"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-28",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 234.0, 258.0, 98.0, 22.0 ],
					"text" : "set 18.192.32.58"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-24",
					"ignoreclick" : 1,
					"maxclass" : "textedit",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 193.0, 313.0, 89.0, 19.0 ],
					"text" : "18.192.32.58",
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-54",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 41.0, 205.5, 280.0, 22.0 ],
					"text" : "tcp address 18.192.32.58, tcp port 12345, tcp open"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-53",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "", "int", "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 1,
							"revision" : 8,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 613.0, 172.0, 722.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 50.0, 174.472473000000036, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-16",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 3,
									"outlettype" : [ "bang", "bang", "" ],
									"patching_rect" : [ 575.09375, 290.0, 44.0, 22.0 ],
									"text" : "sel 1 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-15",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 587.59375, 424.0, 139.0, 22.0 ],
									"text" : "1. 0.125 0.125 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-14",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 575.09375, 391.0, 139.0, 22.0 ],
									"text" : "0. 0.6875 0.171875 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 455.5, 371.0, 95.0, 22.0 ],
									"text" : "prepend oncolor"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-2",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "int", "int" ],
									"patching_rect" : [ 575.09375, 240.472473000000008, 66.90625, 22.0 ],
									"text" : "t i i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 169.0, 97.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-12",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 169.0, 40.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.964705882352941, 0.941176470588235, 0.23921568627451, 0.0 ],
									"id" : "obj-76",
									"maxclass" : "led",
									"numinlets" : 1,
									"numoutlets" : 1,
									"offcolor" : [ 0.23921568627451, 0.211764705882353, 0.23921568627451, 1.0 ],
									"oncolor" : [ 0.0, 0.6875, 0.171875, 1.0 ],
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 455.5, 408.0, 38.0, 38.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 229.0, 100.0, 55.0, 55.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 623.0, 290.0, 82.0, 22.0 ],
									"text" : "ignoreclick $1"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 3,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 374.0, 335.472472999999979, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-3",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "bang", "int", "int" ],
									"patching_rect" : [ 514.71875, 151.472473000000008, 174.0, 22.0 ],
									"text" : "t b 1 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-47",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 467.0, 240.472473000000008, 53.0, 22.0 ],
									"text" : "Connect"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-46",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 414.0, 151.472473000000008, 69.0, 22.0 ],
									"text" : "Connecting"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-44",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 414.0, 286.472472999999979, 111.0, 22.0 ],
									"text" : "set $1 to the server"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-40",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 301.0, 142.472473000000008, 29.5, 22.0 ],
									"text" : "0."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-37",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 262.0, 142.472473000000008, 29.5, 22.0 ],
									"text" : "0.5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-28",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 3,
									"outlettype" : [ "bang", "bang", "" ],
									"patching_rect" : [ 262.0, 108.472472999999994, 97.0, 22.0 ],
									"text" : "sel 0 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-26",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 262.0, 176.472473000000008, 117.0, 22.0 ],
									"text" : "textcolor $1 $1 $1 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-36",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 169.0, 268.472472999999979, 34.0, 22.0 ],
									"text" : "sel 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-34",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 169.0, 148.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-31",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 50.0, 125.0, 91.0, 22.0 ],
									"text" : "route tcp-status"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-21",
									"index" : 3,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 514.71875, 43.472473000000008, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-49",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 40.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-50",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 169.0, 335.472472999999979, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-51",
									"index" : 4,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 623.0, 335.472472999999979, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-52",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 262.0, 335.472472999999979, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-51", 0 ],
									"order" : 0,
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-76", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"source" : [ "obj-15", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"source" : [ "obj-16", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 0 ],
									"source" : [ "obj-16", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-2", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-21", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-52", 0 ],
									"source" : [ "obj-26", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 0 ],
									"source" : [ "obj-28", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-40", 0 ],
									"order" : 1,
									"source" : [ "obj-28", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-47", 0 ],
									"order" : 0,
									"source" : [ "obj-28", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"source" : [ "obj-3", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-47", 0 ],
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-3", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"source" : [ "obj-31", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 0 ],
									"order" : 0,
									"source" : [ "obj-34", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-28", 0 ],
									"order" : 1,
									"source" : [ "obj-34", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-36", 0 ],
									"order" : 2,
									"source" : [ "obj-34", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-50", 0 ],
									"source" : [ "obj-36", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-26", 0 ],
									"source" : [ "obj-37", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-26", 0 ],
									"source" : [ "obj-40", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-52", 0 ],
									"source" : [ "obj-44", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-44", 0 ],
									"source" : [ "obj-46", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-44", 0 ],
									"source" : [ "obj-47", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-31", 0 ],
									"source" : [ "obj-49", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-34", 0 ],
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-51", 0 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"midpoints" : [ 59.5, 216.0, 584.59375, 216.0 ],
									"source" : [ "obj-7", 0 ]
								}

							}
 ],
						"styles" : [ 							{
								"name" : "AudioStatus_Menu",
								"default" : 								{
									"bgfillcolor" : 									{
										"type" : "color",
										"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
										"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
										"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}

								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "maxscore.default.buttons",
								"default" : 								{
									"accentcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
 ]
					}
,
					"patching_rect" : [ 41.0, 116.0, 112.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p togglepatch"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"fontsize" : 14.0,
					"id" : "obj-3",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 72.0, 167.0, 251.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 53.0, 111.0, 179.0, 22.0 ],
					"text" : "Connect to the server",
					"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.086275, 0.870588, 0.827451, 1.0 ],
					"bgcolor2" : [ 0.227451, 0.427451, 0.372549, 1.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.2, 0.2, 0.2, 1.0 ],
					"bgfillcolor_color1" : [ 0.086275, 0.870588, 0.827451, 1.0 ],
					"bgfillcolor_color2" : [ 0.227451, 0.427451, 0.372549, 1.0 ],
					"bgfillcolor_proportion" : 0.5,
					"bgfillcolor_type" : "gradient",
					"gradient" : 1,
					"id" : "obj-241",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 277.0, 10.0, 97.0, 22.0 ],
					"text" : "Online reference",
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-38",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 41.0, 167.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"align" : 1,
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.2, 0.2, 0.2, 1.0 ],
					"bgfillcolor_color1" : [ 0.392157, 0.556863, 0.301961, 1.0 ],
					"bgfillcolor_color2" : [ 0.244319, 0.299304, 0.229028, 1.0 ],
					"bgfillcolor_proportion" : 0.5,
					"bgfillcolor_type" : "gradient",
					"fontface" : 1,
					"fontsize" : 14.0,
					"hint" : "Press this button to connect to the server",
					"id" : "obj-20",
					"items" : [ 1.0, ",", 1.0 ],
					"maxclass" : "umenu",
					"menumode" : 3,
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 32.0, 69.0, 41.083332999999996, 24.0 ],
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-10",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 77.0, 780.333358999999973, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.2, 0.2, 0.2, 1.0 ],
					"bgfillcolor_color1" : [ 0.866667, 0.866667, 0.866667, 1.0 ],
					"bgfillcolor_color2" : [ 0.733333, 0.733333, 0.733333, 1.0 ],
					"bgfillcolor_proportion" : 0.5,
					"bgfillcolor_type" : "gradient",
					"color" : [ 0.294277, 0.455652, 0.215926, 1.0 ],
					"fontface" : 1,
					"id" : "obj-8",
					"items" : [ ",", "Adrian", ",", "Ben", ",", "Dalma", ",", "Hanne", ",", "Kathi", ",", "Magdalena", ",", "Maria", ",", "Mariama", ",", "Max" ],
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 41.0, 702.833333000000039, 100.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 201.0, 68.0, 148.0, 22.0 ],
					"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"fontsize" : 14.0,
					"id" : "obj-5",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 75.25, 665.666666999999961, 173.75, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 53.0, 68.0, 160.0, 22.0 ],
					"text" : "Select participant ID"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.694118, 0.670588, 0.670588, 1.0 ],
					"id" : "obj-2",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 193.0, 467.0, 79.0, 22.0 ],
					"text" : "loadmess set"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.376471, 0.384314, 0.4, 0.07 ],
					"bgcolor2" : [ 0.290196, 0.309804, 0.301961, 0.2 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_color1" : [ 0.376471, 0.384314, 0.4, 0.07 ],
					"bgfillcolor_color2" : [ 0.290196, 0.309804, 0.301961, 0.2 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "gradient",
					"gradient" : 1,
					"id" : "obj-13",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 193.0, 535.0, 113.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 8.0, 138.0, 224.0, 22.0 ],
					"text" : "connected",
					"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.376471, 0.384314, 0.4, 0.07 ],
					"bgcolor2" : [ 0.290196, 0.309804, 0.301961, 0.2 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_color1" : [ 0.376471, 0.384314, 0.4, 0.07 ],
					"bgfillcolor_color2" : [ 0.290196, 0.309804, 0.301961, 0.2 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "gradient",
					"gradient" : 1,
					"id" : "obj-7",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 181.0, 430.0, 113.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.865343332290649, 0.060312297195196, 0.959620356559753, 1.0 ],
					"id" : "obj-30",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 41.0, 591.0, 115.0, 22.0 ],
					"text" : "s amexserver_listen"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.5, 0.0, 0.5, 1.0 ],
					"id" : "obj-27",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 61.5, 254.0, 117.0, 22.0 ],
					"text" : "r amexserver_speak"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-29",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 88.5, 535.0, 90.0, 22.0 ],
					"text" : "print TCPclient"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-23",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 40.0, 24.0, 213.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 67.0, 31.0, 165.0, 20.0 ],
					"text" : "Max8 build | MacOS 10.13.6",
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial Bold",
					"fontsize" : 14.0,
					"id" : "obj-66",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 40.0, 4.0, 213.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 67.0, 9.0, 165.0, 22.0 ],
					"text" : "SoDA node  - v 1.5",
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-1",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "set" ],
					"patching_rect" : [ 41.0, 430.0, 114.0, 22.0 ],
					"text" : "amex_TCPclient"
				}

			}
, 			{
				"box" : 				{
					"angle" : 0.0,
					"background" : 1,
					"grad1" : [ 0.27, 0.35, 0.47, 1.0 ],
					"grad2" : [ 0.85, 0.85, 0.85, 1.0 ],
					"id" : "obj-25",
					"maxclass" : "panel",
					"mode" : 1,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 37.0, 1.0, 345.0, 43.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 6.0, 4.0, 344.0, 57.0 ],
					"proportion" : 0.39,
					"varname" : "autohelp_top_panel"
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"color" : [ 0.985537528991699, 0.009297370910645, 0.999170780181885, 1.0 ],
					"destination" : [ "obj-30", 0 ],
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-55", 0 ],
					"source" : [ "obj-1", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"source" : [ "obj-1", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-134", 2 ],
					"midpoints" : [ 86.5, 813.0, 330.0, 813.0, 330.0, 735.0, 330.0, 735.0, 330.0, 696.0, 310.75, 696.0 ],
					"order" : 1,
					"source" : [ "obj-10", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-165", 0 ],
					"order" : 0,
					"source" : [ "obj-10", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-429", 0 ],
					"order" : 2,
					"source" : [ "obj-10", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-102", 1 ],
					"source" : [ "obj-100", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-88", 0 ],
					"source" : [ "obj-100", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-150", 1 ],
					"order" : 1,
					"source" : [ "obj-101", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-82", 3 ],
					"order" : 0,
					"source" : [ "obj-101", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-74", 3 ],
					"midpoints" : [ 917.166666666666629, 937.0, 1131.0, 937.0, 1131.0, 744.0, 1201.0, 744.0 ],
					"source" : [ "obj-102", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-99", 0 ],
					"source" : [ "obj-102", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-82", 1 ],
					"source" : [ "obj-103", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-88", 0 ],
					"source" : [ "obj-104", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-90", 0 ],
					"source" : [ "obj-104", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-173", 0 ],
					"source" : [ "obj-106", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-168", 0 ],
					"source" : [ "obj-107", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-198", 0 ],
					"source" : [ "obj-108", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-111", 1 ],
					"order" : 1,
					"source" : [ "obj-109", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-81", 0 ],
					"order" : 0,
					"source" : [ "obj-109", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-149", 0 ],
					"source" : [ "obj-110", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-101", 0 ],
					"source" : [ "obj-112", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-115", 1 ],
					"order" : 0,
					"source" : [ "obj-112", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-118", 1 ],
					"source" : [ "obj-112", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 3 ],
					"source" : [ "obj-112", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-370", 1 ],
					"order" : 2,
					"source" : [ "obj-112", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-79", 0 ],
					"order" : 1,
					"source" : [ "obj-112", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-102", 1 ],
					"order" : 1,
					"source" : [ "obj-113", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-117", 1 ],
					"midpoints" : [ 917.166666666666629, 528.0, 1170.5, 528.0 ],
					"order" : 0,
					"source" : [ "obj-113", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 1 ],
					"source" : [ "obj-113", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"source" : [ "obj-113", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-135", 0 ],
					"source" : [ "obj-114", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-318", 4 ],
					"source" : [ "obj-116", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-124", 0 ],
					"source" : [ "obj-119", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-82", 2 ],
					"source" : [ "obj-123", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-113", 0 ],
					"source" : [ "obj-124", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-128", 0 ],
					"source" : [ "obj-125", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"color" : [ 0.598086297512054, 0.06153654307127, 0.848323106765747, 1.0 ],
					"destination" : [ "obj-129", 0 ],
					"source" : [ "obj-127", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-120", 1 ],
					"source" : [ "obj-128", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-122", 1 ],
					"source" : [ "obj-128", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-135", 0 ],
					"source" : [ "obj-128", 7 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-151", 0 ],
					"source" : [ "obj-128", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-156", 0 ],
					"source" : [ "obj-128", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-356", 0 ],
					"source" : [ "obj-128", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-393", 0 ],
					"source" : [ "obj-128", 6 ]
				}

			}
, 			{
				"patchline" : 				{
					"color" : [ 0.059509769082069, 0.501929938793182, 0.998454749584198, 1.0 ],
					"destination" : [ "obj-12", 0 ],
					"order" : 0,
					"source" : [ "obj-129", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-196", 0 ],
					"order" : 1,
					"source" : [ "obj-129", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-139", 0 ],
					"source" : [ "obj-130", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-50", 0 ],
					"source" : [ "obj-130", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-161", 0 ],
					"source" : [ "obj-131", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-148", 0 ],
					"source" : [ "obj-132", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-148", 0 ],
					"source" : [ "obj-133", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-64", 0 ],
					"source" : [ "obj-134", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-78", 0 ],
					"source" : [ "obj-134", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-133", 0 ],
					"source" : [ "obj-136", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-69", 1 ],
					"source" : [ "obj-139", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-69", 0 ],
					"source" : [ "obj-139", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-21", 0 ],
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-225", 0 ],
					"source" : [ "obj-140", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-135", 0 ],
					"order" : 1,
					"source" : [ "obj-142", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-146", 0 ],
					"source" : [ "obj-142", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-151", 0 ],
					"order" : 0,
					"source" : [ "obj-142", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-156", 0 ],
					"order" : 2,
					"source" : [ "obj-142", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-158", 1 ],
					"source" : [ "obj-143", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-148", 0 ],
					"source" : [ "obj-144", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-142", 0 ],
					"source" : [ "obj-145", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-148", 0 ],
					"source" : [ "obj-146", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"color" : [ 0.501960813999176, 0.0, 0.501960813999176, 1.0 ],
					"destination" : [ "obj-1", 0 ],
					"midpoints" : [ 814.75, 598.0, 649.0, 598.0, 649.0, 417.0, 50.5, 417.0 ],
					"order" : 1,
					"source" : [ "obj-147", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-74", 2 ],
					"midpoints" : [ 814.75, 607.0, 1190.0, 607.0 ],
					"order" : 0,
					"source" : [ "obj-147", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-125", 0 ],
					"source" : [ "obj-148", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-131", 0 ],
					"source" : [ "obj-148", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-157", 0 ],
					"order" : 1,
					"source" : [ "obj-149", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-206", 0 ],
					"order" : 2,
					"source" : [ "obj-149", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-228", 0 ],
					"order" : 0,
					"source" : [ "obj-149", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-236", 0 ],
					"order" : 3,
					"source" : [ "obj-149", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 0 ],
					"source" : [ "obj-15", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 0 ],
					"source" : [ "obj-15", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-82", 4 ],
					"source" : [ "obj-150", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-245", 0 ],
					"source" : [ "obj-151", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-247", 1 ],
					"source" : [ "obj-151", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-180", 0 ],
					"source" : [ "obj-152", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-179", 0 ],
					"source" : [ "obj-153", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-158", 0 ],
					"source" : [ "obj-155", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-162", 1 ],
					"source" : [ "obj-156", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-162", 0 ],
					"order" : 0,
					"source" : [ "obj-156", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-235", 1 ],
					"order" : 1,
					"source" : [ "obj-156", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-159", 0 ],
					"order" : 0,
					"source" : [ "obj-157", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-160", 0 ],
					"order" : 0,
					"source" : [ "obj-157", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-273", 0 ],
					"source" : [ "obj-157", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-282", 0 ],
					"order" : 1,
					"source" : [ "obj-157", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-282", 0 ],
					"order" : 1,
					"source" : [ "obj-157", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-180", 0 ],
					"source" : [ "obj-158", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-156", 0 ],
					"source" : [ "obj-159", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"color" : [ 0.501960813999176, 0.0, 0.501960813999176, 1.0 ],
					"destination" : [ "obj-1", 0 ],
					"midpoints" : [ 35.5, 964.0, 5.0, 964.0, 5.0, 420.0, 50.5, 420.0 ],
					"source" : [ "obj-16", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-321", 0 ],
					"source" : [ "obj-16", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-42", 0 ],
					"source" : [ "obj-16", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-151", 0 ],
					"source" : [ "obj-160", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-132", 0 ],
					"source" : [ "obj-161", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-164", 0 ],
					"source" : [ "obj-161", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-226", 0 ],
					"source" : [ "obj-161", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-163", 0 ],
					"source" : [ "obj-162", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-166", 0 ],
					"source" : [ "obj-163", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-252", 0 ],
					"order" : 3,
					"source" : [ "obj-165", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-253", 1 ],
					"order" : 1,
					"source" : [ "obj-165", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-288", 1 ],
					"order" : 0,
					"source" : [ "obj-165", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-290", 1 ],
					"order" : 2,
					"source" : [ "obj-165", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-169", 1 ],
					"order" : 1,
					"source" : [ "obj-166", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-174", 0 ],
					"order" : 0,
					"source" : [ "obj-166", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-201", 1 ],
					"order" : 2,
					"source" : [ "obj-166", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-182", 0 ],
					"source" : [ "obj-167", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-106", 0 ],
					"source" : [ "obj-168", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-107", 0 ],
					"source" : [ "obj-168", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-170", 0 ],
					"source" : [ "obj-168", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-72", 0 ],
					"source" : [ "obj-17", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-210", 0 ],
					"source" : [ "obj-170", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-155", 0 ],
					"order" : 0,
					"source" : [ "obj-172", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-181", 0 ],
					"order" : 1,
					"source" : [ "obj-172", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-107", 0 ],
					"source" : [ "obj-173", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-177", 0 ],
					"source" : [ "obj-174", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-167", 0 ],
					"source" : [ "obj-177", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-232", 1 ],
					"source" : [ "obj-179", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 0 ],
					"source" : [ "obj-18", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-185", 1 ],
					"source" : [ "obj-180", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-185", 0 ],
					"source" : [ "obj-181", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-183", 0 ],
					"source" : [ "obj-182", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-184", 0 ],
					"source" : [ "obj-183", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-184", 0 ],
					"source" : [ "obj-183", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-187", 0 ],
					"source" : [ "obj-184", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-176", 0 ],
					"source" : [ "obj-185", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-167", 0 ],
					"source" : [ "obj-186", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-188", 0 ],
					"source" : [ "obj-187", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-189", 0 ],
					"source" : [ "obj-187", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-214", 1 ],
					"order" : 0,
					"source" : [ "obj-188", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-299", 0 ],
					"order" : 1,
					"source" : [ "obj-188", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-188", 0 ],
					"source" : [ "obj-189", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"color" : [ 0.501960813999176, 0.0, 0.501960813999176, 1.0 ],
					"destination" : [ "obj-1", 0 ],
					"midpoints" : [ 464.5, 417.0, 50.5, 417.0 ],
					"source" : [ "obj-19", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-191", 0 ],
					"source" : [ "obj-190", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-172", 0 ],
					"source" : [ "obj-191", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-172", 0 ],
					"source" : [ "obj-191", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-190", 2 ],
					"order" : 1,
					"source" : [ "obj-192", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-202", 0 ],
					"order" : 0,
					"source" : [ "obj-192", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-380", 4 ],
					"source" : [ "obj-193", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-143", 0 ],
					"source" : [ "obj-195", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-300", 0 ],
					"source" : [ "obj-197", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-192", 0 ],
					"source" : [ "obj-198", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-240", 2 ],
					"order" : 1,
					"source" : [ "obj-199", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-328", 1 ],
					"order" : 0,
					"source" : [ "obj-199", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 0 ],
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-53", 0 ],
					"source" : [ "obj-20", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-212", 0 ],
					"source" : [ "obj-200", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-204", 0 ],
					"source" : [ "obj-202", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-198", 0 ],
					"source" : [ "obj-204", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-208", 0 ],
					"source" : [ "obj-205", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-207", 0 ],
					"order" : 1,
					"source" : [ "obj-206", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-209", 0 ],
					"order" : 0,
					"source" : [ "obj-206", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-190", 0 ],
					"order" : 1,
					"source" : [ "obj-207", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-194", 0 ],
					"order" : 0,
					"source" : [ "obj-207", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 0 ],
					"source" : [ "obj-21", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-319", 0 ],
					"source" : [ "obj-212", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-319", 0 ],
					"source" : [ "obj-212", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-216", 0 ],
					"source" : [ "obj-213", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-407", 1 ],
					"source" : [ "obj-215", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-305", 0 ],
					"source" : [ "obj-216", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-213", 0 ],
					"source" : [ "obj-217", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-213", 0 ],
					"source" : [ "obj-217", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-217", 0 ],
					"order" : 1,
					"source" : [ "obj-219", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-218", 1 ],
					"order" : 0,
					"source" : [ "obj-219", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-219", 0 ],
					"source" : [ "obj-220", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-219", 0 ],
					"source" : [ "obj-220", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-220", 0 ],
					"source" : [ "obj-221", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-278", 0 ],
					"order" : 1,
					"source" : [ "obj-222", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-285", 0 ],
					"source" : [ "obj-222", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-348", 0 ],
					"source" : [ "obj-222", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-432", 0 ],
					"source" : [ "obj-222", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-432", 0 ],
					"order" : 0,
					"source" : [ "obj-222", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-266", 0 ],
					"source" : [ "obj-224", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-266", 0 ],
					"source" : [ "obj-224", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-144", 0 ],
					"source" : [ "obj-225", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-159", 0 ],
					"order" : 1,
					"source" : [ "obj-226", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-160", 0 ],
					"order" : 0,
					"source" : [ "obj-226", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-253", 0 ],
					"source" : [ "obj-227", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-231", 0 ],
					"source" : [ "obj-228", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-322", 0 ],
					"source" : [ "obj-229", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-248", 0 ],
					"source" : [ "obj-230", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-227", 0 ],
					"source" : [ "obj-231", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-167", 0 ],
					"source" : [ "obj-232", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-303", 0 ],
					"source" : [ "obj-233", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-232", 0 ],
					"source" : [ "obj-234", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-234", 0 ],
					"source" : [ "obj-236", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-379", 0 ],
					"source" : [ "obj-237", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-222", 0 ],
					"order" : 5,
					"source" : [ "obj-238", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-229", 0 ],
					"order" : 0,
					"source" : [ "obj-238", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-240", 0 ],
					"order" : 2,
					"source" : [ "obj-238", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-318", 0 ],
					"order" : 3,
					"source" : [ "obj-238", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-380", 0 ],
					"order" : 4,
					"source" : [ "obj-238", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-449", 0 ],
					"order" : 1,
					"source" : [ "obj-238", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-238", 0 ],
					"source" : [ "obj-239", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-36", 0 ],
					"source" : [ "obj-24", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-264", 0 ],
					"source" : [ "obj-240", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-254", 0 ],
					"order" : 1,
					"source" : [ "obj-242", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-425", 1 ],
					"order" : 0,
					"source" : [ "obj-242", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-246", 0 ],
					"source" : [ "obj-245", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-246", 0 ],
					"source" : [ "obj-245", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-247", 0 ],
					"source" : [ "obj-246", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-230", 0 ],
					"source" : [ "obj-247", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-260", 0 ],
					"source" : [ "obj-248", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-250", 0 ],
					"source" : [ "obj-249", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-227", 1 ],
					"source" : [ "obj-250", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-166", 1 ],
					"source" : [ "obj-252", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-248", 0 ],
					"midpoints" : [ 1861.5, 1335.0, 1735.5, 1335.0 ],
					"source" : [ "obj-253", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-240", 3 ],
					"source" : [ "obj-254", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-251", 1 ],
					"order" : 1,
					"source" : [ "obj-254", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-425", 0 ],
					"order" : 0,
					"source" : [ "obj-254", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-374", 0 ],
					"source" : [ "obj-255", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-255", 0 ],
					"source" : [ "obj-256", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-253", 1 ],
					"source" : [ "obj-257", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-402", 0 ],
					"source" : [ "obj-258", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-253", 1 ],
					"source" : [ "obj-259", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"color" : [ 0.985537528991699, 0.009297370910645, 0.999170780181885, 1.0 ],
					"destination" : [ "obj-112", 0 ],
					"order" : 0,
					"source" : [ "obj-26", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"color" : [ 0.985537528991699, 0.009297370910645, 0.999170780181885, 1.0 ],
					"destination" : [ "obj-154", 0 ],
					"order" : 2,
					"source" : [ "obj-26", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"color" : [ 0.985537528991699, 0.009297370910645, 0.999170780181885, 1.0 ],
					"destination" : [ "obj-91", 0 ],
					"order" : 1,
					"source" : [ "obj-26", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-216", 0 ],
					"source" : [ "obj-260", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-265", 0 ],
					"source" : [ "obj-260", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-387", 0 ],
					"source" : [ "obj-261", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-261", 0 ],
					"source" : [ "obj-263", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-272", 1 ],
					"source" : [ "obj-264", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-274", 1 ],
					"source" : [ "obj-264", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-311", 1 ],
					"source" : [ "obj-264", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-380", 2 ],
					"order" : 0,
					"source" : [ "obj-264", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-380", 1 ],
					"order" : 1,
					"source" : [ "obj-264", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-224", 0 ],
					"order" : 1,
					"source" : [ "obj-265", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-262", 1 ],
					"order" : 0,
					"source" : [ "obj-265", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-221", 0 ],
					"source" : [ "obj-266", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-269", 0 ],
					"source" : [ "obj-268", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-271", 0 ],
					"source" : [ "obj-269", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"color" : [ 0.501960813999176, 0.0, 0.501960813999176, 1.0 ],
					"destination" : [ "obj-1", 0 ],
					"source" : [ "obj-27", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-276", 0 ],
					"source" : [ "obj-271", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-268", 0 ],
					"source" : [ "obj-273", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-268", 0 ],
					"source" : [ "obj-275", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-277", 0 ],
					"source" : [ "obj-276", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-283", 0 ],
					"source" : [ "obj-276", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-273", 0 ],
					"source" : [ "obj-277", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-275", 0 ],
					"source" : [ "obj-277", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-284", 0 ],
					"source" : [ "obj-277", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-297", 0 ],
					"source" : [ "obj-278", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-281", 0 ],
					"source" : [ "obj-279", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-24", 0 ],
					"source" : [ "obj-28", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-199", 0 ],
					"source" : [ "obj-280", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-268", 0 ],
					"source" : [ "obj-281", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-268", 0 ],
					"source" : [ "obj-282", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-273", 0 ],
					"source" : [ "obj-283", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-282", 0 ],
					"source" : [ "obj-283", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-286", 0 ],
					"source" : [ "obj-284", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-293", 0 ],
					"source" : [ "obj-285", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-279", 0 ],
					"source" : [ "obj-286", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-223", 0 ],
					"order" : 0,
					"source" : [ "obj-287", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"color" : [ 0.501960813999176, 0.0, 0.501960813999176, 1.0 ],
					"destination" : [ "obj-291", 0 ],
					"order" : 1,
					"source" : [ "obj-287", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-287", 0 ],
					"source" : [ "obj-288", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"color" : [ 0.501960813999176, 0.0, 0.501960813999176, 1.0 ],
					"destination" : [ "obj-291", 0 ],
					"source" : [ "obj-289", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-289", 0 ],
					"source" : [ "obj-290", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-361", 0 ],
					"source" : [ "obj-292", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-199", 0 ],
					"source" : [ "obj-293", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-280", 0 ],
					"source" : [ "obj-293", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-295", 0 ],
					"source" : [ "obj-293", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-298", 0 ],
					"source" : [ "obj-294", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-297", 0 ],
					"source" : [ "obj-295", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-287", 0 ],
					"source" : [ "obj-296", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-199", 0 ],
					"source" : [ "obj-297", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-279", 0 ],
					"source" : [ "obj-298", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-233", 0 ],
					"source" : [ "obj-299", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-304", 0 ],
					"source" : [ "obj-300", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-307", 0 ],
					"source" : [ "obj-300", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-212", 0 ],
					"source" : [ "obj-301", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-290", 0 ],
					"source" : [ "obj-303", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-244", 0 ],
					"source" : [ "obj-304", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-288", 0 ],
					"source" : [ "obj-305", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-205", 0 ],
					"source" : [ "obj-306", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-244", 0 ],
					"source" : [ "obj-307", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-385", 0 ],
					"source" : [ "obj-308", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"source" : [ "obj-31", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-242", 1 ],
					"source" : [ "obj-314", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-237", 0 ],
					"source" : [ "obj-315", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-237", 0 ],
					"source" : [ "obj-317", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-193", 0 ],
					"order" : 1,
					"source" : [ "obj-318", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-363", 1 ],
					"order" : 0,
					"source" : [ "obj-318", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-355", 0 ],
					"source" : [ "obj-319", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-104", 0 ],
					"source" : [ "obj-32", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"color" : [ 0.059509769082069, 0.501929938793182, 0.998454749584198, 1.0 ],
					"destination" : [ "obj-12", 0 ],
					"source" : [ "obj-321", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-196", 0 ],
					"source" : [ "obj-321", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-242", 0 ],
					"source" : [ "obj-322", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-237", 0 ],
					"source" : [ "obj-323", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-347", 0 ],
					"source" : [ "obj-324", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-358", 0 ],
					"source" : [ "obj-324", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-326", 0 ],
					"source" : [ "obj-325", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-326", 0 ],
					"source" : [ "obj-325", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-333", 0 ],
					"order" : 1,
					"source" : [ "obj-326", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-365", 1 ],
					"order" : 0,
					"source" : [ "obj-326", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-384", 0 ],
					"source" : [ "obj-327", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-359", 0 ],
					"order" : 0,
					"source" : [ "obj-329", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-399", 0 ],
					"order" : 1,
					"source" : [ "obj-329", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-69", 0 ],
					"source" : [ "obj-33", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-237", 0 ],
					"source" : [ "obj-333", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-337", 0 ],
					"source" : [ "obj-336", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-361", 1 ],
					"order" : 0,
					"source" : [ "obj-337", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-386", 1 ],
					"order" : 2,
					"source" : [ "obj-337", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-402", 1 ],
					"order" : 1,
					"source" : [ "obj-337", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-336", 0 ],
					"source" : [ "obj-338", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-116", 0 ],
					"source" : [ "obj-339", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-58", 0 ],
					"source" : [ "obj-34", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-445", 0 ],
					"source" : [ "obj-341", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-203", 0 ],
					"order" : 1,
					"source" : [ "obj-342", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-351", 0 ],
					"source" : [ "obj-342", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-396", 0 ],
					"source" : [ "obj-342", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-409", 1 ],
					"order" : 0,
					"source" : [ "obj-342", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-203", 0 ],
					"order" : 0,
					"source" : [ "obj-343", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-349", 1 ],
					"order" : 1,
					"source" : [ "obj-343", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-414", 0 ],
					"source" : [ "obj-344", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-358", 0 ],
					"source" : [ "obj-345", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-359", 0 ],
					"source" : [ "obj-346", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-399", 0 ],
					"source" : [ "obj-347", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-116", 0 ],
					"source" : [ "obj-348", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-339", 0 ],
					"source" : [ "obj-348", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-353", 0 ],
					"source" : [ "obj-348", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-104", 0 ],
					"source" : [ "obj-35", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-344", 0 ],
					"source" : [ "obj-351", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-344", 0 ],
					"source" : [ "obj-351", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-324", 0 ],
					"source" : [ "obj-352", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-116", 0 ],
					"source" : [ "obj-353", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-412", 0 ],
					"source" : [ "obj-354", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-325", 0 ],
					"source" : [ "obj-355", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-366", 1 ],
					"order" : 1,
					"source" : [ "obj-355", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-393", 0 ],
					"order" : 0,
					"source" : [ "obj-355", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-341", 0 ],
					"source" : [ "obj-356", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-373", 1 ],
					"order" : 0,
					"source" : [ "obj-357", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-379", 0 ],
					"order" : 1,
					"source" : [ "obj-357", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-327", 0 ],
					"source" : [ "obj-358", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-327", 1 ],
					"source" : [ "obj-359", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 0 ],
					"source" : [ "obj-36", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-294", 0 ],
					"source" : [ "obj-361", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-346", 0 ],
					"source" : [ "obj-362", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-301", 0 ],
					"source" : [ "obj-364", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-371", 0 ],
					"source" : [ "obj-367", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-301", 0 ],
					"source" : [ "obj-368", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-327", 0 ],
					"source" : [ "obj-369", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"color" : [ 0.501960813999176, 0.0, 0.501960813999176, 1.0 ],
					"destination" : [ "obj-1", 0 ],
					"source" : [ "obj-37", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-56", 0 ],
					"source" : [ "obj-37", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"color" : [ 0.324192345142365, 0.598094642162323, 0.459026545286179, 1.0 ],
					"destination" : [ "obj-65", 0 ],
					"midpoints" : [ 315.5, 627.0, 351.0, 627.0, 351.0, 627.0, 781.590143203735352, 627.0 ],
					"source" : [ "obj-37", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-358", 0 ],
					"source" : [ "obj-371", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 1 ],
					"source" : [ "obj-372", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-372", 0 ],
					"source" : [ "obj-374", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-345", 0 ],
					"source" : [ "obj-375", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-237", 0 ],
					"source" : [ "obj-376", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-376", 0 ],
					"source" : [ "obj-378", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-385", 0 ],
					"source" : [ "obj-379", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-390", 1 ],
					"order" : 1,
					"source" : [ "obj-379", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-395", 0 ],
					"order" : 0,
					"source" : [ "obj-379", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-408", 0 ],
					"source" : [ "obj-379", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-418", 0 ],
					"source" : [ "obj-379", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-449", 1 ],
					"source" : [ "obj-379", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-54", 0 ],
					"source" : [ "obj-38", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-263", 0 ],
					"source" : [ "obj-380", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-237", 0 ],
					"source" : [ "obj-381", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-352", 0 ],
					"source" : [ "obj-382", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-381", 0 ],
					"source" : [ "obj-383", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-342", 0 ],
					"source" : [ "obj-384", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-441", 0 ],
					"source" : [ "obj-384", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-197", 0 ],
					"order" : 2,
					"source" : [ "obj-385", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-384", 1 ],
					"order" : 1,
					"source" : [ "obj-385", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-389", 0 ],
					"order" : 0,
					"source" : [ "obj-385", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-388", 1 ],
					"source" : [ "obj-387", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-256", 0 ],
					"source" : [ "obj-388", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-258", 0 ],
					"source" : [ "obj-388", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-391", 0 ],
					"source" : [ "obj-389", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-388", 0 ],
					"source" : [ "obj-391", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-215", 0 ],
					"source" : [ "obj-392", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-394", 0 ],
					"source" : [ "obj-393", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 0 ],
					"source" : [ "obj-395", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-397", 0 ],
					"source" : [ "obj-395", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-412", 0 ],
					"source" : [ "obj-396", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-406", 0 ],
					"source" : [ "obj-397", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-404", 0 ],
					"source" : [ "obj-398", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-367", 1 ],
					"source" : [ "obj-399", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-367", 0 ],
					"source" : [ "obj-399", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-375", 0 ],
					"source" : [ "obj-399", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-147", 0 ],
					"source" : [ "obj-4", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"color" : [ 0.501960813999176, 0.0, 0.501960813999176, 1.0 ],
					"destination" : [ "obj-1", 0 ],
					"midpoints" : [ 202.5, 417.0, 50.5, 417.0 ],
					"source" : [ "obj-40", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-414", 1 ],
					"source" : [ "obj-401", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-178", 0 ],
					"source" : [ "obj-402", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-398", 0 ],
					"source" : [ "obj-402", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-392", 0 ],
					"source" : [ "obj-404", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-407", 0 ],
					"source" : [ "obj-404", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-297", 0 ],
					"order" : 1,
					"source" : [ "obj-406", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-339", 0 ],
					"order" : 2,
					"source" : [ "obj-406", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-428", 0 ],
					"order" : 0,
					"source" : [ "obj-406", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-410", 0 ],
					"source" : [ "obj-407", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-417", 0 ],
					"source" : [ "obj-408", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"color" : [ 0.501960813999176, 0.0, 0.501960813999176, 1.0 ],
					"destination" : [ "obj-1", 0 ],
					"midpoints" : [ 90.5, 417.0, 50.5, 417.0 ],
					"source" : [ "obj-41", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-411", 0 ],
					"source" : [ "obj-410", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-435", 0 ],
					"source" : [ "obj-411", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-446", 0 ],
					"source" : [ "obj-411", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-447", 0 ],
					"source" : [ "obj-411", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-448", 0 ],
					"source" : [ "obj-411", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-413", 0 ],
					"source" : [ "obj-412", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-369", 0 ],
					"source" : [ "obj-413", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-415", 0 ],
					"source" : [ "obj-414", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-343", 0 ],
					"source" : [ "obj-415", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-433", 1 ],
					"source" : [ "obj-417", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-336", 0 ],
					"source" : [ "obj-418", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-240", 1 ],
					"source" : [ "obj-425", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-425", 0 ],
					"source" : [ "obj-428", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 0 ],
					"order" : 0,
					"source" : [ "obj-43", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-44", 0 ],
					"order" : 1,
					"source" : [ "obj-43", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-53", 2 ],
					"order" : 2,
					"source" : [ "obj-43", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-401", 0 ],
					"source" : [ "obj-430", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-428", 0 ],
					"source" : [ "obj-432", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 0 ],
					"source" : [ "obj-433", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-342", 0 ],
					"source" : [ "obj-435", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-441", 0 ],
					"source" : [ "obj-435", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-24", 0 ],
					"source" : [ "obj-44", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"source" : [ "obj-44", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-440", 0 ],
					"source" : [ "obj-441", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-440", 0 ],
					"source" : [ "obj-442", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-342", 0 ],
					"source" : [ "obj-446", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-441", 0 ],
					"source" : [ "obj-446", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-342", 0 ],
					"source" : [ "obj-447", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-441", 0 ],
					"source" : [ "obj-447", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-342", 0 ],
					"source" : [ "obj-448", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-441", 0 ],
					"source" : [ "obj-448", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-193", 1 ],
					"source" : [ "obj-449", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-264", 1 ],
					"source" : [ "obj-449", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-264", 1 ],
					"source" : [ "obj-449", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-264", 1 ],
					"source" : [ "obj-449", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-80", 0 ],
					"source" : [ "obj-46", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-52", 0 ],
					"source" : [ "obj-47", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-46", 0 ],
					"source" : [ "obj-48", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"source" : [ "obj-49", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-69", 1 ],
					"source" : [ "obj-50", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-69", 0 ],
					"source" : [ "obj-50", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-50", 0 ],
					"source" : [ "obj-51", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"source" : [ "obj-53", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-34", 0 ],
					"source" : [ "obj-53", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-38", 0 ],
					"source" : [ "obj-53", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-76", 0 ],
					"midpoints" : [ 143.5, 145.0, 166.0, 145.0, 166.0, 58.0, 97.0, 58.0 ],
					"source" : [ "obj-53", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"color" : [ 0.501960813999176, 0.0, 0.501960813999176, 1.0 ],
					"destination" : [ "obj-1", 0 ],
					"source" : [ "obj-54", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 1 ],
					"midpoints" : [ 98.0, 503.0, 296.5, 503.0 ],
					"order" : 0,
					"source" : [ "obj-55", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-29", 0 ],
					"order" : 1,
					"source" : [ "obj-55", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-37", 0 ],
					"order" : 0,
					"source" : [ "obj-55", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-53", 0 ],
					"midpoints" : [ 160.666667000000018, 519.0, 21.333333, 519.0, 21.333333, 351.0, 22.0, 351.0, 22.0, 101.0, 50.5, 101.0 ],
					"order" : 1,
					"source" : [ "obj-55", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-134", 1 ],
					"source" : [ "obj-56", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-41", 0 ],
					"midpoints" : [ 154.5, 375.0, 90.5, 375.0 ],
					"source" : [ "obj-58", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-41", 1 ],
					"source" : [ "obj-58", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-109", 0 ],
					"source" : [ "obj-59", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"color" : [ 0.059509769082069, 0.501929938793182, 0.998454749584198, 1.0 ],
					"destination" : [ "obj-119", 0 ],
					"order" : 1,
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"color" : [ 0.059509769082069, 0.501929938793182, 0.998454749584198, 1.0 ],
					"destination" : [ "obj-141", 0 ],
					"order" : 3,
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"color" : [ 0.059509769082069, 0.501929938793182, 0.998454749584198, 1.0 ],
					"destination" : [ "obj-74", 1 ],
					"midpoints" : [ 802.5, 417.0, 1179.0, 417.0 ],
					"order" : 0,
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"color" : [ 0.059509769082069, 0.501929938793182, 0.998454749584198, 1.0 ],
					"destination" : [ "obj-94", 0 ],
					"order" : 2,
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-50", 0 ],
					"source" : [ "obj-63", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 2 ],
					"order" : 1,
					"source" : [ "obj-64", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-37", 1 ],
					"midpoints" : [ 360.0, 726.0, 330.0, 726.0, 330.0, 572.0, 270.75, 572.0 ],
					"order" : 0,
					"source" : [ "obj-64", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-72", 1 ],
					"source" : [ "obj-69", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-72", 0 ],
					"source" : [ "obj-69", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-35", 0 ],
					"order" : 1,
					"source" : [ "obj-73", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-63", 0 ],
					"order" : 0,
					"source" : [ "obj-73", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-74", 0 ],
					"source" : [ "obj-75", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-53", 1 ],
					"source" : [ "obj-76", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"source" : [ "obj-77", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-57", 0 ],
					"midpoints" : [ 50.5, 766.666666999999961, 28.291663, 766.666666999999961, 28.291663, 688.666666999999961, 28.291664999999998, 688.666666999999961, 28.291664999999998, 657.0, 50.5, 657.0 ],
					"source" : [ "obj-77", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"midpoints" : [ 122.5, 762.333333000000039, 151.625, 762.333333000000039, 151.625, 695.666666999999961, 50.5, 695.666666999999961 ],
					"source" : [ "obj-77", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-134", 0 ],
					"source" : [ "obj-78", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-100", 0 ],
					"source" : [ "obj-79", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-102", 0 ],
					"source" : [ "obj-79", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-150", 0 ],
					"source" : [ "obj-79", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"midpoints" : [ 549.0, 876.0, 35.5, 876.0 ],
					"source" : [ "obj-79", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-292", 0 ],
					"source" : [ "obj-79", 6 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-301", 0 ],
					"source" : [ "obj-79", 7 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-37", 2 ],
					"source" : [ "obj-79", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-433", 0 ],
					"midpoints" : [ 509.5, 882.0, 950.833333333333144, 882.0 ],
					"source" : [ "obj-79", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-77", 0 ],
					"source" : [ "obj-8", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-83", 0 ],
					"source" : [ "obj-80", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-121", 1 ],
					"order" : 0,
					"source" : [ "obj-81", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-126", 1 ],
					"order" : 0,
					"source" : [ "obj-81", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-82", 4 ],
					"order" : 1,
					"source" : [ "obj-81", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"color" : [ 0.501960813999176, 0.0, 0.501960813999176, 1.0 ],
					"destination" : [ "obj-96", 0 ],
					"order" : 1,
					"source" : [ "obj-81", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-102", 1 ],
					"source" : [ "obj-82", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"order" : 0,
					"source" : [ "obj-83", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-97", 1 ],
					"order" : 1,
					"source" : [ "obj-83", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-136", 0 ],
					"source" : [ "obj-85", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-145", 0 ],
					"source" : [ "obj-85", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-61", 0 ],
					"source" : [ "obj-85", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-75", 0 ],
					"source" : [ "obj-85", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-81", 1 ],
					"source" : [ "obj-86", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-80", 0 ],
					"source" : [ "obj-90", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-85", 0 ],
					"source" : [ "obj-92", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-92", 0 ],
					"source" : [ "obj-93", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-82", 0 ],
					"source" : [ "obj-98", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-74", 0 ],
					"midpoints" : [ 470.0, 967.114753723144531, 1143.0, 967.114753723144531, 1143.0, 753.0, 1168.0, 753.0 ],
					"source" : [ "obj-99", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-254::obj-2::obj-21::obj-6" : [ "live.tab[3]", "live.tab[1]", 0 ],
			"obj-254::obj-2::obj-35" : [ "[5]", "Level", 0 ],
			"obj-264::obj-24" : [ "live.toggle", "live.toggle", 0 ],
			"obj-69" : [ "live.gain~", "live.gain~", 0 ],
			"parameterbanks" : 			{

			}
,
			"inherited_shortname" : 1
		}
,
		"dependency_cache" : [ 			{
				"name" : "amex_TCPclient.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/SoDA-node/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "SoDA - icon - transparent.png",
				"bootpath" : "~/Documents/Max 8/Projects/SoDA-node/media",
				"patcherrelativepath" : "../media",
				"type" : "PNG",
				"implicit" : 1
			}
, 			{
				"name" : "unix-time.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/SoDA-node/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "parse_AMEX.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/SoDA-node/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "drag-and-drop.png",
				"bootpath" : "~/Documents/Max 8/Projects/SoDA-node/media",
				"patcherrelativepath" : "../media",
				"type" : "PNG",
				"implicit" : 1
			}
, 			{
				"name" : "ml.temporal.classification.hhmm-adapted.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/SoDA-node/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "rms.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/SoDA-node/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "demosound.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/SoDA-node/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "sine.svg",
				"bootpath" : "~/Documents/Max 8/Projects/SoDA-node/media",
				"patcherrelativepath" : "../media",
				"type" : "svg",
				"implicit" : 1
			}
, 			{
				"name" : "saw.svg",
				"bootpath" : "~/Documents/Max 8/Projects/SoDA-node/media",
				"patcherrelativepath" : "../media",
				"type" : "svg",
				"implicit" : 1
			}
, 			{
				"name" : "square.svg",
				"bootpath" : "~/Documents/Max 8/Projects/SoDA-node/media",
				"patcherrelativepath" : "../media",
				"type" : "svg",
				"implicit" : 1
			}
, 			{
				"name" : "random.svg",
				"bootpath" : "~/Documents/Max 8/Projects/SoDA-node/media",
				"patcherrelativepath" : "../media",
				"type" : "svg",
				"implicit" : 1
			}
, 			{
				"name" : "interfacecolor.js",
				"bootpath" : "~/Documents/Max 8/Projects/SoDA-node/code",
				"patcherrelativepath" : "../code",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "WebcamTracking.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/SoDA-node/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "cv.jit.track.draw.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/SoDA-node/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "Motion-model-adapted.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/SoDA-node/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "ml.classification.gmm-adapted.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/SoDA-node/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "SoDA_gallery.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/SoDA-node/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "zsa.easy_mfcc~.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/SoDA-node/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "zsa.abs_mfcc~.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/SoDA-node/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "OSC-route.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "mubu.hhmm.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "mubu.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "mubu.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "cv.jit.track.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "cv.jit.resize.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "mubu.gmm.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "zsa.mfcc~.mxo",
				"type" : "iLaX"
			}
 ],
		"autosave" : 0,
		"styles" : [ 			{
				"name" : "AudioStatus_Menu",
				"default" : 				{
					"bgfillcolor" : 					{
						"type" : "color",
						"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
						"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
						"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
						"angle" : 270.0,
						"proportion" : 0.39,
						"autogradient" : 0
					}

				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "maxscore.default.buttons",
				"default" : 				{
					"accentcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
 ],
		"default_bgcolor" : [ 0.6, 0.6, 0.6, 1.0 ],
		"bgcolor" : [ 0.866667, 0.866667, 0.866667, 1.0 ],
		"editing_bgcolor" : [ 0.866667, 0.866667, 0.866667, 1.0 ]
	}

}
